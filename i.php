<?php

/*
 * Checklist for permissions and settings. Run on each new installation
 */

function assertTrue($test, $message) {
	$class = $test ? "ok" : "failed";
	echo "<div class='$class'>$message</div>";
}

function assertWritable($dir) {
	$res = file_exists($dir) && is_writable($dir);
	assertTrue($res, $dir . ' is ' .($res ? '' : 'NOT '). 'writable');
}


require dirname(__FILE__).'/config.php';

assertWritable(DIR_CACHE);
assertWritable(DIR_LOGS);

assertWritable(__DIR__.'/vqmod/vqcache');

$vqmodLog = __DIR__.'/vqmod/vqmod.log' ;
assertTrue(is_file($vqmodLog) && is_writable($vqmodLog), $vqmodLog);

assertWritable(DIR_APPLICATION . 'tcpdf/cache');

assertWritable(DIR_IMAGE);
assertWritable(DIR_IMAGE . 'cache');
assertWritable(DIR_IMAGE . 'cache/data');

assertWritable(DIR_DOWNLOAD);

assertWritable(__DIR__.'/pdf');

assertTrue(ini_get('short_open_tag'), "php.ini: 'short_open_tag' is ON");

?>
<style>
div {
	padding: 0.5em;
	width: 600px;
}
.ok { background-color: #cfc;}
.failed {background-color: #fcc;}
</style>
