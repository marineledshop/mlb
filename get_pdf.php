<?php

require_once(dirname(__FILE__)."/config.php");
require_once(dirname(__FILE__)."/common.php");
require_once(DIR_SYSTEM . 'startup.php');
require_once(DIR_SYSTEM . 'library/user.php');
require_once(DIR_SYSTEM . 'library/customer.php');
require_once(DIR_DATABASE . 'mysql.php');
$need_configs = array(
	'config_url',
	'config_ssl',
	'config_customer_group_id',
	'config_language'
);

$config = new Config();
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

$registry = new Registry();
$registry->set('db', $db);

$request = new Request();
$registry->set('request', $request);

$session = new Session();
$registry->set('session', $session);        

// If current user is administrator, just give him the requested file
$user = new User($registry);
   
if ($user->isLogged()) {    
  
    // get user group id
    $user_id = $user->getId();  
    $user_query = $db->query("SELECT * FROM " . DB_PREFIX . "user WHERE user_id = " . (int)$user_id);
    $user_group_id = $user_query->row['user_group_id'];
    
    if ($user_group_id == 1) { // admin
        download_file($request->get['name']);
    } else {
        access_denied();
    }
    
} else {

    // If customer id is the same as current customer, give out the file.
    // Show "Access denied" text otherwise.        
    
    // use filename (we have 2 patterns for now: INV-2013-1677.pdf and Order_ID_1388_12-11-2012.pdf) to determine order id and then user id.
    if (preg_match('/^INV\-\d+\-(\d+)\.pdf$/', $request->get['name'], $matches)) {
        $invoice_num = $matches[1];        
        $customer_query = $db->query("SELECT customer_id FROM " . DB_PREFIX . "`order` WHERE invoice_no=" . (int)$invoice_num);
        $customer_id = $customer_query->rows[0]['customer_id'];        
    } else if (preg_match('/^Order_ID_(\d+)_.+\.pdf$/', $request->get['name'], $matches)) {
        $order_id = $matches[1];
        $customer_query = $db->query("SELECT customer_id FROM " . DB_PREFIX . "`order` WHERE order_id=" . (int)$order_id);
        $customer_id = $customer_query->rows[0]['customer_id'];
    } else {        
        access_denied();
        exit();
    }
        
    $customer = new Customer($registry); 
    if ($customer->isLogged() && $customer->getId() == $customer_id) {
        download_file($request->get['name']);        
    } else {
        access_denied();
    }
        
}

exit();

function download_file($filename) {
    $full_name = dirname(__FILE__) . '/pdf/' . $filename;
    if (file_exists($full_name)) {
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        readfile($full_name);        
    } else {        
        header('HTTP/1.0 404 Not Found');
        print '<h1>404 Not Found</h1>';
        print 'The page that you have requested could not be found.';        
    }
}

function access_denied() {
    header('HTTP/1.0 403 Forbidden');
    print '<h1>Access denied</h1>';
}