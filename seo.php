<?php
die("File is disabled by Vadim");
/*
    SEO SCRIPT GENERATOR - v 1.0.1 (1.12.2012)
    generate url_alias from product and category table
    
    seo.php uploaded to the directory and then enter the URL: http://yourdomain.com/seo.php
    
    changes:
    1.0.1 - added ID to url of product and category - duplicity product name possible
*/

	// debug
	//ini_set('error_reporting', 6143);
	//ini_set('display_errors', 1);
		
require_once(dirname(__FILE__)."/config.php");
require_once(dirname(__FILE__)."/common.php");
require_once(DIR_SYSTEM . 'startup.php');
require_once(DIR_DATABASE . 'mysql.php');
$need_configs = array(
	'config_url',
	'config_ssl',
	'config_customer_group_id',
	'config_language'
);

// Config
$config = new Config();
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

// Settings
$query = $db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '0' OR store_id = '" . (int)$config->get('config_store_id') . "' ORDER BY store_id ASC");

foreach ($query->rows as $setting) {
    if (!$setting['serialized']) {
        $config->set($setting['key'], $setting['value']);
    } else {
        $config->set($setting['key'], unserialize($setting['value']));
    }
}

$db->query("TRUNCATE url_alias");

//CATEGORIES
$query = $db->query("SELECT category_id,name FROM " . DB_PREFIX . "category_description;");
foreach ($query->rows as $row) {
    $query_alias = $db->query("SELECT url_alias_id,query,keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'category_id=".((int)$row['category_id'])."';");
    if($query_alias->num_rows){
        $db->query("UPDATE " . DB_PREFIX . "url_alias SET keyword = '".$db->escape(seo($row['name']))."' WHERE query = 'category_id=".((int)$row['category_id'])."';");
    }else{
        $db->query("INSERT INTO " . DB_PREFIX . "url_alias (query,keyword) VALUES ('category_id=".((int)$row['category_id'])."','".$db->escape(seo($row['name']))."');");
    }
}

//PRODUCTS
$query = $db->query("SELECT product_id,name FROM " . DB_PREFIX . "product_description");
foreach ($query->rows as $row) {
    $cat_id = 0;
    $cat_name = '';
    foreach($db->query("SELECT category_id FROM product_to_category INNER JOIN category USING (category_id) WHERE product_id = " . (int)$row['product_id'])->rows as $cat_row) {
        $subcat_query = $db->query("SELECT category_id FROM category WHERE top = 0 AND parent_id = " . (int)$cat_row['category_id']);
        if ($subcat_query->num_rows == 0) {
            $cat_id = (int) $cat_row['category_id'];
            $name_query = $db->query("SELECT name FROM category_description WHERE language_id = 1 AND category_id = " . $cat_id);
            if (!$name_query->num_rows) {
                var_dump($cat_id);
            } else {
                $cat_name = $name_query->row['name'];
            }
        }
    }

    $product_query['product_id'] = $row['product_id'];
    if($cat_id > 0) {
        $product_query['category_id'] = $cat_id;
        $keyword = seo($cat_name) . '/'. seo($row['name']);
    } else {
        $keyword = seo($row['name']);
    }

    $db->query("INSERT INTO " . DB_PREFIX . "url_alias (query,keyword) VALUES ('".http_build_query($product_query)."','". $keyword ."')");
}

echo "done";
?>
