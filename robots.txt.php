<?php
header("Content-Type: text/plain; charset=utf-8");
if ($_SERVER['HTTP_HOST'] == 'marineledbulbs.com')
	include __DIR__.'/robots.txt';
else {
	echo "User-agent: *\n";
	echo "Disallow: /\n";
}
