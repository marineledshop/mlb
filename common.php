<?php
/**
 *
 * Common functions included both in admin panel and frontend
 * User: avasilenko
 * Date: 29.02.12
 * Time: 12:52
 */

function seo($name){
    return toAscii(html_entity_decode($name));
}

function toAscii($string)
{
    // cz
    $source[] = '/a/'; $replace[] = 'a';
    $source[] = '/á/'; $replace[] = 'a';
    $source[] = '/b/'; $replace[] = 'b';
    $source[] = '/c/'; $replace[] = 'c';
    $source[] = '/č/'; $replace[] = 'c';
    $source[] = '/d/'; $replace[] = 'd';
    $source[] = '/ď/'; $replace[] = 'd';
    $source[] = '/é/'; $replace[] = 'e';
    $source[] = '/e/'; $replace[] = 'e';
    $source[] = '/ě/'; $replace[] = 'e';
    $source[] = '/f/'; $replace[] = 'f';
    $source[] = '/g/'; $replace[] = 'g';
    $source[] = '/h/'; $replace[] = 'h';
    $source[] = '/í/'; $replace[] = 'i';
    $source[] = '/i/'; $replace[] = 'i';
    $source[] = '/j/'; $replace[] = 'j';
    $source[] = '/k/'; $replace[] = 'k';
    $source[] = '/l/'; $replace[] = 'l';
    $source[] = '/m/'; $replace[] = 'm';
    $source[] = '/n/'; $replace[] = 'n';
    $source[] = '/ň/'; $replace[] = 'n';
    $source[] = '/o/'; $replace[] = 'o';
    $source[] = '/p/'; $replace[] = 'p';
    $source[] = '/q/'; $replace[] = 'q';
    $source[] = '/ó/'; $replace[] = 'o';
    $source[] = '/r/'; $replace[] = 'r';
    $source[] = '/ř/'; $replace[] = 'r';
    $source[] = '/š/'; $replace[] = 's';
    $source[] = '/s/'; $replace[] = 's';
    $source[] = '/ť/'; $replace[] = 't';
    $source[] = '/t/'; $replace[] = 't';
    $source[] = '/ů/'; $replace[] = 'u';
    $source[] = '/ú/'; $replace[] = 'u';
    $source[] = '/u/'; $replace[] = 'u';
    $source[] = '/v/'; $replace[] = 'v';
    $source[] = '/w/'; $replace[] = 'w';
    $source[] = '/y/'; $replace[] = 'y';
    $source[] = '/ý/'; $replace[] = 'y';
    $source[] = '/ž/'; $replace[] = 'z';
    $source[] = '/z/'; $replace[] = 'z';
    // hu
    $source[] = '/ö/'; $replace[] = 'o';
    $source[] = '/o/'; $replace[] = 'o';
    $source[] = '/ü/'; $replace[] = 'u';

    // CZ
    $source[] = '/A/'; $replace[] = 'a';
    $source[] = '/Á/'; $replace[] = 'a';
    $source[] = '/B/'; $replace[] = 'b';
    $source[] = '/C/'; $replace[] = 'c';
    $source[] = '/Č/'; $replace[] = 'c';
    $source[] = '/D/'; $replace[] = 'd';
    $source[] = '/Ď/'; $replace[] = 'd';
    $source[] = '/É/'; $replace[] = 'e';
    $source[] = '/E/'; $replace[] = 'e';
    $source[] = '/Ě/'; $replace[] = 'e';
    $source[] = '/F/'; $replace[] = 'f';
    $source[] = '/G/'; $replace[] = 'g';
    $source[] = '/H/'; $replace[] = 'h';
    $source[] = '/Í/'; $replace[] = 'i';
    $source[] = '/I/'; $replace[] = 'i';
    $source[] = '/J/'; $replace[] = 'j';
    $source[] = '/K/'; $replace[] = 'k';
    $source[] = '/L/'; $replace[] = 'l';
    $source[] = '/M/'; $replace[] = 'm';
    $source[] = '/N/'; $replace[] = 'n';
    $source[] = '/Ň/'; $replace[] = 'n';
    $source[] = '/O/'; $replace[] = 'o';
    $source[] = '/P/'; $replace[] = 'p';
    $source[] = '/Q/'; $replace[] = 'q';
    $source[] = '/Ó/'; $replace[] = 'o';
    $source[] = '/R/'; $replace[] = 'r';
    $source[] = '/Ř/'; $replace[] = 'r';
    $source[] = '/Š/'; $replace[] = 's';
    $source[] = '/S/'; $replace[] = 's';
    $source[] = '/Ť/'; $replace[] = 't';
    $source[] = '/T/'; $replace[] = 't';
    $source[] = '/Ů/'; $replace[] = 'u';
    $source[] = '/Ú/'; $replace[] = 'u';
    $source[] = '/U/'; $replace[] = 'u';
    $source[] = '/V/'; $replace[] = 'v';
    $source[] = '/W/'; $replace[] = 'w';
    $source[] = '/Y/'; $replace[] = 'y';
    $source[] = '/Ý/'; $replace[] = 'y';
    $source[] = '/Ž/'; $replace[] = 'z';
    $source[] = '/Z/'; $replace[] = 'z';
    // HU
    $source[] = '/Ö/'; $replace[] = 'o';
    $source[] = '/Ü/'; $replace[] = 'u';

    $string = preg_replace($source, $replace, $string);

    for ($i=0; $i<strlen($string); $i++)
    {
        if ($string[$i] >= 'a' && $string[$i] <= 'z') continue;
        if ($string[$i] >= 'A' && $string[$i] <= 'Z') continue;
        if ($string[$i] >= '0' && $string[$i] <= '9') continue;
        $string[$i] = '-';
    }
    $string = preg_replace("/[-]{2,}/","-",$string);
    return $string;
}

