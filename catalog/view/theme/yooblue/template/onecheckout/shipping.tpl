<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($shipping_methods) { ?>
<p><?php echo $text_shipping_method; ?></p>
<table class="form">
  <?php $selected = false; ?>
  <?php 
    $i = 0;
    $methods_num = count($shipping_methods);
    foreach ($shipping_methods as $shipping_provider => $shipping_method) { ?>
  <?php if (!$shipping_method['error']) { ?>    
  <?php 
    
    foreach ($shipping_method['quote'] as $quote) { 
        $next_method = getNextItem($shipping_methods, $shipping_provider);
        $next_quote = current($next_method['quote']);
    ?>
  <tr>
      <td style="width: 1px;"><?php 
          if (($shipping_provider == $selected_shipping_provider || 
          ($quote['cost'] == 0 && ($i + 1 == $methods_num || $next_quote['cost'] != 0) 
          && !$selected_shipping_provider)) && !$selected) { 
      ?>
      <?php $selected = true; ?>
      <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" checked="checked" />
      <?php } else { ?>
      <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" />
      <?php } ?></td>
    <td style="text-align:center; padding-bottom:15px;">
        <?php if ($shipping_provider && file_exists(DIR_IMAGE . 'payments/' . $shipping_provider . '.png')) {
                $img = HTTP_IMAGE . 'payments/' . $shipping_provider . '.png'; ?>
        <img width="100px" src="<?php echo $img; ?>">
        <?php } ?>
        <label for="<?php echo $quote['code']; ?>" class="provider-name <?php echo strtok($quote['code'], '.') ?>" style=""><?php echo $shipping_method['title']; ?><br /><span style="color: #0C3;"><?php echo isset($quote['delivery']) && !empty($quote['delivery']) ? " ({$quote['delivery']})" : "" ?></span></label>
    </td>
    <td style="text-align: right;"><label for="<?php echo $quote['code']; ?>"><?php echo $quote['cost'] == '0' ? 'Free' : $quote['text']; ?></label></td>
  </tr>
  <?php } ?>
  <?php } else { ?>
  <tr>
    <td colspan="3"><div class="error"><?php echo $shipping_method['error']; ?></div></td>
  </tr>
  <?php } 
    $i++;
  ?>
  <?php } ?>
</table>
<? echo $insurance_fee; ?>
<?php } 

function getNextItem($array, $key)
{
    $keys = array_keys($array);
    if ( (false !== ($p = array_search($key, $keys))) && ($p < count($keys) - 1) )
    {
        return $array[$keys[++$p]];
    }
    else
    {
        return false;
    }
}
?>