<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <h2><?php echo $text_my_account; ?></h2>
  <div class="content">
    <ul>
      <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
      <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
      <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
      <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
    </ul>
  </div>
  <h2><?php echo $text_my_orders; ?></h2>
  <div class="content">
    <ul>
      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
      <!--<li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>-->
      <li><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
      <!--li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li-->
      <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
    </ul>
  </div>
  <?php if ($show_last_order) { ?>
  <h2><?php echo $text_my_last_order; ?></h2>
  <div class="content">    
    <div class="my-last-order">
        <div class="summary">
            <div class="order-id"><b>Order ID:</b> #<?php print $last_order['order_id']; ?></div>
            <div class="order-status"><b>Status:</b> <?php print $last_order['status']; ?></div>        
        </div>
        <div class="order-content">
            <div class="col1"><b>Date Added:</b> <?php print $last_order['date_added']; ?><br>
            <b>Products:</b> <?php print $last_order['products']; ?></div>
            <div class="col2"><b>Customer:</b> <?php print $last_order['name']; ?><br>
            <b>Total:</b> <?php print $last_order['total']; ?></div>
            <div class="col3"><?php if ($last_order['invoice_no']) { ?>
            <a href="pdf/<?php echo $last_order['invoice_no']; ?>.pdf" target="_blank" /><img src="image/data/icons/pdf-icon2.gif" /></a>
            <?php } else { print '&nbsp;'; } ?></div>
            <div class="col4"><a class="button" href="<?php print $last_order['href']; ?>"><span>View</span></a></div>
        </div>
    </div>  
  </div>
  <?php } ?>
  <h2><?php echo $text_my_newsletter; ?></h2>
  <div class="content">
    <ul>
      <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
    </ul>
  </div>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?> 