<div>
  <div class="cart-heading active"><?php echo $heading_title; ?></div>
  <div class="cart-content" style="display:block">
    <p><?php echo $text_shipping; ?></p>
    <table id="shipping">
      <tr>
        <td><?php echo $entry_country; ?></td>
        <td><select id="country-id" name="country_id">
            <option value=""><?php echo $text_select; ?></option>
            <?php foreach ($countries as $country) { ?>
            <?php if ($country['country_id'] == $country_id) { ?>
            <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
            <?php } ?>
            <?php } ?>
          </select></td>
      </tr>
    </table>
    <div id="quote" style="display: none;"></div>
    <input type="hidden" name="shipping_method" value="<?php echo $code; ?>" />
  </div>
</div>
<script type="text/javascript"><!--
$('#country-id').change(function() {
	$.ajax({
		type: 'POST',
		url: 'index.php?route=total/shipping/quote',
		data: 'country_id=' + $('#country-id').val(),
		dataType: 'json',		
		beforeSend: function() {
			$('.success, .warning.country').remove();
			$('#country-id').attr('disabled', true);
			$('#country-id').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('#country-id').attr('disabled', false);
			$('.wait').remove();
		},		
		success: function(json) {
			$('.error').remove();

			if (json['redirect']) {
				location = json['redirect'];
			}
						
			if (json['error']) {
				if (json['error']['warning']) {
					$('#basket').before('<div class="warning country">' + json['error']['warning'] + '</div>');
				}
				
				if (json['error']['country']) {
					$('#shipping select[name=\'country_id\'] + br').after('<span class="error">' + json['error']['country'] + '</span>');
				}	
			}
			
			if (json['shipping_methods']) {
				html  = '<br />';
				html += '<table width="100%" cellpadding="3">';
                var chosenMethod = $('input[name=\'shipping_method\']').attr('value');
				
				for (i in json['shipping_methods']) {
                    if (!json['shipping_methods'][i]['error']) {
                        $.each(json['shipping_methods'][i]['quote'], function(key, value) {
                            html += '<tr>';
                            var estimation = '';
                            if (value['delivery']) {
                                estimation = '('+value['delivery']+' days)';
                            }
                            var provider = value['code'].split('.')[0];
                            if (value['cost'] == 0) {
                                html += '<td width="1"><input type="radio" name="shipping_method" value="' + value['code'] + '" id="' + value['code'] + '" checked="checked" /></td>';
                            } else {
                                html += '<td width="1"><input type="radio" name="shipping_method" value="' + value['code'] + '" id="' + value['code'] + '" /></td>';
                            }
                            html += '  <td><label for="'+ value['code'] + '" class="provider ' + provider + '"><strong>' + json['shipping_methods'][i]['title'] + ' ' + estimation + '</strong></label></td>';
                            if (value['cost'] == 0 ) {
                                value['text'] = 'Free';
                            }
                            html += '  <td width="1"><label class="text" for="' + value['code'] + '">' + value['text'] + '</label></td>';
                            html += '<td style="display:none" class="title">' +value['title'] + '</td>';
                            html += '</tr>';
                            return false;
                        });
                    } else {
                        html += '<tr>';
                        html += '  <td colspan="3"><div class="error">' + json['shipping_methods'][i]['error'] + '</div></td>';
                        html += '</tr>	';
                    }
				}
				
				html += '</table>';

				$('#quote').html(html);	
			
				$('#quote').slideDown('slow');
                var inputs = $('input[name=shipping_method]');
                inputs.unbind('change.shipping').bind('change.shipping', function(){
                    var $this = $(this);
                    if (!$this.is(':checked')) return;

                    var tr = $this.closest('tr'),
                        shipping = $('.total-shipping'),
                        label = $this.closest('tr').find('.provider');
                    //save it
                    $.ajax({
                        type: 'POST',
                        url: 'index.php?route=total/shipping/calculate',
                        data: 'shipping_method=' + $this.val(),
                        dataType: 'json',
                        beforeSend: function() {
                            inputs.attr('disabled', true);
                            $('.warning.shipping').remove();
                            label.append('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
                        },
                        complete: function() {
                            inputs.attr('disabled', false);
                            $('.wait').remove();
                        },
                        success: function(json) {
                            if (json['error']) {
                                $this.closest('table').before('<div class="warning shipping">' + json['error'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                                return;
                            }
                            shipping.find('.title').html(tr.find('.title').html());
                            shipping.find('.text').html(tr.find('.text').html());
                        }
                    });
                }).change();
			}
		}
	});
});

$(function(){
    $('#country-id').change();
});
//--></script> 
