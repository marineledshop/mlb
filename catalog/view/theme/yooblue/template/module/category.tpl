<!--<div class="box">-->
  <!--<div class="box-heading"><?php echo $heading_title; ?></div>-->
  <div class="box-content">
  <div class="thumb-category-container">
      <ul class="thumb-category">
        <?php foreach ($categories as $category) { ?>
        <li class="main<?php echo $category['category_id'] == $category_id ? " active" : ""?>">
          <a href="<?php echo $category['href']; ?>">
          <?php
              if (!empty($category['icon'])) {
                  echo "<img src='{$category['icon']}' alt='{$category['name']}' />";
              }
          ?><?php echo $category['name']; ?></a>
        </li>
          <?php if ($category['children']) { ?>
            <?php foreach ($category['children'] as $child) { ?>
            <li class="subcat<?php echo $child['category_id'] == $child_id ? ' active' : '' ?>">
              <a href="<?php echo $child['href']; ?>">
                  <?php
                      if (!empty($child['icon'])) {
                          echo "<img src='{$child['icon']}' alt='{$child['name']}' />";
                      }
                  ?>
                  <?php echo $child['name']; ?></a>
            </li>
            <?php } ?>
          <?php } ?>
        <?php } ?>
      </ul>
	</div>
  </div>
<!--</div>-->
