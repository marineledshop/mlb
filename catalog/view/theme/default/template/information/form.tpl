<?php
//==============================================================================
// Flexible Form v154.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
//==============================================================================
?>

<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content">
	
<?php if ($v14x) { ?>
	<div class="top">
		<div class="left"></div>
		<div class="right"></div>
		<div class="center">
			<h1><?php echo $heading_title; ?></h1>
		</div>
	</div>
	<div class="middle">
<?php } else { ?>
	<?php echo $content_top; ?>
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
	<h1><?php echo $heading_title; ?></h1>
<?php } ?>

	<form action="" method="post" enctype="multipart/form-data" id="form">
		<?php foreach ($errors as $error) { ?>
			<div class="error" style="padding: 10px; margin-bottom: 5px; background: #FEE; border: 1px solid #F00"><?php echo $error; ?></div>
		<?php } ?>
		<?php if ($html_before) { ?>
			<div class="content"><?php echo $html_before; ?></div>
		<?php } ?>
		<div class="content">
			<table width="100%">
			<tr>
				<?php foreach ($columns as $column_number => $column) { ?>
					<?php if ($column_number > 1) { ?>
						<td width="6%">&nbsp;</td>
						<td width="47%" style="vertical-align: top">
					<?php } else { ?>
						<td style="vertical-align: top">
					<?php } ?>
					<?php foreach ($column as $field) { ?>
						<?php $key = 'field' . $field['key']; ?>
						<?php if ($field['required']) { ?>
							<span class="required">*</span>
						<?php } ?>
						<strong><?php echo $field['name'][$this->session->data['language']] . $text_after_title; ?></strong>
						<br />
						<?php if ($field['type'] == 'checkbox') { ?>
							<?php foreach (explode(';', $field['value'][$this->session->data['language']]) as $value) { ?>
								<?php $value = trim($value); ?>
								<span>
									<input type="checkbox" name="<?php echo $key; ?>[]" value="<?php echo $value; ?>" <?php if (in_array($value, ${$key})) echo 'checked="checked"'; ?> />
									<?php echo $value; ?>
								</span>
								<br />
							<?php } ?>
						<?php } elseif ($field['type'] == 'radio') { ?>
							<?php foreach (explode(';', $field['value'][$this->session->data['language']]) as $value) { ?>
								<?php $value = trim($value); ?>
								<span>
									<input type="radio" name="<?php echo $key; ?>" value="<?php echo $value; ?>" <?php if ($value == ${$key}) echo 'checked="checked"'; ?> />
									<?php echo $value; ?>
								</span>
								<br />
							<?php } ?>
						<?php } elseif ($field['type'] == 'select') { ?>
							<select name="<?php echo $key; ?>">
								<?php foreach (explode(';', $field['value'][$this->session->data['language']]) as $value) { ?>
									<?php $value = trim($value); ?>
									<option value="<?php echo $value; ?>" <?php if ($value == ${$key}) echo 'selected="selected"'; ?>><?php echo $value; ?></option>
								<?php } ?>
							</select>
							<br />
						<?php } elseif ($field['type'] == 'email' || $field['type'] == 'text') { ?>
							<input size="30" type="text" name="<?php echo $key; ?>" value="<?php echo ${$key}; ?>" /><br />
						<?php } elseif ($field['type'] == 'email_confirm') { ?>
							<input size="30" type="text" name="<?php echo $key; ?>" value="<?php echo ${$key}; ?>" /><br />
							<br />
							<?php if ($field['required']) { ?>
								<span class="required">*</span>
							<?php } ?>
							<strong><?php echo $text_confirm . ' ' . $field['name'][$this->session->data['language']] . $text_after_title; ?></strong><br />
							<input size="30" type="text" name="<?php echo 'confirm' . $field['key']; ?>" value="<?php echo ${'confirm'.$field['key']}; ?>" /><br />
						<?php } elseif ($field['type'] == 'password') { ?>
							<input size="30" type="password" name="<?php echo $key; ?>" value="<?php echo ${$key}; ?>" /><br />
						<?php } elseif ($field['type'] == 'textarea') { ?>
							<textarea name="<?php echo $key; ?>" style="width: 100%; height: 100px"><?php echo ${$key}; ?></textarea><br />
						<?php } elseif ($field['type'] == 'date' || $field['type'] == 'time' || $field['type'] == 'datetime') { ?>
							<input size="30" type="text" name="<?php echo $key; ?>" value="<?php echo ${$key}; ?>" class="<?php echo $field['type']; ?>" /><br />
						<?php } elseif ($field['type'] == 'file') { ?>
							<input size="30" type="file" name="<?php echo $key; ?>" value="<?php echo ${$key}; ?>" /><br />
							<?php echo $text_allowed_ext; ?><br />
						<?php } ?>
						<br />
					<?php } ?>
					</td>
				<?php } ?>
			</tr>
			</table>
		</div>
		<?php if ($include_captcha) { ?>
			<div class="content">
				<strong><?php echo $text_enter_code; ?></strong><br />
				<input type="text" name="captcha" value="" autocomplete="off" style="width: 142px" /><br />
				<img src="index.php?route=information/form/captcha" />
			</div>
		<?php } ?>
		<?php if ($html_after) { ?>
			<div class="content"><?php echo $html_after; ?></div>
		<?php } ?>
		<div class="buttons">
			<a onclick="$('#form').submit();" class="button"><span><?php echo $button_submit; ?></span></a>
		</div>
	</form>

<?php if ($v14x) { ?>
	</div>
	<div class="bottom">
		<div class="left"></div>
		<div class="right"></div>
		<div class="center"></div>
	</div>
<?php } else { ?>
	<?php echo $content_bottom; ?>
<?php } ?>

</div>

<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/css/ui-lightness/jquery-ui-lightness.css" />
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-datepicker.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
	$('.datetime').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: 'h:mm tt',
		separator: ' @ ',
		ampm: true
	});
	$('.time').timepicker({
		timeFormat: 'h:mm tt',
		ampm: true
	});
//--></script>

<?php echo $footer; ?>