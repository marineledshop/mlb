<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($shipping_methods) { ?>
<p><?php echo $text_shipping_method; ?></p>
<table class="form">
  <?php $selected = false; ?>
  <?php foreach ($shipping_methods as $shipping_provider => $shipping_method) { ?>
  <?php if (!$shipping_method['error']) { ?>
  <?php foreach ($shipping_method['quote'] as $quote) { ?>
  <tr>
      <td style="width: 1px;"><?php if (($shipping_provider == $selected_shipping_provider || ($quote['cost'] == 0 && !$selected_shipping_provider)) && !$selected) { ?>
      <?php $selected = true; ?>
      <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" checked="checked" />
      <?php } else { ?>
      <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" />
      <?php } ?></td>
    <td><label for="<?php echo $quote['code']; ?>" class="provider-name <?php echo strtok($quote['code'], '.') ?>" style=""><?php echo $shipping_method['title']; ?><br /><span style="color: #0C3;"><?php echo isset($quote['delivery']) && !empty($quote['delivery']) ? " ({$quote['delivery']})" : "" ?></span></label></td>
    <td style="text-align: right;"><label for="<?php echo $quote['code']; ?>"><?php echo $quote['cost'] == '0' ? 'Free' : $quote['text']; ?></label></td>
  </tr>
  <?php } ?>
  <?php } else { ?>
  <tr>
    <td colspan="3"><div class="error"><?php echo $shipping_method['error']; ?></div></td>
  </tr>
  <?php } ?>
  <?php } ?>
</table>
<? echo $insurance_fee; ?>
<?php } ?>