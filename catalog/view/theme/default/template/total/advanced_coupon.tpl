<div>
  <div class="cart-heading"><?php echo $heading_title; ?></div>
  <div class="cart-content" id="advanced_coupon"><?php echo $entry_advanced_coupon; ?>&nbsp;
    <input type="text" name="advanced_coupon" value="" />
    &nbsp;<a id="button-advanced_coupon" class="button"><span><?php echo $button_advanced_coupon; ?></span></a></div>
</div>
<script type="text/javascript"><!--
$('#button-advanced_coupon').bind('click', function() {
	$.ajax({
		type: 'POST',
		url: 'index.php?route=total/advanced_coupon/calculate',
		data: $('#advanced_coupon :input'),
		dataType: 'json',		
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-coupon').attr('disabled', true);
			$('#button-coupon').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('#button-coupon').attr('disabled', false);
			$('.wait').remove();
		},		
		success: function(json) {
			if (json['error']) {
				$('#basket').before('<div class="warning">' + json['error'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
			}
			
			if (json['redirect']) {
				location = json['redirect'];
			}
		}
	});
});
//--></script> 