<?php
/**
 * Facebook OpenCart Module. Creates a Facebook like box.
 *
 * @author 		www.opencartstore.com
 * @support		www.opencartstore.com/support/
 * @version		1.5.1.*
 */
?>

<div id="fb-root"></div>
<script>
	window.fbAsyncInit = function() {
      FB.init({status: true, cookie: true, xfbml: true});
    };
    
    (function() {
      var e = document.createElement('script'); e.async = true;
      e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
      document.getElementById('fb-root').appendChild(e);
    }());
</script>
<!--<div class="fb-like" data-href="http://www.marineledbulbs.com" data-send="true" data-width="170" data-show-faces="false" data-font="arial" data-layout-button_count="true"></div>-->
<fb:fan profile_id="<?php echo $facebooklike_profile_id; ?>" stream="<?php echo $facebooklike_stream; ?>" connections="<?php echo $facebooklike_connections; ?>" logobar="<?php echo $facebooklike_header; ?>" width="<?php echo $facebooklike_width; ?>" height="<?php echo $facebooklike_height; ?>" css="<?php echo HTTPS_SERVER; ?>/catalog/view/theme/default/stylesheet/facebooklike.css?1"></fb:fan>
