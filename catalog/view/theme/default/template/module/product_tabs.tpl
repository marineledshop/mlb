<?php
// -------------------------------------
// Product Tabs or Modules for OpenCart
// By Best-Byte
// www.best-byte.com
// -------------------------------------
?>
<?php if(!empty($tabs)){ ?>
<?php if($use_tab == 1){?>
<div id="product_tabs<?php echo $module; ?>" class="htabs" style="padding-top:5px;">
	<?php $numTab = 1; ?>
	 <?php foreach ($tabs as $tab) { ?>
	  <a href="#tab-<?php echo $numTab; ?><?php echo $module; ?>"><?php echo $tab['title']; ?></a>
	 <?php $numTab++; ?>
	<?php } ?>
</div>
<?php }?>
<?php $numTab = 1; ?>
<?php foreach ($tabs as $tab) { ?>
<div id="tab-<?php echo $numTab; ?><?php echo $module; ?>">
  <div class="box">
	  <?php if($use_tab == 0){ ?>
		<div class="box-heading"><?php echo $tab['title']; ?></div>
	  <?php }?>
	  <div class="box-content">
		<div class="box-product">
		  <?php if(!empty($tab['products'])){ ?>
		  <?php foreach ($tab['products'] as $product) { ?>
		    <div>
				<?php if ($product['thumb']) { ?>
				<div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
				<?php } ?>
				<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
				<?php if ($product['price']) { ?>
				<div class="price">
				<?php if (!$product['special']) { ?>
				<?php echo $product['price']; ?>
				<?php } else { ?>
				<span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
				<?php } ?>
				</div>
				<?php } ?>
				<?php if ($product['rating']) { ?>
				<div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
				<?php } ?>
				<div class="cart"><a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button"><span><?php echo $button_cart; ?></span></a></div>
        </div>
			  <?php } ?>
		  <?php } ?>
		</div>
	  </div>
  </div>
</div>
<?php $numTab++; ?>
<?php } ?>
<script type="text/javascript"><!--
$('#product_tabs<?php echo $module; ?> a').tabs();
//--></script>
<?php } ?>