<?php
/**
 * ProSlider OpenCart Module
 * Create and position a slideshow using the Nivo Slider library.
 * 
 * @author 		Ian Gallacher
 * @version		1.5.1.*
 * @email		info@opencartstore.com
 * @support		www.opencartstore.com/support/
 */
?>
<div <?php echo ($proslider_display_box == 'false') ? 'style="margin-bottom: 20px"' : 'class="box"'; ?>>
	<div class="box-heading"><?php echo ($proslider_display_box == 'false') ? '' : $proslider_title_text; ?></div>
	<div class="box-content">
		<?php if (!empty($proslider_images)) { ?>
			<div id="proslider" style="display: none">
				<?php foreach ($proslider_images as $image) { ?>
					<a href="<?php echo $image['link']; ?>"><img src="<?php echo 'image/' . $image['image']; ?>"></a>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
</div>

<script type="text/javascript" src="catalog/view/javascript/jquery/nivo-slider/jquery.nivo.slider.pack.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/slideshow.css" media="screen" />

<script type="text/javascript">
$(document).ready(function() {
	var proslider = $('#proslider');

	proslider.css({
		'height' 	: '<?php echo $proslider_slide_height; ?>px',
		'width'	 	: '<?php echo $proslider_slide_width; ?>px',
		'display'	: 'block'
	});

	proslider.nivoSlider({
		effect: 		'<?php echo $proslider_slide_effect; ?>',
		slices: 		<?php echo $proslider_slices; ?>,
		animSpeed: 		<?php echo $proslider_animation_speed; ?>,
		pauseTime: 		<?php echo $proslider_animation_pause; ?>,
		directionNav: 	<?php echo $proslider_show_navigation; ?>,
		controlNav: 	false,
      	pauseOnHover: 	<?php echo $proslider_pause_onhover; ?>
	});
});
</script>
