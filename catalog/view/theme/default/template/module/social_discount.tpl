<?php if(isset($social_discount['name']) && $social_discount['name']!="") { ?>
<h3><?php echo $social_discount['name'] ?></h3>

<?php if($social_discount['social_action'] == "Facebook Like") { ?>
<script type="text/javascript" src="http://connect.facebook.net/en_US/all.js#xfbml=1" /></script>
<fb:like href="<?php echo $facebook_page; ?>" layout="button_count" action="like" font="arial" show_faces="true" data-width="250" onclick="myFunction();"></fb:like> 
<script type="text/javascript">
try
{
    if(FB != undefined){
        FB.Event.subscribe('edge.create', function(href, widget) {
			if(href=="<?php echo $facebook_page; ?>") {
			applySocialDiscount(<?php echo $social_discount['social_discount_id']; ?>);
			}
        });
        
         FB.Event.subscribe('edge.remove', function(href, widget) {
			if(href=="<?php echo $facebook_page; ?>") {
			removeSocialDiscount(<?php echo $social_discount['social_discount_id']; ?>);
			}
        });
    }
}catch(err){}
</script>	
<?php } else if($social_discount['social_action'] == "Twitter Follow" or $social_discount['social_action'] == "Twitter Tweet") { ?>
<script type="text/javascript" src="http://platform.twitter.com/widgets.js" /></script>
<?php if($social_discount['social_action'] == "Twitter Follow") { ?>
<a href="https://twitter.com/<?php echo $twitter_page; ?>" class="twitter-follow-button" data-show-count="true" data-show-screen-name="false" data-lang="en">Follow Us</a>
<script type="text/javascript">
try
{
    if(twttr != undefined)
    {
        twttr.events.bind('follow', function(event) {
             //alert("Following");
			 if(event.data.screen_name=="<?php echo $twitter_page; ?>") {
			 applySocialDiscount(<?php echo $social_discount['social_discount_id']; ?>);
			 }
        });
		
		twttr.events.bind('unfollow', function(event) {
             //alert("Un Following");
			 if(event.data.screen_name=="<?php echo $twitter_page; ?>") {
			 removeSocialDiscount(<?php echo $social_discount['social_discount_id']; ?>);
			 }
        });
    }
}catch(err){}
</script>
<?php } else if($social_discount['social_action'] == "Twitter Tweet") { ?>
<a href="http://twitter.com/share" class="twitter-share-button" data-text="<?php echo $social_discount['tweet_text']; ?>" data-via="<?php echo $twitter_page; ?>"></a>
<script type="text/javascript">
try
{
    if(twttr != undefined)
    {
        twttr.events.bind('tweet', function(event) {
             //alert("Tweeting");
			 //if(event.data.screen_name=="<?php echo $twitter_page; ?>") {
			 applySocialDiscount(<?php echo $social_discount['social_discount_id']; ?>);
			 //}
        });
    }
}catch(err){}
</script>
<?php } ?>	
<?php } ?>

<script type="text/javascript">
function applySocialDiscount(social_discount_id) {
$.ajax({
        url: 'index.php?route=module/social_discount/applySocialDiscount',
        type: 'post',
        data: 'social_discount_id='+social_discount_id,
        dataType: 'json',
        success: function(json) {
            	
				if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');				
				$('.success').fadeIn('slow');
				$('#cart').load('index.php?route=module/cart #cart > *');
				$('.cart-total').load('index.php?route=checkout/cart .cart-total > *');
				$('html, body').animate({ scrollTop: 0 }, 'slow');
				}	else if (json['error']) {
				$('#notification').html('<div class="warning" style="display: none;">' + json['error'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');				
				$('.warning').fadeIn('slow');
				}
		 }
       }); 
 }  

function removeSocialDiscount(social_discount_id) {
$.ajax({
        url: 'index.php?route=module/social_discount/removeSocialDiscount',
        type: 'post',
        data: 'social_discount_id='+social_discount_id,
        dataType: 'json',
        success: function(json) {
            	if (json['remove']) {
				$('#notification').html('<div class="warning" style="display: none;">' + json['remove'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');				
				$('.warning').fadeIn('slow');
				$('#cart').load('index.php?route=module/cart #cart > *');
				$('.cart-total').load('index.php?route=checkout/cart .cart-total > *');
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	else if (json['error']) {
				$('#notification').html('<div class="warning" style="display: none;">' + json['error'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');				
				$('.warning').fadeIn('slow');
				}
		 }
       });    
}
</script>
<?php } ?>	