<div id="shipping-container" style="padding-top: 20px;">
    <input type="hidden" id="shipping-country-input" name="shipping_country" value="<?php echo isset($country) ? $country['country_id'] : 0 ?>" />
    <input type="hidden" id="shipping-quote-input" name="shipping_quote" value="" />
	<div id="default-shipping">
		<?php if (!isset($country)) { ?>
		<div style="padding-top: 0px; padding-right: 2px;">
			<img src="image/data/banners/estimator-free-shipping.gif" alt="Free Shipping">
		</div>
		<div style="padding-top: 3px;">
			<a class="delivery-method" href="<?php echo $this->url->link("module/pp_shipping_estimator/form") ?>"><strong>WorldWide</strong></a>
		</div>
		<?php } ?>
	</div>
    <p id="shipping-text" style="display: none">
        <strong>Estimated Shipping Cost:</strong> <strong id="shipping-cost">?</strong><br /> in <span id="shipping-time">? </span><br />
        to <strong id="shipping-country"><?php echo isset($country) ? $country['name'] : '?' ?></strong> via <a class="delivery-method" href="<?php echo $this->url->link("module/pp_shipping_estimator/form") ?>"><strong id="shipping-provider">?</strong></a>
    </p>
    <div class="error" style="color: red;"></div>
</div>
<script type="text/javascript" src="<?=$this->url->link("module/pp_shipping_estimator/countriesJS")?>"></script>
<script type="text/javascript"><!--
$(function(){
    var countries = pp_shipping_countries ,
        shipping = $('#shipping-container'),
        shippingCountryInput = $('#shipping-country-input'),
        shippingQuoteInput = $('#shipping-quote-input'),
        shippingCost = $('#shipping-cost'),
        shippingCountry = $('#shipping-country'),
        shippingProvider = $('#shipping-provider'),
        shippingTime = $('#shipping-time'),
        quantity = $('input[name=quantity]'),
        shippingText = $('#shipping-text'),
		defaultShipping = $('#default-shipping'),
        loading = $('#loading');

    function getData() {
        return {
            product_id:$('input[name=product_id]').val(),
            country_id:shippingCountryInput.val(),
            quantity:quantity.val(),
            quote: shippingQuoteInput.val()
        };
    }

    function fillInShipping(cost, deliveryTime, providerName, quoteCode, country) {
        var text = '';
        if (cost.substr(1, 4) == '0.00') {
            text += '<div><div style="padding-top: 0px; padding-right: 2px;"><img src="image/data/banners/estimator-free-shipping.gif" alt="Free Shipping"></div>';
        } else {
            text += '<strong>' + cost +' shipping</strong>';
        }
        text += ' to <strong>' + country + '</strong>';
        if (deliveryTime && deliveryTime != "" && deliveryTime != 0) {
            text += ' in<br /></div><div><strong>' + deliveryTime + '</strong> business days';
        }
        
        text += '&nbsp;w/ <a class="delivery-method" href="<?php echo $this->url->link("module/pp_shipping_estimator/form") ?>"><strong>' + providerName + '</strong></a>';
        shippingText.html(text);
        shippingQuoteInput.val(quoteCode);
		$.post('<?php echo $this->url->link('total/shipping/calculate'); ?>', {shipping_method: quoteCode}, $.noop);
    }

    quantity.change(function() {
        if (quantity.val() > 0) {
            shippingCountryInput.change();
        }
    });

    shippingCountryInput.change(function(e) {
		if ($(this).val() < 1) {
			return
		}
        var data = getData(),
            countryId = data.country_id;

        $.post('<?php echo $this->url->link("module/pp_shipping_estimator/quote") ?>', data, function(data) {

            var error_text = "";
            if (data['error']) {
                shipping.find('.error').html(data['text_error']);
                return;
            }
            var min = 100500,
                minQuote = null,
                minProvider = null,
				interrupt = false;
            $.each(data.shipping_methods, function(name, method) {
                $.each(method.quote, function(quote_name, quote) {
					if (data.shipping_provider == name) {
						min = quote.cost;
						minQuote = quote;
						minProvider = method.title;
						interrupt = true;
						return false;
					}
                    if (min > quote.cost) {
                        min = quote.cost;
                        minQuote = quote;
                        minProvider = method.title
                    }
                });
				return !interrupt;
            });

            fillInShipping(minQuote.text, minQuote.delivery, minProvider, minQuote.code, countries[countryId].name);
			defaultShipping.hide();
            shippingText.show();
        });
    });

    shipping.on('click', '.delivery-method', function(e){
        e.preventDefault();
        $.post(this.href, getData(), function(response) {
            $.nmData(response);
            var countrySelect = $('#countryPullDown');
            countrySelect.change(function(e) {
                var shippingMethods = $('#shipping-methods'),
                    loader = $('#loader');
                shippingMethods.empty();
                loader.show();
                var data = getData();
                data.country_id = $(this).val();
                $.post("<?php echo $this->url->link("module/pp_shipping_estimator/methods") ?>", data, function(response) {
                    loader.hide();
                    shippingMethods.html(response);
                });
            }).change();
            $('#shipping-submit').click(function(e){
                e.preventDefault();
                shippingCountryInput.val(countrySelect.val());
                var selectedQuote = $('#shipping-form :radio:checked').closest('.shipping-method');
                fillInShipping(selectedQuote.find('.cost').html(), selectedQuote.find('.delivery').html(),
                    selectedQuote.find('.provider-name').html(), selectedQuote.find('.code').html(), countrySelect.find(':selected').html());
                $.nmTop().close();
				defaultShipping.hide();
				shippingText.show()
			})

        });
    });

	if ($("#product_buy").length > 0){
		shipping.detach();
		$('#product_buy').append(shipping);
	}else if ($('.product-info .cart').length > 0){
		shipping.detach();
		$('.product-info .cart').append(shipping);
	} else {
		//be there, be cool
	}

	defaultShipping.show();

	if (shippingCountryInput.val() < 1 && 0) {
		//first start from default, in case getJSON fails
		$.getJSON("http://freegeoip.net/json/?callback=?", function(data) {
			$.each(countries, function(country_id, country) {
				if (country.name == data.country_name) {
					shippingCountryInput.val(country_id);
					shippingCountryInput.change();
					return false;
				}
			});
		});
	}
	shippingCountryInput.change()
});

//--></script>

