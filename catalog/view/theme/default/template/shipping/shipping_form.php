<div id="shipping-form">
    <p class="popupHeader">Select Your Shipping Options</p>
    <p class="select"><label for="countryPullDown"><strong>Ship this product to:</strong></label>
    <select id="countryPullDown" name="country_id" class="countryPullDown">
        <?php foreach($countries as $country_id => $one_country) {
            $selected = "";
            if (isset($selected_country_id) && $selected_country_id == $one_country['country_id']) {
                $selected = ' selected="selected"';
            }
            echo "<option value='$country_id'$selected>${one_country['name']}</option>";
        }?>
    </select></p>
    <p class="select"><strong>Select Shipping Method:</strong></p><br />
    <div id="loader" class="loading" style="display:none; height:100px;width: 100%"></div>
    <div id="shipping-methods">
        <?php include(dirname(__FILE__) . '/shipping_methods.php'); ?>
    </div>
    <div class="buttons2">
        <a class="button" href="#" id="shipping-submit">
            <span>&nbsp;OK&nbsp;</span>
        </a>
        <a class="button" href="<?php echo $this->url->link('checkout/checkout') ?>" id="buynow">
            <span>&nbsp;Buy Now&nbsp;</span>
        </a>
    </div>
    <div class="free-terms">Free <strong>USPS First Class</strong> shipping on <strong>All US</strong> orders.<br />Free <strong>USPS Priority WorldWide</strong> shipping for orders above <strong>$300</strong>.<br /><a href="free-shipping-worldwide">Click here for Free Shipping Terms</a></div>
</div>
<script type="text/javascript">
    $('#buynow').click(function(e){
        var $this = $(this);
        e.preventDefault();
        $.ajax({
            url: '<?php echo $this->url->link('checkout/cart/update') ?>',
            type: 'post',
            data: {product_id : $('input[name=product_id]').val()},
            dataType: 'json',
            beforeSend: function() {
                $this.after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
            },
            success: function(json) {
                if (!json['error']) {
                    location = $this.attr('href');
                } else if (json['error']['warning']) {
                    $('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                    $('#shipping-submit').click();
                    $('.warning').fadeIn('slow');
                }
            },
            complete: function() {
                $('.loading').remove();
            }
        });
    });
</script>