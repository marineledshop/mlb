<?php
$is_first = true;
if (!isset($shipping_methods)) {
    echo "<strong>No shipping methods found</strong>";
    return;
}
?>
<div class="shipping-method">
    <p class="provider-name" style="text-align: left;">Shipping Company:</p>
    <p style="text-align: left;">Estimated Delivery Time:</p>
    <p style="text-align: left;">Shipping Cost:</p>
</div>
<?php foreach($shipping_methods as $shipping_name => $shipping_method) {
    foreach($shipping_method['quote'] as $quote_name => $quote) { ?>
    <div class="shipping-method">
        <label>
            <?php if ($shipping_name && file_exists(DIR_IMAGE . 'payments/' . $shipping_name . '.png')) {
                $img = HTTP_IMAGE . 'payments/' . $shipping_name . '.png'; ?>
                <img width="100px" src="<?php echo $img; ?>">
            <?php } ?>
            <p class="provider-name <?php echo strtok($quote['code'], '.') ?>"><?php echo $shipping_method['title'] ?></p>
            <?php if (isset($quote['delivery']) && !empty($quote['delivery'])) { ?>
                <p class="estimatedTime"><span class="delivery"><?php echo $quote['delivery'] ?></span></p>
            <?php } else { ?>
                <p>Unknown</p>
            <?php } ?>
            <p class="cost"><?php echo $quote['cost'] != 0 ? $quote['text'] : "Free "?></p>
            <input type="radio" name="shipping-quote" value="<?php echo $quote_name ?>" <?php echo (!isset($selected_quote) && $is_first) || (isset($selected_quote) && $selected_quote == $quote['code']) ? 'checked="checked"' : ''?> />
<!--            <strong class="provider-name"><?php /*echo $shipping_method['title'] */?></strong> <?php /*echo isset($quote['delivery']) && !empty($quote['delivery']) ? "(<span class=\"estimatedTime\"><span class='delivery'>{$quote['delivery']}</span> business days)</span> " : '' */?><br />
            <span><?php /*echo $quote['title'] */?> <strong class="cost"><?php /*echo $quote['cost'] != 0 ? $quote['text'] : "Free" */?></strong></span>-->
            <span style="display: none" class="code"><?php echo $quote['code'] ?></span>
        </label>
    </div>
    <?php
        $is_first = false;
    }
} ?>
