(function($){
    $.mlsAForm = function(options){
        var dOptions = {
            isTest: false,
            isDie: false,
            eName: 'form',
            cName: 'mls_form',
            sFunc: 'all', // scanForm|loadForm
            cBack: function(){}
        };
        var options = $.extend(dOptions,options);
        var logs = function(tag,type,text,method,myTest){
            if(options.isTest || myTest){
                if(method == 'console.log'){
                    window.console && console.log('tag:['+tag+'];', 'type:['+type+'];', 'text:['+text+'];');
                }else if(method == 'console.warn'){
                    window.console && console.warn('tag:['+tag+'];', 'type:['+type+'];', 'text:['+text+'];');
                }else if(method == 'alert'){
                    alert('tag:['+tag+']; type:['+type+']; text:['+text+'];');
                }
            }
        }
        var getCookie = function(){
            var result = [];
            if(typeof $.cookie != 'undefined'){
                var cvalue = $.cookie(options.cName);
                if(cvalue != null){
                    var array = cvalue.split('&');
                    for(var i=0; i<array.length; i++){
                        result[i] = array[i].split('=');
                    }
                }
            }
            return result;
        }
        var setCookie = function(name,value){
            if(typeof $.cookie != 'undefined'){
                var array = [];
                var add = 1;
                var l = 0;
                var cValue = $.cookie(options.cName);
                value = encodeURIComponent(value);
                if(cValue != null){
                    array = cValue.split('&');
                    l = array.length;
                    for(var i=0; i<l; i++){
                        var param = array[i].split('=');
                        if([param[0]] == name){
                            array[i] = param[0]+'='+value;
                            add = 0;
                        }
                    }
                }
                if(add) array[l] = name+'='+value;
                cValue = array.join('&');
                $.cookie(options.cName, cValue);
            }
        }
        var scanForm = function(){
            findInput('text');
            findInput('checkbox');
            findInput('radio');
            findSelect();
            findTextarea();
        }
        var loadForm = function(){
            logs(
                'loadForm',
                'DEBUG',
                'start - loadForm | eName=('+options.eName+')',
                'console.log'
            );
            var array = getCookie();
            for(var i=0; i<array.length; i++){
                var param = array[i];
                var name = param[0];
                var value = decodeURIComponent(param[1]);
                $(options.eName).find('input[name="'+name+'"]').each(function(){
                    var type = $(this).attr('type');
                    switch(type){
                        case 'text':
                            $(this).val(value);
                        break;
                        case 'checkbox':
                            if(value == '1'){
                                $(this).attr('checked','checked');
                            }else{
                                $(this).attr('checked',false);
                            }
                        break;
                        case 'radio':
                            if($(this).val() == value){
                                $(this).attr('checked','checked');
                            }
                        break;
                    }
                });
                $(options.eName).find('select[name="'+name+'"]').each(function(){
                    $(this).val(value);
                });
                $(options.eName).find('textarea[name="'+name+'"]').each(function(){
                    $(this).val(value);
                });
            }
        }
        var findInput = function(type){
            logs(
                'findInput',
                'DEBUG',
                'start - findInput | eName=('+options.eName+') | type=('+type+')',
                'console.log'
            );
            if(options.isDie) $(options.eName).find('input[type="'+type+'"]').die('change');
            $(options.eName).find('input[type="'+type+'"]').live('change',function(){
                logs(
                    'findInput',
                    'DEBUG',
                    'change cookie('+options.cName+')',
                    'console.log'
                );
                $(options.eName).find('input[type="'+type+'"]').each(function(){
                    var name = $(this).attr('name');
                    var type = $(this).attr('type');
                    var value = '';
                    switch(type){
                        case 'text':
                        default:
                            value = $(this).val();
                            setCookie(name,value);
                        break;
                        case 'checkbox':
                            if($(this).attr('checked') == 'checked'){
                                value = '1';
                            }else{
                                value = '0';
                            }
                            setCookie(name,value);
                        break;
                        case 'radio':
                            if($(this).is(':checked')){
                                value = $(this).val();
                                setCookie(name,value);
                            }
                        break;
                    }
                });
            });
        }
        var findSelect = function(){
            logs(
                'findSelect',
                'DEBUG',
                'start - findSelect | eName=('+options.eName+')',
                'console.log'
            );
            if(options.isDie) $(options.eName).find('select').die('change');
            $(options.eName).find('select').live('change',function(){
                logs(
                    'findSelect',
                    'DEBUG',
                    'change cookie('+options.cName+')',
                    'console.log'
                );
                $(options.eName).find('select').each(function(){
                    var name = $(this).attr('name');
                    var value = $(this).val();
                    setCookie(name,value);
                });
            });
        }
        var findTextarea = function(){
            logs(
                'findTextarea',
                'DEBUG',
                'start - findTextarea | eName=('+options.eName+')',
                'console.log'
            );
            if(options.isDie) $(options.eName).find('textarea').die('change');
            $(options.eName).find('textarea').live('change',function(){
                logs(
                    'findTextarea',
                    'DEBUG',
                    'change cookie('+options.cName+')',
                    'console.log'
                );
                $(options.eName).find('textarea').each(function(){
                    var name = $(this).attr('name');
                    var value = $(this).val();
                    setCookie(name,value);
                });
            });
        }
        switch(options.sFunc){
            case 'scanForm':
                scanForm();
            break;
            case 'loadForm':
                loadForm();
            break;
            default:
                loadForm();
                scanForm();
            break;
        }
        options.cBack();
    };
})(jQuery);