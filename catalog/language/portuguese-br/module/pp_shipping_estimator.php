<?php
// Heading 
$_['heading_title']     = 'Simulador de Frete';
// Text
$_['text_shipping']     = 'Calcule o frete e o prazo de entrega estimados para sua região.';
$_['text_success']      = 'Frete estimado para %s produto(s):';
$_['text_shipping_success'] =  'A simulação de frete foi aplicada com sucesso!';
$_['text_error']        = 'Atenção:Não foi possível calcular o frete';

// Entry
$_['entry_country']     = 'País:';
$_['entry_zone']        = 'Estado:';
$_['entry_postcode']    = 'CEP:';

//button
$_['button_simular']    = 'Calcular';

// Error
$_['error_quantity']    = 'Digite a quantidade';
$_['error_product']     = 'Selecione um produto';
$_['error_option']      = 'As opções de produto com * são requeridas.<br/>
                          Você deve preenchê-las para simular o frete!';
$_['error_postcode']    = 'O CEP deve ter no mínimo 8 caracteres.';
$_['error_country']     = 'Por favor, selecione um país!';
$_['error_zone']        = 'Por favor, selecione um estado!';
$_['error_shipping']    = 'Atenção: É necessário escolher um método de envio!';
$_['error_no_shipping'] = 'Atenção: Não há opções de envio disponíveis. Por favor, <a href="%s">entre em contato</a> para assistência!';
?>