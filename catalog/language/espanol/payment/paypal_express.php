<?php

// Text
$_['text_title'] = '<img style="vertical-align:middle" src="image/data/paypal-credit-card-logo.gif" alt="PayPal Express Checkout" /> PayPal Express Checkout (incluye tarjetas de cr&eacute;dito y de d&eacute;bito de tarjeta)';
$_['text_wait'] = 'Por favor, espere!';
$_['text_payment_processing'] = 'De procesamiento de pagos en curso...';
$_['text_use_paypal_data'] = 'Los datos utilizados son los datos de PayPal';

#Error
$_['text_error_request'] = 'Se produjo un error en la ejecuci&oacute;n de la petici&oacute;n';
?>