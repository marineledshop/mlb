<?php

// Text
$_['text_title'] = '<img style="vertical-align:middle" src="image/data/paypal-credit-card-logo.gif" alt="PayPal Express Checkout" /> PayPal Express Checkout (include Carte di Credito e Carte di Debito)';
$_['text_wait'] = 'Si prega di attendere!';
$_['text_payment_processing'] = 'Elaborazione del pagamento in corso...';
$_['text_use_paypal_data'] = 'I dati usati sono i tuoi dati PayPal';

#Error
$_['text_error_request'] = 'Si &egrave; verificato un errore nell\'elaborare la richiesta';
?>