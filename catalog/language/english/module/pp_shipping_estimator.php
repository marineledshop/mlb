<?php
// Heading 
$_['heading_title']     = 'Shipping estimator';
// Text
$_['text_shipping']     = 'Calculate the shipping cost and time for your destination:';
$_['text_success']      = 'Estimated shipping for %s product(s):';
$_['text_shipping_success']  = 'Success: Your shipping estimate has been applied!';
$_['text_error']        = 'Warning: No Shipping options are available to your location.';

// Entry
$_['entry_country']     = 'Country:';
$_['entry_zone']        = 'Region / State:';
$_['entry_postcode']    = 'Post Code:';

//button
$_['button_simular']    = 'Calculate';

// Error
$_['error_quantity']    = 'Type the quantity!';
$_['error_product']     = 'Select a product';
$_['error_option']      = 'Product options with * are required. <br/>
                           You must fill them to simulate the shipping!';
$_['error_postcode']    = 'Postcode must be between 2 and 10 characters!';
$_['error_country']     = 'Please select a country!';
$_['error_zone']        = 'Please select a region / state!';
$_['error_shipping']    = 'Warning: Shipping method required!';
$_['error_no_shipping'] = 'Warning: No Shipping options are available.';
?>