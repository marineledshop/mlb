<?php
//==============================================================================
// MailChimp Integration v155.3
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
//==============================================================================

// Heading
$_['heading_title']			= 'Newsletter';

// Buttons
$_['button_subscribe']		= 'Subscribe';

// Text
$_['text_name']				= 'Name:';
$_['text_email_address']	= 'E-mail Address:';
$_['text_success']			= 'Success! Please click the confirmation link in the e-mail sent to you.';
$_['text_error']			= 'There\'s been an error processing your request.';
$_['text_please_fill_in']	= 'Please fill in the required fields!';
$_['text_please_use']		= 'Please use a valid email address!';
?>