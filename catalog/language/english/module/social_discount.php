<?php
// Heading 
$_['heading_title'] = 'Social Discount';

$_['text_social_discount_success'] = 'Success: Your Social Discount is Applied.';

$_['text_social_discount_error'] = 'Warning: Social Discount code is either invalid, expired or reached it\'s usage limit!';

$_['text_social_discount_used'] = 'Success: This Social Discount is already applied in your Shopping Cart.';

$_['text_social_discount_remove'] = 'Warning: Your Social Discount is Removed.';

?>