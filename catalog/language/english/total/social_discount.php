<?php
// Heading 
$_['heading_title'] = '';

// Text
$_['text_social_discount']   = 'Social Discount (%s)';
$_['text_success']  = 'Success: Your Social Discount discount has been applied!';

// Entry
$_['entry_coupon']  = 'Enter your Social Discount code here:';

// Error
$_['error_coupon']  = 'Warning: Social Discount code is either invalid, expired or reached it\'s usage limit!';
?>