<?php
// Heading
$_['heading_title'] = 'Your Order Has Been Processed!';
$_['heading_title_error'] = 'Order Processing Failed!';

// Text
$_['text_customer'] = '<p>Your order has been successfully processed!</p>
<p>You should receive two e-mails from us about your order. First one should arrive in next 10 minutes and this will be your order confirmation. Second mail will be sent once we ship your order. You will find details for tracking your parcel and your invoice attached in this 2nd e-mail.</p>
<p>If you do not receive any of these e-mails please use our <a href="/index.php?route=information/form&form=contact_us">contact us form</a> to inform us or send an e-mail to <a href="mailto:support@marineledbulbs.com">support@marineledbulbs.com</a>.</p>
<p>You can view your order history by going to the <a href="/index.php?route=account/account">my account page</a> and by clicking on <a href="/index.php?route=account/order">history</a>.</p>
<p>Thanks for shopping with us online!</p>';
$_['text_guest']    = '<p>Your order has been successfully processed!</p>
<p>You should receive two e-mails from us about your order. First one should arrive in next 10 minutes and this will be your order confirmation. Second mail will be sent once we ship your order. You will find details for tracking your parcel and your invoice attached in this 2nd e-mail.</p>
<p>If you do not receive any of these e-mails please use our <a href="/index.php?route=information/form&form=contact_us">contact us form</a> to inform us or send an e-mail to <a href="mailto:support@marineledbulbs.com">support@marineledbulbs.com</a>.</p>
<p>Thanks for shopping with us online!</p>';
$_['text_error']    = '<p>Oops... It looks like there was a problem with your Paypal payment transaction.</p><p>The products you added to your shopping cart is still in our memory &gt; <a href="%s">shopping cart</a></p>';
$_['text_basket']   = 'Basket';
$_['text_checkout'] = 'Checkout';
$_['text_success']  = 'Success';
?>