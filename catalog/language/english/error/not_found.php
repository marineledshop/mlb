<?php
// Heading
$_['heading_title'] = 'Not Found - 404';

// Text
$_['text_error']    = 'We are sorry for inconvenience but the page you are looking for cannot be found.';
?>