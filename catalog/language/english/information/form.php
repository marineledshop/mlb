<?php
//==============================================================================
// Flexible Form v154.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
//==============================================================================

// Heading
$_['heading_title']			= 'Success';

// Buttons
$_['button_submit']			= 'Submit';

// Text
$_['text_after_title']		= ':';
$_['text_confirm']			= 'Confirm';
$_['text_allowed_ext']		= '<span style="font-size: 11px; color: #888">(Allowed file extensions: %s)</span>';
$_['text_enter_code']		= 'Enter the code in the box below:';
$_['text_no_answer']		= '(No answer given)';
$_['text_no_file']			= '(No file uploaded)';
$_['text_error']			= 'Form not found!';

// Errors
$_['error_required']		= 'Please fill in all required form fields!';
$_['error_email']			= 'Please use a valid e-mail address format!';
$_['error_email_confirm']	= 'E-mail address does not match confirmation!';
$_['error_password']		= 'Password is incorrect!';
$_['error_captcha']			= 'Verification code does not match the image!';
$_['error_file_name']		= '(File name too long or too short)';
$_['error_file_size']		= '(File size too large)';
$_['error_file_ext']		= '(File extension .%s not allowed)';
$_['error_file_upload']		= '(File upload error)';
?>