<?php
// Text
$_['text_new_subject']          = '%s - Order %s';
$_['text_new_greeting']         = '<p>Thank you for your interest in %s products. Your order has been received and payment has been confirmed. We will process & ship your order in 24 hours or less except for weekends & national holidays. Our next mail will contain tracking details for your parcel. If you don\'t receive this mail in next 36-48 hours please check your spam folder.</p>
<p>If you have any questions please use our <a href="/index.php?route=information/form&form=contact_us">contact us form</a> or send an e-mail to support@marineledbulbs.com.</p>';
$_['text_new_received']         = 'You have received an order.';
$_['text_new_link']             = 'To view your order click on the link below:';
$_['text_new_order_detail']     = 'Order Details.';
$_['text_new_instruction']      = 'Instructions';
$_['text_new_order_id']         = 'Order ID:';
$_['text_new_date_added']       = 'Date Added:';
$_['text_new_order_status']     = 'Order Status:';
$_['text_new_payment_method']   = 'Payment Method:';
$_['text_new_shipping_method']  = 'Shipping Method:';
$_['text_new_email']  			= 'Email:';
$_['text_new_telephone']  		= 'Telephone:';
$_['text_new_ip']  				= 'IP Address:';
$_['text_new_payment_address']  = 'Payment Address';
$_['text_new_shipping_address'] = 'Shipping Address';
$_['text_new_products']         = 'Products';
$_['text_new_product']          = 'Product';
$_['text_new_model']            = 'Model';
$_['text_new_quantity']         = 'Quantity';
$_['text_new_price']            = 'Price';
$_['text_new_order_total']      = 'Order Totals';
$_['text_new_total']            = 'Total';
$_['text_new_download']         = 'Once your payment has been confirmed you can click on the link below to access your downloadable products:';
$_['text_new_comment']          = 'The comments for your order are:';
$_['text_new_footer']           = '';
$_['text_new_powered']          = 'Sent by <a href="http://www.marineledbulbs.com">Marine LED Bulbs</a>';
$_['text_update_subject']       = '%s - Order Update %s';
$_['text_update_order']         = 'Order ID:';
$_['text_update_date_added']    = 'Date Ordered:';
$_['text_update_order_status']  = 'Your order has been updated to the following status:';
$_['text_update_comment']       = 'The comments for your order are:';
$_['text_update_link']          = 'To view your order click on the link below:';
$_['text_update_footer']        = '';
?>
