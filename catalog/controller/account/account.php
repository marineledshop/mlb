<?php 
class ControllerAccountAccount extends Controller { 
	public function index() {
		if (!$this->customer->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('account/account', '', 'SSL');
	  
	  		$this->redirect($this->url->link('account/login', '', 'SSL'));
    	} 
	
		$this->language->load('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
		
		if (isset($this->session->data['success'])) {
    		$this->data['success'] = $this->session->data['success'];
			
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
    	$this->data['heading_title'] = $this->language->get('heading_title');

    	$this->data['text_my_account'] = $this->language->get('text_my_account');
		$this->data['text_my_orders'] = $this->language->get('text_my_orders');
		$this->data['text_my_newsletter'] = $this->language->get('text_my_newsletter');
    	$this->data['text_edit'] = $this->language->get('text_edit');
    	$this->data['text_password'] = $this->language->get('text_password');
    	$this->data['text_address'] = $this->language->get('text_address');
		$this->data['text_wishlist'] = $this->language->get('text_wishlist');
    	$this->data['text_order'] = $this->language->get('text_order');
    	$this->data['text_download'] = $this->language->get('text_download');
		$this->data['text_reward'] = $this->language->get('text_reward');
		$this->data['text_return'] = $this->language->get('text_return');
		$this->data['text_transaction'] = $this->language->get('text_transaction');
		$this->data['text_newsletter'] = $this->language->get('text_newsletter');

    	$this->data['edit'] = $this->url->link('account/edit', '', 'SSL');
    	$this->data['password'] = $this->url->link('account/password', '', 'SSL');
		$this->data['address'] = $this->url->link('account/address', '', 'SSL');
		$this->data['wishlist'] = $this->url->link('account/wishlist');
    	$this->data['order'] = $this->url->link('account/order', '', 'SSL');
    	$this->data['download'] = $this->url->link('account/download', '', 'SSL');
		$this->data['reward'] = $this->url->link('account/reward', '', 'SSL');
		$this->data['return'] = $this->url->link('account/return', '', 'SSL');
		$this->data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
		$this->data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
                
                // update for mls
                // My Last Order block
                $this->data['text_my_last_order'] = $this->language->get('text_my_last_order');
                $this->load->model('account/order');
		$results = $this->model_account_order->getOrders(0, 1);
                if ($results) {
                    $this->data['show_last_order'] = true;
                    $result = $results[0];
                    $product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);

                    if ($result['status'] == 'Shipped') {                            
                        $query = $this->db->query('SELECT ' . DB_PREFIX . 'date_added FROM order_history WHERE order_id=' . $result['order_id'] . ' AND order_status_id=3 ORDER BY order_history_id DESC LIMIT 1');
                        $date_shipped =  $query->rows[0]['date_added'];                            
                        $status = 'Shipped on ' . date($this->language->get('date_format_short'), strtotime($date_shipped));

                        if ($result['track']) {                            
                            if ($result['shipping_provider'] == 'singpost') {
                                $tracking_url = 'http://www.singpost.com/index.php?option=com_tablink&controller=tracking&task=trackdetail&layout=show_detail&tmpl=component&ranumber=' . $result['track'];
                            } else if ($result['shipping_provider'] == 'dhl') {
                                $tracking_url = 'http://www.dhl.com/content/g0/en/express/tracking.shtml?brand=DHL&AWB=' . $result['track'];
                            } else {
                                $tracking_url = '';
                            }
                            if ($tracking_url) {
                                $status .= ' - <a href="' . $tracking_url . '" target="_blank">' . $this->language->get('text_click_to_track') . '</a>';
                            }
                        }

                    } else {
                        $status = $result['status'];
                    }
                    
                    
                    
                    if ($result['invoice_no']) {
                        $invoice_no = $result['invoice_prefix'] . $result['invoice_no'];
                    } else {
                        $invoice_no = '';
                    }
                                        
                    $this->data['last_order'] = array(
                            'order_id'   => $result['order_id'],
                            'name'       => $result['firstname'] . ' ' . $result['lastname'],
                            'status'     => $status,
                            'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                            'products'   => $product_total,
                            'total'      => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
                            'href'       => $this->url->link('account/order/info', 'order_id=' . $result['order_id'], 'SSL'),
                            'invoice_no' => $invoice_no
                    );
		} else {
                    $this->data['show_last_order'] = false;
                }

                
                // end of update
                
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/account.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/account/account.tpl';
		} else {
			$this->template = 'default/template/account/account.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'		
		);
				
		$this->response->setOutput($this->render());
  	}
}
?>