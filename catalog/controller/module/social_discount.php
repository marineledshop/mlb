<?php
class ControllerModuleSocialDiscount extends Controller {
	protected function index($setting) {
		
		$this->language->load('module/social_discount'); 
		
      	$this->load->model('checkout/social_discount');
		
		$this->data['social_discount'] = $this->model_checkout_social_discount->getSocialDiscountDetail($setting['social_discount_id']);
		
		if(isset($this->data['social_discount']['social_network_page']) && (!empty($this->data['social_discount']['social_network_page']))) {
			$this->data['facebook_page'] = $this->data['social_discount']['social_network_page'];
			$this->data['twitter_page'] = $this->data['social_discount']['social_network_page'];
		} else {
			$this->data['facebook_page'] = $this->config->get('facebook_page');
			$this->data['twitter_page'] = $this->config->get('twitter_page');
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/social_discount.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/social_discount.tpl';
		} else {
			$this->template = 'default/template/module/social_discount.tpl';
		}

		$this->render();
	}
	
	public function applySocialDiscount() {
		$this->language->load('module/social_discount'); 
		if (isset($this->request->post['social_discount_id']) && $this->validateSocialDiscount($this->request->post['social_discount_id'])) {
			if(isset($this->session->data['social_discount'])) {
				if(in_array($this->request->post['social_discount_id'], $this->session->data['social_discount']))	{		
				$json['success'] = $this->language->get('text_social_discount_used');	
				} else {
				$this->session->data['social_discount'][] = $this->request->post['social_discount_id'];		
				$json['success'] = $this->language->get('text_social_discount_success');			
				}	
			} else {
				$this->session->data['social_discount'][] = $this->request->post['social_discount_id'];		
				$json['success'] = $this->language->get('text_social_discount_success');					
			}
		} else {
			$json['error'] = $this->language->get('text_social_discount_error');			
			}
			$this->response->setOutput(json_encode($json));		
	}
	
	public function removeSocialDiscount() {
		$this->language->load('module/social_discount'); 
		if (isset($this->request->post['social_discount_id']) && isset($this->session->data['social_discount'])) {
			if(in_array($this->request->post['social_discount_id'], $this->session->data['social_discount']))	{			
				foreach ($this->session->data['social_discount'] as $key => $row) {
					if($row == $this->request->post['social_discount_id']) {
					unset($this->session->data['social_discount'][$key]);
					$json['remove'] = $this->language->get('text_social_discount_remove');	
					}
				}	
			} 
		} else {
			$json['error'] = $this->language->get('text_social_discount_error');			
			}		
			$this->response->setOutput(json_encode($json));		
	}
	
	public function validateSocialDiscount($social_discount_id) {
		$this->load->model('checkout/social_discount');
				
		$social_discount_info = $this->model_checkout_social_discount->getSocialDiscount($social_discount_id);			
		
		if (!$social_discount_info) {
			return false;
			}  else {
			return true;	
			}
		}
	
}
?>