<?php
/**
 * Facebook OpenCart Module. Creates a Facebook like box.
 *
 * @author 		www.opencartstore.com
 * @support		www.opencartstore.com/support/
 * @version		1.5.1.*
 */
  
class ControllerModuleFacebooklike extends Controller {
	/**
	 * Main controller function that handles the modules required config
	 * , language and template loading.
	 * 
	 * @access protected
	 * @return void
	 */
	protected function index() {
		$this->load->language('module/facebooklike');

		$this->data['heading_title'] 			= $this->language->get('heading_title');
		$this->data['facebooklike_profile_id']	= $this->config->get('facebooklike_profile_id');
		$this->data['facebooklike_height']		= $this->config->get('facebooklike_height');
		$this->data['facebooklike_width']		= $this->config->get('facebooklike_width');
		$this->data['facebooklike_stream']		= $this->config->get('facebooklike_stream');
		$this->data['facebooklike_connections']	= $this->config->get('facebooklike_connections');
		$this->data['facebooklike_header']		= $this->config->get('facebooklike_header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/facebooklike.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/facebooklike.tpl';
		} else {
			$this->template = 'default/template/module/facebooklike.tpl';
		}
		
		$this->render();
	}
}
?>