<?php  
/**
 * ProSlider OpenCart Module
 * Create and position a slideshow using the Nivo Slider library.
 * 
 * @author 		Ian Gallacher
 * @version		1.5.1.*
 * @email		info@opencartstore.com
 * @support		www.opencartstore.com/support/
 */

class ControllerModuleProslider extends Controller {
	protected function index() {
		//Pull in data from config table
		$this->data['proslider_title_text'] = $this->config->get('proslider_title_text');
		$this->data['proslider_slide_effect'] = $this->config->get('proslider_slide_effect');
		$this->data['proslider_display_box'] = $this->config->get('proslider_display_box');
		$this->data['proslider_show_navigation'] = $this->config->get('proslider_show_navigation');
		$this->data['proslider_pause_onhover'] = $this->config->get('proslider_pause_onhover');
		$this->data['proslider_slide_width'] = $this->config->get('proslider_slide_width');
		$this->data['proslider_slide_height'] = $this->config->get('proslider_slide_height');
		$this->data['proslider_animation_speed'] = $this->config->get('proslider_animation_speed');
		$this->data['proslider_animation_pause'] = $this->config->get('proslider_animation_pause');
		$this->data['proslider_slices'] = $this->config->get('proslider_slices');
				
		$this->data['proslider_images'] = unserialize($this->config->get('proslider_images')); 
		
		$this->id = 'proslider';

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/proslider.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/proslider.tpl';
		} else {
			$this->template = 'default/template/module/proslider.tpl';
		}
				
		$this->render();
	}
}
?>