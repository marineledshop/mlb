<?php
/*
 * Version: 1.5.1.3
 * Updated: 06/04/2012
 */
class ControllerModuleReferralcheckout extends Controller 
{
	protected function index($setting) 
	{
		$this->language->load('module/referralcheckout');
		
		$this->id = 'referralcheckout';
		$this->template = 'default/template/module/referralcheckout.tpl';
		$referralcheckout_js = '';
		$this->data['referralcheckout_js'] = '';
		if(isset($this->request->get['rccs']) && strlen($this->request->get['rccs'])>10 && isset($this->request->get['product_id'])) {
			
			$this->startSession();
			if(isset($this->session->data['ctoken']) && strlen($this->session->data['ctoken'])>3 && strpos($this->request->get['rccs'], base64_encode(base64_decode($this->session->data['ctoken']).'TPNO'.$this->request->get['product_id']))>0 ) {
				$this->redirect($this->request->get['rccs']);
			}
			$this->load->model('catalog/product');
			$product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
			if(!empty($product_info)) {
				$this->data['st_title'] = str_replace('"', '', $this->language->get('text_share_title'));
				$this->data['st_url'] = $this->request->get['rccs'].'&shid='.$this->request->get['product_id'];
				$this->load->model('tool/image');
				$this->data['st_image'] = '';
				if($product_info['image']) {
					$this->data['st_image'] = $this->model_tool_image->resize($product_info['image'], 90, 80);
				}
				$this->data['st_summary'] = str_replace('"', '', $product_info['name'].': '.$this->language->get('text_share_description'));
				$this->data['st_site_name'] = str_replace('"', '', $this->config->get('config_name'));
				$this->template = 'default/template/module/referralcheckout_rccs.tpl';
ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $this->data['st_title'];?></title>
<meta property="og:title" content="<?php echo $this->data['st_title'];?>"/>
<meta property="og:type" content="image" />
<meta property="og:description" content="<?php echo $this->data['st_summary'];?>"/>
<meta property="og:url" content="<?php echo $this->data['st_url'];?>"/>
<meta property="og:image" content="<?php echo $this->data['st_image'];?>"/>
<meta property="og:site_name" content="<?php echo $this->data['st_site_name'];?>"/>
</head>
<body></body></html>
<?php
echo $referralcheckout_meta = ob_get_clean();
exit;				
			}
		}
		$this->load->model('checkout/order');
		$this->load->model('account/order');
		// referral coupon
		if(isset($this->request->get['route']) && $this->request->get['route']=='onecheckout/cart') {
			if(isset($_GET['ctoken'])) {
				$ctoken = $_GET['ctoken'];
				$ctoken = base64_decode($ctoken);
				if($ctoken && strpos($ctoken, 'TPNO')>0 ) {
					$atoken = explode('TPNO', $ctoken);
					$coupon = $atoken[0];
					$product_id = $atoken[1];
					$this->startSession();
					$this->session->data['ctoken'] = base64_encode($coupon);
					$this->session->data['ctokenid'] = $product_id;
					$redirect = HTTP_SERVER . 'index.php?route=product/product&product_id=' . $product_id;

                    if(isset($this->session->data['referralcheckout_token'])) {
                        unset($this->session->data['referralcheckout_token']);
                    }
					if(isset($this->request->get['shid'])) {
						$rccs = HTTP_SERVER . 'index.php?route=checkout/cart&ctoken=' . $_GET['ctoken'];
						$rccs = urlencode($rccs);
						$redirect = HTTP_SERVER . 'index.php?route=checkout/cart&product_id=' . $product_id.'&rccs='.$rccs;
					}
					$this->redirect($redirect);
				}
			}
		}
			
		if(isset($this->request->get['route']) && $this->request->get['route']=='onecheckout/checkout') {
            $this->onCheckout();
		}
				
		$this->data['referralcheckout_token'] = false;
		// remove referral coupon
		if(isset($this->request->get['route']) && $this->request->get['route']=='checkout/success') {
						
			$coupon = $this->getReferralCoupon();
			if($coupon) {
				$this->unsetReferralCoupon();
			}
			$this->startSession();
			if(!isset($this->session->data['referralcheckout_token']) && isset($this->session->data['referralcheckout_order_id'])) {
				$referralcheckout_order_id = $this->session->data['referralcheckout_order_id'];
				$product_id = (int)$this->getProductByOrderId($referralcheckout_order_id);
				if($product_id>0) {
					$coupon = $this->generateReferralCheckoutCoupon();
					$referralcheckout_token = $coupon.'TPNO'.$product_id;
					$referralcheckout_token = base64_encode($referralcheckout_token);
					$this->session->data['referralcheckout_token'] = $referralcheckout_token;
				}
			}
			if(isset($this->session->data['referralcheckout_token'])) {
				$referralcheckout_token = base64_decode($this->session->data['referralcheckout_token']);
				$areferralcheckout_token = explode('TPNO', $referralcheckout_token);
				$product_id = $areferralcheckout_token[1];
				$this->load->model('catalog/product');
				$product_info = $this->model_catalog_product->getProduct($product_id);														
				
				if($product_info) {
					$this->data['referralcheckout_token'] = true;
					$this->data['st_title'] = str_replace('"', '', $this->language->get('text_share_title'));
					$this->data['st_url'] = HTTP_SERVER . 'index.php?route=checkout/cart&ctoken=' . urlencode($this->session->data['referralcheckout_token']);
                    $shortened = $this->shortenUrl($this->data['st_url'] . '&shid='.$product_id);
                    if ($shortened && $shortened->status == 'SUCCESS') {
                        $this->data['st_url'] = $shortened->data->sharURL;
                    } else {
                        throw new Exception("Sharethis returned incorrect status");
                    }

					$this->load->model('tool/image');
					$this->data['st_image'] = '';
					if($product_info['image']) {
						$this->data['st_image'] = $this->model_tool_image->resize($product_info['image'], 100, 100);
					}
					$this->data['st_summary'] = str_replace('"', '', $product_info['name'].': '.$this->language->get('text_share_description'));
				}
			}
		}
		$this->data['text_share_info'] = $this->language->get('text_share_info');
		$this->render();
	}
			
	private function getReferralCoupon()
	{
		if(!isset($this->session->data)) {
			return NULL;
		}
		
		if(!isset($this->session->data['ctoken'])) {
			return NULL;
		}
		$coupon = $this->session->data['ctoken'];
		$coupon = base64_decode($coupon);
		if($coupon) {
			return $coupon;
		}
		return NULL;
	}
	
	private function applyReferralCoupon($coupon)
	{
		if($coupon) {
			$this->load->model('checkout/coupon');
			$coupon_info = $this->model_checkout_coupon->getCoupon($coupon);
			if($coupon_info) {
				$this->startSession();
				$this->session->data['coupon'] = $coupon;
				return true;
			}
		}
		return false;
	}
	
	private function unsetReferralCoupon()
	{
		if(isset($this->session->data)) {
			unset($this->session->data['ctoken']);
		}
		return true;
	}
	
	private function startSession()
	{
        //started automatically by oc
	}
	
	public function getProductByOrderId($order_id) {
		$query = "SELECT p.product_id FROM " . DB_PREFIX . "order_product AS op LEFT JOIN " . DB_PREFIX . "product AS p ON p.product_id=op.product_id WHERE op.order_id = '" . (int)$order_id . "' AND p.product_id>0 ORDER BY op.order_product_id DESC LIMIT 1";
		$query = $this->db->query($query);
		$aRow = $query->row;
		if(!empty($aRow['product_id'])) {
			return $aRow['product_id'];
		}
		return NULL;
	}
	// coupon settings
	public function generateReferralCheckoutCoupon() {
		
		$name = $this->language->get('text_coupon_name');
		$code = $this->randomNumber();
		$discount = '10';
		$type = 'P';
		$total = '';
		$logged = '0';
		$shipping = '0';
		$uses_total = '2';
		$uses_customer = '1';
		$status = '1';
		$this->db->query("INSERT INTO " . DB_PREFIX . "coupon SET name = '" . $this->db->escape($name) . "', code = '" . $this->db->escape($code) . "', discount = '" . (float)$discount . "', type = '" . $this->db->escape($type) . "', total = '" . (float)$total . "', logged = '" . (int)$logged . "', shipping = '" . (int)$shipping . "', date_start = NOW(), date_end = NOW() + INTERVAL 5 DAY, uses_total = '" . (int)$uses_total . "', uses_customer = '" . (int)$uses_customer . "', status = '" . (int)$status . "', date_added = NOW()");

      	$coupon_id = $this->db->getLastId();
		
		return $code;
	}
	
	public function randomNumber() {
		
		$characters = array(
		"A","B","C","D","E","F","G","H","J","K","L","M",
		"N","P","Q","R","S","T","U","V","W","X","Y","Z",
		"1","2","3","4","5","6","7","8","9");
		$random_number = '';
		//make an "empty container" or array for our keys
		$keys = array();
		
		//first count of $keys is empty so "1", remaining count is 1-6 = total 7 times
		while(count($keys) < 10) {
			//"0" because we use this to FIND ARRAY KEYS which has a 0 value
			//"-1" because were only concerned of number of keys which is 32 not 33
			//count($characters) = 33
			$x = mt_rand(0, count($characters)-1);
			if(!in_array($x, $keys)) {
			   $keys[] = $x;
			}
		}
		
		foreach($keys as $key){
		   $random_number .= $characters[$key];
		}
		return $random_number;
	}

    private function shortenUrl($url) {
        $handler = curl_init('http://rest.sharethis.com/share/createSharURL.php');
        curl_setopt_array($handler, array(
            CURLOPT_POSTFIELDS => array(
                'url' => $url,
                'pub_key' => 'a0860bcc-0d48-44fb-bcbd-3c1f55bb4d42',
                'access_key' => '167b0a6ecfd72b0fcc2328302cb91823',
            ),
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
        ));
        $res = curl_exec($handler);
        curl_close($handler);
        return json_decode($res);
    }

    public function onCheckout(){
        // get referral coupon
        $coupon = $this->getReferralCoupon();
        if($coupon) {
            $this->applyReferralCoupon($coupon);
        }

        if(isset($this->session->data['referralcheckout_token'])) {
            unset($this->session->data['referralcheckout_token']);
        }
    }
}
?>