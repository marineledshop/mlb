<?php
//==============================================================================
// Flexible Form v154.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
//==============================================================================

class ControllerInformationForm extends Controller {
	private $error = array();
	private $type = 'information';
	private $name = 'form';
	
	public function index() {
		$this->data['type'] = $this->type;
		$this->data['name'] = $this->name;
		
		$v14x = $this->data['v14x'] = (!defined('VERSION') || VERSION < 1.5);
		$v150 = $this->data['v150'] = (defined('VERSION') && strpos(VERSION, '1.5.0') === 0);
		
		$this->loadLanguage();
		
		$form_data = ($v14x || $v150) ? unserialize($this->config->get($this->name . '_data')) : $this->config->get($this->name . '_data');
		
		if (isset($this->request->get['form']) && isset($form_data[$this->request->get['form']])) {
			
			$form = $form_data[$this->request->get['form']];
			
			$fields = $form['fields'];
			foreach ($fields as $key => &$add_to_field) {
				$add_to_field['key'] = $key;
			}
			
			$column = array();
			$sort_order = array();
			foreach ($fields as $key => $value) {
				$column[$key] = $value['column'];
				$sort_order[$key] = $value['sort_order'];
			}
			array_multisort($column, SORT_ASC, $sort_order, SORT_ASC, $fields);
			
			if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate($form)) {
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');
				//$mail->setSubject($form['subject'][$this->session->data['language']]);
				
				$customer_email = array();
				$body = 'Thank you for contacting with us. We have received your mail as seen below and we will respond as soon as possible.'."\n\n".'Marine LED Shop Support'."\n".'__________________________________________________'."\n\n";
				//echo '<pre>'; print_r($fields); echo '</pre>'; exit();
				foreach ($fields as $field) {
				    
                    $key = 'field' . $field['key'];
				    
                    if ($field['name'][$this->session->data['language']] == 'First Name') {
                        $userName = $this->request->post[$key];
                    }
					
					if (($field['type'] == 'email' || $field['type'] == 'email_confirm') && !empty($this->request->post[$key])) {
						$customer_email[] = $this->request->post[$key];
					}
					
					$body .= $field['name'][$this->session->data['language']] . $this->data['text_after_title'] . "\n";
					
					if ($field['type'] == 'file') {
						$file_error = false;
						if (!empty($this->request->files[$key]['name'])) {
							if ((strlen(utf8_decode($this->request->files[$key]['name'])) < 3) || (strlen(utf8_decode($this->request->files[$key]['name'])) > 128)) {
								$file_error = true;
								$body .= $this->data['error_file_name'] . "\n";
							}
							
							if ($this->request->files[$key]['size']/1024 > $form['filesize_limit']) {
								$file_error = true;
								$body .= $this->data['error_file_size'] . "\n";
							}
							
							$allowed = array();
							$filetypes = explode(',', $form['allowed_ext']);
							foreach ($filetypes as $filetype) {
								$allowed[] = trim($filetype);
							}
							
							$ext = strtolower(substr(strrchr($this->request->files[$key]['name'], '.'), 1));
							if (!in_array($ext, $allowed)) {
								$file_error = true;
								$body .= sprintf($this->data['error_file_ext'], $ext) . "\n";
							}
							
							if ($this->request->files[$key]['error'] != UPLOAD_ERR_OK) {
								$file_error = true;
								$body .= $this->data['error_file_upload'] . "\n";
							}
						} else {
							$file_error = true;
							$body .= $this->data['text_no_file'] . "\n";
						}
						
						if (!$file_error && is_uploaded_file($this->request->files[$key]['tmp_name']) && file_exists($this->request->files[$key]['tmp_name'])) {
							rename($this->request->files[$key]['tmp_name'], DIR_CACHE . $this->request->files[$key]['name']);
							$mail->addAttachment(DIR_CACHE . $this->request->files[$key]['name']);
							$body .= $this->request->files[$key]['name'] . "\n";
						}
					} else {
						if (!isset($this->request->post[$key]) || !$this->request->post[$key]) {
							$body .= $this->data['text_no_answer'] . "\n";
						} elseif (is_array($this->request->post[$key])) {
							foreach ($this->request->post[$key] as $value) {
								$body .= strip_tags(html_entity_decode($value, ENT_QUOTES, 'UTF-8')) . "\n";
							}
						} else {
							$body .= strip_tags(html_entity_decode($this->request->post[$key], ENT_QUOTES, 'UTF-8')) . "\n";
						}
					}
					
					$body .= "\n";
				}
                
                if (isset($userName) && $userName != '') {
                    $mail->setSubject('Enquire '.$userName);
                } else {
                    $mail->setSubject($form['subject'][$this->session->data['language']]);
                }
				
				$mail->setText($body);
				$mail->setSender(str_replace(array(',', '&'), array('', 'and'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')));
				
				$admin_email = explode(',', $form['email']);
				$mail->setFrom($admin_email[0]);
				
				if (!empty($customer_email)) {
					foreach ($customer_email as $email) {
						$mail->setTo(trim($email));
						$mail->send();
					}
					$mail->setSender(substr($customer_email[0], 0, strpos($customer_email[0], '@')));
					$mail->setFrom($customer_email[0]);
				}
				
				foreach ($admin_email as $email) {
					$mail->setTo(trim($email));
					$mail->send();
				}
				
				foreach ($fields as $field) {
					$key = 'field' . $field['key'];
					if (!empty($this->request->files[$key]['name']) && file_exists(DIR_CACHE . $this->request->files[$key]['name'])) {
						unlink(DIR_CACHE . $this->request->files[$key]['name']);
					}
				}
				
				$this->redirect($this->makeURL($this->type . '/' . $this->name . '/success', 'form=' . $this->request->get['form']));
			}
			
			$this->data['html_before'] = html_entity_decode($form['html_before'][$this->session->data['language']], ENT_QUOTES, 'UTF-8');
			$this->data['html_after'] = html_entity_decode($form['html_after'][$this->session->data['language']], ENT_QUOTES, 'UTF-8');
			
			$this->data['text_allowed_ext'] = sprintf($this->data['text_allowed_ext'], $form['allowed_ext']);
			
			$this->data['columns'] = array();
			
			foreach ($fields as $field) {
				$key = 'field' . $field['key'];
				
				if (isset($this->request->post[$key])) {
					$this->data[$key] = $this->request->post[$key];
				} else {
					if ($field['type'] == 'checkbox' || $field['type'] == 'radio' || $field['type'] == 'select') {
						$this->data[$key] = array();
					} elseif ($field['type'] == 'password') {
						$this->data[$key] = '';
					} else {
						$this->data[$key] = $field['value'][$this->session->data['language']];
					}
				}
				
				if ($field['type'] == 'email_confirm') {
					$this->data['confirm' . $field['key']] = (isset($this->request->post['confirm' . $field['key']])) ? $this->request->post['confirm' . $field['key']] : '';
				}
				
				$this->data['columns'][(int)$field['column']][$field['key']] = $field;
			}
			
			$this->data['include_captcha'] = $form['captcha'];
			
			$title = $form['title'][$this->session->data['language']];
			
			$this->template = $this->loadTemplate($this->type . '/' . $this->name);
			
		} else {
			
			$this->request->get['form'] = '';
			
			$title = $this->data['text_error'];
			
			$this->template = $this->loadTemplate('error/not_found');
			
			$this->data['continue'] = $this->makeURL('common/home');
			
		}
		
		$breadcrumbs = array();
		$breadcrumbs[] = array(
			'text'		=> $this->data['text_home'],
			'href'		=> $this->makeURL('common/home'),
			'separator'	=> false
		);
		$breadcrumbs[] = array(
			'text'		=> $title,
			'href'		=> $this->makeURL($this->type . '/' . $this->name, 'form=' . $this->request->get['form']),
			'separator'	=> $this->data['text_separator']
		);
		
		$this->data['heading_title'] = $title;
		$this->data['errors'] = $this->error;
		
		if ($v14x) {
			$this->document->title = $title;
			$this->document->breadcrumbs = $breadcrumbs;
			$this->children = array(
				'common/column_right',
				'common/footer',
				'common/column_left',
				'common/header'
			);
			$this->response->setOutput($this->render(true), $this->config->get('config_compression'));
		} else {
			$this->document->setTitle($title);
			$this->data['breadcrumbs'] = $breadcrumbs;
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
			$this->response->setOutput($this->render());
		}
	}
	
	public function success() {
		$v14x = (!defined('VERSION') || VERSION < 1.5);
		$v150 = (strpos(VERSION, '1.5.0') === 0);
		
		$this->loadLanguage();
		
		$breadcrumbs = array();
		$breadcrumbs[] = array(
			'text'		=> $this->data['text_home'],
			'href'		=> $this->makeURL('common/home'),
			'separator'	=> false
		);
		$breadcrumbs[] = array(
			'text'		=> $this->data['heading_title'],
			'href'		=> $this->makeURL($this->type . '/' . $this->name, 'form=' . $this->request->get['form']),
			'separator'	=> $this->data['text_separator']
		);
		
		$form_data = ($v14x || $v150) ? unserialize($this->config->get($this->name . '_data')) : $this->config->get($this->name . '_data');
		
		$this->data['text_message'] = html_entity_decode($form_data[$this->request->get['form']]['html_success'][$this->session->data['language']], ENT_QUOTES, 'UTF-8');
		$this->data['continue'] = $this->makeURL('common/home');
		
		$this->template = $this->loadTemplate('common/success');
		
		if ($v14x) {
			$this->document->title = $this->data['heading_title'];
			$this->document->breadcrumbs = $breadcrumbs;
			$this->children = array(
				'common/column_right',
				'common/footer',
				'common/column_left',
				'common/header'
			);
			$this->response->setOutput($this->render(true), $this->config->get('config_compression'));
		} else {
			$this->document->setTitle($this->data['heading_title']);
			$this->data['breadcrumbs'] = $breadcrumbs;
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
			$this->response->setOutput($this->render());
		}
	}
	
	public function captcha() {
		$this->load->library('captcha');
		$captcha = new Captcha();
		$this->session->data['captcha'] = $captcha->getCode();
		$captcha->showImage();
	}
	
	private function loadLanguage() {
		if (!defined('VERSION') || strpos(VERSION, '1.4.7') === 0) {
			$language = $this->db->query("SELECT * FROM " . DB_PREFIX . "language WHERE code = '" . $this->session->data['language'] . "'");
			$directory = (file_exists(DIR_LANGUAGE . $language->row['directory'] . '/' . $this->type . '/' . $this->name . '.php')) ? $language->row['directory'] : 'english';
			if (!file_exists(DIR_LANGUAGE . $directory . '/' . $this->type . '/' . $this->name . '.php')) {
				echo 'Error: Could not load language ' . $this->type . '/' . $this->name . '!';
				exit();
			}
			$_ = array();
			require(DIR_LANGUAGE . $language->row['directory'] . '/' . $language->row['filename'] . '.php');
			require(DIR_LANGUAGE . $directory . '/' . $this->type . '/' . $this->name . '.php');
			$this->data = array_merge($this->data, $_);
		} else {
			$this->data = array_merge($this->data, $this->load->language($this->type . '/' . $this->name));
		}
	}
	
	private function makeURL($route, $args = '', $connection = 'NONSSL') {
		if (!defined('VERSION') || VERSION < 1.5) {
			$url = ($connection == 'NONSSL') ? HTTP_SERVER : HTTPS_SERVER;
			$url .= 'index.php?route=' . $route;
			$url .= ($args) ? '&' . ltrim($args, '&') : '';
			return $url;
		} else {
			return $this->url->link($route, $args, $connection);
		}
	}
	
	private function loadTemplate($route) {
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $route . '.tpl')) {
			$template = $this->config->get('config_template');
		} else {
			$template = 'default';
		}
		return $template . '/template/' . $route . '.tpl';
	}
	
	private function validate($form = array()) {
		if (isset($form['fields'])) {
			foreach ($form['fields'] as $fieldnum => $field) {
				$key = 'field' . $fieldnum;
				if ($field['required'] && empty($this->request->post[$key]) && empty($this->request->files[$key]['name'])) {
					$this->error['required'] = $this->data['error_required'];
				}
				if (($field['type'] == 'email' || $field['type'] == 'email_confirm') && !empty($this->request->post[$key]) && !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post[$key])) {
					$this->error['email'] = $this->data['error_email'];
				}
				if ($field['type'] == 'email_confirm' && $this->request->post[$key] != $this->request->post['confirm' . $fieldnum]) {
					$this->error['email_confirm'] = $this->data['error_email_confirm'];
				}
				if ($field['type'] == 'password' && !empty($this->request->post[$key]) && $this->request->post[$key] != $field['value'][$this->session->data['language']]) {
					$this->error['password'] = $this->data['error_password'];
				}
			}
		}
		
		if ($form['captcha']) {
			if (!isset($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
				$this->error['captcha'] = $this->data['error_captcha'];
			}
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>