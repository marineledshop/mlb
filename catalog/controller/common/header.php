<?php
global $aFolder;
    if (!defined('HTTP_ADMIN')) define('HTTP_ADMIN','admin');
    $aFolder = preg_replace('/.*\/([^\/].*)\//is','$1',HTTP_ADMIN);
    if (!isset($GLOBALS['magictoolbox']['magiczoom']) && !isset($GLOBALS['magiczoom_module_loaded'])) {
        include (preg_match("/components\/com_ayelshop\/opencart\//ims",__FILE__)?'components/com_ayelshop/opencart/':'').$aFolder.'/controller/module/magictoolbox/module.php';
    };
class ControllerCommonHeader extends Controller {
	protected function index() {
	   
// Enhanced Customer Information - START
	if ($this->customer->isLogged()) {

	if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != '') {
		$route = $_SERVER['REQUEST_URI'];
	} 
	$ip_address = $_SERVER['REMOTE_ADDR'];
	$customer_id = "";
	if(isset($_SESSION['customer_id'])) {
		$customer_id = $_SESSION['customer_id'];
	}
	
	if (isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'] != '') {
		$current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") 
							? "https://" : "http://";
		if( isset($_SERVER["SERVER_PORT"]) && $_SERVER["SERVER_PORT"] != "80") {
		    $current_url .= $_SERVER["HTTP_HOST"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
		    $current_url .= $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
		}
	} else {
		$current_url = "";
	}
	

	if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != '') {
		$referrer = $_SERVER['HTTP_REFERER'];
	} else {
		$referrer = "";
	}
	
	if (isset($_SERVER['HTTP_USER_AGENT'])) {
				$agent_type = $_SERVER['HTTP_USER_AGENT'];
				if (preg_match( "|Opera/ ([0-9].[0-9]{1,2})|",$agent_type,$matched)) {
					$agent_type = "Opera " . $matched[1];
				} elseif (preg_match("|MSIE ([0-9].[0-9]{1,2})|",$agent_type,$matched)) {
					$agent_type = "IE " . $matched[1];
				} elseif(preg_match("|Firefox/([0-9\.]+)|",$agent_type,$matched)) {
					$agent_type = "Firefox " . $matched[1];
				} elseif(preg_match("|Chrome/([0-9\.]+)|",$agent_type,$matched)) {
					$agent_type = "Chrome " . $matched[1];
				} elseif(preg_match("|Safari/([0-9\.]+)|",$agent_type,$matched)) {
					$agent_type = "Safari " . $matched[1];
				} else {
					$agent_type = "Other";
				}
	} else {
		$agent_type = '';
	}	
	
	$query = $this->db->query("INSERT INTO " . DB_PREFIX . "customer_track (route, ip_address, customer_id, current_url, referrer, agent_type, access_time) VALUES ('" . $route . "', '" . htmlentities($ip_address) . "', '" . $customer_id	. "', '" . htmlentities($current_url) . "', '" . htmlentities($referrer) . "', '" . htmlentities($agent_type) . "', '" . date("Y/m/d H:i:s") . "')");
	
	}
// Enhanced Customer Information - END

       
		$this->data['title'] = $this->document->getTitle();
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['base'] = $this->config->get('config_ssl');
		} else {
			$this->data['base'] = $this->config->get('config_url');
		}
		
		$this->data['description'] = $this->document->getDescription();
		$this->data['keywords'] = $this->document->getKeywords();
		$this->data['links'] = $this->document->getLinks();	 
		$this->data['styles'] = $this->document->getStyles();
		$this->data['scripts'] = $this->document->getScripts();
        $this->data['header_text'] = $this->document->getHeaderText();
		$this->data['lang'] = $this->language->get('code');
		$this->data['direction'] = $this->language->get('direction');
		$this->data['google_analytics'] = html_entity_decode($this->config->get('config_google_analytics'), ENT_QUOTES, 'UTF-8');
		
		$this->language->load('common/header');
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$server = HTTPS_IMAGE;
		} else {
			$server = HTTP_IMAGE;
		}	
				
		if ($this->config->get('config_icon') && file_exists(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->data['icon'] = $server . $this->config->get('config_icon');
		} else {
			$this->data['icon'] = '';
		}
		
		$this->data['name'] = $this->config->get('config_name');
				
		if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo'))) {
			$this->data['logo'] = $server . $this->config->get('config_logo');
		} else {
			$this->data['logo'] = '';
		}
		
		// Calculate Totals
		$total_data = array();					
		$total = 0;
		$taxes = $this->cart->getTaxes();
		
		if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {						 
			$this->load->model('setting/extension');
			
			$sort_order = array(); 
			
			$results = $this->model_setting_extension->getExtensions('total');
			
			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}
			
			array_multisort($sort_order, SORT_ASC, $results);
			
			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);
		
					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
				}
			}
		}
		
		$this->data['text_home'] = $this->language->get('text_home');
		$this->data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		$this->data['text_cart'] = $this->language->get('text_cart');
		$this->data['text_items'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
    	$this->data['text_search'] = $this->language->get('text_search');

        if (!$this->customer->isLogged()) {
            if(!isset($this->fbconnect)){
                require_once(DIR_SYSTEM . 'vendor/facebook-sdk/facebook.php');
                $this->fbconnect = new Facebook(array(
                    'appId'  => $this->config->get('fbconnect_apikey'),
                    'secret' => $this->config->get('fbconnect_apisecret'),
                ));
            }

            $fbUrl = $this->fbconnect->getLoginUrl(
                array(
                    'scope' => 'email,user_birthday,user_location,user_hometown',
                    'redirect_uri'  => $this->url->link('account/fbconnect', '', 'SSL')
                )
            );
            $imageSrc = '<img src="' .HTTP_IMAGE . 'data/facebook-login-top.gif" alt="Login with facebook" style="vertical-align: middle" height="19px" width="130px" />';
            $imageSrc = '';

            $fbUrl = '';
            $this->data['text_welcome'] = sprintf($this->language->get('text_welcome'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'), $fbUrl, $imageSrc);
        } else {
            $this->data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));
        }
		$this->data['text_account'] = $this->language->get('text_account');
    	$this->data['text_checkout'] = $this->language->get('text_checkout');
		$this->data['text_language'] = $this->language->get('text_language');
    	$this->data['text_currency'] = $this->language->get('text_currency');
				
		$this->data['home'] = $this->url->link('common/home');
		$this->data['wishlist'] = $this->url->link('account/wishlist');
		$this->data['logged'] = $this->customer->isLogged();
		$this->data['account'] = $this->url->link('account/account', '', 'SSL');
		$this->data['cart'] = $this->url->link('checkout/cart');
		$this->data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
		
        //Wholesale Price List
        if($this->config->get('pricelist_status') && ($this->config->get('pricelist_customer_group') == 0 || $this->customer->getCustomerGroupId() == $this->config->get('pricelist_customer_group'))) {
	    $this->data['text_pricelist'] = $this->language->get('text_pricelist');
	    $this->data['pricelist'] = HTTPS_SERVER . 'index.php?route=product/pricelist';
        }
        //--end WPL
		if (isset($this->request->get['filter_name'])) {
			$this->data['filter_name'] = $this->request->get['filter_name'];
		} else {
			$this->data['filter_name'] = '';
		}
		
		$this->data['action'] = $this->url->link('common/home');

		if (!isset($this->request->get['route'])) {
			$this->data['redirect'] = $this->url->link('common/home');
		} else {
			$data = $this->request->get;
			
			unset($data['_route_']);
			
			$route = $data['route'];
			
			unset($data['route']);
			
			$url = '';
			
			if ($data) {
				$url = '&' . urldecode(http_build_query($data, '', '&'));
			}			
			
			$this->data['redirect'] = $this->url->link($route, $url);
		}

    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->post['language_code'])) {
			$this->session->data['language'] = $this->request->post['language_code'];
		
			if (isset($this->request->post['redirect'])) {
				$this->redirect($this->request->post['redirect']);
			} else {
				$this->redirect($this->url->link('common/home'));
			}
    	}		
						
		$this->data['language_code'] = $this->session->data['language'];
		
		$this->load->model('localisation/language');
		
		$this->data['languages'] = array();
		
		$results = $this->model_localisation_language->getLanguages();
		
		foreach ($results as $result) {
			if ($result['status']) {
				$this->data['languages'][] = array(
					'name'  => $result['name'],
					'code'  => $result['code'],
					'image' => $result['image']
				);	
			}
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->post['currency_code'])) {
      		$this->currency->set($this->request->post['currency_code']);
			
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['shipping_method']);
				
			if (isset($this->request->post['redirect'])) {
				$this->redirect($this->request->post['redirect']);
			} else {
				$this->redirect($this->url->link('common/home'));
			}
   		}
						
		$this->data['currency_code'] = $this->currency->getCode(); 
		
		$this->load->model('localisation/currency');
		 
		 $this->data['currencies'] = array();
		 
		$results = $this->model_localisation_currency->getCurrencies();	
		
		foreach ($results as $result) {
			if ($result['status']) {
   				$this->data['currencies'][] = array(
					'title'        => $result['title'],
					'code'         => $result['code'],
					'symbol_left'  => $result['symbol_left'],
					'symbol_right' => $result['symbol_right']				
				);
			}
		}
		
		// Menu
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		
		$this->data['categories'] = array();
					
		$categories = $this->model_catalog_category->getCategories(0);
		
		foreach ($categories as $category) {
			if ($category['top']) {
				$children_data = array();
				
				$children = $this->model_catalog_category->getCategories($category['category_id']);
				
				foreach ($children as $child) {
					$data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true	
					);		
						
					$product_total = $this->model_catalog_product->getTotalProducts($data);
									
					$children_data[] = array(
						'name'  => $child['name'],
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])	
					);					
				}
				
				// Level 1
				$this->data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}
				
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/header.tpl';
		} else {
			$this->template = 'default/template/common/header.tpl';
		}

        $this->render();
        if($this->config->get('magiczoom_status') != 0) {
            $tool = magiczoom_load_core_class($this);
            if(use_effect_on($tool)) {
                $this->output = set_headers($this->output);
            }
        };
	} 	
}
?>