<?php
class ControllerCommonSeoUrl extends Controller {
	public function index() {
		// Add rewrite to url class
		if ($this->config->get('config_seo_url')) {
			$this->url->addRewrite($this);
		}

		// Decode URL
		if (isset($this->request->get['_route_'])) {
//			$parts = explode('/', $this->request->get['_route_']);
			
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($this->request->get['_route_']) . "'");

            if ($query->num_rows) {
                parse_str($query->row['query'], $url);

                if (isset($url['product_id'])) {
                    $this->request->get['product_id'] = $url['product_id'];
                }

                if (isset($url['category_id'])) {
                    if (!isset($this->request->get['path'])) {
                        $this->request->get['path'] = $url['category_id'];
                    } else {
                        $this->request->get['path'] .= '_' . $url['category_id'];
                    }
                }

                if (isset($url['manufacturer_id'])) {
                    $this->request->get['manufacturer_id'] = $url['manufacturer_id'];
                }

                if (isset($url['information_id'])) {
                    $this->request->get['information_id'] = $url['information_id'];
                }
            } else {
                $this->request->get['route'] = 'error/not_found';
            }

			if (isset($this->request->get['product_id'])) {
				$this->request->get['route'] = 'product/product';
			} elseif (isset($this->request->get['path'])) {
				$this->request->get['route'] = 'product/category';
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$this->request->get['route'] = 'product/manufacturer/product';
			} elseif (isset($this->request->get['information_id'])) {
				$this->request->get['route'] = 'information/information';
			}

			if (isset($this->request->get['route'])) {
				return $this->forward($this->request->get['route']);
			}
		}
	}
	
	public function rewrite($link) {
		if ($this->config->get('config_seo_url')) {
			$url_data = parse_url(str_replace('&amp;', '&', $link));
		
			$url = ''; 
			
			$data = array();
			
			parse_str($url_data['query'], $data);
			
            if (isset($data['route'])) {
                if ($data['route'] == 'product/product' && isset($data['product_id'])) {
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` LIKE 'product_id=" . (int)$data['product_id'] . "&%' LIMIT 1");

                    if ($query->num_rows) {
                        $url = '/' . $query->row['keyword'];
                        //remove category that is added in query
                        if (isset($data['path'])) {
                            $categories = explode('_', $data['path']);
                            parse_str($query->row['query'], $query_string);
                            if (isset($query_string['category_id']) && ($key = array_search($query_string['category_id'], $categories)) !== FALSE) {
                                unset($categories[$key]);
                            }
                            if (empty($categories)) {
                                unset($data['path']);
                            } else {
                                $data['path'] = implode('_', $categories);
                            }
                        }
                    }
                    unset($data['product_id']);
                } elseif ($data['route'] == 'product/category' && isset($data['path'])) {
                    $categories = explode('_', $data['path']);
                    $category = end($categories);
                    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_id=" . (int)$category . "'");

                    if ($query->num_rows) {
                        $url = '/' . $query->row['keyword'];
                        array_pop($categories);
                    }

                    if (!empty($categories)) {
                        $data['path'] = implode('_', $categories);
                    } else {
                        unset($data['path']);
                    }
                } elseif ($data['route'] == 'information/information' && isset($data['information_id'])) {
                    $query = $this->db->query('SELECT keyword FROM url_alias WHERE query = "information_id='.(int)$data['information_id'].'"');
                    if ($query->num_rows) {
                        $url = '/' . $query->row['keyword'];
                        unset($data['information_id']);
                    }
                }
            }

			if ($url) {
				unset($data['route']);
			
				$query = '';
			
				if ($data) {
					foreach ($data as $key => $value) {
						$query .= '&' . $key . '=' . $value;
					}
					
					if ($query) {
						$query = '?' . trim($query, '&');
					}
				}

                $rewritten_url = $url_data['scheme'] . '://' . $url_data['host'] . (isset($url_data['port']) ? ':' . $url_data['port'] : '') . str_replace('/index.php', '', $url_data['path']) . $url . $query;
                return $rewritten_url;
			} else {
				return $link;
			}
		} else {
			return $link;
		}		
	}	
}
?>