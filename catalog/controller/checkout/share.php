<?php
/**
 * User: avasilenko
 * Date: 06.03.12
 * Time: 14:20
 */
class ControllerCheckoutShare extends Controller {

    const FB_COUPON = "fb6969";
    const TEMPLATE_CHECKOUT_SHARE_TPL = '/template/checkout/share.php';

    public function index() {
        $this->language->load('checkout/checkout');
        $json = array();
        $is_error = false;
        if ((!$this->cart->hasProducts() && (!isset($this->session->data['vouchers']) || !$this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
            $json['redirect'] = $this->url->link('checkout/cart');
            $is_error = true;
        }

        $this->data['button_continue'] = $this->language->get('button_continue');
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . self::TEMPLATE_CHECKOUT_SHARE_TPL)) {
            $this->template = $this->config->get('config_template') . self::TEMPLATE_CHECKOUT_SHARE_TPL;
        } else {
            $this->template = 'default' . self::TEMPLATE_CHECKOUT_SHARE_TPL;
        }

        if (!$is_error) {
            $json['output'] = $this->render();
        }

        $this->response->addHeader("Content-type: application/json");
        $this->response->setOutput(json_encode($json));
    }

    public function save() {
        if (!isset($this->request->post['post_id'])) {
            return;
        }
        $this->load->model('checkout/coupon');
        $coupon_info = $this->model_checkout_coupon->getCoupon(self::FB_COUPON);

        if ($coupon_info) {
            $this->session->data['coupon'] = self::FB_COUPON;
            $response = 0;
        } else {
            $response = 1;
        }

        $this->response->addHeader("Content-type: application/json");
        $this->response->setOutput(json_encode($response));
    }
}
