<?php 
class ControllerCheckoutSuccess extends Controller { 
	public function index() {                
            
		if (isset($this->session->data['order_id'])) {
                    
                    // mls update
                    $order_query = $this->db->query("SELECT order_status_id FROM `" . DB_PREFIX . "order` WHERE order_id = " . (int)$this->session->data['order_id']);                    
                    $order_status_id = $order_query->rows[0]['order_status_id'];                    

                    if ($order_status_id) {
			// Begin Referral Checkout Coupon
			if(isset($this->session->data['referralcheckout_token']))
				unset($this->session->data['referralcheckout_token']);
			$this->session->data['referralcheckout_order_id'] = $this->session->data['order_id'];
			// End Referral Checkout Coupon
			$this->cart->clear();
			
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);	
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
                    } 
		} else {
                    $order_status_id = -1;
                }	
									   
		$this->language->load('checkout/success');
                
		if ($order_status_id == 0) {
                    // this normally should not happen: order with status = 0                    
                    $this->document->setTitle($this->language->get('heading_title_error'));
                } else {
                    $this->document->setTitle($this->language->get('heading_title'));
                }
                
                
		
		$this->data['breadcrumbs'] = array(); 

      	$this->data['breadcrumbs'][] = array(
        	'href'      => $this->url->link('common/home'),
        	'text'      => $this->language->get('text_home'),
        	'separator' => false
      	); 
		
      	$this->data['breadcrumbs'][] = array(
        	'href'      => $this->url->link('checkout/cart'),
        	'text'      => $this->language->get('text_basket'),
        	'separator' => $this->language->get('text_separator')
      	);
				
		$this->data['breadcrumbs'][] = array(
			'href'      => $this->url->link('checkout/checkout', '', 'SSL'),
			'text'      => $this->language->get('text_checkout'),
			'separator' => $this->language->get('text_separator')
		);	
					
      	$this->data['breadcrumbs'][] = array(
        	'href'      => $this->url->link('checkout/success'),
        	'text'      => $this->language->get('text_success'),
        	'separator' => $this->language->get('text_separator')
      	);

        // mls update
        if ($order_status_id == 0) {
            // this normally should not happen: order with status = 0
            $this->data['heading_title'] = $this->language->get('heading_title_error');
            $this->data['text_message'] = sprintf($this->language->get('text_error'), $this->url->link('checkout/cart', '', 'SSL'), $this->url->link('account/order', '', 'SSL'));
            
        } else {
            $this->data['heading_title'] = $this->language->get('heading_title');
            
            if ($this->customer->isLogged()) {
    		$this->data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', 'SSL'), $this->url->link('account/order', '', 'SSL'), $this->url->link('account/download', '', 'SSL'), $this->url->link('information/contact'));
            } else {
    		$this->data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
            }
        }
       
    	$this->data['button_continue'] = $this->language->get('button_continue');

        // mls update	
	if ($order_status_id == 0) {
            // this normally should not happen: order with status = 0
            $this->data['continue'] = $this->url->link('checkout/cart', '', 'SSL');
        } else {
            $this->data['continue'] = $this->url->link('common/home');
        }

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/success.tpl';
		} else {
			$this->template = 'default/template/common/success.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'			
		);
				
		$this->response->setOutput($this->render());
  	}
}
?>