<?php

/*
 * Copyright (c) 2012 Web Project Solutions LLC (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 *  @author Antonello Venturino <info@webprojectsol.com>
 *  @copyright  2012 Web Project Solutions LLC
 *  @license    http://www.webprojectsol.com/license.php
 *  @url  http://www.webprojectsol.com/en/modules-of-payment/paypal-express-checkout.html
 */

class ControllerPaymentPaypalExpressCredit extends Controller {

    private $repeatDoExpressCheckoutPayment = 0;
    private $repeatSetExpressCheckout = 0;
    private $_supportedCurrencyCodes = array('AUD', 'CAD', 'CZK', 'DKK', 'EUR', 'HKD', 'HUF', 'ILS', 'JPY', 'MXN', 'NOK', 'NZD', 'PLN', 'GBP', 'SGD', 'SEK', 'CHF', 'USD', 'TWD', 'THB');
    private $skip_confirm = false;

	public function __construct($registry) {
        parent::__construct($registry);
        set_error_handler(array($this, 'exceptionsErrorHandler'));
        register_shutdown_function(array($this, 'registerShutdownFunction'));
    }

    protected function index() {
        if ($this->request->get['route'] == 'checkout/confirm' && !isset($this->session->data['pec']['login'])) {
            $this->data['PECheckout'] = 'true';
        }

        $this->language->load('payment/paypal_express_credit');

        $this->data['text_wait'] = $this->language->get('text_wait');
		$this->data['text_payment_processing'] = $this->language->get('text_payment_processing');

        $this->data['button_confirm'] = $this->language->get('button_confirm');

        if (isset($this->session->data['pec']['skip_confirm'])) {
            $this->data['skip_confirm'] = $this->session->data['pec']['skip_confirm'];
        } else {
            $this->data['skip_confirm'] = false;
        }

        $this->data['actionSetExpressCheckout'] = $this->url->link('payment/paypal_express_credit/SetExpressCheckout', '', (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'SSL' : 'NONSSL'));
        $this->data['actionDoExpressCheckoutPayment'] = $this->url->link('payment/paypal_express_credit/DoExpressCheckoutPayment', '', (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'SSL' : 'NONSSL'));

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/paypal_express.tpl')) {
            $this->template = $this->config->get('config_template') . '/template/payment/paypal_express.tpl';
        } else {
            $this->template = 'default/template/payment/paypal_express.tpl';
        }

        $this->render();
    }
private function sendTransactionToGateway($url, $parameters) {
        $server = parse_url($url);

        if (!isset($server['port'])) {
            $server['port'] = ($server['scheme'] == 'https') ? 443 : 80;
        }

        if (!isset($server['path'])) {
            $server['path'] = '/';
        }

        if (function_exists('curl_init')) {
            $curl = curl_init($server['scheme'] . '://' . $server['host'] . $server['path'] . (isset($server['query']) ? '?' . $server['query'] : ''));
            curl_setopt($curl, CURLOPT_PORT, $server['port']);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);

            $result = curl_exec($curl);

            if (!isset($result) || !$result) {
                $this->log->write('PayPal Express Checkout Request failed: ' . curl_error($curl) . '(' . curl_errno($curl) . ')');
                curl_close($curl);
                return false;
            } else {
                curl_close($curl);
            }
        } else {
            $this->log->write('PayPal Express Checkout Request failed: Your server doesn\'t support the curl functions, it\'s need to work correctly');
            $result = '';
        }

        return $result;
    }

    public function cancel() {
        unset($this->session->data['pec']);
        $this->redirect($this->url->link('checkout/cart', '', 'SSL'));
    }

    public function DoExpressCheckoutPayment($directly = false, $totalSum = false, $noOrderDetail = false) {
        $json = array();
        if (!$this->isEnabled()) {
            $json['error'] = 'Disabled';
            if ($directly) {
                return $json;
            }
            $this->response->setOutput(json_encode($json));
            return;
        }

        try {
            if (!isset($this->session->data['pec']['login']) || !isset($this->session->data['pec']['token'])) {
                unset($this->session->data['pec']);
                $this->SetExpressCheckout();
                return;
            }

            if (!isset($this->session->data['pec']['payerid'])) {
                $this->GetExpressCheckoutDetails();
                return;
            }

            if (!isset($this->session->data['order_id']) || ($this->cart->hasShipping() && !isset($this->session->data['shipping_method']))) {
                $json['success'] = $this->url->link('checkout/pec_checkout', '', 'SSL');
                if ($directly) {
                    return $json;
                }
                $this->response->setOutput(json_encode($json));
                return;
            }

            if (!$this->config->get('paypal_express_credit_test')) {
                $curl = 'https://api-3t.paypal.com/nvp';
                $paypal_url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout';
                $p_user = $this->config->get('paypal_express_credit_username');
                $p_pwd = $this->config->get('paypal_express_credit_password');
                $p_signature = $this->config->get('paypal_express_credit_signature');
            } else {
                $curl = 'https://api-3t.sandbox.paypal.com/nvp';
                $paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout';
                $p_user = $this->config->get('paypal_express_credit_username_test');
                $p_pwd = $this->config->get('paypal_express_credit_password_test');
                $p_signature = $this->config->get('paypal_express_credit_signature_test');
            }

            $order_total = $this->getOrderTotals();

            $this->getSupportedCurrencyCode();

            if (!$this->config->get('paypal_express_credit_method')) {
                $payment_type = 'Authorization';
            } else {
                $payment_type = 'Sale';
            }

            $request = 'USER=' . $p_user;
            $request .= '&PWD=' . $p_pwd;
            $request .= '&VERSION=85.0';
            $request .= '&SIGNATURE=' . $p_signature;
            $request .= '&METHOD=DoExpressCheckoutPayment';
            $request .= '&TOKEN=' . $this->session->data['pec']['token'];
            $request .= '&PAYMENTREQUEST_0_PAYMENTACTION=' . $payment_type;
            $request .= '&PAYERID=' . $this->session->data['pec']['payerid'];
            $request .= '&PAYMENTREQUEST_0_CURRENCYCODE=' . urlencode($this->session->data['pec']['currency']);
            $request .= '&PAYMENTREQUEST_0_NOTETEXT=' . urlencode($this->config->get('config_name') . ' Order ID: ' . $this->session->data['order_id']);

            $use_address = ($this->config->get('paypal_express_credit_send_address') ? $this->config->get('paypal_express_credit_send_address') : 'shipping');
            if ($this->customer->isLogged() && isset($this->session->data[$use_address . '_address_id'])) {
                $this->load->model('account/address');
                $address = $this->model_account_address->getAddress($this->session->data[$use_address . '_address_id']);

                $request .= '&PAYMENTREQUEST_0_SHIPTONAME=' . urlencode($address['firstname'] . ' ' . $address['lastname']);
                $request .= '&PAYMENTREQUEST_0_SHIPTOPHONENUM=' . urlencode($this->customer->getTelephone());
                $request .= '&PAYMENTREQUEST_0_SHIPTOSTREET=' . urlencode($address['address_1']);
                $request .= '&PAYMENTREQUEST_0_SHIPTOSTREET2=' . urlencode($address['address_2']);
                $request .= '&PAYMENTREQUEST_0_SHIPTOCITY=' . urlencode($address['city']);
                $request .= '&PAYMENTREQUEST_0_SHIPTOSTATE=' . urlencode(($address['iso_code_2'] != 'US') ? $address['zone'] : $address['zone_code']);
                $request .= '&PAYMENTREQUEST_0_SHIPTOZIP=' . urlencode($address['postcode']);
                $request .= '&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=' . urlencode($address['iso_code_2']);
            } elseif (isset($this->session->data['guest'])) {
                if (empty($this->session->data['guest'][$use_address]['firstname'])) {
                    unset($this->session->data['guest']);
                } else {
                    $request .= '&PAYMENTREQUEST_0_SHIPTONAME=' . urlencode($this->session->data['guest'][$use_address]['firstname'] . ' ' . $this->session->data['guest'][$use_address]['lastname']);
                    $request .= '&PAYMENTREQUEST_0_SHIPTOPHONENUM=' . (isset($this->session->data['guest']['telephone']) ? urlencode($this->session->data['guest']['telephone']) : '');
                    $request .= '&PAYMENTREQUEST_0_SHIPTOSTREET=' . urlencode($this->session->data['guest'][$use_address]['address_1']);
                    $request .= '&PAYMENTREQUEST_0_SHIPTOSTREET2=' . urlencode($this->session->data['guest'][$use_address]['address_2']);
                    $request .= '&PAYMENTREQUEST_0_SHIPTOCITY=' . urlencode($this->session->data['guest'][$use_address]['city']);
                    $request .= '&PAYMENTREQUEST_0_SHIPTOSTATE=' . urlencode(($this->session->data['guest'][$use_address]['iso_code_2'] != 'US') ? $this->session->data['guest'][$use_address]['zone'] : $this->session->data['guest'][$use_address]['zone_code']);
                    $request .= '&PAYMENTREQUEST_0_SHIPTOZIP=' . urlencode($this->session->data['guest'][$use_address]['postcode']);
                    $request .= '&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=' . urlencode($this->session->data['guest'][$use_address]['iso_code_2']);
                }
            }

            if (!$noOrderDetail) {
                if ($totalSum) {
                    $total = 0;
                }
                if ($this->config->get('paypal_express_credit_senditem')) {
                    $n = 0;
                    foreach ($this->cart->getProducts() as $product) {
                        $options = '';
                        $optionsName = '';
                        if (isset($product['option']) && $product['option']) {
                            foreach ($product['option'] as $option) {
                                $options .= $option['name'] . ': ' . $option['option_value'] . ', ';
                            }
                            $optionsName = substr($options, 0, -2);
                        }
                        $request .= '&L_PAYMENTREQUEST_0_NAME' . $n . '=' . urlencode($product['name']);
                        $request .= '&L_PAYMENTREQUEST_0_NUMBER' . $n . '=' . urlencode($product['model']);
                        if ($optionsName) {
                            $request .= '&L_PAYMENTREQUEST_0_DESC' . $n . '=' . urlencode($optionsName);
                        }
                        $request .= '&L_PAYMENTREQUEST_0_AMT' . $n . '=' . urlencode($this->PriceFormat($this->currency->format($product['price'], $this->session->data['pec']['currency'], false, false)));
                        $request .= '&L_PAYMENTREQUEST_0_QTY' . $n . '=' . urlencode($product['quantity']);
                        ++$n;
                        if ($totalSum) {
                            $total += $this->PriceFormat($this->currency->format($product['price'], $this->session->data['pec']['currency'], false, false));
                        }
                    }
                }

                $request .= '&PAYMENTREQUEST_0_ITEMAMT=' . urlencode($this->PriceFormat($this->currency->format($order_total['subtotal'], $this->session->data['pec']['currency'], false, false)));
                $request .= '&PAYMENTREQUEST_0_TAXAMT=' . urlencode($this->PriceFormat($this->currency->format($order_total['taxcost'], $this->session->data['pec']['currency'], false, false)));
                $request .= '&PAYMENTREQUEST_0_SHIPDISCAMT=' . urlencode($this->PriceFormat($this->currency->format($order_total['discount'], $this->session->data['pec']['currency'], false, false)));
                $request .= '&PAYMENTREQUEST_0_HANDLINGAMT=' . urlencode($this->PriceFormat($this->currency->format($order_total['handling'], $this->session->data['pec']['currency'], false, false)));
                $request .= '&PAYMENTREQUEST_0_SHIPPINGAMT=' . urlencode($this->PriceFormat($this->currency->format($order_total['shippingcost'], $this->session->data['pec']['currency'], false, false)));
                if ($totalSum) {
                    $total += $this->PriceFormat($this->currency->format($order_total['taxcost'], $this->session->data['pec']['currency'], false, false));
                    $total += $this->PriceFormat($this->currency->format($order_total['discount'], $this->session->data['pec']['currency'], false, false));
                    $total += $this->PriceFormat($this->currency->format($order_total['handling'], $this->session->data['pec']['currency'], false, false));
                    $total += $this->PriceFormat($this->currency->format($order_total['shippingcost'], $this->session->data['pec']['currency'], false, false));
                    $request .= '&PAYMENTREQUEST_0_AMT=' . urlencode($this->PriceFormat($total));
                } else {
                    $request .= '&PAYMENTREQUEST_0_AMT=' . urlencode($this->PriceFormat($this->currency->format($order_total['total'], $this->session->data['pec']['currency'], false, false)));
                }
            } else {
                $request .= '&PAYMENTREQUEST_0_AMT=' . urlencode($this->PriceFormat($this->currency->format($order_total['total'], $this->session->data['pec']['currency'], false, false)));
            }

            $response = $this->sendTransactionToGateway($curl, $request);

            $response_data = array();
            parse_str($response, $response_data);

            if (($response_data['ACK'] == 'Success') || ($response_data['ACK'] == 'SuccessWithWarning')) {
                $this->load->model('checkout/order');
                $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
                if (!$order_info['order_status_id']) {
                    $this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('paypal_express_credit_order_status_id'));
                }
                $message = '';

                if (isset($response_data['PAYMENTINFO_0_TRANSACTIONID'])) {
                    $message .= 'TRANSACTIONID: ' . $response_data['PAYMENTINFO_0_TRANSACTIONID'] . "\n";
                }
                if (isset($response_data['PAYMENTINFO_0_PAYMENTSTATUS'])) {
                    $message .= 'PAYPAL STATUS: ' . $response_data['PAYMENTINFO_0_PAYMENTSTATUS'] . "\n";
                }

                $message .= "\n";
                foreach ($response_data as $field => $value) {
                    $message .= $field . ': ' . $value . "\n";
                }

                if ($message) {
                    if ($response_data['PAYMENTINFO_0_PAYMENTSTATUS'] == 'Completed') {
                        $this->model_checkout_order->update($this->session->data['order_id'], $this->config->get('paypal_express_credit_order_status_id_complete'), '', false);
                        $this->model_checkout_order->update($this->session->data['order_id'], $this->config->get('paypal_express_credit_order_status_id_complete'), $message, false);
                    } else {
                        $this->model_checkout_order->update($this->session->data['order_id'], $this->config->get('paypal_express_credit_order_status_id'), $message, false);
                    }
                }

                unset($this->session->data['pec']);
                $json['success'] = $this->url->link('checkout/success', '', 'SSL');
            } else {
                $json['error'] = '';
                for ($i = 0; $i < 10; ++$i) {
                    if (isset($response_data['L_ERRORCODE' . $i])) {
                        if ($response_data['L_ERRORCODE' . $i] == '10413') {
                            if ($this->repeatDoExpressCheckoutPayment <= 2) {
                                ++$this->repeatDoExpressCheckoutPayment;
                                if ($this->repeatDoExpressCheckoutPayment == 1) {
                                    $this->DoExpressCheckoutPayment($directly, true);
                                } elseif ($this->repeatDoExpressCheckoutPayment == 2) {
                                    $this->DoExpressCheckoutPayment($directly, false, true);
                                }
                                return;
                            }
                        }
                        if (isset($response_data['L_SEVERITYCODE' . $i]) && isset($response_data['L_ERRORCODE' . $i]) && isset($response_data['L_LONGMESSAGE' . $i])) {
                            $json['error'] .= $response_data['L_SEVERITYCODE' . $i] . ': ' . $response_data['L_ERRORCODE' . $i] . ' - ' . $response_data['L_LONGMESSAGE' . $i] . '<br />';
                        }
                    } else {
                        $i = 10;
                    }
                }
                $json['success'] = $this->url->link('checkout/cart', '', 'SSL');
            }
            if ($directly) {
                return $json;
            }
            $this->response->setOutput(json_encode($json));
        } catch (Exception $e) {
            $json['error'] = $e->getMessage();
        }
        if ($directly) {
            return $json;
        }
        $this->response->setOutput(json_encode($json));
    }

    public function GetExpressCheckoutDetails() {
        try {
            if (isset($this->session->data['pec']['token'])) {
                if ($this->session->data['pec']['token'] == $this->request->get['token']) {
                    $this->session->data['pec']['login'] = true;
                }
            } else {
                $this->redirect($this->url->link('payment/paypal_express/cancel', '', 'SSL'));
            }

            if (!$this->config->get('paypal_express_credit_test')) {
                $curl = 'https://api-3t.paypal.com/nvp';
                $paypal_url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout';
                $p_user = $this->config->get('paypal_express_credit_username');
                $p_pwd = $this->config->get('paypal_express_credit_password');
                $p_signature = $this->config->get('paypal_express_credit_signature');
            } else {
                $curl = 'https://api-3t.sandbox.paypal.com/nvp';
                $paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout';
                $p_user = $this->config->get('paypal_express_credit_username_test');
                $p_pwd = $this->config->get('paypal_express_credit_password_test');
                $p_signature = $this->config->get('paypal_express_credit_signature_test');
            }

            $request = 'METHOD=GetExpressCheckoutDetails';
            $request .= '&USER=' . $p_user;
            $request .= '&PWD=' . $p_pwd;
            $request .= '&SIGNATURE=' . $p_signature;
            $request .= '&VERSION=85.0';
            $request .= '&TOKEN=' . $this->session->data['pec']['token'];

            $response = $this->sendTransactionToGateway($curl, $request);

            $response_data = array();
            parse_str($response, $response_data);

            $this->session->data['pec']['payerid'] = isset($response_data['PAYERID']) ? $response_data['PAYERID'] : $this->request->get['PayerID'];
            $this->CreateOrder($response_data);
        } catch (Exception $e) {
            $this->session->data['pec']['error'] = $e->getMessage();
            $this->redirect($this->url->link('checkout/pec_checkout', '', 'SSL'));
        }
    }

    public function SetExpressCheckout($totalSum = false, $noOrderDetail = false) {
        $json = array();
        if (!$this->isEnabled()) {
            $json['error'] = 'Disabled';
            $this->response->setOutput(json_encode($json));
            return;
        }

        try {
            #Validate cart has products and has stock.
            if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
                $json['success'] = $this->url->link('checkout/cart', '', 'SSL');
                $this->response->setOutput(json_encode($json));
                return;
            }

            $order_total = $this->getOrderTotals();

            #Check if the payment is authorized go to confirm step
            if (isset($this->session->data['pec']['payerid'])) {
                $this->session->data['payment_method'] = array(
                  'code' => 'paypal_express',
                  'title' => html_entity_decode($this->config->get('paypal_express_credit_title_' . $this->config->get('config_language_id'))),
                  'sort_order' => $this->config->get('paypal_express_credit_sort_order')
                );
                $json['success'] = $this->url->link('checkout/pec_checkout', '', 'SSL');
                $this->response->setOutput(json_encode($json));
                return;
            } elseif (isset($this->session->data['pec']['token']) && isset($this->session->data['pec']['login']) && $this->session->data['pec']['login'] == true) {
                $json['success'] = $this->url->link('payment/paypal_express/GetExpressCheckoutDetails', '', 'SSL');
                $this->response->setOutput(json_encode($json));
                return;
            }

            if (isset($this->session->data['order_id']) && $this->config->get('paypal_express_credit_confirm_order') && $this->config->get('paypal_express_credit_skip_confirm')) {
                $this->load->model('checkout/order');
                $this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('paypal_express_credit_order_status_id'));
            }

            if (isset($this->session->data['pec'])) {
                unset($this->session->data['pec']);
            }

            if (!$this->config->get('paypal_express_credit_method')) {
                $payment_type = 'Authorization';
            } else {
                $payment_type = 'Sale';
            }

            if (!class_exists('uagent_info')) {
                require_once(DIR_SYSTEM . 'helper/device_detect.php');
                $DeviceDetect = new uagent_info();
            } else {
                global $DeviceDetect;
                if (!is_object($DeviceDetect)) {
                    $DeviceDetect = new uagent_info();
                }
            }

            if (!$this->config->get('paypal_express_credit_test')) {
                $curl = 'https://api-3t.paypal.com/nvp';
                $paypal_url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout';
                $p_user = $this->config->get('paypal_express_credit_username');
                $p_pwd = $this->config->get('paypal_express_credit_password');
                $p_signature = $this->config->get('paypal_express_credit_signature');
            } else {
                $curl = 'https://api-3t.sandbox.paypal.com/nvp';
                $paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout';
                $p_user = $this->config->get('paypal_express_credit_username_test');
                $p_pwd = $this->config->get('paypal_express_credit_password_test');
                $p_signature = $this->config->get('paypal_express_credit_signature_test');
            }

            $mobile_checkout = false;
            if ($DeviceDetect->isIphone || $DeviceDetect->isAndroidPhone || $DeviceDetect->isTierIphone || $DeviceDetect->isTierTablet) {
                $paypal_url .= '-mobile';
                $mobile_checkout = true;
            }

            if ($this->config->get('paypal_express_credit_skip_confirm') && isset($this->session->data['order_id'])) {
                $paypal_url .= '&useraction=commit';
            }

            $request = 'USER=' . $p_user;
            $request .= '&PWD=' . $p_pwd;
            $request .= '&VERSION=85.0';
            $request .= '&SIGNATURE=' . $p_signature;
            $request .= '&METHOD=SetExpressCheckout';
            $request .= '&PAYMENTREQUEST_0_PAYMENTACTION=' . $payment_type;
            $request .= '&RETURNURL=' . urlencode($this->url->link('payment/paypal_express/GetExpressCheckoutDetails', '', 'SSL'));
            $request .= '&CANCELURL=' . urlencode($this->url->link('payment/paypal_express/cancel', '', 'SSL'));
            $request .= '&SOLUTIONTYPE=Sole';
            $request .= '&LANDINGPAGE=' . ($this->config->get('paypal_express_credit_landing') == 'Billing' ? 'Billing' : 'Login');
            if (isset($this->session->data['order_id'])) {
                $request .= '&TOTALTYPE=Total';
            } else {
                $request .= '&TOTALTYPE=EstimatedTotal';
            }
            if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
                $server = $this->config->get('config_ssl');
            } else {
                $server = $this->config->get('config_url');
            }
            if ($this->config->get('paypal_express_credit_logo')) {
                $request .= '&HDRIMG=' . urlencode($server . 'image/' . $this->config->get('paypal_express_credit_logo'));
            }

            if ($this->customer->isLogged()) {
                $request .= '&EMAIL=' . $this->customer->getEmail();
            } elseif (isset($this->session->data['email'])) {
                $request .= '&EMAIL=' . $this->session->data['email'];
            }

            $use_address = ($this->config->get('paypal_express_credit_send_address') ? $this->config->get('paypal_express_credit_send_address') : 'shipping');
            $send_address = false;
            $customer_country = '';
            if ($this->customer->isLogged() && isset($this->session->data[$use_address . '_address_id'])) {
                $send_address = true;
                $this->load->model('account/address');
                $address = $this->model_account_address->getAddress($this->session->data[$use_address . '_address_id']);
                $customer_country = $address['iso_code_2'];
                $request .= '&PAYMENTREQUEST_0_SHIPTONAME=' . urlencode($address['firstname'] . ' ' . $address['lastname']);
                $request .= '&PAYMENTREQUEST_0_SHIPTOPHONENUM=' . urlencode($this->customer->getTelephone());
                $request .= '&PAYMENTREQUEST_0_SHIPTOSTREET=' . urlencode($address['address_1']);
                $request .= '&PAYMENTREQUEST_0_SHIPTOSTREET2=' . urlencode($address['address_2']);
                $request .= '&PAYMENTREQUEST_0_SHIPTOCITY=' . urlencode($address['city']);
                $request .= '&PAYMENTREQUEST_0_SHIPTOSTATE=' . urlencode(($address['iso_code_2'] != 'US') ? $address['zone'] : $address['zone_code']);
                $request .= '&PAYMENTREQUEST_0_SHIPTOZIP=' . urlencode($address['postcode']);
                $request .= '&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=' . urlencode($address['iso_code_2']);
            } elseif (isset($this->session->data['guest'])) {
                if (empty($this->session->data['guest'][$use_address]['firstname'])) {
                    unset($this->session->data['guest']);
                } else {
                    $send_address = true;
                    $customer_country = $this->session->data['guest'][$use_address]['iso_code_2'];
                    $request .= '&PAYMENTREQUEST_0_SHIPTONAME=' . urlencode($this->session->data['guest'][$use_address]['firstname'] . ' ' . $this->session->data['guest'][$use_address]['lastname']);
                    $request .= '&PAYMENTREQUEST_0_SHIPTOPHONENUM=' . (isset($this->session->data['guest']['telephone']) ? urlencode($this->session->data['guest']['telephone']) : '');
                    $request .= '&PAYMENTREQUEST_0_SHIPTOSTREET=' . urlencode($this->session->data['guest'][$use_address]['address_1']);
                    $request .= '&PAYMENTREQUEST_0_SHIPTOSTREET2=' . urlencode($this->session->data['guest'][$use_address]['address_2']);
                    $request .= '&PAYMENTREQUEST_0_SHIPTOCITY=' . urlencode($this->session->data['guest'][$use_address]['city']);
                    $request .= '&PAYMENTREQUEST_0_SHIPTOSTATE=' . urlencode(($this->session->data['guest'][$use_address]['iso_code_2'] != 'US') ? $this->session->data['guest'][$use_address]['zone'] : $this->session->data['guest'][$use_address]['zone_code']);
                    $request .= '&PAYMENTREQUEST_0_SHIPTOZIP=' . urlencode($this->session->data['guest'][$use_address]['postcode']);
                    $request .= '&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=' . urlencode($this->session->data['guest'][$use_address]['iso_code_2']);
                }
            }

            $this->session->data['pec']['noshipping'] = false;
            if ($this->cart->hasShipping() && $send_address) {
                $request .= '&NOSHIPPING=0';
            } elseif ($this->cart->hasShipping() && !$send_address) {
                $request .= '&NOSHIPPING=2';
            } else {
                $request .= '&NOSHIPPING=1';
                $this->session->data['pec']['noshipping'] = true;
            }

            if ($this->config->get('paypal_express_credit_merchant_country')) {
                $localecode = $this->config->get('paypal_express_credit_merchant_country');
            } else {
                $localecode = $customer_country;
            }
            if ($localecode) {
                if ($mobile_checkout) {
                    $mobile_localecode_denied = array('AD', 'AL', 'AM', 'BR', 'GE', 'VA', 'IN', 'MC', 'MD', 'UA');
                    if (in_array($localecode, $mobile_localecode_denied)) {
                        $localecode = 'US';
                        $request .= '&LOCALECODE=US';
                    } else {
                        $request .= '&LOCALECODE=' . $localecode;
                    }
                } else {
                    $request .= '&LOCALECODE=' . $localecode;
                }
            }

            $this->session->data['pec']['localecode'] = $localecode;
            $this->getSupportedCurrencyCode();

            if (!$noOrderDetail) {
                if ($totalSum) {
                    $total = 0;
                }
                if ($this->config->get('paypal_express_credit_senditem')) {
                    $n = 0;
                    foreach ($this->cart->getProducts() as $product) {
                        $options = '';
                        $optionsName = '';
                        if (isset($product['option']) && $product['option']) {
                            foreach ($product['option'] as $option) {
                                $options .= $option['name'] . ': ' . $option['option_value'] . ', ';
                            }
                            $optionsName = substr($options, 0, -2);
                        }
                        $request .= '&L_PAYMENTREQUEST_0_NAME' . $n . '=' . urlencode($product['name']);
                        $request .= '&L_PAYMENTREQUEST_0_NUMBER' . $n . '=' . urlencode($product['model']);
                        if ($optionsName) {
                            $request .= '&L_PAYMENTREQUEST_0_DESC' . $n . '=' . urlencode($optionsName);
                        }
                        $request .= '&L_PAYMENTREQUEST_0_AMT' . $n . '=' . urlencode($this->PriceFormat($this->currency->format($product['price'], $this->session->data['pec']['currency'], false, false)));
                        $request .= '&L_PAYMENTREQUEST_0_QTY' . $n . '=' . urlencode($product['quantity']);
                        ++$n;
                        if ($totalSum) {
                            $total += $this->PriceFormat($this->currency->format($product['price'], $this->session->data['pec']['currency'], false, false));
                        }
                    }
                }

                $request .= '&PAYMENTREQUEST_0_ITEMAMT=' . urlencode($this->PriceFormat($this->currency->format($order_total['subtotal'], $this->session->data['pec']['currency'], false, false)));
                $request .= '&PAYMENTREQUEST_0_TAXAMT=' . urlencode($this->PriceFormat($this->currency->format($order_total['taxcost'], $this->session->data['pec']['currency'], false, false)));
                $request .= '&PAYMENTREQUEST_0_SHIPDISCAMT=' . urlencode($this->PriceFormat($this->currency->format($order_total['discount'], $this->session->data['pec']['currency'], false, false)));
                $request .= '&PAYMENTREQUEST_0_HANDLINGAMT=' . urlencode($this->PriceFormat($this->currency->format($order_total['handling'], $this->session->data['pec']['currency'], false, false)));
                $request .= '&PAYMENTREQUEST_0_SHIPPINGAMT=' . urlencode($this->PriceFormat($this->currency->format($order_total['shippingcost'], $this->session->data['pec']['currency'], false, false)));
                if ($totalSum) {
                    $total += $this->PriceFormat($this->currency->format($order_total['taxcost'], $this->session->data['pec']['currency'], false, false));
                    $total += $this->PriceFormat($this->currency->format($order_total['discount'], $this->session->data['pec']['currency'], false, false));
                    $total += $this->PriceFormat($this->currency->format($order_total['handling'], $this->session->data['pec']['currency'], false, false));
                    $total += $this->PriceFormat($this->currency->format($order_total['shippingcost'], $this->session->data['pec']['currency'], false, false));
                    $request .= '&PAYMENTREQUEST_0_AMT=' . urlencode($this->PriceFormat($total));
                } else {
                    $request .= '&PAYMENTREQUEST_0_AMT=' . urlencode($this->PriceFormat($this->currency->format($order_total['total'], $this->session->data['pec']['currency'], false, false)));
                }
            } else {
                $request .= '&PAYMENTREQUEST_0_AMT=' . urlencode($this->PriceFormat($this->currency->format($order_total['total'], $this->session->data['pec']['currency'], false, false)));
            }
            $request .= '&PAYMENTREQUEST_0_CURRENCYCODE=' . urlencode($this->session->data['pec']['currency']);
            $response = $this->sendTransactionToGateway($curl, $request);

            $response_data = array();
            parse_str($response, $response_data);

            if (($response_data['ACK'] == 'Success') || ($response_data['ACK'] == 'SuccessWithWarning')) {
                $json['success'] = $paypal_url . '&token=' . $response_data['TOKEN'];
                $this->session->data['pec']['token'] = $response_data['TOKEN'];
            } else {
                $json['error'] = '';
                for ($i = 0; $i < 10; ++$i) {
                    if (isset($response_data['L_ERRORCODE' . $i])) {
                        if ($response_data['L_ERRORCODE' . $i] == '10413') {
                            if ($this->repeatSetExpressCheckout <= 2) {
                                ++$this->repeatSetExpressCheckout;
                                if ($this->repeatSetExpressCheckout == 1) {
                                    $this->SetExpressCheckout(true);
                                } elseif ($this->repeatSetExpressCheckout == 2) {
                                    $this->SetExpressCheckout(false, true);
                                }
                                return;
                            }
                        }
                        if (isset($response_data['L_SEVERITYCODE' . $i]) && isset($response_data['L_ERRORCODE' . $i]) && isset($response_data['L_LONGMESSAGE' . $i])) {
                            $json['error'] .= $response_data['L_SEVERITYCODE' . $i] . ': ' . $response_data['L_ERRORCODE' . $i] . ' - ' . $response_data['L_LONGMESSAGE' . $i] . "\n";
                        }
                    } else {
                        $i = 10;
                    }
                }
            }
            $this->response->setOutput(json_encode($json));
        } catch (Exception $e) {
            $json['error'] = $e->getMessage();
        }
        $this->response->setOutput(json_encode($json));
    }

    private function getSupportedCurrencyCode() {
        if (in_array($this->session->data['currency'], $this->_supportedCurrencyCodes)) {
            $this->session->data['pec']['currency'] = $this->session->data['currency'];
        } elseif ($this->session->data['pec']['localecode'] == 'BR' && $this->session->data['currency'] == 'BRL') {
            $this->session->data['pec']['currency'] = 'BRL';
        } elseif ($this->session->data['pec']['localecode'] == 'MY' && $this->session->data['currency'] == 'MYR') {
            $this->session->data['pec']['currency'] = 'MYR';
        } elseif ($this->session->data['pec']['localecode'] == 'TR' && $this->session->data['currency'] == 'TRY') {
            $this->session->data['pec']['currency'] = 'TRY';
        } else {
            $this->session->data['pec']['currency'] = $this->config->get('paypal_express_credit_default_currency') ? $this->config->get('paypal_express_credit_default_currency') : 'USD';
        }
    }

    private function PriceFormat($number) {
        return number_format($number, 2, '.', ',');
    }

    private function CreateOrder($data) {
        $this->language->load('payment/paypal_express');

        $this->session->data['payment_method'] = array(
          'code' => 'paypal_express',
          'title' => html_entity_decode($this->config->get('paypal_express_credit_title_' . $this->config->get('config_language_id'))),
          'sort_order' => $this->config->get('paypal_express_credit_sort_order')
        );

        if ($this->config->get('paypal_express_credit_skip_confirm') && isset($this->session->data['order_id']) && (!$this->cart->hasShipping() || ($this->cart->hasShipping() && isset($this->session->data['shipping_method'])))) {
            $this->skip_confirm = true;
        }

        $this->tryLogin($data['EMAIL']);

        if ($this->session->data['pec']['noshipping']) {
            unset($this->session->data['pec']['address_id']);
            $this->session->data['pec']['address_already_exists'] = false;
        } else {
            $country_query = $this->db->query("SELECT country_id FROM " . DB_PREFIX . "country WHERE iso_code_2 = '" . $this->db->escape($data['PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE']) . "'");
            if ($country_query->num_rows) {
                $data['country_id'] = $country_query->row['country_id'];
            } else {
                $data['country_id'] = 0;
            }

            if (!isset($data['PAYMENTREQUEST_0_SHIPTOSTATE'])) {
                $data['PAYMENTREQUEST_0_SHIPTOSTATE'] = '0';
            }

            if ($data['PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE'] == 'US') {
                $zone_query = $this->db->query("SELECT zone_id FROM " . DB_PREFIX . "zone WHERE code = '" . $this->db->escape($data['PAYMENTREQUEST_0_SHIPTOSTATE']) . "' AND country_id = '" . $data['country_id'] . "'");
            } else {
                $zone_query = $this->db->query("SELECT zone_id FROM " . DB_PREFIX . "zone WHERE name = '" . $this->db->escape($data['PAYMENTREQUEST_0_SHIPTOSTATE'] ? ucwords(strtolower($data['PAYMENTREQUEST_0_SHIPTOSTATE'])) : $data['PAYMENTREQUEST_0_SHIPTOCITY']) . "' AND country_id = '" . $data['country_id'] . "'");
                if (!$zone_query->num_rows) {
                    $zone_query = $this->db->query("SELECT zone_id FROM " . DB_PREFIX . "zone WHERE name LIKE '%" . $this->db->escape($data['PAYMENTREQUEST_0_SHIPTOSTATE'] ? ucwords(strtolower($data['PAYMENTREQUEST_0_SHIPTOSTATE'])) : $data['PAYMENTREQUEST_0_SHIPTOCITY']) . "%' AND country_id = '" . $data['country_id'] . "'");
                }
            }

            if ($zone_query->num_rows) {
                $data['zone_id'] = $zone_query->row['zone_id'];
            } else {
                $data['zone_id'] = 0;
            }

            if (($data['FIRSTNAME'] . ' ' . $data['LASTNAME']) == $data['PAYMENTREQUEST_0_SHIPTONAME']) {
                $company = '';
                $firstname = $data['FIRSTNAME'];
                $lastname = $data['LASTNAME'];
            } else {
                $names = explode(' ', $data['PAYMENTREQUEST_0_SHIPTONAME']);
                if (sizeof($names) == 2) {
                    $firstname = $names['0'];
                    $lastname = $names['1'];
                    $company = '';
                } else {
                    $firstname = $data['FIRSTNAME'];
                    $lastname = $data['LASTNAME'];
                    $company = $data['PAYMENTREQUEST_0_SHIPTONAME'];
                }
            }

            if ($this->customer->isLogged()) {
                #Check PayPal address if it already exists
                $address_query = $this->db->query("SELECT address_id from " . DB_PREFIX . "address WHERE customer_id = '" . $this->customer->getId() . "' AND firstname = '" . $this->db->escape($firstname) . "' AND lastname = '" . $this->db->escape($lastname) . "' AND city = '" . $this->db->escape($data['PAYMENTREQUEST_0_SHIPTOCITY']) . "' AND address_1 = '" . $this->db->escape($data['PAYMENTREQUEST_0_SHIPTOSTREET']) . "' AND postcode = '" . $this->db->escape(isset($data['PAYMENTREQUEST_0_SHIPTOZIP']) ? $data['PAYMENTREQUEST_0_SHIPTOZIP'] : '') . "' AND country_id = '" . $this->db->escape($data['country_id']) . "' AND zone_id = '" . $this->db->escape($data['zone_id']) . "'");
                if ($address_query->num_rows > 0) {
                    $this->session->data['pec']['address_id'] = $address_query->row['address_id'];
                    $this->session->data['pec']['address_already_exists'] = true;
                } else {
                    unset($this->session->data['pec']['address_id']);
                    $this->session->data['pec']['address_already_exists'] = false;
                }

                #If it doesn't exist, create address
                if (!isset($this->session->data['pec']['address_id'])) {
                    $PaypalAddress = array();
                    $PaypalAddress['firstname'] = $firstname;
                    $PaypalAddress['lastname'] = $lastname;
                    $PaypalAddress['email'] = $data['EMAIL'];
                    if (isset($data['PAYMENTREQUEST_0_SHIPTOPHONENUM'])) {
                        $PaypalAddress['telephone'] = $data['PAYMENTREQUEST_0_SHIPTOPHONENUM'];
                    } elseif (isset($data['PHONENUM'])) {
                        $PaypalAddress['telephone'] = $data['PHONENUM'];
                    } else {
                        $PaypalAddress['telephone'] = '';
                    }
                    $PaypalAddress['fax'] = '';
                    $PaypalAddress['company'] = $company;
                    $PaypalAddress['address_1'] = $data['PAYMENTREQUEST_0_SHIPTOSTREET'];
                    $PaypalAddress['address_2'] = (isset($data['PAYMENTREQUEST_0_SHIPTOSTREET2'])) ? $data['PAYMENTREQUEST_0_SHIPTOSTREET2'] : '';
                    $PaypalAddress['postcode'] = (isset($data['PAYMENTREQUEST_0_SHIPTOZIP'])) ? $data['PAYMENTREQUEST_0_SHIPTOZIP'] : '';
                    $PaypalAddress['city'] = $data['PAYMENTREQUEST_0_SHIPTOCITY'];
                    $PaypalAddress['country_id'] = $data['country_id'];
                    $PaypalAddress['zone_id'] = $data['zone_id'];

                    $this->load->model('account/address');
                    $this->session->data['pec']['address_id'] = $this->model_account_address->addAddress($PaypalAddress);
                    $this->session->data['payment_address_id'] = $this->session->data['pec']['address_id'];
                    if ($this->cart->hasShipping()) {
                        $this->session->data['shipping_address_id'] = $this->session->data['pec']['address_id'];
                    }
                }

                if (!isset($this->session->data['payment_address_id'])) {
                    $this->session->data['payment_address_id'] = $this->session->data['pec']['address_id'];
                }

                if ($this->cart->hasShipping()) {
                    if (!isset($this->session->data['shipping_address_id'])) {
                        $this->session->data['shipping_address_id'] = $this->session->data['pec']['address_id'];
                    }
                }
            } else {
                #Check PayPal address if it is the same to guest
                $create_account = true;
                if (isset($this->session->data['guest'])) {
                    $use_address = ($this->config->get('paypal_express_credit_send_address') ? $this->config->get('paypal_express_credit_send_address') : 'shipping');
                    if (isset($this->session->data['guest'][$use_address]) &&
                       $this->session->data['guest'][$use_address]['firstname'] == $firstname &&
                       $this->session->data['guest'][$use_address]['lastname'] == $lastname &&
                       $this->session->data['guest'][$use_address]['city'] == $data['PAYMENTREQUEST_0_SHIPTOCITY'] &&
                       $this->session->data['guest'][$use_address]['address_1'] == $data['PAYMENTREQUEST_0_SHIPTOSTREET'] &&
                       $this->session->data['guest'][$use_address]['postcode'] == (isset($data['PAYMENTREQUEST_0_SHIPTOZIP']) ? $data['PAYMENTREQUEST_0_SHIPTOZIP'] : '') &&
                       $this->session->data['guest'][$use_address]['country_id'] == $data['country_id'] &&
                       $this->session->data['guest'][$use_address]['zone_id'] == $data['zone_id']) {
                        $this->session->data['pec']['address_already_exists'] = true;
                        $create_account = false;
                    } else {
                        unset($this->session->data['guest']);
                        $this->session->data['pec']['address_already_exists'] = false;
                    }
                }

                if (!isset($this->session->data['guest'])) {
                    $guest = array();
                    $guest['firstname'] = $firstname;
                    $guest['lastname'] = $lastname;
                    $guest['email'] = $data['EMAIL'];

                    if (isset($data['PAYMENTREQUEST_0_SHIPTOPHONENUM'])) {
                        $guest['telephone'] = $data['PAYMENTREQUEST_0_SHIPTOPHONENUM'];
                    } elseif (isset($data['PHONENUM'])) {
                        $guest['telephone'] = $data['PHONENUM'];
                    } else {
                        $guest['telephone'] = '';
                    }

                    $guest['fax'] = '';
                    $guest['company'] = $company;
                    $guest['address_1'] = $data['PAYMENTREQUEST_0_SHIPTOSTREET'];
                    $guest['address_2'] = (isset($data['PAYMENTREQUEST_0_SHIPTOSTREET2'])) ? $data['PAYMENTREQUEST_0_SHIPTOSTREET2'] : '';
                    $guest['postcode'] = (isset($data['PAYMENTREQUEST_0_SHIPTOZIP'])) ? $data['PAYMENTREQUEST_0_SHIPTOZIP'] : '';
                    $guest['city'] = $data['PAYMENTREQUEST_0_SHIPTOCITY'];
                    $guest['country_id'] = $data['country_id'];
                    $guest['zone_id'] = $data['zone_id'];

                    $this->load->model('localisation/country');
                    $country_info = $this->model_localisation_country->getCountry($data['country_id']);

                    if ($country_info) {
                        $guest['country'] = $country_info['name'];
                        $guest['iso_code_2'] = $country_info['iso_code_2'];
                        $guest['iso_code_3'] = $country_info['iso_code_3'];
                        $guest['address_format'] = $country_info['address_format'];
                    } else {
                        $guest['country'] = '';
                        $guest['iso_code_2'] = '';
                        $guest['iso_code_3'] = '';
                        $guest['address_format'] = '';
                    }

                    $this->load->model('localisation/zone');
                    $zone_info = $this->model_localisation_zone->getZone($data['zone_id']);

                    if ($zone_info) {
                        $guest['zone'] = $zone_info['name'];
                        $guest['zone_code'] = $zone_info['code'];
                    } else {
                        $guest['zone'] = '';
                        $guest['zone_code'] = '';
                    }

                    $this->session->data['guest'] = $guest;
                    $this->session->data['guest']['payment'] = $guest;
                    $this->session->data['guest']['shipping'] = $guest;
                }

                if ($create_account && $this->config->get('paypal_express_credit_createaccount') == 'create') {
                    if ($this->CreateAccount($this->session->data['guest'])) {
                        unset($this->session->data['guest']);

                        $address_id = $this->customer->getAddressId();
                        $this->session->data['shipping_address_id'] = $address_id;
                        $this->session->data['payment_address_id'] = $address_id;
                    }
                }
            }
        }

        $this->session->data['comment'] = (isset($this->session->data['comment'])) ? $this->session->data['comment'] : '';

        if ($this->skip_confirm) {
            $json = $this->DoExpressCheckoutPayment(true);
            if (isset($json['error'])) {
                $this->session->data['pec']['error'] = $json['error'];
                $this->redirect($this->url->link('checkout/pec_checkout', '', 'SSL'));
            } else {
                $this->redirect($this->url->link('checkout/success', '', 'SSL'));
            }
        } else {
            $this->redirect($this->url->link('checkout/pec_checkout', '', 'SSL'));
        }
    }

    private function tryLogin($login_email) {
        if ($this->customer->isLogged()) {
            return true;
        }
        $this->load->model('account/customer');

        #Check if account already exists
        if ($this->model_account_customer->getTotalCustomersByEmail($login_email)) {
            $password_query = $this->db->query("SELECT password FROM " . DB_PREFIX . "customer WHERE email = '" . $this->db->escape($login_email) . "'");
            $original_password = $password_query->row['password'];
            $temp_password = 'paypal_express';
            $temp_md5_password = md5($temp_password);
            $this->db->query("UPDATE " . DB_PREFIX . "customer SET password = '" . $temp_md5_password . "' WHERE email = '" . $this->db->escape($login_email) . "'");
            if ($this->skip_confirm) {
                $cart = $this->session->data['cart'];
            }
            if ($this->customer->login($login_email, $temp_password)) {
                $this->session->data['account'] = 1;
                if ($this->skip_confirm) {
                    $this->session->data['cart'] = $cart;
                }
                $this->db->query("UPDATE " . DB_PREFIX . "customer SET password = '" . $original_password . "' WHERE email = '" . $this->db->escape($login_email) . "'");
                return true;
            } else {
                return false;
            }
        }
    }

    private function CreateAccount($data) {
        $login_email = urldecode($data['email']);

        $login = $this->tryLogin($login_email);
        if ($login === true) {
            return true;
        } elseif ($login === false) {
            return false;
        }

        $login_password = $this->CreatePassword();

        $this->customer_data = array(
          'firstname' => $data['firstname'],
          'lastname' => $data['lastname'],
          'email' => $login_email,
          'telephone' => $data['telephone'],
          'fax' => '',
          'password' => $login_password,
          'newsletter' => '1',
          'customer_group_id' => $this->config->get('config_customer_group_id'),
          'status' => '1',
          'ip' => $this->request->server['REMOTE_ADDR'],
          'company' => $data['payment']['company'],
          'address_1' => $data['payment']['address_1'],
          'address_2' => $data['payment']['address_2'],
          'city' => $data['payment']['city'],
          'postcode' => $data['payment']['postcode'],
          'zone_id' => $data['payment']['zone_id'],
          'country_id' => $data['payment']['country_id'],
          'default' => '1'
        );

        $this->request->post['email'] = $data['email'];

        $this->load->model('account/customer');
        $this->model_account_customer->addCustomer($this->customer_data);

        $approve_query = $this->db->query("SELECT approved FROM " . DB_PREFIX . "customer");
        if ($approve_query->num_rows) {
            $this->db->query("UPDATE " . DB_PREFIX . "customer SET approved = '1' WHERE email = '" . $login_email . "'");
        }

        $this->session->data['pec']['generated'] = true;
        $this->customer->login($login_email, $login_password);
        $this->session->data['account'] = 1;

        #Send email welcome with password generated
        $this->language->load('mail/customer');

        $subject = sprintf($this->language->get('text_subject'), $this->config->get('config_name'));

        $message = sprintf($this->language->get('text_welcome'), $this->config->get('config_name')) . "\n\n";
        $message .= $this->language->get('text_login') . "\n";
        $message .= $this->url->link('account/login', '', 'SSL') . "\n\n";
        $message .= 'E-mail: ' . $login_email . "\n";
        $message .= 'Password: ' . $login_password . "\n\n";
        $message .= $this->language->get('text_services') . "\n\n";
        $message .= $this->language->get('text_thanks') . "\n";
        $message .= $this->config->get('config_name');

        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->hostname = $this->config->get('config_smtp_host');
        $mail->username = $this->config->get('config_smtp_username');
        $mail->password = $this->config->get('config_smtp_password');
        $mail->port = $this->config->get('config_smtp_port');
        $mail->timeout = $this->config->get('config_smtp_timeout');
        $mail->setTo($login_email);
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender($this->config->get('config_name'));
        $mail->setSubject($subject);
        $mail->setText($message);
        $mail->send();

        return true;
    }

    private function CreatePassword($length = 8) {
        $chars = '0123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!$%()=?[]#';
        $chars_length = (strlen($chars) - 1);

        $password = $chars{rand(0, $chars_length)};

        for ($i = 1; $i < $length; $i = strlen($password)) {
            $r = $chars{rand(0, $chars_length)};

            if ($r != $password{$i - 1})
                $password .= $r;
        }

        return $password;
    }

    private function getOrderTotals() {
        $this->load->model('setting/extension');

        $total_data = array();
        $total = 0;
        $taxes = $this->cart->getTaxes();

        $sort_order = array();

        $results = $this->model_setting_extension->getExtensions('total');

        foreach ($results as $key => $value) {
            $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
        }

        array_multisort($sort_order, SORT_ASC, $results);

        foreach ($results as $result) {
            if ($this->config->get($result['code'] . '_status')) {
                $this->load->model('total/' . $result['code']);

                $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
            }

            $sort_order = array();

            foreach ($total_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $total_data);
        }

        $order_totals = array();
        $order_totals['subtotal'] = 0;
        $order_totals['shippingcost'] = 0;
        $order_totals['taxcost'] = 0;
        $order_totals['discount'] = 0;
        $order_totals['handling'] = 0;
        $order_totals['total'] = 0;

        foreach ($total_data as $order_total) {
            if ($order_total['code'] == 'sub_total') {
                $order_totals['subtotal'] += $order_total['value'];
            } elseif ($order_total['code'] == 'shipping') {
                $order_totals['shippingcost'] += $order_total['value'];
            } elseif ($order_total['code'] == 'tax') {
                $order_totals['taxcost'] += $order_total['value'];
            } elseif ($order_total['code'] == 'total') {
                $order_totals['total'] += $order_total['value'];
            } elseif ($order_total['code'] == 'coupon' || $order_total['code'] == 'voucher' || $order_total['code'] == 'credit' || $order_total['code'] == 'reward') {
                $order_totals['discount'] += $order_total['value'];
			} elseif ($order_total['code'] == 'sales_promotion') {
                $order_totals['discount'] -= $order_total['value'];
            } elseif ($order_total['code'] == 'handling' || $order_total['code'] == 'low_order_fee') {
                $order_totals['handling'] += $order_total['value'];
            } else {
                if ($order_total['value'] > 0) {
                    $order_totals['handling'] += $order_total['value'];
                } else {
                    $order_totals['discount'] += $order_total['value'];
                }
            }
        }

        return $order_totals;
    }

    public function isEnabled() {
        if ($this->config->get('paypal_express_credit_status')) {
            $totals = $this->getOrderTotals();
            if ($this->config->get('paypal_express_credit_total') > $totals['total']) {
                return false;
            } elseif ($this->config->get('paypal_express_credit_total_over') > 0 && $this->config->get('paypal_express_credit_total_over') < $totals['total']) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    public function exceptionsErrorHandler($severity, $message, $filename, $lineno) {
        if (error_reporting() == 0) {
            return;
        }
        switch ($severity) {
            case E_NOTICE:
            case E_USER_NOTICE:
                $errors = "Notice";
                break;
            case E_WARNING:
            case E_USER_WARNING:
                $errors = "Warning";
                break;
            case E_ERROR:
            case E_USER_ERROR:
                $errors = "Fatal Error";
                break;
            default:
                $errors = "Unknown";
                if ($this->config->get('config_error_log')) {
                    $this->log->write('PHP ' . $errors . ':  ' . $message . ' in ' . $filename . ' on line ' . $lineno);
                }
                return;
                break;
        }

        if ($this->config->get('config_error_display')) {
            throw new Exception($errors . ': ' . $message . ' in ' . $filename . ' on line ' . $lineno);
        }

        if ($this->config->get('config_error_log')) {
            $this->log->write('PHP ' . $errors . ':  ' . $message . ' in ' . $filename . ' on line ' . $lineno);
        }

        return true;
    }

    public function registerShutdownFunction() {
        $last_error = error_get_last();
        if ($last_error) {
            switch ($last_error['type']) {
                case E_NOTICE:
                case E_USER_NOTICE:
                    $errors = "Notice";
                    break;
                case E_WARNING:
                case E_USER_WARNING:
                    $errors = "Warning";
                    break;
                case E_ERROR:
                case E_USER_ERROR:
                    $errors = "Fatal Error";
                    break;
                default:
                    $errors = "Unknown";
                    if ($this->config->get('config_error_log')) {
                        $this->log->write('PHP ' . $errors . ':  ' . $last_error['message'] . ' in ' . $last_error['file'] . ' on line ' . $last_error['line']);
                    }
                    return;
                    break;
            }
            if ($this->config->get('config_error_log')) {
                $this->log->write('PHP ' . $errors . ':  ' . $last_error['message'] . ' in ' . $last_error['file'] . ' on line ' . $last_error['line']);
            }
            echo '<b>' . $errors . '</b>: ' . $last_error['message'] . ' in <b>' . $last_error['file'] . '</b> on line <b>' . $last_error['line'] . '</b>';
            exit;
        }
    }

}

?>