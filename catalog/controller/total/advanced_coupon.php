<?php 

class ControllerTotalAdvancedCoupon extends Controller {

	public function index() {

		$this->language->load('total/advanced_coupon');
		$this->data['heading_title'] = $this->language->get('text_use_advanced_coupon');
		$this->data['entry_advanced_coupon'] = $this->language->get('entry_advanced_coupon');
		
		$this->data['button_advanced_coupon'] = $this->language->get('button_coupon');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/total/advanced_coupon.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/total/advanced_coupon.tpl';
		} else {
			$this->template = 'default/template/total/advanced_coupon.tpl';
		}

		$this->render();
  	}

	public function calculate() {

		$this->language->load('total/advanced_coupon');
		$json = array();

		if (!$this->cart->hasProducts()) {
			$json['redirect'] = $this->url->link('checkout/cart');				
		}	

		if (isset($this->request->post['advanced_coupon'])) {

		$this->load->model('checkout/advanced_coupon');
			$advanced_coupon_info = $this->model_checkout_advanced_coupon->getAdvancedCoupon($this->request->post['advanced_coupon']);			

			if ($advanced_coupon_info) {			
				$this->session->data['advanced_coupon'][] = $this->request->post['advanced_coupon'];
			$this->session->data['success'] = $this->language->get('text_advanced_coupon_success');
				$json['redirect'] = $this->url->link('checkout/cart', '', 'SSL');
			} else {
				$json['error'] = $this->language->get('text_advanced_coupon_error');
			}
		}

		$this->response->setOutput(json_encode($json));		
	}
}

?>