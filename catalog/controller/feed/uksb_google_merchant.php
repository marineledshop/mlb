﻿<?php 
class ControllerFeedUksbGoogleMerchant extends Controller {
	public function plainText($string) {
	    $table = array(
		'“'=>'&#39;', '”'=>'&#39;', '‘'=>"&#34;", '’'=>"&#34;", '•'=>'*', '—'=>'-', '–'=>'-', '¿'=>'?', '¡'=>'!', '°'=>' deg. ',
		'÷'=>' / ', '×'=>'X', '±'=>'+/-',
		'&nbsp;'=> ' ', '"'=> '&#34;', "'"=> '&#39;', '<'=> '&lt;', '>'=> '&gt;', "\n"=> ' ', "\r"=> ' '
	    );

	    $string = strip_tags(html_entity_decode($string));
	    $string = strtr($string, $table);
	    $string = preg_replace('/&#?[a-z0-9]+;/i',' ',$string);	
	    $string = preg_replace('/\s{2,}/i', ' ', $string );	
	    if($this->config->get('uksb_google_merchant_characters')){

		$table2 = array(
				'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 
				'Þ'=>'B', 'þ'=>'b', 'ß'=>'Ss',
				'ç'=>'c',
				'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 
				'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i',
				'ñ'=>'n',
				'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'œ'=>'o', 'ð'=>'o',
				'š'=>'s',
				'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ü'=>'u',
				'ý'=>'y', 'ÿ'=>'y', 
				'ž'=>'z', 'ž'=>'z',
				'©'=>'(c)', '®'=>'(R)'
		);

		$string = strtr($string, $table2);
		$string = preg_replace('/[^(\x20-\x7F)]*/','', $string ); 
	    }
	    return substr($string, 0, 5000 );	
	}

	public function index() {
		if ($this->config->get('uksb_google_merchant_status')) { 
			$output  = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
			$output .= '<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">'."\n";
		        $output .= '<channel>'."\n";
			$output .= '<title><![CDATA[' . $this->config->get('config_name') . ']]></title>'."\n";
			if($this->config->get('config_meta_description')!==''){$output .= '<description><![CDATA[' . $this->config->get('config_meta_description') . ']]></description>'."\n";}
			$output .= '<link>' . HTTP_SERVER . '</link>'."\n\n\n";
			
			$this->load->model('catalog/category');
			
			$this->load->model('catalog/product');
			$this->load->model('tool/image');
			
			$this->load->model('feed/uksb_google');
		
			if(isset($this->request->get['send'])){
				$split = explode("-", $this->request->get['send']);
				$data = array('start' => ($split[0]-1), 'limit' => ($split[1]-$split[0]+1));
				$products = $this->model_catalog_product->getProducts($data);
			}else{
				$products = $this->model_catalog_product->getProducts();
			}

			$lang = ($this->request->get['language']?$this->request->get['language']:$this->config->get('config_language'));
			$curr = ($this->request->get['currency']?$this->request->get['currency']:$this->config->get('config_currency'));
			
			if($lang == 'en'){
				if($curr == 'GBP'){
					$id_suffix = 'gb';
					$gpc = html_entity_decode($this->config->get('uksb_google_merchant_google_category_gb'),ENT_QUOTES, 'UTF-8');
					$tax = $this->config->get('uksb_google_merchant_tax_gb');
				}elseif($curr == 'USD'){
					$id_suffix = 'us';
					$gpc = html_entity_decode($this->config->get('uksb_google_merchant_google_category_us'),ENT_QUOTES, 'UTF-8');
					$tax = 0;
				}else{
					$id_suffix = 'au';
					$gpc = html_entity_decode($this->config->get('uksb_google_merchant_google_category_au'),ENT_QUOTES, 'UTF-8');	
					$tax = $this->config->get('uksb_google_merchant_tax_au');
				}
			}else{
				$id_suffix = $lang;	
				if($lang == 'fr'){
					$gpc = html_entity_decode($this->config->get('uksb_google_merchant_google_category_fr'),ENT_QUOTES, 'UTF-8');
					$tax = $this->config->get('uksb_google_merchant_tax_fr');
				}elseif($lang == 'de'){
					$gpc = html_entity_decode($this->config->get('uksb_google_merchant_google_category_de'),ENT_QUOTES, 'UTF-8');
					$tax = $this->config->get('uksb_google_merchant_tax_de');
				}elseif($lang == 'it'){
					$gpc = html_entity_decode($this->config->get('uksb_google_merchant_google_category_it'),ENT_QUOTES, 'UTF-8');
					$tax = $this->config->get('uksb_google_merchant_tax_it');
				}elseif($lang == 'nl'){
					$gpc = html_entity_decode($this->config->get('uksb_google_merchant_google_category_nl'),ENT_QUOTES, 'UTF-8');
					$tax = $this->config->get('uksb_google_merchant_tax_nl');
				}elseif($lang == 'es'){
					$gpc = html_entity_decode($this->config->get('uksb_google_merchant_google_category_es'),ENT_QUOTES, 'UTF-8');
					$tax = $this->config->get('uksb_google_merchant_tax_es');
				}
			}
			
			$col = ($curr == 'GBP' ? 'colour' : 'color');

			foreach ($products as $product) {
				
				$sizes = explode(",", $product['size']); $num_sizes = count($sizes);
				$colours = explode(",", $product['colour']); $num_colours = count($colours);
				$materials = explode(",", $product['material']); $num_materials = count($materials);
				$patterns = explode(",", $product['pattern']); $num_patterns = count($patterns);
				$mpns = explode(",", $product['vmpn']); $num_mpns = count($mpns);
				$gtins = explode(",", $product['vgtin']); $num_gtins = count($gtins);
				$prices = explode(",", $product['vprices']); $num_prices = count($prices);
				
				if ($product['description']&&$product['price']>0&&$product['ongoogle']==1) {
					
					$variant = 0;
					$variant = (count($colours)>1||count($sizes)>1||count($materials)>1||count($patterns)>1?1:0);
					$max_variants = max($num_sizes, $num_colours, $num_materials, $num_patterns, $num_mpns, $num_gtins, $num_prices);
					
					$group_id = ($variant==1?$product['product_id'] . '_' . $id_suffix:'');
					
					if($variant==1){
						for($i = 1; $i <= $max_variants; $i++){
							
							$pass = 1;
							if($this->config->get('uksb_google_merchant_required_mpn')==1){
								if($num_mpns<1){
									$pass = 0;
								}
							}
							
							if($this->config->get('uksb_google_merchant_required_gtin')==1){
								if($num_gtins<1){
									$pass = 0;
								}
							}
							
							if($this->config->get('uksb_google_merchant_required_brand')==1){
								if($product['brand']=='' && $product['manufacturer']==''){
									$pass = 0;
								}
							}
							
							if($pass==1){
								$j = $i-1;
								
								$output .= '<item>'."\n";
								$output .= '<title><![CDATA[' . (isset($product['title_pla']) && !empty($product['title_pla']) ? $product['title_pla'] : $product['name']) . (isset($colours[$j])&&trim($colours[$j])!=''?' - '.trim($colours[$j]):'') . (isset($sizes[$j])&&trim($sizes[$j])!=''?' - '.trim($sizes[$j]):'') . ']]></title>'."\n";
								
								if($this->config->get('uksb_google_merchant_fullpath')=='full'&&$this->model_feed_uksb_google->getCategoryPath($product['product_id'])!='0'){
									$output .= '<link><![CDATA[' . $this->url->link('product/product', 'path=' . $this->model_feed_uksb_google->getCategoryPath($product['product_id']) . '&product_id=' . $product['product_id'] . '&language=' . $lang . '&currency='. $curr) . ']]></link>'."\n";
								}else{
									$output .= '<link><![CDATA[' . $this->url->link('product/product', 'product_id=' . $product['product_id'] . '&language=' . $lang . '&currency='. $curr) . ']]></link>'."\n";
								}
								
								$output .= '<description><![CDATA[' . $this->plainText((isset($product['description_pla']) && !empty($product['description_pla']) ? $product['description_pla'] : $product['description'])) . ']]></description>'."\n";
								if($product['brand'] || $product['manufacturer']){
									$output .= '<g:brand><![CDATA[' . ($product['brand'] ? $product['brand'] : $product['manufacturer']) . ']]></g:brand>'."\n";
								}
								$output .= '<g:condition>' . ($product['gcondition']?$product['gcondition']:$this->config->get('uksb_google_merchant_condition')) . '</g:condition>'."\n";
								$output .= '<g:item_group_id><![CDATA[' . $group_id . ']]></g:item_group_id>'."\n";
								$output .= '<g:id><![CDATA[' . $product['product_id'] . '_' . $i . '_' . $id_suffix . ']]></g:id>'."\n";
			
								if ($product['image']) {
									$output .= '<g:image_link><![CDATA[' . str_replace(" ", "%20", $this->model_tool_image->resize($product['image'], 500, 500)) . ']]></g:image_link>'."\n";
								} else {
									$output .= '<g:image_link><![CDATA[' . $this->model_tool_image->resize('no_image.jpg', 500, 500) . ']]></g:image_link>'."\n";
								}
								
								$addimages = $this->model_catalog_product->getProductImages($product['product_id']);
								
								$addimnum = 0;
								foreach($addimages as $addimage){
									if($addimnum<10){
										$output .= '<g:additional_image_link><![CDATA[' . str_replace(" ", "%20", $this->model_tool_image->resize($addimage['image'], 500, 500)) . ']]></g:additional_image_link>'."\n";
									}
									$addimnum++;
								}
								
			
								if($this->config->get('config_stock_checkout')==0){
									if ($product['quantity']>0) {
										$output .= '<g:availability>in stock</g:availability>'."\n";
									} else {
										$output .= '<g:availability>out of stock</g:availability>'."\n";
									}
								}else{
									if ($product['quantity']>0) {
										$output .= '<g:availability>in stock</g:availability>'."\n";
									} else {
										$output .= '<g:availability>available for order</g:availability>'."\n";
									}
								}
	
								if(isset($mpns[$j])&&trim($mpns[$j])!=''){$output .= '<g:mpn><![CDATA[' . trim($mpns[$j]) . ']]></g:mpn>'."\n";}
			
								$supported_currencies = array('USD', 'EUR', 'GBP', 'AUD');
			
								if (in_array($curr, $supported_currencies)) {
									$currency = $curr;
								} else {
									$currency = 'GBP';
								}
										
								if(isset($prices[$j])&&trim($prices[$j])!=''){
									 
									$quantifier = strval($prices[$j]);
									$pricevalue = floatval($prices[$j]);
									if($quantifier == ''){
										$quantifier = '+';
									}
									if($pricevalue==''){
										$pricevalue=0;
									}
								}else{
									$quantifier = '+';
									$pricevalue = '0';
								}
								
								if ((float)$product['special']) {
									$sprice = ($quantifier=='-'?$product['special'] - $pricevalue:$product['special'] + $pricevalue);

									if($tax > 0){
										$output .= '<g:sale_price>' .  $this->currency->format($this->tax->calculate($sprice, $product['tax_class_id']), $currency, FALSE, FALSE) . ' ' . $currency . '</g:sale_price>'."\n";
									}else{
										$output .= '<g:sale_price>' .  $this->currency->format($sprice, $currency, FALSE, FALSE) . ' ' . $currency . '</g:sale_price>'."\n";
									}
									$output .= '<g:sale_price_effective_date>' . $this->model_feed_uksb_google->getFeedSpecialStartDate($product['product_id']).'T00:00:00'.date("P").'/'.$this->model_feed_uksb_google->getFeedSpecialEndDate($product['product_id']).'T23:59:59'.date("P").'</g:sale_price_effective_date>'."\n";
								}
								
								$price = ($quantifier=='-'?$product['price'] - $pricevalue:$product['price'] + $pricevalue);
								if($tax > 0){
									$output .= '<g:price>' . $this->currency->format($this->tax->calculate($price, $product['tax_class_id']), $currency, FALSE, FALSE) . ' ' . $currency . '</g:price>'."\n";
								}else{
									$output .= '<g:price>' . $this->currency->format($price, $currency, FALSE, FALSE) . ' ' . $currency . '</g:price>'."\n";
								}
								
								if($product['reviews']>0){
									$output .= '<g:product_review_count>' . $product['reviews'] . '</g:product_review_count>'."\n";
									$output .= '<g:product_review_average>' . $product['rating'] . '</g:product_review_average>'."\n";
								}
						   
								$categories = $this->model_catalog_product->getCategories($product['product_id']);
								
								$catno = 1;
								$gpcc = '';
								foreach ($categories as $category) {
									if($catno<11){
										$path = $this->getPath($category['category_id']);
										$gpcc = ($gpcc==''?$this->model_feed_uksb_google->getCategoryGoogleCategories($category['category_id'], $id_suffix):$gpcc);
									
										if ($path) {
											$string = '';
											
											foreach (explode('_', $path) as $path_id) {
												$category_info = $this->model_catalog_category->getCategory($path_id);
												
												if ($category_info) {
													if (!$string) {
														$string = $category_info['name'];
													} else {
														$string .= ' &gt; ' . $category_info['name'];
													}
												}
											}
										 
											$output .= '<g:product_type><![CDATA[' . $string . ']]></g:product_type>'."\n";
											$catno++;
										}
									}
								}
								
								if(isset($gtins[$j])&&trim($gtins[$j])!=''){$output .= '<g:gtin><![CDATA[' . $gtins[$j] . ']]></g:gtin>'."\n";}
			
								if((float)$product['weight']){
									$output .= '<g:shipping_weight>' . $this->weight->format($product['weight'], $product['weight_class_id']) . '</g:shipping_weight>'."\n";
								}
								
								if($product['google_category_'.$id_suffix]!=''){
									$output .= '<g:google_product_category><![CDATA[' . html_entity_decode($product['google_category_'.$id_suffix], ENT_QUOTES, 'UTF-8') . ']]></g:google_product_category>'."\n";
								}elseif($gpcc!=''){
									$output .= '<g:google_product_category><![CDATA[' . html_entity_decode($gpcc, ENT_QUOTES, 'UTF-8') . ']]></g:google_product_category>'."\n";
								}elseif($gpc!=''){
									$output .= '<g:google_product_category><![CDATA[' . $gpc . ']]></g:google_product_category>'."\n";
								}
								
								if($product['gender']){
									$output .= '<g:gender>' . $product['gender'] . '</g:gender>'."\n";
								}
								
								if($product['age_group']){
									$output .= '<g:age_group>' . $product['age_group'] . '</g:age_group>'."\n";
								}
								
								if(isset($colours[$j])&&trim($colours[$j])!=''){
									$output .= '<g:'.$col.'><![CDATA[' . trim($colours[$j]) . ']]></g:'.$col.'>'."\n";
								}
								
								if(isset($sizes[$j])&&trim($sizes[$j])!=''){
									$output .= '<g:size><![CDATA[' . trim($sizes[$j]) . ']]></g:size>'."\n";
								}
								
								if(isset($materials[$j])&&trim($materials[$j])!=''){
									$output .= '<g:material><![CDATA[' . trim($materials[$j]) . ']]></g:material>'."\n";
								}
								
								if(isset($patterns[$j])&&trim($patterns[$j])!=''){
									$output .= '<g:pattern><![CDATA[' . trim($patterns[$j]) . ']]></g:pattern>'."\n";
								}
								
								if($product['google_adwords_publish']=='1'){
									$output .= '<g:adwords_publish><![CDATA[true]]></g:adwords_publish>'."\n";
									if($product['google_adwords_grouping']!=''){$output .= '<g:adwords_grouping><![CDATA[' . $product['google_adwords_grouping'] . ']]></g:adwords_grouping>'."\n";}
									if($product['google_adwords_labels']!=''){
										$labels = array();
										$labels = explode(",", $product['google_adwords_labels']);
										foreach($labels as $label){
											$label = trim($label);
											$output .= '<g:adwords_labels><![CDATA[' . $label . ']]></g:adwords_labels>'."\n";
										}
										unset($labels);
									}								
									if($product['google_adwords_redirect']!=''){$output .= '<g:adwords_redirect><![CDATA[' . $product['google_adwords_redirect'] . ']]></g:adwords_redirect>'."\n";}
									if($product['google_adwords_queryparam']!=''){$output .= '<g:adwords_queryparam><![CDATA[' . $product['google_adwords_queryparam'] . ']]></g:adwords_queryparam>'."\n";}
									
								}else{
									$output .= '<g:adwords_publish><![CDATA[false]]></g:adwords_publish>'."\n";
									$output .= '<g:excluded_destination><![CDATA[shopping]]></g:excluded_destination>'."\n";
								}

								$output .= '</item>'."\n\n\n";
							}
						}
					}else{
						$pass = 1;
						if($this->config->get('uksb_google_merchant_required_mpn')==1){
							if($this->config->get('uksb_google_merchant_mpn')=='mpn' && $product['mpn']==''){
								$pass = 0;
							}elseif($this->config->get('uksb_google_merchant_mpn')=='location' && $product['location']==''){
								$pass = 0;
							}elseif($this->config->get('uksb_google_merchant_mpn')=='sku' && $product['sku']==''){
								$pass = 0;
							}elseif($product['model']==''){
								$pass = 0;
							}
						}
						
						if($this->config->get('uksb_google_merchant_required_gtin')==1){
							if($this->config->get('uksb_google_merchant_gtin')=='gtin' && $product['gtin']==''){
								$pass = 0;
							}elseif($this->config->get('uksb_google_merchant_gtin')=='location' && $product['location']==''){
								$pass = 0;
							}elseif($this->config->get('uksb_google_merchant_gtin')=='sku' && $product['sku']==''){
								$pass = 0;
							}elseif($this->config->get('uksb_google_merchant_gtin')=='upc' && $product['upc']==''){
								$pass = 0;
							}
						}
						
						if($this->config->get('uksb_google_merchant_required_brand')==1){
							if($product['brand']=='' && $product['manufacturer']==''){
								$pass = 0;
							}	
						}
						
						if($pass==1){
							$output .= '<item>'."\n";
							$output .= '<title><![CDATA[' . (isset($product['title_pla']) && !empty($product['title_pla']) ? $product['title_pla'] : $product['name']) . ']]></title>'."\n";
							
							if($this->config->get('uksb_google_merchant_fullpath')=='full'&&$this->model_feed_uksb_google->getCategoryPath($product['product_id'])!='0'){
								$output .= '<link><![CDATA[' . $this->url->link('product/product', 'path=' . $this->model_feed_uksb_google->getCategoryPath($product['product_id']) . '&product_id=' . $product['product_id'] . '&language=' . $lang . '&currency='. $curr) . ']]></link>'."\n";
							}else{
								$output .= '<link><![CDATA[' . $this->url->link('product/product', 'product_id=' . $product['product_id'] . '&language=' . $lang . '&currency='. $curr) . ']]></link>'."\n";
							}
							
							$output .= '<description><![CDATA[' . $this->plainText((isset($product['description_pla']) && !empty($product['description_pla']) ? $product['description_pla'] : $product['description'])) . ']]></description>'."\n";
							if($product['brand'] || $product['manufacturer']){
								$output .= '<g:brand><![CDATA[' . ($product['brand'] ? $product['brand'] : $product['manufacturer']) . ']]></g:brand>'."\n";
							}
							$output .= '<g:condition>' . ($product['gcondition']?$product['gcondition']:$this->config->get('uksb_google_merchant_condition')) . '</g:condition>'."\n";
							$output .= '<g:id><![CDATA[' . $product['product_id'] . '_' . $id_suffix . ']]></g:id>'."\n";
		
							if ($product['image']) {
								$output .= '<g:image_link><![CDATA[' . str_replace(" ", "%20", $this->model_tool_image->resize($product['image'], 500, 500)) . ']]></g:image_link>'."\n";
							} else {
								$output .= '<g:image_link><![CDATA[' . $this->model_tool_image->resize('no_image.jpg', 500, 500) . ']]></g:image_link>'."\n";
							}
							
							$addimages = $this->model_catalog_product->getProductImages($product['product_id']);
							
							$addimnum = 0;
							foreach($addimages as $addimage){
								if($addimnum<10){
									$output .= '<g:additional_image_link><![CDATA[' . str_replace(" ", "%20", $this->model_tool_image->resize($addimage['image'], 500, 500)) . ']]></g:additional_image_link>'."\n";
								}
								$addimnum++;
							}
		
							if($this->config->get('config_stock_checkout')==0){
								if ($product['quantity']>0) {
									$output .= '<g:availability>in stock</g:availability>'."\n";
								} else {
									$output .= '<g:availability>out of stock</g:availability>'."\n";
								}
							}else{
								if ($product['quantity']>0) {
									$output .= '<g:availability>in stock</g:availability>'."\n";
								} else {
									$output .= '<g:availability>available for order</g:availability>'."\n";
								}
							}
							
							if($this->config->get('uksb_google_merchant_mpn')=='mpn'){
								if($product['mpn']){$output .= '<g:mpn><![CDATA[' . $product['mpn'] . ']]></g:mpn>'."\n";}
							}elseif($this->config->get('uksb_google_merchant_mpn')=='location'){
								if($product['location']){$output .= '<g:mpn><![CDATA[' . $product['location'] . ']]></g:mpn>'."\n";}
							}elseif($this->config->get('uksb_google_merchant_mpn')=='sku'){
								if($product['sku']){$output .= '<g:mpn><![CDATA[' . $product['sku'] . ']]></g:mpn>'."\n";}
							}else{
								if($product['model']){$output .= '<g:mpn><![CDATA[' . $product['model'] . ']]></g:mpn>'."\n";}
							}
		
							$supported_currencies = array('USD', 'EUR', 'GBP', 'AUD');
		
									if (in_array($curr, $supported_currencies)) {
										$currency = $curr;
									} else {
										$currency = 'GBP';
									}
											
							if ((float)$product['special']) {
								if($tax > 0){
									$output .= '<g:sale_price>' .  $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id']), $currency, FALSE, FALSE) . ' ' . $currency . '</g:sale_price>'."\n";
								}else{
									$output .= '<g:sale_price>' .  $this->currency->format($product['special'], $currency, FALSE, FALSE) . ' ' . $currency . '</g:sale_price>'."\n";
								}
								$output .= '<g:sale_price_effective_date>' . $this->model_feed_uksb_google->getFeedSpecialStartDate($product['product_id']).'T00:00:00'.date("P").'/'.$this->model_feed_uksb_google->getFeedSpecialEndDate($product['product_id']).'T23:59:59'.date("P").'</g:sale_price_effective_date>'."\n";
							}
								
							if($tax > 0){
								$output .= '<g:price>' . $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id']), $currency, FALSE, FALSE) . ' ' . $currency . '</g:price>'."\n";
							}else{
								$output .= '<g:price>' . $this->currency->format($product['price'], $currency, FALSE, FALSE) . ' ' . $currency . '</g:price>'."\n";
							}
							
							if($product['reviews']>0){
								$output .= '<g:product_review_count>' . $product['reviews'] . '</g:product_review_count>'."\n";
								$output .= '<g:product_review_average>' . $product['rating'] . '</g:product_review_average>'."\n";
							}
					   
							$categories = $this->model_catalog_product->getCategories($product['product_id']);
							
							$catno = 1;
							$gpcc = '';
							
							foreach ($categories as $category) {
								if($catno<11){
									$path = $this->getPath($category['category_id']);

									$gpcc = ($gpcc==''?$this->model_feed_uksb_google->getCategoryGoogleCategories($category['category_id'], $id_suffix):$gpcc);
									
									if ($path) {
										$string = '';
										
										foreach (explode('_', $path) as $path_id) {
											$category_info = $this->model_catalog_category->getCategory($path_id);
											
											if ($category_info) {
												if (!$string) {
													$string = $category_info['name'];
												} else {
													$string .= ' &gt; ' . $category_info['name'];
												}
											}
										}
									 
										$output .= '<g:product_type><![CDATA[' . $string . ']]></g:product_type>'."\n";
										$catno++;
									}
								}
							}
							
							if($this->config->get('uksb_google_merchant_gtin')=='gtin'){
								if($product['gtin']){$output .= '<g:gtin><![CDATA[' . $product['gtin'] . ']]></g:gtin>'."\n";}
							}elseif($this->config->get('uksb_google_merchant_gtin')=='location'){
								if($product['location']){$output .= '<g:gtin><![CDATA[' . $product['location'] . ']]></g:gtin>'."\n";}
							}elseif($this->config->get('uksb_google_merchant_gtin')=='sku'){
								if($product['sku']){$output .= '<g:gtin><![CDATA[' . $product['sku'] . ']]></g:gtin>'."\n";}
							}else{
								if($product['upc']){$output .= '<g:gtin><![CDATA[' . $product['upc'] . ']]></g:gtin>'."\n";}
							}
		
							if((float)$product['weight']){
								$output .= '<g:shipping_weight>' . $this->weight->format($product['weight'], $product['weight_class_id']) . '</g:shipping_weight>'."\n";
							}
							
							if($product['google_category_'.$id_suffix]!=''){
								$output .= '<g:google_product_category><![CDATA[' . html_entity_decode($product['google_category_'.$id_suffix], ENT_QUOTES, 'UTF-8') . ']]></g:google_product_category>'."\n";
							}elseif($gpcc!=''){
								$output .= '<g:google_product_category><![CDATA[' . html_entity_decode($gpcc, ENT_QUOTES, 'UTF-8') . ']]></g:google_product_category>'."\n";
							}elseif($gpc!=''){
								$output .= '<g:google_product_category><![CDATA[' . $gpc . ']]></g:google_product_category>'."\n";
							}
							
							if($product['gender']){
								$output .= '<g:gender>' . $product['gender'] . '</g:gender>'."\n";
							}
							
							if($product['age_group']){
								$output .= '<g:age_group>' . $product['age_group'] . '</g:age_group>'."\n";
							}
							
							if($product['colour']){
								$output .= '<g:'.$col.'><![CDATA[' . $product['colour'] . ']]></g:'.$col.'>'."\n";
							}
							
							if($product['size']){
								$output .= '<g:size><![CDATA[' . $product['size'] . ']]></g:size>'."\n";
							}
							
							if($product['material']){
								$output .= '<g:material><![CDATA[' . $product['material'] . ']]></g:material>'."\n";
							}
							
							if($product['pattern']){
								$output .= '<g:pattern><![CDATA[' . $product['pattern'] . ']]></g:pattern>'."\n";
							}
							
							if($product['google_adwords_publish']=='1'){
								$output .= '<g:adwords_publish><![CDATA[true]]></g:adwords_publish>'."\n";
								if($product['google_adwords_grouping']!=''){$output .= '<g:adwords_grouping><![CDATA[' . $product['google_adwords_grouping'] . ']]></g:adwords_grouping>'."\n";}
								if($product['google_adwords_labels']!=''){
									$labels = array();
									$labels = explode(",", $product['google_adwords_labels']);
									foreach($labels as $label){
										$label = trim($label);
										$output .= '<g:adwords_labels><![CDATA[' . $label . ']]></g:adwords_labels>'."\n";
									}
									unset($labels);
								}								
								if($product['google_adwords_redirect']!=''){$output .= '<g:adwords_redirect><![CDATA[' . $product['google_adwords_redirect'] . ']]></g:adwords_redirect>'."\n";}
								if($product['google_adwords_queryparam']!=''){$output .= '<g:adwords_queryparam><![CDATA[' . $product['google_adwords_queryparam'] . ']]></g:adwords_queryparam>'."\n";}
								
							}else{
								$output .= '<g:adwords_publish><![CDATA[false]]></g:adwords_publish>'."\n";
							}
							
							$output .= '</item>'."\n\n\n";
						}
					}
				}
			}
			
			$output .= '</channel>'."\n"; 
			$output .= '</rss>';	
			
			$this->response->addHeader('Content-Type: text/xml; charset=utf-8');
			$this->response->setCompression(0);
			$this->response->setOutput($output);
		}
	}
	
	protected function getPath($parent_id, $current_path = '') {
		$category_info = $this->model_catalog_category->getCategory($parent_id);
	
		if ($category_info) {
			if (!$current_path) {
				$new_path = $category_info['category_id'];
			} else {
				$new_path = $category_info['category_id'] . '_' . $current_path;
			}	
		
			$path = $this->getPath($category_info['parent_id'], $new_path);
					
			if ($path) {
				return $path;
			} else {
				return $new_path;
			}
		}
	}		
}
?>
