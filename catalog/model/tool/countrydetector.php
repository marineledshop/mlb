<?php
/**
 * @author avasilenko
 */

class ModelToolCountryDetector extends Model {
	public function detectCountry($ip) {
		$ch = curl_init("http://freegeoip.net/json/$ip");
		curl_setopt_array($ch, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_TIMEOUT => 3
		));
		$res = curl_exec($ch);
		$detectedCountry =  $this->config->get('config_country_id');
		if (curl_errno($ch) == 0) {
			$res = json_decode($res);
			$this->load->model('localisation/country');
			$countries = $this->model_localisation_country->getCountries();
			foreach($countries as $country) {
				if (isset($country['iso_code_2']) && $country['iso_code_2'] == $res->country_code) {
					$detectedCountry = $country['country_id'];
					break;
				}
			}
		}
		curl_close($ch);

		return $detectedCountry;
	}
}