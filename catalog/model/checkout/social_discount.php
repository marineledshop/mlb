<?php
class ModelCheckoutSocialDiscount extends Model {
	
	public function getSocialDiscount($social_discount_id) {
			
		$social_discount_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "social_discount c WHERE social_discount_id = " . (int)$social_discount_id . " AND ((c.date_start = '0000-00-00' OR c.date_start < NOW()) AND (c.date_end = '0000-00-00' OR c.date_end > NOW())) AND c.status = '1'");
		
		//c2s.store_id = '" . (int)$this->config->get('config_store_id')
		
		if ($social_discount_query->num_rows) {
		foreach ($social_discount_query->rows as $social_discount_result) { 
			$status = true;
			
			$social_discount_options = unserialize($social_discount_result['options']);
		
			unset($social_discount_result['options']);
		
			$result = array_merge($social_discount_result, $social_discount_options);
			
			// Condition to display discount when user logged in
			if ($result['logged'] && !$this->customer->getId()) {
				$status = false;
			}
					
			
			// Condition for total uses of discount
			$social_discount_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "social_discount_history` ch WHERE ch.social_discount_id = '" . (int)$result['social_discount_id'] . "'");

				if ($result['uses_total'] > 0 && ($social_discount_history_query->row['total'] >= $result['uses_total'])) {
					$status = false;
				}
			
			// Condition for total uses of discount per customer							
			if ($this->customer->getId()) {
				$social_discount_history_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "social_discount_history` ch WHERE ch.social_discount_id = '" . (int)$result['social_discount_id'] . "' AND ch.customer_id = '" . (int)$this->customer->getId() . "'");
				
				if ($result['uses_customer'] > 0 && ($social_discount_history_query->row['total'] >= $result['uses_customer'])) {
					$status = false;
				}
			}
								
			// Condition for Total Amount
			$sd_total = $result['total'];
			$cart_subtotal = $this->cart->getSubTotal();
		
			if(strpos($sd_total,"-") !== false) {
     			$sd_total_limit = explode("-", $sd_total);
    			if (($sd_total_limit[0] > $cart_subtotal) || ($sd_total_limit[1] <  $cart_subtotal)) {
				$status = false;
				}     
   			}
			else {
  				if ($sd_total >= $cart_subtotal) {
				$status = false;
       			}
			}	
			
			// Condition for Total Quantity
			$sd_quantity_total = $result['quantity_total'];
			$cart_quantity_total = $this->cart->countProducts();
			
			if(strpos($sd_quantity_total,"-") !== false) {
     			$sd_quantity_total_limit = explode("-", $sd_quantity_total);
    			if (($sd_quantity_total_limit[0] > $cart_quantity_total) || ($sd_quantity_total_limit[1] <  $cart_quantity_total)) {
				$status = false;
				}     
   			}
			else {
  				if ($sd_quantity_total >= $cart_quantity_total) {
				$status = false;
       			}
			}	
			
			if($status == false)
			if($result['shipping'] && $result['discount']==0) {
			$status = true;	
			}
			
							
		if ($status) {
			 	$social_discount_data[] = array(
					'social_discount_id' 		=> $result['social_discount_id'],
					'name'           			=> $result['name'],
					'type'           			=> $result['type'],
					'discount'       			=> $result['discount'],
					'total'          			=> $result['total'],
					'quantity_total' 			=> $result['quantity_total'],
					'shipping'       			=> $result['shipping'],
					'date_start'     			=> $result['date_start'],
					'date_end'       			=> $result['date_end'],
					'uses_total'     			=> $result['uses_total'],
					'uses_customer'  			=> $result['uses_customer'],
					'status'         			=> $result['status'],
					'date_added'     			=> $result['date_added']
				);
			  } 
		}
		
		if(isset($social_discount_data)) {
			return $social_discount_data;
			}
		}
				
		
	}
	
	
	public function redeem($social_discount_id, $order_id, $customer_id, $amount) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "social_discount_history` SET social_discount_id = '" . (int)$social_discount_id . "', order_id = '" . (int)$order_id . "', customer_id = '" . (int)$customer_id . "', amount = '" . (float)$amount . "', date_added = NOW()");
	}
	
	public function getSocialDiscountId($code) {
	
		$social_discount_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "social_discount WHERE name = '" . $this->db->escape($code) . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) AND status = '1'");
	
	    if ($social_discount_query->num_rows) {
			$social_discount_id = $social_discount_query->row['social_discount_id'];
			}
		return $social_discount_id;
		}
	
	public function getSocialDiscountDetail($social_discount_id) {
	
		$social_discount_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "social_discount c WHERE c.social_discount_id = '" . $social_discount_id . "' AND ((c.date_start = '0000-00-00' OR c.date_start < NOW()) AND c.status = '1' AND (c.date_end = '0000-00-00' OR c.date_end > NOW()))");
		
		if ($social_discount_query->num_rows) {
			$social_discount_result = $social_discount_query->row;
			$social_discount_options = unserialize($social_discount_result['options']);
			unset($social_discount_result['options']);
			$social_discount = array_merge($social_discount_result, $social_discount_options);
		} else {
			$social_discount = "";
			
		}
			
		return $social_discount;
	
	}
	
	public function getSocialDiscountName($social_discount_id) {
	
		$social_discount_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "social_discount c WHERE c.social_discount_id = '" . $social_discount_id . "' AND ((c.date_start = '0000-00-00' OR c.date_start < NOW()) AND c.status = '1' AND (c.date_end = '0000-00-00' OR c.date_end > NOW()))");
		
		if ($social_discount_query->num_rows) {
			$social_discount_options = unserialize($social_discount_query->row['options']);
			// Condition for store
			if ((in_array((int)$this->config->get('config_store_id'), $social_discount_options['store']))) {
				$social_discount_name = $social_discount_query->row['name'];	
			}
		} 
		else {
			$social_discount_name = ""; 
			}
		
		return $social_discount_name;
	
	}
}
?>