<?php
class ModelFeedUksbSitemaps extends Model {
	public function getTotalProductsByStore($store_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_store WHERE store_id = '" . (int)$store_id . "' Order By store_id ASC");

		return $query->row['total'];
	}			

	public function getCategoryPath($product_id) {
		$category = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
		
		if($category->num_rows){
			$path = array();
			foreach($category->rows as $paths){
				$path[0] = $paths['category_id'];
			}
			
			$parent = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$path[0] . "'");

			$pid = $parent->row['parent_id'];
			
			$p = 1;
			while($pid>0){
				$path[$p] = $pid;
				
				$parent2 = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$pid . "'");
				$pid = $parent2->row['parent_id'];
				$p++;
			}
		
			$path = array_reverse($path);
			
			$fullpath = '';
			
			foreach($path as $val){
				$fullpath .= '_'.$val;
			}
		
			return ltrim($fullpath, '_');
		}else{
			return '0';
		}
	}			
}
?>