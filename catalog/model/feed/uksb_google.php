<?php
class ModelFeedUKSBGoogle extends Model {
	public function getCategoryPath($product_id) {
		$category = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
		
		if($category->num_rows){
			$path = array();
			foreach($category->rows as $paths){
				$path[0] = $paths['category_id'];
			}
			
			$parent = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$path[0] . "'");

			$pid = $parent->row['parent_id'];
			
			$p = 1;
			if($pid>0){
				do{
					$path[$p] = $pid;
					
					$parent2 = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$pid . "'");
					$pid = $parent2->row['parent_id'];
					$p++;
				}while($pid>0);
			}
		
			$path = array_reverse($path);
			
			$fullpath = '';
			
			foreach($path as $val){
				$fullpath .= '_'.$val;
			}
		
			return ltrim($fullpath, '_');
		}else{
			return '0';
		}
	}			

	public function getCategoryGoogleCategories($category_id, $lang) {
		$col = 'google_category_'.$lang;
		
		$query = $this->db->query("SELECT " . $this->db->escape($col) . " as cat FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "'");
		
		if($query->num_rows){
			return $query->row['cat'];
		}else{
			return '';
		}
	}			

	public function getFeedSpecialStartDate($product_id) {
		$query = $this->db->query("SELECT ps.date_start AS start FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = '" . (int)$product_id . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.date_start");
		
		if($query->row['start']=='0000-00-00'){
			return '2011-09-06';
		}else{
			return $query->row['start'];
		}
	}	

	public function getFeedSpecialEndDate($product_id) {
		$query = $this->db->query("SELECT ps.date_end AS end FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = '" . (int)$product_id . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) GROUP BY ps.date_end");
		
		if($query->row['end']=='0000-00-00'){
			return '2034-09-06';
		}else{
			return $query->row['end'];
		}
	}	
}
