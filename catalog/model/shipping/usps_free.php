<?php 
class ModelShippingUspsFree extends Model {
	public function getQuote($address) {
		$this->language->load('shipping/usps_free');
		
		$quote_data = array();

		//No this shipping option available if cart total less than minimum limit
		if ($this->cart->getTotal() < (float)$this->config->get('usps_free_min_limit')) {
			return $quote_data;
		}

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "geo_zone ORDER BY name");

        foreach ($query->rows as $result) {
			$status = true;
			if ($this->config->get('usps_free_' . $result['geo_zone_id'] . '_status')) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$result['geo_zone_id'] . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

				if ($query->num_rows) {
					$status = true;
				} else {
					$status = false;
				}

			} else {
				$status = false;
			}
			if ($status) {
				$cost = '';
				$weight = $this->cart->getWeight();

				$rates = explode(',', $this->config->get('usps_free_' . $result['geo_zone_id'] . '_rate'));
				foreach ($rates as $rate) {
					$data = explode(':', $rate);

					//To convert LBS from extension weight settings to OZ
					if ($this->config->get('config_weight_class_id') == 6) {
						$data[0] *= 16;
					}
					if ($data[0] >= $weight) {
						if (isset($data[1])) {
							$cost = $data[1];
						}
						break;
					}
				}

                $wcid = $wcid2 = $this->config->get('config_weight_class_id');

                if ($wcid == 1) { //Kg
                    $wcid2 = 5; //Lbs
                } elseif ($wcid == 2) { //gr
                    $wcid2 = 6; //oz
                } elseif ($wcid == 5) {
                    $wcid2 = 1;
                } elseif ($wcid == 6) {
                    $wcid2 = 2;
                }
                $w2 = $this->weight->convert($weight, $wcid, $wcid2);
                $old_cost = $cost;

				if ((string)$cost != '') {

                    //if (isset($this->session->data['estimator_total_only'])) {
                    //    $total_only = $this->session->data['estimator_total_only']['value'];
                    //} else {
                        $total_only = $this->cart->getTotal();
                    //}
                    //
                    //$cst - то что нужно отнять от общей суммы, что не участвует в подсчете Spend X for Free (налоги, доставка)
                    $cst = isset($this->session->data['shipping_method']['cost']) ? $this->session->data['shipping_method']['cost'] : 0;
                    foreach ($t = $this->cart->getTaxes() as $tx) {
                        $cst += $tx;
                    }
                    $total_only = $total_only - $cst;

                    $al_min_limit = (float)$this->config->get('usps_free_free_limit');
                    $al_min_limit_ca = (float)$this->config->get('usps_free_free_limit_ca');

                    if ($address['country_id'] == 38) {
                        $al_min_limit = $al_min_limit_ca;
                    }
                    if ($total_only >= $al_min_limit) {
						$cost = '0.0';
						$text = 'FREE';
						//$text = $this->currency->format($this->tax->calculate($cost, $this->config->get('usps_free_tax_class_id'), $this->config->get('config_tax')));
                    } else {
					//	//$text = $this->currency->format($this->tax->calculate($cost, $this->config->get('usps_free_tax_class_id'), $this->config->get('config_tax')));
                     //   //LIAL Скидки на последующий способ в зависимости от предыдушего способа оплаты
                     //   //https://app.asana.com/0/31781288216855/81755299777336
                     //   if (($total_only < $al_min_limit)) {
                     //       $cost = round($cost - $cost * ($total_only / $al_min_limit), 1);
                     //   }
                        $text = $this->currency->format($this->tax->calculate($cost, $this->config->get('usps_free_tax_class_id'), $this->config->get('config_tax')));
					}

					$quote_data['usps_free_' . $result['geo_zone_id']] = array(
						'code'         => 'usps_free.usps_free_' . $result['geo_zone_id'],
						//'title'        => $result['name'] . '  (' . $this->language->get('text_weight') . ' ' . $this->weight->format($weight, $this->config->get('config_weight_class_id')) . ')',
						'title'        => $this->language->get('text_title'),
						//'weight'       => $this->language->get('text_weight') . '<br>' . $this->weight->format($weight, $this->config->get('config_weight_class_id')),
                        'weight'       => $this->weight->format($weight, $wcid, $this->language->get('decimal_point'), $this->language->get('thousand_point')) . ' / ' . $this->weight->format($w2, $wcid2, $this->language->get('decimal_point'), $this->language->get('thousand_point')),
                        'cost'         => $cost,
                        'old_cost'     => $this->currency->format($this->tax->calculate($old_cost, $this->config->get('usps_free_tax_class_id'), $this->config->get('config_tax'))),
                        //'left_to_free' => $al_min_limit - $total_only,
						'tax_class_id' => $this->config->get('usps_free_tax_class_id'),
						'text'         => $text,
						'delivery'     => $this->config->get("usps_free_{$result['geo_zone_id']}_delivery"),
					);
				}
			}
		}

		$method_data = array();

		if ($quote_data) {
			$method_data = array(
				'code'       => 'usps_free',
				'title'      => $this->language->get('text_title'),
				'us_only'    => $this->config->get('usps_free_us_only'),
				'min_limit'  => (float)$this->config->get('usps_free_min_limit'),
				//'free_limit' => (float)$this->config->get('weight_usps_free_limit'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('usps_free_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}

	//For Shipping Estimator only
	public function getQuoteRange($address) {
		$this->language->load('shipping/usps_free');

		$quote_data = array();

		//No this shipping option available if cart total less than minimum limit
		if ($this->cart->getTotal() < (float)$this->config->get('usps_free_min_limit')) {
			return $quote_data;
		}

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "geo_zone ORDER BY name");
		foreach ($query->rows as $result) {
			$status = true;

			if ($this->config->get('usps_free_' . $result['geo_zone_id'] . '_status')) {
				if (isset($address['zone_id']) && $address['zone_id'] !== 'null' && $address['zone_id'] != '0') {
					$q = "SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$result['geo_zone_id'] . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')";
					$query = $this->db->query($q);

					if ($query->num_rows) {
						$status = true;
					} else {
						$status = false;
					}
				} else { //We have no zone_id and need to show cost range from min cost to max
					$q = "SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$result['geo_zone_id'] . "' AND country_id = '" . (int)$address['country_id']."'";
					$query = $this->db->query($q);

					if ($query->num_rows) {
						$status = true;
					} else {
						$status = false;
					}
				}
			} else {
				$status = false;
			}
			//echo '<pre>'; var_dump($status); echo '</pre>';
			if ($status) {
				$cost = '';
				$weight = $this->cart->getWeight();

				$rates = explode(',', $this->config->get('usps_free_' . $result['geo_zone_id'] . '_rate'));
				foreach ($rates as $rate) {
					$data = explode(':', $rate);

					//To convert LBS from extension weight settings to OZ
					if ($this->config->get('config_weight_class_id') == 6) {
						$data[0] *= 16;
					}
					if ($data[0] >= $weight) {
						if (isset($data[1])) {
							$cost = $data[1];
						}
						break;
					}
				}
				if ((string)$cost != '') {
					if (($this->cart->getTotal() > (float)$this->config->get('usps_free_free_limit')) && $address['country_id'] == 223) {
						$cost = '0';
						//$text = 'FREE';
						$text = $this->currency->format($this->tax->calculate($cost, $this->config->get('usps_free_tax_class_id'), $this->config->get('config_tax')));
					} else {
						$text = $this->currency->format($this->tax->calculate($cost, $this->config->get('usps_free_tax_class_id'), $this->config->get('config_tax')));
					}
					$quote_data['usps_free_' . $result['geo_zone_id']] = array(
						'code'         => 'usps_free.usps_free_' . $result['geo_zone_id'],
						'title'        => $result['name'] . '  (' . $this->language->get('text_weight') . ' ' . $this->weight->format($weight, $this->config->get('config_weight_class_id')) . ')',
						'weight'       => $this->language->get('text_weight') . '<br>' . $this->weight->format($weight, $this->config->get('config_weight_class_id')),
						'cost'         => $cost,
						'tax_class_id' => $this->config->get('usps_free_tax_class_id'),
						'text'         => $text,
						'delivery'     => $this->config->get("usps_free_{$result['geo_zone_id']}_delivery"),
					);
				}
			}
		}

		$method_data = array();

		if ($quote_data) {
			$method_data = array(
				'code'       => 'usps',
				'title'      => $this->language->get('text_title'),
				'us_only'    => $this->config->get('usps_free_us_only'),
				'min_limit'  => (float)$this->config->get('usps_free_min_limit'),
				//'free_limit' => (float)$this->config->get('usps_free_free_limit'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('usps_free_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}
}
?>