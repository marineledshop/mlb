<?php 
class ModelShippingWeightUsps extends Model {

	public function getQuote($address) {
		$this->language->load('shipping/weight_usps');

		$quote_data = array();

		//No this shipping option available if cart total less than minimum limit
		if ($this->cart->getTotal() < (float)$this->config->get('weight_usps_min_limit')) {
			return $quote_data;
		}

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "geo_zone ORDER BY name");
		foreach ($query->rows as $result) {
			if ($this->config->get('weight_usps_' . $result['geo_zone_id'] . '_status')) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$result['geo_zone_id'] . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

				if ($query->num_rows) {
					$status = true;
				} else {
					$status = false;
				}

			} else {
				$status = false;
			}
			if ($status) {
				$cost = '';
				$weight = $this->cart->getWeight();

				$rates = explode(',', $this->config->get('weight_usps_' . $result['geo_zone_id'] . '_rate'));
				foreach ($rates as $rate) {
					$data = explode(':', $rate);

					//To convert LBS from extension weight settings to OZ
					if ($this->config->get('config_weight_class_id') == 6) {
						$data[0] *= 16;
					}
					if ($data[0] >= $weight) {
						if (isset($data[1])) {
							$cost = $data[1];
						}
						break;
					}
				}

                $wcid = $wcid2 = $this->config->get('config_weight_class_id');

                if ($wcid == 1) { //Kg
                    $wcid2 = 5; //Lbs
                } elseif ($wcid == 2) { //gr
                    $wcid2 = 6; //oz
                } elseif ($wcid == 5) {
                    $wcid2 = 1;
                } elseif ($wcid == 6) {
                    $wcid2 = 2;
                }
                $w2 = $this->weight->convert($weight, $wcid, $wcid2);
                $old_cost = $cost;
				
				if ((string)$cost != '') {
                    //$al_total = $this->cart->getDiscountedSubTotal();
					$total_only = $this->cart->getTotal();

					//$cst - то что нужно отнять от общей суммы, что не участвует в подсчете Spend X for Free (налоги, доставка)
					$cst = isset($this->session->data['shipping_method']['cost']) ? $this->session->data['shipping_method']['cost'] : 0;
					foreach ($t = $this->cart->getTaxes() as $tx) {
						$cst += $tx;
					}
					$total_only = $total_only - $cst;

                    $al_min_limit = $address['country_id'] == 223 ? (float)$this->config->get('weight_usps_free_limit_us') : (float)$this->config->get('weight_usps_free_limit');
                    if (($total_only > $al_min_limit)) {
                        $cost = '0.0';
                        $text = 'FREE';
                    } else {
                        //LIAL Скидки на последующий способ в зависимости от предыдушего способа оплаты
                        //https://app.asana.com/0/31781288216855/81755299777336
                        //if (($al_total <= $al_min_limit)) {
                        //    $cost = round($cost - $cost * ($al_total / $al_min_limit), 1);
                        //}
                        $text = $this->currency->format($this->tax->calculate($cost, $this->config->get('weight_usps_tax_class_id'), $this->config->get('config_tax')));
                    }

                    //if (isset($this->session->data['estimator_total_only'])) {
                    //    $total_only = $this->session->data['estimator_total_only']['value'];
                    //} else {
                    //    $total_only = $this->cart->getTotal();
                    //}

					$quote_data['weight_usps_' . $result['geo_zone_id']] = array(
						'code'         => 'weight_usps.weight_usps_' . $result['geo_zone_id'],
						'title'        => $this->language->get('text_title') . '  (' . $this->language->get('text_weight') . ' ' . $this->weight->format($weight, $this->config->get('config_weight_class_id')) . ')',
						//'weight'       => $this->language->get('text_weight') . '<br>' . $this->weight->format($weight, $this->config->get('config_weight_class_id')),
                        'weight'       => $this->weight->format($weight, $wcid, $this->language->get('decimal_point'), $this->language->get('thousand_point')) . ' / ' . $this->weight->format($w2, $wcid2, $this->language->get('decimal_point'), $this->language->get('thousand_point')),
						'cost'         => $cost,
                        'old_cost'     => $this->currency->format($this->tax->calculate($old_cost, $this->config->get('weight_usps_tax_class_id'), $this->config->get('config_tax'))),
                        //'left_to_free' => $al_min_limit - $total_only,
						'tax_class_id' => $this->config->get('weight_usps_tax_class_id'),
						'text'         => empty($cost) ? 'FREE' : $text,
						'delivery'     => $this->config->get("weight_usps_{$result['geo_zone_id']}_delivery"),
					);	
				}
			}
		}

		$method_data = array();

		if ($quote_data) {
			$method_data = array(
				'code'       => 'weight_usps',
				'title'      => $this->language->get('text_title'),
				'us_only'    => $this->config->get('weight_usps_us_only'),
				'min_limit'  => (float)$this->config->get('weight_usps_min_limit'),
				//'free_limit' => (float)$this->config->get('weight_usps_free_limit'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('weight_usps_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}

	//For Shipping Estimator only
	public function getQuoteRange($address) {
		$this->language->load('shipping/weight_usps');

		$quote_data = array();

		//No this shipping option available if cart total less than minimum limit
		if ($this->cart->getTotal() < (float)$this->config->get('weight_usps_min_limit')) {
			return $quote_data;
		}

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "geo_zone ORDER BY name");
		foreach ($query->rows as $result) {
			$status = true;

			if ($this->config->get('weight_usps_' . $result['geo_zone_id'] . '_status')) {
				if (isset($address['zone_id']) && $address['zone_id'] !== 'null' && $address['zone_id'] != '0') {
					$q = "SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$result['geo_zone_id'] . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')";
					$query = $this->db->query($q);

					if ($query->num_rows) {
						$status = true;
					} else {
						$status = false;
					}
				} else { //We have no zone_id and need to show cost range from min cost to max
					$q = "SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$result['geo_zone_id'] . "' AND country_id = '" . (int)$address['country_id']."'";
					$query = $this->db->query($q);

					if ($query->num_rows) {
						$status = true;
					} else {
						$status = false;
					}
				}
			} else {
				$status = false;
			}
			if ($status) {
				$cost = '';
				$weight = $this->cart->getWeight();

				$rates = explode(',', $this->config->get('weight_usps_' . $result['geo_zone_id'] . '_rate'));
				foreach ($rates as $rate) {
					$data = explode(':', $rate);

					//To convert LBS from extension weight settings to OZ
					if ($this->config->get('config_weight_class_id') == 6) {
						$data[0] *= 16;
					}
					if ($data[0] >= $weight) {
						if (isset($data[1])) {
							$cost = $data[1];
						}
						break;
					}
				}
				if ((string)$cost != '') {
                    $al_total = $this->cart->getTotal();
                    $al_min_limit = (float)$this->config->get('weight_usps_free_limit');

                    if (($al_total > $al_min_limit)) {
						$cost = '0.0';
						$text = 'FREE';
					} else {
                        //LIAL Скидки на последующий способ в зависимости от предыдушего способа оплаты
                        //https://app.asana.com/0/31781288216855/81755299777336
                        if (($al_total < $al_min_limit)) {
                            $cost = round($cost - $cost * ($al_total / $al_min_limit), 1);
                        }
                        $text = $this->currency->format($this->tax->calculate($cost, $this->config->get('weight_usps_tax_class_id'), $this->config->get('config_tax')));
					}
					$quote_data['weight_usps_' . $result['geo_zone_id']] = array(
						'code'         => 'weight_usps.weight_usps_' . $result['geo_zone_id'],
						'title'        => $result['name'] . '  (' . $this->language->get('text_weight') . ' ' . $this->weight->format($weight, $this->config->get('config_weight_class_id')) . ')',
						'weight'       => $this->language->get('text_weight') . '<br>' . $this->weight->format($weight, $this->config->get('config_weight_class_id')),
						'cost'         => $cost,
                        'left_to_free' => (float)$this->config->get('weight_usps_free_limit') - $this->cart->getTotal(),
						'tax_class_id' => $this->config->get('weight_usps_tax_class_id'),
						'text'         => empty($cost) ? 'FREE' : $text,
						'delivery'     => $this->config->get("weight_usps_{$result['geo_zone_id']}_delivery"),
					);
				}
			}
		}

		$method_data = array();

		if ($quote_data) {
			$method_data = array(
				'code'       => 'weight',
				'title'      => $this->language->get('text_title'),
				'us_only'    => $this->config->get('weight_usps_us_only'),
				'min_limit'  => (float)$this->config->get('weight_usps_min_limit'),
				//'free_limit' => (float)$this->config->get('weight_usps_free_limit'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('weight_usps_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}
}
?>