<?php
class ModelShippingFreeUsps extends Model {
	function getQuote($address) {
		$this->language->load('shipping/free_usps');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('free_usps_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if (!$this->config->get('free_usps_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}
		if ($this->cart->getSubTotal() < $this->config->get('free_usps_total')) {
			$status = false;
		}

        //If set US Only but country not USA, will not show this shipping method
        if ($status == true && $this->config->get('free_usps_us_only') == 1 && $address['country_id'] != 223) {
            $status = false;
        }
		$method_data = array();

		if ($status) {
			$quote_data = array();
			$weight = $this->cart->getWeight();

			$fn = 'free_shipping.png';
			if (!file_exists(DIR_IMAGE.$fn)) {
				$image = '';
			} else {
				$image = HTTP_SERVER.'image/'.$fn;
			}

            $wcid = $wcid2 = $this->config->get('config_weight_class_id');

            if ($wcid == 1) { //Kg
                $wcid2 = 5; //Lbs
            } elseif ($wcid == 2) { //gr
                $wcid2 = 6; //oz
            } elseif ($wcid == 5) {
                $wcid2 = 1;
            } elseif ($wcid == 6) {
                $wcid2 = 2;
            }
            $w2 = $this->weight->convert($weight, $wcid, $wcid2);

			$quote_data['usps'] = array(
				'code'         => 'free_usps.usps',
				'title'        => $this->language->get('text_description'),
				'cost'         => 0.00,
				'tax_class_id' => 0,
				'image'        => $image,
				'text'         => $this->currency->format(0.00), //'FREE',
				//'weight'       => $this->language->get('text_weight') . '<br>' . $this->weight->format($weight, $this->config->get('config_weight_class_id')),
                'weight'       => $this->weight->format($weight, $wcid, $this->language->get('decimal_point'), $this->language->get('thousand_point')) . ' / ' . $this->weight->format($w2, $wcid2, $this->language->get('decimal_point'), $this->language->get('thousand_point')),
				'delivery'     => $this->config->get("free_usps_delivery"),
			);

			$method_data = array(
				'code'       => 'free',
				'title'      => $this->language->get('text_title'),
				'us_only'    => $this->config->get('free_usps_us_only'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('free_usps_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}
}
