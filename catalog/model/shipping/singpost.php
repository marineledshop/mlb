<?php 
class ModelShippingsingpost extends Model {    
  	public function getQuote($address) {
		$this->load->language('shipping/singpost');
		
		$quote_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "geo_zone ORDER BY name");
	
		foreach ($query->rows as $result) {
			if ($this->config->get('singpost_' . $result['geo_zone_id'] . '_status')) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$result['geo_zone_id'] . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
			
				if ($query->num_rows) {
					$status = true;
				} else {
					$status = false;
				}
			} else {
				$status = false;
			}
		
			if ($status) {
				$cost = 0;
				$weight = $this->cart->getWeight();
                $realCost = null;

				$rates = explode(',', $this->config->get('singpost_' . $result['geo_zone_id'] . '_rate'));

				foreach ($rates as $rate) {
					$data = explode(':', $rate);
				
					if ($data[0] >= $weight) {
						if (isset($data[1])) {
							$realCost = $data[1];
						}
				
						break;
					}
				}

                $quote_data['singpost_' . $result['geo_zone_id']] = array(
                    'code'         => 'singpost.singpost_' . $result['geo_zone_id'],
                    'title'        => $this->language->get('text_title') . '  (' . $this->language->get('text_weight') . ' ' . $this->weight->format($weight, $this->config->get('config_weight_class_id')) . ')',
                    'cost'         => $cost,
                    'real_cost'    => $realCost ?: $cost,
                    'tax_class_id' => $this->config->get('singpost_tax_class_id'),
                    'text'         => $this->currency->format($this->tax->calculate($cost, $this->config->get('singpost_tax_class_id'), $this->config->get('config_tax'))),
                    'delivery'     => $this->config->get("singpost_{$result['geo_zone_id']}_delivery"),
                );
			}
		}
		
		$method_data = array();
	
		if ($quote_data) {
      		$method_data = array(
        		'code'       => 'singpost',
        		'title'      => $this->language->get('text_title'),
        		'quote'      => $quote_data,
				'sort_order' => $this->config->get('singpost_sort_order'),
        		'error'      => false
      		);
		}
	
		return $method_data;
  	}
}
?>