<?php
class ModelShippingSpendx4free extends Model {
	function getQuote($address) {
		$this->load->language('shipping/spendx4free');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('free_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if (!$this->config->get('free_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		if ($this->cart->getSubTotal() < $this->config->get('dhl_free_price')) {
			$spend = $this->config->get('dhl_free_price') - $this->cart->getSubTotal();
		}else{
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$quote_data = array();

			$quote_data['free'] = array(
				'code'         => 'spendx4free',
				'title'        => sprintf($this->language->get('text_description'), $this->currency->format($spend)),
				'cost'         => $spend,
				'tax_class_id' => 0,
				'text'         => '<style type="text/css">#spendx4free{display:none;}</style>'
			);

      		$method_data = array(
        		'code'       => 'spendx4free',
                        'title'      => sprintf($this->language->get('text_description'), '$' . $spend),
        		/*'title'      => sprintf($this->language->get('text_title'), $this->currency->format(($this->config->get('free_total')-0.01))),*/
        		'quote'      => $quote_data,
				'sort_order' => $this->config->get('spendx4free_sort_order'),
        		'error'      => false
      		);
		}

		return $method_data;
	}
}
?>