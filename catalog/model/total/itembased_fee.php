<?php
//==============================================================================
// Item-Based Fee/Discount v155.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
//==============================================================================

class ModelTotalItembasedFee extends Model {
	private $type = 'total';
	private $name = 'itembased_fee';
	
	private function getSetting($setting) {
		$value = $this->config->get($this->name . '_' . $setting);
		return (is_string($value) && strpos($value, 'a:') === 0) ? unserialize($value) : $value;
	}
	
	public function getTotal(&$total_data, &$order_total, &$taxes) {
		if (!$this->getSetting('status') || !$this->getSetting('data')) {
			return;
		}
		
		$version = (!defined('VERSION')) ? 140 : (int)substr(str_replace('.', '', VERSION), 0, 3);
		
		$default_currency = $this->config->get('config_currency');
		$currency = $this->session->data['currency'];
		$language = $this->session->data['language'];
		$length_unit = ($version < 151) ? 'length_class' : 'length_class_id';
		
		if ($this->type == 'shipping') {
			$shipping_geozones = array();
			$geozones = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '0' OR zone_id = '" . (int)$address['zone_id'] . "')");
			foreach ($geozones->rows as $geozone) {
				$shipping_geozones[] = $geozone['geo_zone_id'];
			}
			$shipping_postcode = preg_replace('/[^A-Za-z0-9 ]/', '', isset($address['postcode']) ? $address['postcode'] : '');
			
			$keycode = ($version < 150) ? 'key' : 'code';
			$total_data = array();
			$order_total = 0;
			$taxes = $this->cart->getTaxes();
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "extension WHERE `type` = 'total'");
			$order_totals = $query->rows;
			$sort_order = array();
			foreach ($order_totals as $key => $value) $sort_order[$key] = $this->config->get($value[$keycode] . '_sort_order');
			array_multisort($sort_order, SORT_ASC, $order_totals);
			foreach ($order_totals as $ot) {
				if ($ot[$keycode] == $this->type) break;
				if ($this->config->get($ot[$keycode] . '_status')) {
					$this->load->model('total/' . $ot[$keycode]);
					$this->{'model_total_' . $ot[$keycode]}->getTotal($total_data, $order_total, $taxes);
				}
			}
		} else {
			$this->load->model('account/address');
			foreach (array('shipping', 'payment') as $address_type) {
				$address = array();
				if ($this->customer->isLogged()) 								$address = $this->model_account_address->getAddress($this->customer->getAddressId());
				if (isset($this->session->data['country_id']))					$address['country_id'] = $this->session->data['country_id'];
				if (isset($this->session->data['zone_id']))						$address['zone_id'] = $this->session->data['zone_id'];
				if (isset($this->session->data['postcode']))					$address['postcode'] = $this->session->data['postcode'];
				if (isset($this->session->data['shipping_country_id']))			$address['country_id'] = $this->session->data['shipping_country_id'];
				if (isset($this->session->data['shipping_zone_id']))			$address['zone_id'] = $this->session->data['shipping_zone_id'];
				if (isset($this->session->data['shipping_postcode']))			$address['postcode'] = $this->session->data['shipping_postcode'];
				if (isset($this->session->data['guest']))						$address = $this->session->data['guest'];
				if (isset($this->session->data['guest'][$address_type]))		$address = $this->session->data['guest'][$address_type];
				if (isset($this->session->data[$address_type . '_address_id']))	$address = $this->model_account_address->getAddress($this->session->data[$address_type . '_address_id']);		
				if (isset($this->session->data[$address_type . '_country_id']))	$address['country_id'] = $this->session->data[$address_type . '_country_id'];
				if (isset($this->session->data[$address_type . '_zone_id']))	$address['zone_id'] = $this->session->data[$address_type . '_zone_id'];
				if (isset($this->session->data[$address_type . '_postcode']))	$address['postcode'] = $this->session->data[$address_type . '_postcode'];
				if (empty($address['country_id']))								$address['country_id'] = $this->config->get('config_country_id');
				if (empty($address['zone_id']))									$address['zone_id'] =  $this->config->get('config_zone_id');
				if (empty($address['postcode']))								$address['postcode'] = '';
				${$address_type.'_geozones'} = array();
				$geozones = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '0' OR zone_id = '" . (int)$address['zone_id'] . "')");
				foreach ($geozones->rows as $geozone) {
					${$address_type.'_geozones'}[] = $geozone['geo_zone_id'];
				}
				${$address_type.'_postcode'} = preg_replace('/[^A-Za-z0-9 ]/', '', $address['postcode']);
			}
		}
		
		// Get rates
		$this->load->model('catalog/product');
		$quote_data = array();
		
		foreach ($this->getSetting('data') as $row_num => $row) {
			// Check Order Criteria
			$geozone_comparison = ($this->type == 'shipping') ? 'shipping' : $row['geozone_comparison'];
			if (empty($row['stores']) ||
				!in_array((int)$this->config->get('config_store_id'), $row['stores']) ||
				empty($row['currencys']) ||
				(!in_array('autoconvert', $row['currencys']) && !in_array($currency, $row['currencys'])) ||
				empty($row['customer_groups']) ||
				!in_array((int)$this->customer->getCustomerGroupId(), $row['customer_groups']) ||
				empty($row['geo_zones']) ||
				(empty(${$geozone_comparison.'_geozones'}) && !in_array(0, $row['geo_zones'])) ||
				(!empty(${$geozone_comparison.'_geozones'}) && !array_intersect($row['geo_zones'], ${$geozone_comparison.'_geozones'})) ||
				empty($row['costs'])
			) {
				continue;
			}
			
			// Generate Comparison Values
			$prediscounted = 0;
			$subtotal = 0;
			$taxed = 0;
			$total = $order_total;
			// Extension-specific
			$item = 0;
			// end
			
			foreach ($this->cart->getProducts() as $product) {
				if (!$product['shipping'] && $this->type == 'shipping') continue;
				
				$product_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product WHERE product_id = " . (int)$product['product_id']);
				$product_info = $this->model_catalog_product->getProduct($product['product_id']);
				$price = ($product_info['special']) ? $product_info['special'] : $product_info['price'];
				
				$prediscounted += $product['total'] + ($product['quantity'] * ($product_query->row['price'] - $price));
				$subtotal += $product['total'];
				$taxed += $this->tax->calculate($product['total'], $product['tax_class_id'], $this->config->get('config_tax'));
				
				// Extension-specific
				$item += $product['quantity'];
				// end
			}
			
			// Check Cart Criteria
			$autoconvert = (!in_array($currency, $row['currencys']));
			$conversion_currency = $row['currencys'][0];
			if ($conversion_currency == 'autoconvert') {
				$conversion_currency = (isset($row['currencys'][1])) ? $row['currencys'][1] : $default_currency;
			}
			
			$total_value = ${$row['total_value']};
			if (strpos($this->name, 'totalbased') === 0) {
				$total_value += (strpos($row['adjustment'], '%')) ? $total_value * (float)$row['adjustment'] / 100 : (float)$row['adjustment'];
			}
			$total_value = $this->currency->convert($total_value, $default_currency, $currency);
			$total_value = ($autoconvert) ? $this->currency->convert($total_value, $currency, $conversion_currency) : $total_value;
			
			// Calculate Cost
			$comparison_value = ${str_replace(array('based', '_fee'), '', $this->name)};
			if (strpos($this->name, 'totalbased') !== 0 && strpos($this->name, 'postcodebased') !== 0) {
				$comparison_value += (strpos($row['adjustment'], '%')) ? $comparison_value * (float)$row['adjustment'] / 100 : (float)$row['adjustment'];
			}
			$cost = 0;
			$round = (float)$this->getSetting('round');
			
			for ($i = 0; $i < count($row['costs']['from']); $i++) {
				// Extension-specific
				$from = (!empty($row['costs']['from'][$i])) ? (float)$row['costs']['from'][$i] : 0;
				$to = (!empty($row['costs']['to'][$i])) ? (float)$row['costs']['to'][$i] : 999999;
				
				$top = min($to, $comparison_value);
				$bottom = ($row['final_cost'] == 'single') ? 0 : $from;
				$difference = $top - $bottom;
				
				$bracket_match = (round($from, 3) <= round($comparison_value, 3) && round($comparison_value, 3) <= round($to, 3));
				// end
				
				$multiplier = (!empty($row['costs']['per'][$i])) ? ceil($difference / (float)$row['costs']['per'][$i]) : 1;
				$charge = (float)$row['costs']['charge'][$i] * $multiplier;
				$charge *= (strpos($row['costs']['charge'][$i], '%')) ? $total_value / 100 : 1;
				$cost = ($row['final_cost'] == 'single') ? $charge : $cost + $charge;
				
				if ($bracket_match) {
					$cost += (strpos($row['add_cost'], '%')) ? $total_value * (float)$row['add_cost'] / 100 : (float)$row['add_cost'];
					$cost = ($row['min_cost'] && $cost < (float)$row['min_cost']) ? (float)$row['min_cost'] : $cost;
					$cost = ($row['max_cost'] && $cost > (float)$row['max_cost']) ? (float)$row['max_cost'] : $cost;
					
					$cost = ($autoconvert) ? $this->currency->convert($cost, $conversion_currency, $currency) : $cost;
					$cost = $this->currency->convert($cost, $currency, $default_currency);
					$cost = ($round) ? round($cost / $round) * $round : $cost;
					if ($cost == 0 && $this->type == 'total') continue;
					
					if ($this->type == 'shipping') {
						$quote_data[$this->name . '_' . $row_num] = array(
							'id'			=> $this->name . '.' . $this->name . '_' . $row_num,
							'code'			=> $this->name . '.' . $this->name . '_' . $row_num,
							'title'			=> html_entity_decode($row['title'][$language], ENT_QUOTES, 'UTF-8'),
							'cost'			=> $cost,
							'tax_class_id'	=> $row['tax_class_id'],
							'text'			=> $this->currency->format($this->tax->calculate($cost, $row['tax_class_id'], $this->config->get('config_tax')))
						);
					} else {
						$total_data[] = array(
							'code'			=> $this->name,
							'title'			=> html_entity_decode($row['title'][$language], ENT_QUOTES, 'UTF-8') . ($version < 150 ? ':' : ''),
							'text'			=> $this->currency->format($cost),
							'value'			=> $cost,
							'sort_order'	=> $this->getSetting('sort_order')
						);
						
						$tax_class_id = $row['tax_class_id'];
						if ($tax_class_id) {
							if (method_exists($this->tax, 'getRates')) {
								$tax_rates = $this->tax->getRates($cost, $tax_class_id);
								foreach ($tax_rates as $tax_rate) {
									$taxes[$tax_rate['tax_rate_id']] = (isset($taxes[$tax_rate['tax_rate_id']])) ? $taxes[$tax_rate['tax_rate_id']] : 0;
									$taxes[$tax_rate['tax_rate_id']] += $tax_rate['amount'];
								}
							} else {
								$taxes[$tax_class_id] = (isset($taxes[$tax_class_id])) ? $taxes[$tax_class_id] : 0;
								$taxes[$tax_class_id] += $cost * $this->tax->getRate($tax_class_id) / 100;
							}
						}
						
						$order_total += $cost;
					}
					
					break;
				}
			}
		}
		
		if ($this->type == 'shipping') {
			$method_data = array();
			if ($quote_data) {
				$sort_by_cost = array();
				foreach ($quote_data as $key => $value) $sort_by_cost[$key] = $value['cost'];
				array_multisort($sort_by_cost, SORT_ASC, $quote_data);
				
				$heading = $this->getSetting('heading');
				$method_data = array(
					'id'			=> $this->name,
					'code'			=> $this->name,
					'title'			=> html_entity_decode($heading[$language], ENT_QUOTES, 'UTF-8'),
					'quote'			=> $quote_data,
					'sort_order'	=> $this->getSetting('sort_order'),
					'error'			=> false
				);
			}
			return $method_data;
		}
	}
	
	
	public function getRates()
	{
		if (!$this->getSetting('status') || !$this->getSetting('data'))
		{
			return;
		}
		
		$disc = array();
		
		$default_currency = $this->config->get('config_currency');
		$currency = $this->session->data['currency'];
		
		$this->load->model('account/address');
		foreach (array('shipping', 'payment') as $address_type) {
			$address = array();
			if ($this->customer->isLogged()) 								$address = $this->model_account_address->getAddress($this->customer->getAddressId());
			if (isset($this->session->data['country_id']))					$address['country_id'] = $this->session->data['country_id'];
			if (isset($this->session->data['zone_id']))						$address['zone_id'] = $this->session->data['zone_id'];
			if (isset($this->session->data['postcode']))					$address['postcode'] = $this->session->data['postcode'];
			if (isset($this->session->data['shipping_country_id']))			$address['country_id'] = $this->session->data['shipping_country_id'];
			if (isset($this->session->data['shipping_zone_id']))			$address['zone_id'] = $this->session->data['shipping_zone_id'];
			if (isset($this->session->data['shipping_postcode']))			$address['postcode'] = $this->session->data['shipping_postcode'];
			if (isset($this->session->data['guest']))						$address = $this->session->data['guest'];
			if (isset($this->session->data['guest'][$address_type]))		$address = $this->session->data['guest'][$address_type];
			if (isset($this->session->data[$address_type . '_address_id']))	$address = $this->model_account_address->getAddress($this->session->data[$address_type . '_address_id']);		
			if (isset($this->session->data[$address_type . '_country_id']))	$address['country_id'] = $this->session->data[$address_type . '_country_id'];
			if (isset($this->session->data[$address_type . '_zone_id']))	$address['zone_id'] = $this->session->data[$address_type . '_zone_id'];
			if (isset($this->session->data[$address_type . '_postcode']))	$address['postcode'] = $this->session->data[$address_type . '_postcode'];
			if (empty($address['country_id']))								$address['country_id'] = $this->config->get('config_country_id');
			if (empty($address['zone_id']))									$address['zone_id'] =  $this->config->get('config_zone_id');
			if (empty($address['postcode']))								$address['postcode'] = '';
			${$address_type.'_geozones'} = array();
			$geozones = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '0' OR zone_id = '" . (int)$address['zone_id'] . "')");
			foreach ($geozones->rows as $geozone) {
				${$address_type.'_geozones'}[] = $geozone['geo_zone_id'];
			}
			${$address_type.'_postcode'} = preg_replace('/[^A-Za-z0-9 ]/', '', $address['postcode']);
		}
		
		$round = (float)$this->getSetting('round');
			
		foreach ($this->getSetting('data') as $row_num => $row)
		{
			$geozone_comparison = ($this->type == 'shipping') ? 'shipping' : $row['geozone_comparison'];
			if (empty($row['stores']) ||
				!in_array((int)$this->config->get('config_store_id'), $row['stores']) ||
				empty($row['currencys']) ||
				(!in_array('autoconvert', $row['currencys']) && !in_array($currency, $row['currencys'])) ||
				empty($row['customer_groups']) ||
				!in_array((int)$this->customer->getCustomerGroupId(), $row['customer_groups']) ||
				empty($row['geo_zones']) ||
				(empty(${$geozone_comparison.'_geozones'}) && !in_array(0, $row['geo_zones'])) ||
				(!empty(${$geozone_comparison.'_geozones'}) && !array_intersect($row['geo_zones'], ${$geozone_comparison.'_geozones'})) ||
				empty($row['costs'])
			) {
				continue;
			}
		
			foreach($row['costs']['from'] as $key=>$from)
			{
				$disc[$row_num]['from'] = (!empty($from)) ? (float)$from : 1;
				$disc[$row_num]['charge'] = $row['costs']['charge'][$key];
				$disc[$row_num]['adjustment'] = $row['adjustment'];
				$disc[$row_num]['round'] = $round;

			}
		}
		return $disc; 
	}
	
}
?>