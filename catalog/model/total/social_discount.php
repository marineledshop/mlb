<?php
class ModelTotalSocialDiscount extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
	
	
			$this->load->language('total/social_discount');
			$this->load->model('checkout/social_discount');
			//unset($this->session->data['social_discount']);
			
			if(isset($this->session->data['social_discount'])) {
				$social_discounts = array_unique($this->session->data['social_discount']);
				
				foreach($social_discounts as $social_discount_id) {
					$social_discount_info[] = $this->model_checkout_social_discount->getSocialDiscount($social_discount_id);
				}
			
			if (isset($social_discount_info)) {
	
				foreach ($social_discount_info as $results) { 
				$result = $results[0];
			
			if(isset($result)) {
				
				$discount_total = 0;
				$sd_discount_id = $result['social_discount_id'];
				$sd_name = $result['name'];
				$sd_type = $result['type'];
				$sd_discount = $result['discount'];
				$sd_shipping = $result['shipping'];
				
				
				if ($sd_type == 'F') {
						$discount_total = $sd_discount;
					}
				else {
						$sub_total = $this->cart->getSubTotal();;
						$discount_total = ((($sub_total)*($sd_discount))/100);
						}
				
				$total_social_discounts_data[$sd_discount_id] = array(
					'social_discount_id'       => $sd_discount_id,
        				'title'                    => sprintf($this->language->get('text_social_discount'), $sd_name),
	    				'name'                     => $sd_name,
					'value'                    => $discount_total,
					'shipping'                 => $sd_shipping,
					'sort_order'               => $this->config->get('social_discount_sort_order')
					);
			}
	
		}
		
		}

	if(isset($total_social_discounts_data)) {
	foreach ($total_social_discounts_data as $key => $row) {
		$value[$key]  = $row['value'];
		}
	array_multisort($value, SORT_DESC, $total_social_discounts_data);		
	
	$cart_total = $this->cart->getSubTotal();
	
	if($this->config->get('social_discount_multiple') == 1) {
				
	// Multiple Social Discounts
			
		foreach ($total_social_discounts_data as $key => $row) {
			$row_value = $row['value'];
			if($total >= $row_value) {		
				
				if(!isset($shipping_discounted)) {
					if ($row['shipping'] && isset($this->session->data['shipping_method'])) {
						if (!empty($this->session->data['shipping_method']['tax_class_id'])) {
							$tax_rates = $this->tax->getRates($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id']);
							foreach ($tax_rates as $tax_rate) {
								$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
								}
							}
						$row_value += $this->session->data['shipping_method']['cost'];	
						$shipping_discounted = 1;
						}					
					}
					
				if($row_value > 0 & ($total >= $row_value)) {		
					$total_data[] = array(
						'code' => 'social_discount',
						'title'      => $row['title'],
						'text'       => $this->currency->format(-$row_value),
						'value'      => $row_value,
						'sort_order' => $row['sort_order']
						);
						
					$total -= $row_value;		
					}
				}
			}
		}				
		
		else {
		
		// Single Social Discount
		$row_value = $total_social_discounts_data[0]['value'];
		if($total >= $row_value) {	
			
				if ($total_social_discounts_data[0]['shipping'] && isset($this->session->data['shipping_method'])) {
					if (!empty($this->session->data['shipping_method']['tax_class_id'])) {
						$tax_rates = $this->tax->getRates($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id']);
						foreach ($tax_rates as $tax_rate) {
							$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
							}
						}
					$row_value += $this->session->data['shipping_method']['cost'];	
					}		
					
				if($row_value > 0 & ($total >= $row_value)) {	
					$total_data[] = array(
						'code' => 'social_discount',
						'title'      => $total_social_discounts_data[0]['title'],
						'text'       => $this->currency->format(-$row_value),
						'value'      => $row_value,
						'sort_order' => $total_social_discounts_data[0]['sort_order']
						);
					$total -= $row_value;			
					}
				}
			}
		}	
	} 
	}
		
	public function confirm($order_info, $order_total) {
		
		$this->load->model('checkout/social_discount');
		$code = '';
		
		$start = strpos($order_total['title'], '(') + 1;
		$end = strrpos($order_total['title'], ')');
		
		if ($start && $end) {  
			$code = substr($order_total['title'], $start, $end - $start);
		}	
		
		$this->load->model('checkout/social_discount');
		
		$social_discount_id = $this->model_checkout_social_discount->getSocialDiscountId($code);
	
		if ($social_discount_id) {
			$this->model_checkout_social_discount->redeem($social_discount_id, $order_info['order_id'], $order_info['customer_id'], $order_total['value']);	
		   }						
	    }
		
}
?>
