<?php

require_once(DIR_CATALOG.'tcpdf/tcpdf_class_dhl.php');

	$l = Array();
	$l['a_meta_charset'] = 'UTF-8';
	$l['a_meta_dir'] = 'ltr';
	$l['a_meta_language'] = 'en';
	$l['w_page'] = 'page';

	$this->load->language('sale/order');
	$txt = array(
		'direction' 		=> $this->language->get('direction'),
		'language' 			=> $this->language->get('code'),
		'text_invoice' 		=> $this->language->get('text_invoice'),
		'text_order_id' 	=> $this->language->get('text_order_id'),
		'text_invoice_no'	=> $this->language->get('text_invoice_no'),
		'text_invoice_date' => $this->language->get('text_invoice_date'),
		'text_date_added' 	=> $this->language->get('text_date_added'),
		'text_telephone'	=> $this->language->get('text_telephone'),
		'text_fax' 			=> $this->language->get('text_fax'),
		'text_to' 			=> $this->language->get('text_to'),
		'text_ship_to' 		=> $this->language->get('text_ship_to'),
		'column_product' 	=> $this->language->get('column_product'),
		'column_model' 		=> $this->language->get('column_model'),
		'column_quantity' 	=> $this->language->get('column_quantity'),
		'column_price' 		=> $this->language->get('column_price'),
		'column_total' 		=> $this->language->get('column_total'),
		'column_comment' 	=> $this->language->get('column_comment'),
		'subject'			=> 'Your Marine LED Shop order has been shipped - %s',

		'text_email_header'		=> '<p>Dear Sir/Madam,</p><p>Your order from Marine LED Shop has been shipped and your invoice you can download using the link below.</p>',
		'text_email_footer' => '',
    );

	$this->load->model('sale/order');
	$this->load->model('setting/setting');
	$this->load->model('catalog/product');
	$this->load->model('localisation/weight_class');

	$order = array();

	if (!isset($order_id) || empty($order_id)) {
		$order_id = $this->request->get['order_id'];
	}

	if (!isset($no_email)) {
		$no_email = $this->request->get['no_email'];
	} else {
		$no_email = '';
	}

	$order_info = $this->model_sale_order->getOrder($order_id);

	if($order_info)
	{

	//$this->load->language('shipping/'.$order_info['shipping_provider']);
/*
	$sh_placeholder = array(
		'{shipping_company}',
		'{shipping_site}',
		'{shipping_activation_days}',
		'{shipping_arrival_days}',
		'{track}'
	);

	$sh_replace = array(
		$this->language->get('heading_title'),
		$txt[$order_info['shipping_provider']]['site'],
		$txt[$order_info['shipping_provider']]['activation'],
		$order_info['d_keys'][0],
		$order_info['track']
	);
*/
	$store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);
	if ($store_info) {
		$store_address = $store_info['config_address'];
		$store_email = $store_info['config_email'];
		$store_telephone = $store_info['config_telephone'];
		$store_fax = $store_info['config_fax'];
	} else {
		$store_address = $this->config->get('config_address');
		$store_email = $this->config->get('config_email');
		$store_telephone = $this->config->get('config_telephone');
		$store_fax = $this->config->get('config_fax');
	}

	if ($order_info['invoice_no']) {
		$invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
	} else {
		$invoice_no = '';
	}

	if ($order_info['shipping_address_format']) {
		$format = $order_info['shipping_address_format'];
	} else {
		$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
	}

	$find = array(
		'{firstname}',
		'{lastname}',
		'{company}',
		'{address_1}',
		'{address_2}',
		'{city}',
		'{postcode}',
		'{zone}',
		'{zone_code}',
		'{country}'
	);

	$replace = array(
		'firstname' => $order_info['shipping_firstname'],
		'lastname'  => $order_info['shipping_lastname'],
		'company'   => $order_info['shipping_company'],
		'address_1' => $order_info['shipping_address_1'],
		'address_2' => $order_info['shipping_address_2'],
		'city'      => $order_info['shipping_city'],
		'postcode'  => $order_info['shipping_postcode'],
		'zone'      => $order_info['shipping_zone'],
		'zone_code' => $order_info['shipping_zone_code'],
		'country'   => $order_info['shipping_country']
	);

	$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

	if ($order_info['payment_address_format']) {
		$format = $order_info['payment_address_format'];
	} else {
		$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
	}

	$find = array(
		'{firstname}',
		'{lastname}',
		'{company}',
		'{address_1}',
		'{address_2}',
		'{city}',
		'{postcode}',
		'{zone}',
		'{zone_code}',
		'{country}'
	);

	$replace = array(
		'firstname' => $order_info['payment_firstname'],
		'lastname'  => $order_info['payment_lastname'],
		'company'   => $order_info['payment_company'],
		'address_1' => $order_info['payment_address_1'],
		'address_2' => $order_info['payment_address_2'],
		'city'      => $order_info['payment_city'],
		'postcode'  => $order_info['payment_postcode'],
		'zone'      => $order_info['payment_zone'],
		'zone_code' => $order_info['payment_zone_code'],
		'country'   => $order_info['payment_country']
	);

	$payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

	$product_data = array();
	$products = $this->model_sale_order->getOrderProducts($order_id);
	$wcid = 5; //Weight class ID for converting to 5=lbs, 6=oz

	foreach ($products as $product)
	{
		$product_weight_data = $this->model_catalog_product->getProduct($product['product_id']);

		//echo '<pre>'; var_dump($product_weight_data); echo '</pre>';
		if (!empty($product_weight_data)) {
			$weight_descr = $this->model_localisation_weight_class->getWeightClass($wcid);
			$err = '';
		} else {
			$err = 'Product with ID '.$product['product_id'].' not found in database';
		}

		$option_data = array();
		$options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);
		foreach ($options as $option)
		{
			if ($option['type'] != 'file') {
				$option_data[] = array(
									'name'  => $option['name'],
									'value' => $option['value']
										);
			} else {
				$option_data[] = array(
									'name'  => $option['name'],
									'value' => utf8_substr($option['value'], 0, strrpos($option['value'], '.'))
										);
			}
		}
		$product_data[] = array(
			'name'     => $product['name'],
			'model'    => $product['model'],
			'option'   => $option_data,
			'quantity' => $product['quantity'],
			'price'    => $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value']),
			'total'    => $this->currency->format($product['total'], $order_info['currency_code'], $order_info['currency_value']),
			'weight'   => (empty($err)) ? $this->weight->convert($product_weight_data['weight'], $product_weight_data['weight_class_id'], $wcid) : 0,
			'weight_unit'   => (empty($err)) ? $weight_descr['unit'] : '',
			'error'    => (empty($err)) ? '' : $err,
		);
	}

	$total_data = $this->model_sale_order->getOrderTotals($order_id);
	$order = array(
		'order_id'	       => $order_id,
		'invoice_no'       => $invoice_no,
		'date_added'       => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
		'store_name'       => $order_info['store_name'],
		'store_url'        => rtrim($order_info['store_url'], '/'),
		'store_address'    => nl2br($store_address),
		'store_email'      => $store_email,
		'store_telephone'  => $store_telephone,
		'store_fax'        => $store_fax,
		'email'            => $order_info['email'],
		'telephone'        => $order_info['telephone'],
		'shipping_address' => $shipping_address,
		'payment_address'  => $payment_address,
		'product'          => $product_data,
		'total'            => $total_data,
		'customer'         => $order_info['customer'],
		'firstname'        => $order_info['shipping_firstname'],
		'lastname'         => $order_info['shipping_lastname'],
		'address1'         => $order_info['shipping_address_1'],
		'address2'         => $order_info['shipping_address_2'],
		'postcode'         => $order_info['shipping_postcode'],
		'city'             => $order_info['shipping_city'],
		'state'            => $order_info['shipping_zone'],
		'country'          => $order_info['shipping_country'],
		'phone'            => $order_info['telephone'],
		'comment'          => nl2br($order_info['comment']),
		'awb'              => $awb,
		'total_pieces'     => $tp,
		'total_gross_weight' => $tgw,
	);
	}


	if (!defined('STORE_LOGO')) {
		define ('STORE_LOGO', $this->config->get('config_logo'));
	}

	$header_data = $order['store_name'].'<br />';
    $header_data .= $order['store_address'].'<br />';
    if ($order['store_fax'])
	{
        $header_data .= $txt['text_fax'].' '.$order['store_fax'].'<br />';
    }
    $header_data .= $order['store_email'].'<br />';
    $header_data .= $order['store_url'];
	
	define ('HEADER_DATA',$header_data);

	$footer_data = '';
	
	$footer_data .= '<b>'.$order['store_name'].'</b> | '.$order['store_address'].' - ';
/*	$footer_data .= $txt['text_telephone'].' '.$order['store_telephone'].', '.$order['store_email'].', ';*/
	$footer_data .= $order['store_email'].', ';
	$footer_data .= $order['store_url'];
	
	define ('FOOTER_DATA', $footer_data);
	
	$qr = 0; 
	
	$qr_style = array(
		'border' => false,
		'vpadding' => 'auto',
		'hpadding' => 'auto',
		'fgcolor' => array(0,0,0),
		'bgcolor' => false, 
		'module_width' => 1, 
		'module_height' => 1 
	);
	

	//if (!class_exists('PDF')) {
	//	require_once(DIR_CATALOG.'tcpdf/tcpdf_class.php');
	//}

	$pdf = new PDFDHL(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	
	$pdf->SetCreator($order['store_name']);
	
	$pdf->SetAuthor($order['store_name']);
	
	$pdf->SetTitle($txt['text_invoice'].' - '.$order['invoice_no']);
	
	$pdf->SetSubject($txt['text_invoice'].' - '.$order['invoice_no']);
	
	$pdf->SetKeywords($order['store_name']);
	
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
	
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	$pdf->setLanguageArray($l);
	
	$pdf->SetFont('freeserif', '', 8);
	
	$pdf->AddPage();
	
	$pdf->SetY(10);
	
	$pdf->invoiceData($order,$txt);
	
	$pdf->SetY(150);
	
	$pdf->addinfo($order,$txt);

	$pdf->SetY(180);

	$pdf->addtext($order,$txt);

	$pdf->SetY(200);

	$pdf->addsigns($order,$txt);

	if($qr == 1)
	{
		if($pdf->getY() <= 60)
		{
			$pdf->AddPage();
		}
		$pdf->write2DBarcode($order['store_url'], 'QRCODE,Q', PDF_MARGIN_LEFT, $pdf->getY()+10, 30, 30, $style, 'N');
	}

	//$filename = 'DHL_'.$order['invoice_no'].'.pdf';
	$filename = $order['order_id'].'_DHL_Invoice.pdf';
	$pdf->Output(DIR_ROOT . 'pdf/' . $filename , 'F');

	$eml_body = $txt['text_email_header'];
	//$eml_body .= str_replace($sh_placeholder,$sh_replace,$txt[$order_info['shipping_provider']]['text']);
	//$eml_body .= $txt['text_email_footer'];
    //if (isset($order_info['track'])) {
	//    $eml_body .= '<br>Number to track your shipment: '.$order_info['track'];
    //}
	//$eml_body .= '<br><br>Download your PDF invoice: '.HTTP_CATALOG . 'pdf/' . $filename;

	//if (isset($no_email) && $no_email != '')
	//	return;

	//LIAL Comment email sending, because we send link to PDF invoice in Email Template Extension.
//		$mail = new Mail();
//		$mail->protocol = $this->config->get('config_mail_protocol');
//		$mail->parameter = $this->config->get('config_mail_parameter');
//		$mail->hostname = $this->config->get('config_smtp_host');
//		$mail->username = $this->config->get('config_smtp_username');
//		$mail->password = $this->config->get('config_smtp_password');
//		$mail->port = $this->config->get('config_smtp_port');
//		$mail->timeout = $this->config->get('config_smtp_timeout');
//		$mail->setTo($order_info['email']);
//		$mail->setFrom($this->config->get('config_email'));
//		$mail->setSender($order_info['store_name']);
//		$mail->setSubject( sprintf($txt['subject'], $invoice_no) );
//		$mail->setHtml(html_entity_decode($eml_body, ENT_QUOTES, 'UTF-8'));
	//$mail->addAttachment(DIR_IMAGE . $this->config->get('config_logo'), md5(basename($this->config->get('config_logo'))));
	//$mail->addAttachment('../pdf/' . $order['invoice_no'].'.pdf', $order['invoice_no'].'.pdf' );
	//$mail->send();

    //$mail->setTo('orders@marineledbulbs.com');
	//$mail->setTo('mlstest56@gmail.com'); //Use test mail while on dev branch
	//$mail->send();
//============================================================+
// END OF FILE
//============================================================+
