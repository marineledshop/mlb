<?php	
	$order_info = $this->getOrder($order_id);
	
		$txt['text_greeting'] = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
		$txt['text_order_detail'] = $language->get('text_new_order_detail');
		$txt['text_instruction'] = $language->get('text_new_instruction');
		$txt['text_order_id'] = $language->get('text_new_order_id');
		$txt['text_date_added'] = $language->get('text_new_date_added');
		$txt['text_payment_method'] = $language->get('text_new_payment_method');	
		$txt['text_shipping_method'] = $language->get('text_new_shipping_method');
		$txt['text_email'] = $language->get('text_new_email');
		$txt['text_telephone'] = $language->get('text_new_telephone');
		$txt['text_ip'] = $language->get('text_new_ip');
		$txt['text_payment_address'] = $language->get('text_new_payment_address');
		$txt['text_shipping_address'] = $language->get('text_new_shipping_address');
		$txt['text_product'] = $language->get('text_new_product');
		$txt['text_model'] = $language->get('text_new_model');
		$txt['text_quantity'] = $language->get('text_new_quantity');
		$txt['text_price'] = $language->get('text_new_price');
		$txt['text_total'] = $language->get('text_new_total');
		$txt['text_after_payment'] = '<b style="color:red">You will receive the invoice, after payment confirmation.</b>';

		$order = array();

		

		if ($order_info) 
		{
			$store_address = $this->config->get('config_address');
			$store_email = $this->config->get('config_email');
			$store_telephone = $this->config->get('config_telephone');
			$store_fax = $this->config->get('config_fax');

		if ($order_info['invoice_no']) {
			$invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
		} else {
			$invoice_no = '';
		}

		if ($order_info['shipping_address_format']) {
			$format = $order_info['shipping_address_format'];
		} else {
			$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
		}

		$find = array(
			'{firstname}',
			'{lastname}',
			'{company}',
			'{address_1}',
			'{address_2}',
			'{city}',
			'{postcode}',
			'{zone}',
			'{zone_code}',
			'{country}'
		);

		$replace = array(
			'firstname' => $order_info['shipping_firstname'],
			'lastname'  => $order_info['shipping_lastname'],
			'company'   => $order_info['shipping_company'],
			'address_1' => $order_info['shipping_address_1'],
			'address_2' => $order_info['shipping_address_2'],
			'city'      => $order_info['shipping_city'],
			'postcode'  => $order_info['shipping_postcode'],
			'zone'      => $order_info['shipping_zone'],
			'zone_code' => $order_info['shipping_zone_code'],
			'country'   => $order_info['shipping_country']
		);

		$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

		if ($order_info['payment_address_format']) {
			$format = $order_info['payment_address_format'];
		} else {
			$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
		}

		$find = array(
			'{firstname}',
			'{lastname}',
			'{company}',
			'{address_1}',
			'{address_2}',
			'{city}',
			'{postcode}',
			'{zone}',
			'{zone_code}',
			'{country}'
		);

		$replace = array(
			'firstname' => $order_info['payment_firstname'],
			'lastname'  => $order_info['payment_lastname'],
			'company'   => $order_info['payment_company'],
			'address_1' => $order_info['payment_address_1'],
			'address_2' => $order_info['payment_address_2'],
			'city'      => $order_info['payment_city'],
			'postcode'  => $order_info['payment_postcode'],
			'zone'      => $order_info['payment_zone'],
			'zone_code' => $order_info['payment_zone_code'],
			'country'   => $order_info['payment_country']
		);

		$payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

		$product_data = array();

		foreach ($order_product_query->rows as $product) {
				$option_data = array();
				
				$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");
				
				foreach ($order_option_query->rows as $option) {
					if ($option['type'] != 'file') {
						$option_data[] = array(
							'name'  => $option['name'],
							'value' => (strlen($option['value']) > 20 ? substr($option['value'], 0, 20) . '..' : $option['value'])
						);
					} else {
						$filename = substr($option['value'], 0, strrpos($option['value'], '.'));
						
						$option_data[] = array(
							'name'  => $option['name'],
							'value' => (strlen($filename) > 20 ? substr($filename, 0, 20) . '..' : $filename)
						);	
					}
				}
			  
				$product_data[] = array(
					'name'     => $product['name'],
					'model'    => $product['model'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value']),
					'total'    => $this->currency->format($product['total'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

		$total_data = $order_total_query->rows;;
		
		$order = array(
			'order_id'	       => $order_id,
			'invoice_no'       => $invoice_no,
			'date_added'       => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
			'store_name'       => $order_info['store_name'],
			'store_url'        => rtrim($order_info['store_url'], '/'),
			'store_address'    => nl2br($store_address),
			'store_email'      => $store_email,
			'store_telephone'  => $store_telephone,
			'store_fax'        => $store_fax,
			'email'            => $order_info['email'],
			'telephone'        => $order_info['telephone'],
			'shipping_address' => $shipping_address,
			'payment_address'  => $payment_address,
			'product'          => $product_data,
			'total'            => $total_data,
			'comment'          => nl2br($order_info['comment']),
			'payment_method'   => $order_info['payment_method'],
			'shipping_method'  => $order_info['shipping_method']
			
		);
	}
	
	define ('STORE_LOGO', $this->config->get('config_logo'));
	
	$header_data = $order['store_name'].'<br />';
    $header_data .= $order['store_address'].'<br />';
    $header_data .= $txt['text_telephone'].' '.$order['store_telephone'].'<br />';
    $header_data .= $order['store_email'].'<br />';
    $header_data .= $order['store_url'];
	
	define ('HEADER_DATA',$header_data);
	
	$footer_data = '<b>'.$order['store_name'].'</b> | '.$order['store_address'].' - ';
	$footer_data .= $txt['text_telephone'].' '.$order['store_telephone'].', '.$order['store_email'].', ';
	$footer_data .= $order['store_url'];
	
	define ('FOOTER_DATA',$footer_data);
	
	$qr = 1; 
	
	$qr_style = array(
				'border' => false,
				'vpadding' => 'auto',
				'hpadding' => 'auto',
				'fgcolor' => array(0,0,0),
				'bgcolor' => false, 
				'module_width' => 1, 
				'module_height' => 1 
				);
	
	require_once(DIR_APPLICATION.'tcpdf/config/lang/eng.php');
	
	require_once(DIR_APPLICATION.'tcpdf/tcpdf.php');

class PDF extends TCPDF {

	public function Header() 
	{
		
		$this->SetFont('freeserif', '', 7);
		
		if(STORE_LOGO && STORE_LOGO != '' && is_file(DIR_IMAGE.STORE_LOGO) && is_readable(DIR_IMAGE.STORE_LOGO) )
		{		
			$this->Image(DIR_IMAGE.STORE_LOGO, 10, 10, '', '', '', '', 'T', false, 100, '', false, false, 0, false, false, false);
		}
		
		$this->writeHTMLCell(0,0,'100','10',$html = HEADER_DATA ,$border = 0,$ln = 0,$fill = false,$reseth = true,$align = '',$autopadding = true );
	}

	public function Footer() 
	{
		$this->SetY(-10);
		
		$this->SetFont('freeserif', '', 7);
		
		$data = '';
		$data .='<table cellpadding="3">';
		$data .= '<tr>';
		$data .= '<td width="70%" style=" border-top:#000000 solid 1px;">'.FOOTER_DATA.'</td>';
		$data .= '<td width="30%" align="right" style=" border-top:#000000 solid 1px;">Page: '.$this->getAliasNumPage().'/'.$this->getAliasNbPages().'</td>';
		$data .= '</tr>';
  		$data .= '</table>';
		
		$this->writeHTML($data, true, false, true, false, '');
	}
	
	function invoiceData($order,$txt) 
	{
		$data = '';
		$data .='<table>';
		$data .= '<tr>';
		$data .= '<td>&nbsp;</td>';
      	$data .= '<td align="right" valign="top">';
		$data .= '<table>';
		$data .= '<tr>';
		$data .= '<td><b>'.$txt['text_date_added'].'</b></td>';
        $data .= '<td>'.$order['date_added'].'</td>';
        $data .= '</tr>';
        if ($order['invoice_no']) 
		{
        	$data .= '<tr>';
            $data .= '<td><b>Proforma</b></td>';
            $data .= '<td>'.$order['invoice_no'].'</td>';
          	$data .= '</tr>';
        }
        $data .= '<tr>';
        $data .= '<td><b>'.$txt['text_order_id'].'</b></td>';
        $data .= '<td>'.$order['order_id'].'</td>';
       	$data .= '</tr>';
		$data .= '<tr>';
		$data .= '<td><b>'.$txt['text_payment_method'].'</b></td>';
		$data .= '<td>'.str_replace(HTTP_IMAGE, DIR_IMAGE, $order['payment_method']).'</td>';
		$data .= '</tr>';
		$data .= '<tr>';
		$data .= '<td><b>'.$txt['text_shipping_method'].'</b></td>';
		$data .= '<td>'.$order['shipping_method'].'</td>';
		$data .= '</tr>';
        $data .= '</table>';
		$data .= '</td>';
    	$data .= '</tr>';
  		$data .= '</table>';
		
		$this->writeHTML($data, true, false, true, false, '');
	}
	
	function address($order,$txt)
	{
		$data  = '';
		$data .= '<table cellpadding="3">';
		$data .= '<tr>';
		$data .= '<td width="40%" bgcolor="#eeeeee"><b>'.$txt['text_payment_address'].'</b></td>';
		$data .= '<td width="20%">&nbsp;</td>';
		if($order['shipping_address'])
		{
			$data .= '<td width="40%" bgcolor="#eeeeee" align="right"><b>'.$txt['text_shipping_address'].'</b></td>';
		}
		else
		{
			$data .= '<td width="40%">&nbsp;</td>';
		}
		$data .= '</tr>';
		$data .= '<tr>';
		$data .= '<td>'.$order['payment_address'].'<br/>';
		$data .= $order['email'].'<br/>';
		$data .= $order['telephone'].'</td>';
		$data .= '<td>&nbsp;</td>';
		if($order['shipping_address'])
		{
			$data .= '<td align="right">'.$order['shipping_address'].'</td>';
		}
		else
		{
			$data .= '<td>&nbsp;</td>';
		}
		$data .= '</tr>';
	  	$data .= '</table>';
		
		$this->writeHTML($data, true, false, true, false, '');
	}
	
	function products($order,$txt)
	{
		$data  = '';
		$data .= '<table cellpadding="3">';
		$data .= '<thead>';   
		$data .= '<tr>';
      	$data .= '<td bgcolor="#eeeeee" style=" padding-left:10px;" width="50%"><b>'.$txt['text_product'].'</b></td>';
      	$data .= '<td bgcolor="#eeeeee" width="20%"><b>'.$txt['text_model'].'</b></td>';
      	$data .= '<td align="right" bgcolor="#eeeeee" width="10%"><b>'.$txt['text_quantity'].'</b></td>';
      	$data .= '<td align="right" bgcolor="#eeeeee" width="10%"><b>'.$txt['text_price'].'</b></td>';
      	$data .= '<td align="right" bgcolor="#eeeeee" width="10%"><b>'.$txt['text_total'].'</b></td>';
    	$data .= '</tr>';
		$data .= '</thead>';
    	foreach ($order['product'] as $product)
        {
    		$data .= '<tr>';
          	$data .= '<td width="50%" style="border-bottom:#dddddd solid 1px">'.$product['name'];
            foreach ($product['option'] as $option)
            {
                $data .= '<br />';
                $data .= '&nbsp;<small> - '.$option['name'].' : '.$option['value'].'</small>';
        	}
            $data .= '</td>';
            $data .= '<td width="20%" style="border-bottom:#dddddd solid 1px">'.$product['model'].'</td>';
            $data .= '<td width="10%" align="right" style="border-bottom:#dddddd solid 1px">'.$product['quantity'].'</td>';
            $data .= '<td width="10%" align="right" style="border-bottom:#dddddd solid 1px">'.$product['price'].'</td>';
            $data .= '<td width="10%" align="right" style="border-bottom:#dddddd solid 1px">'.$product['total'].'</td>';
            $data .= '</tr>';
        }
    	foreach ($order['total'] as $total)
        {
        	$data .= '<tr>';
			$data .= '<td>&nbsp;</td>';
      		$data .= '<td align="right" colspan="3" style="border-bottom:#dddddd solid 1px"><b>'.$total['title'].':</b></td>';
      		$data .= '<td align="right" style="border-bottom:#dddddd solid 1px">'.$total['text'].'</td>';
    		$data .= '</tr>';
    	}
  		$data .= '</table>';
		if ($order['comment']) 
		{
			$data .= '<table class="comment">';
			$data .= '<tr class="heading">';
			$data .= '<td><b>'.$txt['text_instruction'].'</b></td>';
			$data .= '</tr>';
			$data .= '<tr>';
			$data .= '<td>'.$order['comment'].'</td>';
			$data .= '</tr>';
			$data .= '</table>';
		}
		$this->writeHTML($data, true, false, true, false, '');
	}
}

	$pdf = new PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	
	$pdf->SetCreator($order['store_name']);
	
	$pdf->SetAuthor($order['store_name']);
	
	$pdf->SetTitle($txt['text_order_id'].' - '.$order_id);
	
	$pdf->SetSubject($txt['text_order_id'].' - '.$order_id);
	
	$pdf->SetKeywords($order['store_name']);
	
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
	
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	$pdf->setLanguageArray($l);
	
	$pdf->SetFont('freeserif', '', 10);
	
	$pdf->AddPage();
	
	$pdf->SetY(40);
	
	$pdf->invoiceData($order,$txt);
	
	$pdf->SetY(70);
	
	$pdf->address($order,$txt);
	
	$pdf->writeHTML($txt['text_order_detail'], true, false, true, false, '');
	
	$pdf->products($order,$txt);
	
	$pdf->writeHTML($txt['text_greeting'], true, false, true, false, '');
	
	$pdf->writeHTML($txt['text_after_payment'], true, false, true, false, '');	
	
	if($qr == 1)
	{
		if($pdf->getY() <= 60) 
		{
			$pdf->AddPage();
		}
		
		$pdf->write2DBarcode($order['store_url'], 'QRCODE,Q', PDF_MARGIN_LEFT, $pdf->getY()+10, 30, 30, '', 'N');
	}
	
	$pdf->Output('pdf/Order_ID_'.$order_id.'_'.date('d-m-Y').'.pdf', 'F');

//============================================================+
// END OF FILE                                                
//============================================================+

