<?php
		$order_id = $this->request->get['order_id'];
		
		$output_mod = $this->request->get['output'];
		
		$order = array();
		
		$this->load->model('sale/order');

		$order_info = $this->model_sale_order->getOrder($order_id);
		
		$language = new Language($order_info['language_directory']); 
		
		$language->load($order_info['language_filename']);
		
		$language->load('sale/order');

		$this->load->model('setting/setting');
		
		$txt['text_order_detail'] = $language->get('tab_order');
		$txt['text_instruction'] = $language->get('text_comment');
		$txt['text_order_id'] = $language->get('text_order_id');
		$txt['text_invoice_no']	= $language->get('text_invoice_no');
		$txt['text_date_added'] = $language->get('text_date_added');
		$txt['text_payment_method'] = $language->get('text_payment_method');	
		$txt['text_shipping_method'] = $language->get('text_shipping_method');
		$txt['text_email'] = $language->get('text_email');
		$txt['text_telephone'] = $language->get('text_telephone');
		$txt['text_ip'] = $language->get('text_ip');
		$txt['text_payment_address'] = $language->get('text_to');
		$txt['text_shipping_address'] = $language->get('text_ship_to');
		$txt['text_product'] = $language->get('column_product');
		$txt['text_model'] = $language->get('column_model');
		$txt['text_quantity'] = $language->get('column_quantity');
		$txt['text_price'] = $language->get('column_price');
		$txt['text_total'] = $language->get('column_total');
		$txt['text_invoice'] = $language->get('text_invoice');
		

		if ($order_info) 
		{
			$store_address = $this->config->get('config_address');
			$store_email = $this->config->get('config_email');
			$store_telephone = $this->config->get('config_telephone');
			$store_fax = $this->config->get('config_fax');

			if( $order_info['invoice_no'] )
			{
				$invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			}
			else
			{
				$invoice_no = '';
			}

			if( $order_info['shipping_address_format'] )
			{
				$format = $order_info['shipping_address_format'];
			}
			else
			{
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);

			$shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			if( $order_info['payment_address_format'] )
			{
				$format = $order_info['payment_address_format'];
			}
			else
			{
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));
			
			
			$address = $shipping_address ? $shipping_address : $payment_address;

			$product_data = array();
			
			$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

			foreach ($order_product_query->rows as $product)
			{
				$option_data = array();
				
				$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");
				
				foreach ($order_option_query->rows as $option)
				{
					if ($option['type'] != 'file')
					{
						$option_data[] = array(
							'name'  => $option['name'],
							'value' => (strlen($option['value']) > 20 ? substr($option['value'], 0, 20) . '..' : $option['value'])
						);
					}
					else
					{
						$filename = substr($option['value'], 0, strrpos($option['value'], '.'));
						
						$option_data[] = array(
							'name'  => $option['name'],
							'value' => (strlen($filename) > 20 ? substr($filename, 0, 20) . '..' : $filename)
						);	
					}
				}
				
				$fields = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product['product_id'] . "'");
			  
				$product_data[] = array(
					'product_id'	=> $product['product_id'],
					'fields'		=> $fields->row,
					'name'     => $product['name'],
					'model'    => $product['model'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value']),
					'total'    => $this->currency->format($product['total'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}
		
		$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

		$total_data = $order_total_query->rows;;
		
		$order = array(
			'order_id'	       => $order_id,
			'invoice_no'       => $invoice_no,
			'date_added'       => date($language->get('date_format_short'), strtotime($order_info['date_added'])),
			'store_name'       => $order_info['store_name'],
			'store_url'        => rtrim($order_info['store_url'], '/'),
			'store_address'    => nl2br($store_address),
			'store_email'      => $store_email,
			'store_telephone'  => $store_telephone,
			'store_fax'        => $store_fax,
			'email'            => $order_info['email'],
			'telephone'        => $order_info['telephone'],
			'shipping_address' => $address,
			'payment_address'  => $address,
			'product'          => $product_data,
			'total'            => $total_data,
			'comment'          => nl2br($order_info['comment']),
			'payment_method'   => $order_info['payment_method'],
			'shipping_method'  => $order_info['shipping_method']
			
		);
	}
	
	$pdf_general = $this->config->get('pdf_general');
	
	$pdf_setting = $this->config->get('pdf_pack');
	
	define ( 'HEADER_DATA', html_entity_decode( $pdf_general['header_text'][(int)$order_info['language_id']], ENT_QUOTES, 'UTF-8') ) ;
	
	if( isset($pdf_setting['logo']) )
	{
		define ('STORE_LOGO', $this->config->get('config_logo'));
	}
	else
	{
		define ('STORE_LOGO', '');
	}

	define ('FOOTER_DATA',  html_entity_decode( $pdf_general['footer_text'][(int)$order_info['language_id']], ENT_QUOTES, 'UTF-8') ) ;
	
	$barcode_style = array(
					'position' => '',
					'align' => 'C',
					'stretch' => true,
					'fitwidth' => false,
					'cellfitalign' => '',
					'border' => true,
					'padding'	=> 5,
					'hpadding' => 'auto',
					'vpadding' => 2,
					'fgcolor' => array(0,0,0),
					'text' => false,
					'font' => 'freeserif',
					'fontsize' => 8,
					'stretchtext' => 4
				);
	
	$barcode_item = array(
						  'type'	=> $pdf_setting['barcode_item'] ? $pdf_setting['barcode_item'] : false,
						  'field'	=> $pdf_setting['barcode_item_val'],
						  );
	
	if( $pdf_setting['barcode_invoice'] && $pdf_setting['barcode_invoice_val'] )
	{
		switch( $pdf_setting['barcode_invoice_val'] )
		{
			case 1:
			$bc1dinvoiceval = $invoice_no;	
			break;
			case 2:
			$bc1dinvoiceval = $invoice_no.date('dmY', strtotime($order_info['date_added']) );
			break;
			default:
			$bc1dinvoiceval = $invoice_no;
			break;
		}
	}
	
	if( $pdf_setting['barcode_order'] && $pdf_setting['barcode_order_val'] )
	{
		switch( $pdf_setting['barcode_order_val'] )
		{
			case 1:
			$bc1dorderval = $order_id;	
			break;
			case 2:
			$bc1dorderval = $order_id.date('dmY', strtotime($order_info['date_added']) );
			break;
			default:
			$bc1dorderval = $order_id;
			break;
		}
	}
	
require_once(DIR_CATALOG.'tcpdf/config/lang/eng.php');
	
require_once(DIR_CATALOG.'tcpdf/tcpdf.php');

class PDF extends TCPDF {

	public function Header() 
	{
		$x_pos = 10;
		
		$this->SetFont('freeserif', '', 7);
		
		if( STORE_LOGO && STORE_LOGO != '' )
		{
			$this->Image(DIR_IMAGE.STORE_LOGO, 10, 10, '', '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
			
			$size = getimagesize( DIR_IMAGE.STORE_LOGO );
		
			$x_pos = $size[0] / $this->getScaleFactor();		
		}		
		
		$this->writeHTMLCell('','',$x_pos,10,$html = HEADER_DATA ,$border = 0,$ln = 0,$fill = false,$reseth = true,$align = '',$autopadding = true );
	}

	public function Footer() 
	{
		$this->SetY(-10);
		
		$this->SetFont('freeserif', '', 7);
		
		$data = '';
		$data .='<table cellpadding="3">';
		$data .= '<tr>';
		$data .= '<td style=" border-top:#000000 solid 1px;">'.FOOTER_DATA.'</td>';
		#$data .= '<td width="30%" align="right" style=" border-top:#000000 solid 1px;">'.$this->getAliasNumPage().'/'.$this->getAliasNbPages().'</td>';
		$data .= '</tr>';
  		$data .= '</table>';
		
		$this->writeHTML($data, true, false, true, false, '');
	}
	
	function invoiceData($order, $txt, $order_info, $pdf_setting ) 
	{
		$x_pos = 125;
		
		$y = '';
		
		$this->setCellPaddings(1, 2, 1, 1);
		
		
		$this->SetFont('freeserif', '', 12);		
		$this->writeHTMLCell( '', 6, $x_pos , $y, $pdf_setting['pack_title'][(int)$order_info['language_id']] , 1, 1, false, true, 'R', true);

		
		$this->SetFont('freeserif', '', 7);
		
		$this->writeHTMLCell( 20, 4, $x_pos , $y, $txt['text_date_added'], 1, 0, false, true, 'R', true);			
		$this->writeHTMLCell( 55, 4, '', $y, $order['date_added'], 1, 1, false, true, 'R', true);

		if ($order['invoice_no']) 
		{
			$this->writeHTMLCell( 20, 4, $x_pos , $y, $txt['text_invoice_no'], 1, 0, false, true, 'R', true);			
			$this->writeHTMLCell( 55, 4, '', $y, $order['invoice_no'], 1, 1, false, true, 'R', true);
        }
		
		$this->writeHTMLCell( 20, 4, $x_pos , $y, $txt['text_order_id'], 1, 0, false, true, 'R', true);		
		$this->writeHTMLCell( 55, 4, '', $y, $order['order_id'], 1, 1, false, true, 'R', true);		
		$this->writeHTMLCell( 20, 4, $x_pos , $y, $txt['text_payment_method'], 1, 0, false, true, 'R', true);		
		$this->writeHTMLCell( 55, 4, '', $y, $order['payment_method'], 1, 1, false, true, 'R', true);		
		$this->writeHTMLCell( 20, 4, $x_pos , $y, $txt['text_shipping_method'], 1, 0, false, true, 'R', true);		
		$this->writeHTMLCell( 55, 4, '', $y, $order['shipping_method'], 1, 1, false, true, 'R', true);	
	}
	
	function shipping_address($order,$txt)
	{		
		$x_pos = 10;
		
		$y = $this->getY();
		
		$this->SetFont('freeserif', '', 12);
		
		$this->setCellPaddings(1, 2, 1, 2);
		
		if($order['shipping_address'])
		{
			$this->writeHTMLCell( 75, '', $x_pos , $y, $txt['text_shipping_address'], 1, 1, false, true, 'L', true);
		}
		
		$this->SetFont('freeserif', '', 12);
		
		if($order['shipping_address'])
		{
			$this->writeHTMLCell( 75, '', $x_pos, '' , $order['shipping_address'], 1, 1, false, true, 'L', true);
		}
		
		$this->setCellPaddings(1, 2, 1, 2);
	}
	
	function tableHeader( $cell, $txt )
	{
		$this->SetFillColor(0, 0, 0);
		
		$this->SetTextColor(255, 255, 255);
		
		$this->setCellPaddings(1, 2, 1, 2);
		
		$y = '';
		
		$this->SetFont('freeserif', '', 12);

		$this->writeHTMLCell( $cell[0], '', '', $y, '#', 1, 0, true, true, 'L', true);
		$this->writeHTMLCell( $cell[1], '', '', $y, $txt['text_product'], 1, 0, true, true, 'L', true);
		$this->writeHTMLCell( $cell[3], '', '', $y, $txt['text_quantity'], 1, 0, true, true, 'C', true);
		$this->writeHTMLCell( $cell[2], '', '', $y, $txt['text_model'], 1, 0, true, true, 'R', true);
		
		$this->Ln();
	}		
}

	$pdf = new PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	
	$pdf->SetCreator($order['store_name']);
	
	$pdf->SetAuthor($order['store_name']);
	
	$pdf->SetTitle($txt['text_order_id'].' - '.$order_id);
	
	$pdf->SetSubject($txt['text_order_id'].' - '.$order_id);
	
	$pdf->SetKeywords($order['store_name']);
	
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
	
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	$pdf->setLanguageArray($l);
	
	$pdf->SetFont('freeserif', '', 8);
		
	$pdf->SetTextColor(0, 0, 0);
	
	$pdf->AddPage();
	
	$pdf->SetY(40);
	
	$pdf->invoiceData($order, $txt, $order_info, $pdf_setting );
	
	$pdf->setCellPaddings(1, 0, 1, 0);
	
	$pdf->SetY(40);
	
	#$pdf->payment_address($order,$txt);
	
	$pdf->shipping_address($order,$txt);
	
	$pdf->setCellPaddings(1, 0, 1, 0);
	
	$pdf->Ln(5);
	
	#$pdf->SetY();
	
	$cell = array( 10,75,75,30); 
	
	$pdf->tableHeader( $cell, $txt );
	
	$pdf->setCellPaddings(1, 0, 1, 0);
	
	$pdf->SetFillColor(250, 250, 250);
	
	$pdf->SetTextColor(0, 0, 0);
	
	$fill = true;
	
	$items = 1;

	foreach ($order['product'] as $product)
    {

		$cell_h = 6;
		
		$y = '';
		
		$pdf->setCellPaddings(1, 2, 1, 1);
		
		if( $barcode_item['type'] && $product['fields'][$barcode_item['field']] )
		{
			$cell_h = 14;
			
			$pdf->writeHTMLCell( $cell[0], $cell_h, '', $y, $items, 1, 0, false, true, 'J', true);
			
			$pdf->writeHTMLCell( $cell[1], $cell_h, '', $y, $product['name'].'<br /><small>'.$txt['text_model'].': '.$product['model'].'</small>', 1, 0,false, true, 'J', true);
			
			$pdf->writeHTMLCell( $cell[3], $cell_h, '', $y, $product['quantity'], 1, 0, false, true, 'C', true);
			
			$pdf->write1DBarcode($product['fields'][$barcode_item['field']], $barcode_item['type'], '', '', $cell[2], $cell_h, 0.4, $barcode_style, 'T');
			
		}
		else
		{
			$cell_h = 6;
			
			$pdf->writeHTMLCell( $cell[0], $cell_h, '', $y, $items, 1, 0, false, true, 'J', true);
			
			$pdf->writeHTMLCell( $cell[1], $cell_h, '', $y, $product['name'], 1, 0, false, true, 'L', true);
			
			$pdf->writeHTMLCell( $cell[3], $cell_h, '', $y, $product['quantity'], 1, 0, false, true, 'C', true);
			
			$pdf->writeHTMLCell( $cell[2], $cell_h, '', $y, $product['model'], 1, 0, false, true, 'R', true);
		}
		
		$pdf->Ln();
		
		if( $pdf->getY() + ( $cell_h  ) >= 250 )
		{
			$pdf->addPage();
			
			$pdf->setY(40);
			
			$pdf->tableHeader( $cell, $txt );
			
			$pdf->setCellPaddings(1, 0, 1, 0);
	
			$pdf->SetFillColor(250, 250, 250);
			
			$pdf->SetTextColor(0, 0, 0);
		}
		
		$items++;
    }
	
	if( $pdf->getY() >= 250 )
	{
		$pdf->addPage();
	}
	
	$pdf->SetFont('freeserif', '', 8);
	
	$pdf->setCellPaddings(1, 0, 1, 0);
	
	$pdf->Ln();
	
	if( $pdf_setting['additional_text'][(int)$order_info['language_id']] )
	{
		$y = $pdf->getY() + 10;
		
		$add_txt = sprintf( html_entity_decode( $pdf_setting['additional_text'][(int)$order_info['language_id']], ENT_QUOTES, 'UTF-8'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
		
		$pdf->writeHTMLCell('', '', '', $y, $add_txt, 0, 0, 0, true, 'C', true);

	}
	
	$pdf->Ln();
	
	$pdf->setY( $pdf->getY() + 3 );
	
	if( $pdf_setting['barcode_invoice'] && $pdf_setting['barcode_invoice_val'] )
	{
		$style = array(
						'position' => '',
						'align' => 'C',
						'stretch' => false,
						'fitwidth' => false,
						'cellfitalign' => '',
						'border' => true,
						'hpadding' => 'auto',
						'vpadding' => 'auto',
						'fgcolor' => array(0,0,0),
						'bgcolor' => false,
						'text' => false,
						'border'	=> true
					);
			
		$pdf->write1DBarcode( $bc1dinvoiceval, $pdf_setting['barcode_invoice'], '', '', 75, 16, 0.4, $style, 'T');			
	}
	
	if( $pdf_setting['barcode_order'] && $pdf_setting['barcode_order_val'] )
	{
		$style = array(
						'position' => '',
						'align' => 'C',
						'stretch' => false,
						'fitwidth' => false,
						'cellfitalign' => '',
						'border' => true,
						'hpadding' => 'auto',
						'vpadding' => 'auto',
						'fgcolor' => array(0,0,0),
						'bgcolor' => false,
						'text' => false,
						'border'	=> true
					);
			
		$pdf->write1DBarcode( $bc1dorderval, $pdf_setting['barcode_order'], 125, '', 75, 16, 0.4, $style, 'N');			
	}
		
	switch( $output_mod )
	{
		case 'screen':
		$output = 'I';
		break;
		
		case 'print':		
		$js = 'print(true);';		
		$pdf->IncludeJS($js);
		$output = 'I';
		break;

		
		case 'download':
		$output = 'D';
		break;
		
		default:
		$output = 'I';
		break;
	}
	
	$pdf_name = 'Packing-List-'.$order_info['invoice_no'].'-'.$order_id.'-'.date('d-m-Y').'.pdf';
	
	$pdf->Output( $pdf_name , $output );

//============================================================+
// END OF FILE                                                
//============================================================+
