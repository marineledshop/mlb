<?php
$this->load->language('sale/order');
$txt = array(
    'direction'         => $this->language->get('direction'),
    'language'          => $this->language->get('code'),
    'text_invoice'      => $this->language->get('text_invoice'),
    'text_order_id'     => $this->language->get('text_order_id'),
    'text_invoice_no'   => $this->language->get('text_invoice_no'),
    'text_invoice_date' => $this->language->get('text_invoice_date'),
    'text_date_added'   => $this->language->get('text_date_added'),
    'text_telephone'    => $this->language->get('text_telephone'),
    'text_fax'          => $this->language->get('text_fax'),
    'text_to'           => $this->language->get('text_to'),
    'text_ship_to'      => $this->language->get('text_ship_to'),
    'column_product'    => $this->language->get('column_product'),
    'column_model'      => $this->language->get('column_model'),
    'column_quantity'   => $this->language->get('column_quantity'),
    'column_price'      => $this->language->get('column_price'),
    'column_total'      => $this->language->get('column_total'),
    'column_comment'    => $this->language->get('column_comment'),
    'subject'           => 'Your Marine LED Shop order has been shipped - %s',

    'text_email_header' => '<p>Dear Sir/Madam,</p><p>Your order from Marine LED Bulbs has been shipped and your invoice is attached.</p>',


    'dhl'      => array(
        'site'       => '<a href="http://www.dhl.com">www.dhl.com</a>',
        'activation' => '2',
        'text'       => '<p>You can find your shipping details below:<br>
                                            Shipping Company & Tracking # :<br>
                                            <b><i>DHL Worldwide Express</i> - {track}</b></p>

                                            <p>You can trace your package with the link below:<br>
                                            <a href="http://www.dhl.com/content/g0/en/express/tracking.shtml?brand=DHL&AWB={track}">http://www.dhl.com/content/g0/en/express/tracking.shtml?brand=DHL&AWB={track}</a></p>

                                            <p>Estimated delivery time:<br> 
                                            <b>{shipping_arrival_days} business days</b></p>
                                            <p>Kindest Regards,<br>
                                            Marine LED Bulbs<br>
                                            http://marineledbulbs.com</p>'
    ),
    'ems'      => array(
        'site'       => '<a href="http://www.singpost.com">www.singpost.com</a>',
        'activation' => '2',
        'text'       => 'Your order has been shipped with <em>{shipping_company}</em>. You can track your order with tracking number <strong>{track}</strong> at <strong>{shipping_site}</strong>. This tracking number will be activated in {shipping_activation_days} business day(s). Your parcel should arrive in {shipping_arrival_days} business day(s)'
    ),
    'singpost' => array(
        'site'       => '<a href="http://www.singpost.com">www.singpost.com</a>',
        'activation' => '2',
        'text'       => '<p>Shipping Company & Tracking No. :<br>
<b><i>SingPost Registered Airmail</i></b> - {track}</p>

<p>You can track your package with the link below:<br>
<a href="http://www.singpost.com/index.php?option=com_tablink&controller=tracking&task=trackdetail&layout=show_detail&tmpl=component&ranumber={track}">http://www.singpost.com/index.php?option=com_tablink&controller=tracking&task=trackdetail&layout=show_detail&tmpl=component&ranumber={track}</a></p>

<p>Please note: It may take 48-72 hours for your tracking to be activated.</p>

<p>Estimated delivery time:<br>
<b>{shipping_arrival_days} business days</b></p>

<p>Please note: These times are <u>estimates</u> given by postal companies. While more than 90% of our shipments arrive within estimated time there may be occasional delays. In such event you should contact your local postal company first.</p> 

<p>Once your parcel is handed over to logistics company we use the same resources as you do for tracking. It’s your local postal office who can give you the most accurate information.</p> 

<p>You can also track this parcel using the local postal company in your country with the tracking # provided:<br>
USA – http://www.usps.com<br> 
Canada - http://www.canadapost.ca<br>
Australia - http://auspost.com.au<br>
New Zealand - http://www.nzpost.co.nz<br>
United Kingdom - http://www.royalmail.com<br>
Germany - http://www.deutschepost.de<br>
For other countries: Please use your national postal company website for tracking.</p>
<p>Kindest Regards,<br>Marine LED Bulbs<br>http://marineledbulbs.com</p>'
    ));

$this->load->model('sale/order');
$this->load->model('setting/setting');

$order = array();

$order_id = $this->request->get['order_id'];
$no_email = $this->request->get['no_email'];

$order_info = $this->model_sale_order->getOrder($order_id);

if ($order_info) {

    $this->load->language('shipping/' . $order_info['shipping_provider']);

    $sh_placeholder = array(
        '{shipping_company}',
        '{shipping_site}',
        '{shipping_activation_days}',
        '{shipping_arrival_days}',
        '{track}'
    );

    $sh_replace = array(
        $this->language->get('heading_title'),
        $txt[$order_info['shipping_provider']]['site'],
        $txt[$order_info['shipping_provider']]['activation'],
        $order_info['d_keys'][0],
        $order_info['track']
    );


    $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);
    if ($store_info) {
        $store_address = $store_info['config_address'];
        $store_email = $store_info['config_email'];
        $store_telephone = $store_info['config_telephone'];
        $store_fax = $store_info['config_fax'];
    }
    else {
        $store_address = $this->config->get('config_address');
        $store_email = $this->config->get('config_email');
        $store_telephone = $this->config->get('config_telephone');
        $store_fax = $this->config->get('config_fax');
    }

    if ($order_info['invoice_no']) {
        $invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
    }
    else {
        $invoice_no = '';
    }

    if ($order_info['shipping_address_format']) {
        $format = $order_info['shipping_address_format'];
    }
    else {
        $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
    }

    $find = array(
        '{firstname}',
        '{lastname}',
        '{company}',
        '{address_1}',
        '{address_2}',
        '{city}',
        '{postcode}',
        '{zone}',
        '{zone_code}',
        '{country}'
    );

    $replace = array(
        'firstname' => $order_info['shipping_firstname'],
        'lastname'  => $order_info['shipping_lastname'],
        'company'   => $order_info['shipping_company'],
        'address_1' => $order_info['shipping_address_1'],
        'address_2' => $order_info['shipping_address_2'],
        'city'      => $order_info['shipping_city'],
        'postcode'  => $order_info['shipping_postcode'],
        'zone'      => $order_info['shipping_zone'],
        'zone_code' => $order_info['shipping_zone_code'],
        'country'   => $order_info['shipping_country']
    );

    $shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

    if ($order_info['payment_address_format']) {
        $format = $order_info['payment_address_format'];
    }
    else {
        $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
    }

    $find = array(
        '{firstname}',
        '{lastname}',
        '{company}',
        '{address_1}',
        '{address_2}',
        '{city}',
        '{postcode}',
        '{zone}',
        '{zone_code}',
        '{country}'
    );

    $replace = array(
        'firstname' => $order_info['payment_firstname'],
        'lastname'  => $order_info['payment_lastname'],
        'company'   => $order_info['payment_company'],
        'address_1' => $order_info['payment_address_1'],
        'address_2' => $order_info['payment_address_2'],
        'city'      => $order_info['payment_city'],
        'postcode'  => $order_info['payment_postcode'],
        'zone'      => $order_info['payment_zone'],
        'zone_code' => $order_info['payment_zone_code'],
        'country'   => $order_info['payment_country']
    );

    $payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

    $product_data = array();
    $products = $this->model_sale_order->getOrderProducts($order_id);

    foreach ($products as $product) {
        $option_data = array();
        $options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);
        foreach ($options as $option) {
            if ($option['type'] != 'file') {
                $option_data[] = array(
                    'name'  => $option['name'],
                    'value' => $option['value']
                );
            }
            else {
                $option_data[] = array(
                    'name'  => $option['name'],
                    'value' => utf8_substr($option['value'], 0, strrpos($option['value'], '.'))
                );
            }
        }
        $product_data[] = array(
            'name'     => $product['name'],
            'model'    => $product['model'],
            'option'   => $option_data,
            'quantity' => $product['quantity'],
            'price'    => $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value']),
            'total'    => $this->currency->format($product['total'], $order_info['currency_code'], $order_info['currency_value'])
        );
    }

    $total_data = $this->model_sale_order->getOrderTotals($order_id);
    $order = array(
        'order_id'         => $order_id,
        'invoice_no'       => $invoice_no,
        'date_added'       => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
        'store_name'       => $order_info['store_name'],
        'store_url'        => rtrim($order_info['store_url'], '/'),
        'store_address'    => nl2br($store_address),
        'store_email'      => $store_email,
        'store_telephone'  => $store_telephone,
        'store_fax'        => $store_fax,
        'email'            => $order_info['email'],
        'telephone'        => $order_info['telephone'],
        'shipping_address' => $shipping_address,
        'payment_address'  => $payment_address,
        'product'          => $product_data,
        'total'            => $total_data,
        /*			'comment'          => nl2br($order_info['comment'])*/
    );
}


define('STORE_LOGO', $this->config->get('config_logo'));

$header_data = $order['store_name'] . '<br />';
$header_data .= $order['store_address'] . '<br />';
/*  $header_data .= $txt['text_telephone'].' '.$order['store_telephone'].'<br />';*/
if ($order['store_fax']) {
    $header_data .= $txt['text_fax'] . ' ' . $order['store_fax'] . '<br />';
}
$header_data .= $order['store_email'] . '<br />';
$header_data .= $order['store_url'];

define('HEADER_DATA', $header_data);

$footer_data = '';

$footer_data .= '<b>' . $order['store_name'] . '</b> | ' . $order['store_address'] . ' - ';
/*	$footer_data .= $txt['text_telephone'].' '.$order['store_telephone'].', '.$order['store_email'].', ';*/
$footer_data .= $order['store_email'] . ', ';
$footer_data .= $order['store_url'];

define('FOOTER_DATA', $footer_data);

$qr = 0;

$qr_style = array(
    'border'        => false,
    'vpadding'      => 'auto',
    'hpadding'      => 'auto',
    'fgcolor'       => array(0, 0, 0),
    'bgcolor'       => false,
    'module_width'  => 1,
    'module_height' => 1
);

require_once(DIR_CATALOG . 'tcpdf/config/lang/eng.php');

require_once(DIR_CATALOG . 'tcpdf/tcpdf.php');

class PDFN extends TCPDF
{

    public function Header() {
        $this->SetFont('freeserif', '', 7);
        if (STORE_LOGO && STORE_LOGO != '') {
            $this->bgcolor = array('R' => 23, 'G' => 47, 'B' => 64);
            $this->writeHTML('<div>', true, true, true, false, '');
            $this->Image('../image/' . STORE_LOGO, 10, 10, '', '', '', '', 'T', false, 100, '', false, false, 0, false, false, false);
            $this->writeHTML('</div>', true, true, true, false, '');
            $this->bgcolor = false;
        }
        $w = 100;
        $h = 100;
        $this->writeHTMLCell($w, $h, '100', '10', $html = HEADER_DATA, $border = 0, $ln = 0, $fill = false, $reseth = true, $align = '', $autopadding = true);
    }

    public function Footer() {
        $this->SetY(-10);
        $this->SetFont('freeserif', '', 7);
        $data = '';
        $data .= '<table cellpadding="3">';
        $data .= '<tr>';
        $data .= '<td width="70%" style=" border-top:#000000 solid 1px;">' . FOOTER_DATA . '</td>';
        $data .= '<td width="30%" align="right" style=" border-top:#000000 solid 1px;">Page: ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages() . '</td>';
        $data .= '</tr>';
        $data .= '</table>';
        $this->writeHTML($data, true, false, true, false, '');
    }

    function invoiceData($order, $txt) {
        $data = '';
        $data .= '<table>';
        $data .= '<tr>';
        $data .= '<td>&nbsp;</td>';
        $data .= '<td align="right" valign="top">';
        $data .= '<table>';
        $data .= '<tr>';
        $data .= '<td><b>' . $txt['text_date_added'] . '</b></td>';
        $data .= '<td>' . $order['date_added'] . '</td>';
        $data .= '</tr>';
        if ($order['invoice_no']) {
            $data .= '<tr>';
            $data .= '<td><b>' . $txt['text_invoice_no'] . '</b></td>';
            $data .= '<td>' . $order['invoice_no'] . '</td>';
            $data .= '</tr>';
        }
        $data .= '<tr>';
        $data .= '<td><b>' . $txt['text_order_id'] . '</b></td>';
        $data .= '<td>' . $order['order_id'] . '</td>';
        $data .= '</tr>';
        $data .= '</table>';
        $data .= '</td>';
        $data .= '</tr>';
        $data .= '</table>';
        $this->writeHTML($data, true, false, true, false, '');
    }

    function address($order, $txt) {
        $data = '';
        $data .= '<table cellpadding="3">';
        $data .= '<tr>';
        $data .= '<td width="40%" bgcolor="#eeeeee"><b>' . $txt['text_to'] . '</b></td>';
        $data .= '<td width="20%">&nbsp;</td>';
        if ($order['shipping_address']) {
            $data .= '<td width="40%" bgcolor="#eeeeee" align="right"><b>' . $txt['text_ship_to'] . '</b></td>';
        }
        else {
            $data .= '<td width="40%">&nbsp;</td>';
        }
        $data .= '</tr>';
        $data .= '<tr>';
        $data .= '<td>' . $order['payment_address'] . '<br/>';
        $data .= $order['email'] . '<br/>';
        $data .= $order['telephone'] . '</td>';
        $data .= '<td>&nbsp;</td>';
        if ($order['shipping_address']) {
            $data .= '<td align="right">' . $order['shipping_address'] . '</td>';
        }
        else {
            $data .= '<td>&nbsp;</td>';
        }
        $data .= '</tr>';
        $data .= '</table>';
        $this->writeHTML($data, true, false, true, false, '');
    }

    function products($order, $txt) {
        $data = '';
        $data .= '<table cellpadding="3">';
        $data .= '<thead>';
        $data .= '<tr>';
        $data .= '<td bgcolor="#eeeeee" style=" padding-left:10px;" width="50%"><b>' . $txt['column_product'] . '</b></td>';
        $data .= '<td bgcolor="#eeeeee" width="20%"><b>' . $txt['column_model'] . '</b></td>';
        $data .= '<td align="right" bgcolor="#eeeeee" width="10%"><b>' . $txt['column_quantity'] . '</b></td>';
        $data .= '<td align="right" bgcolor="#eeeeee" width="10%"><b>' . $txt['column_price'] . '</b></td>';
        $data .= '<td align="right" bgcolor="#eeeeee" width="10%"><b>' . $txt['column_total'] . '</b></td>';
        $data .= '</tr>';
        $data .= '</thead>';
        foreach ($order['product'] as $product) {
            $data .= '<tr>';
            $data .= '<td width="50%" style="border-bottom:#dddddd solid 1px">' . $product['name'];
            foreach ($product['option'] as $option) {
                $data .= '<br />';
                $data .= '&nbsp;<small> - ' . $option['name'] . ' : ' . $option['value'] . '</small>';
            }
            $data .= '</td>';
            $data .= '<td width="20%" style="border-bottom:#dddddd solid 1px">' . $product['model'] . '</td>';
            $data .= '<td width="10%" align="right" style="border-bottom:#dddddd solid 1px">' . $product['quantity'] . '</td>';
            $data .= '<td width="10%" align="right" style="border-bottom:#dddddd solid 1px">' . $product['price'] . '</td>';
            $data .= '<td width="10%" align="right" style="border-bottom:#dddddd solid 1px">' . $product['total'] . '</td>';
            $data .= '</tr>';
        }
        foreach ($order['total'] as $total) {
            $data .= '<tr><td>&nbsp;</td>';
            $data .= '<td align="right" colspan="3" style="border-bottom:#dddddd solid 1px"><b>' . $total['title'] . ':</b></td>';
            $data .= '<td align="right" style="border-bottom:#dddddd solid 1px">' . $total['text'] . '</td>';
            $data .= '</tr>';
        }
        $data .= '</table>';
        if ($order['comment']) {
            $data .= '<table class="comment">';
            $data .= '<tr class="heading">';
            $data .= '<td><b>' . $txt['column_comment'] . '</b></td>';
            $data .= '</tr>';
            $data .= '<tr>';
            $data .= '<td>' . $order['comment'] . '</td>';
            $data .= '</tr>';
            $data .= '</table>';
        }
        $this->writeHTML($data, true, false, true, false, '');
    }
}

$pdf = new PDFN(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator($order['store_name']);

$pdf->SetAuthor($order['store_name']);

$pdf->SetTitle($txt['text_invoice'] . ' - ' . $order['invoice_no']);

$pdf->SetSubject($txt['text_invoice'] . ' - ' . $order['invoice_no']);

$pdf->SetKeywords($order['store_name']);

$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->setLanguageArray($l);

$pdf->SetFont('freeserif', '', 10);

$pdf->AddPage();

$pdf->SetY(40);

$pdf->invoiceData($order, $txt);

$pdf->SetY(70);

$pdf->address($order, $txt);

$pdf->products($order, $txt);


if ($qr == 1) {
    if ($pdf->getY() <= 60) {
        $pdf->AddPage();
    }
    $pdf->write2DBarcode($order['store_url'], 'QRCODE,Q', PDF_MARGIN_LEFT, $pdf->getY() + 10, 30, 30, $style, 'N');
}

$pdf->Output('../pdf/' . $order['invoice_no'] . '.pdf', 'F');

$eml_body = $txt['text_email_header'];
$eml_body .= str_replace($sh_placeholder, $sh_replace, $txt[$order_info['shipping_provider']]['text']);
$eml_body .= $txt['text_email_footer'];


if ($no_email) {
    return;
}

$mail = new Mail();
$mail->protocol = $this->config->get('config_mail_protocol');
$mail->parameter = $this->config->get('config_mail_parameter');
$mail->hostname = $this->config->get('config_smtp_host');
$mail->username = $this->config->get('config_smtp_username');
$mail->password = $this->config->get('config_smtp_password');
$mail->port = $this->config->get('config_smtp_port');
$mail->timeout = $this->config->get('config_smtp_timeout');
$mail->setTo($order_info['email']);
$mail->setFrom($this->config->get('config_email'));
$mail->setSender($order_info['store_name']);
$mail->setSubject(sprintf($txt['subject'], $invoice_no));
$mail->setHtml(html_entity_decode($eml_body, ENT_QUOTES, 'UTF-8'));
/*		$mail->addAttachment(DIR_IMAGE . $this->config->get('config_logo'), md5(basename($this->config->get('config_logo'))));*/
$mail->addAttachment('../pdf/' . $order['invoice_no'] . '.pdf', $order['invoice_no'] . '.pdf');
$mail->send();

if (PRODUCTION) {
    $mail->setTo('orders@marineledbulbs.com');
}
else {
    $mail->setTo('mlstest21@gmail.com');
}
$mail->send();
