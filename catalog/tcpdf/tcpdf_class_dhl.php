<?php

require_once(DIR_CATALOG.'tcpdf/config/lang/eng.php');
require_once(DIR_CATALOG.'tcpdf/tcpdf.php');

class PDFDHL extends TCPDF {

	public function Header($header_data = '')
	{
		$this->SetFont('freeserif', '', 7);
		if( STORE_LOGO && STORE_LOGO != '')
		{
			$this->Image('../image/'.STORE_LOGO, 10, 10, '', '', '', '', 'T', false, 100, '', false, false, 0, false, false, false);
		}
		$w = 100;
		$h = 100;
		$this->writeHTMLCell($w,$h,'100','10',$html = $header_data ,$border = 0,$ln = 0,$fill = false,$reseth = true,$align = '',$autopadding = true );
	}

	public function Footer($footer_data = '')
	{
		$this->SetY(-10);
		$this->SetFont('freeserif', '', 7);
		$data = '';
		$data .='<table cellpadding="3">';
		$data .= '<tr>';
		$data .= '<td width="70%" style=" border-top:#000000 solid 1px;">'.$footer_data.'</td>';
		$data .= '<td width="30%" align="right" style=" border-top:#000000 solid 1px;">Page: '.$this->getAliasNumPage().'/'.$this->getAliasNbPages().'</td>';
		$data .= '</tr>';
		$data .= '</table>';
		$this->writeHTML($data, true, false, true, false, '');
	}

	function invoiceData($order,$txt)
	{
		$data = '';
		$data .= '<table style="width: 100%; border-collapse: collapse;" cellpadding="3" border="1px">';
		$data .= '<tr><td style="width: 50%;">
            <b>Shipper:</b><br>
            Marine LED Shop<br><br>
            APS Fulfillment, Inc.<br>
            1371 S.W. 8th St. #1<br>
            Pompano Beach, FL, USA<br>
            ZIP: 33069<br><br>
            <b>Phone:</b> 954-582-7450<br><br>
            <b>VAT/GST No:</b>
	        </td>
	        <td valign="middle" align="center" style="background-color:lightgrey; font-size: 2em;">Commercial Invoice</td>
		</tr>';

		if ($order['address2']) {
			$addr2 = $order['address2'].'<br>';
		} else {
			$addr2 = '  ';
		}
		$data .= '<tr>
        <td rowspan="3">
            <b>Receiver:</b><br>
            '.$order['customer'].'<br><br>
            '.$order['firstname'] . ' ' . $order['lastname'] .'<br>
            '.$order['address1'].'<br>'.$addr2.$order['city'].', '.$order['state'].'<br>
            '.$order['country'].'<br>
            Postal code: '.$order['postcode'].'<br><br>
            <b>Phone:</b>&nbsp;'.$order['phone'].'<br><br>
            <b>VAT/GST No:</b>
        </td>
        <td><b>Date:</b>&nbsp;'.$order['date_added'].'</td>
	    </tr>
	    <tr><td><b>Invoice Number:</b>&nbsp;'.$order['invoice_no'].'</td></tr>
	    <tr><td><b>Shipment Reference:</b>&nbsp;'.$order['order_id'].'</td></tr>';

		$data .= '<tr>
	        <td rowspan="2"><b>Bill To Third Party:</b></td>
	        <td><b>Comments:</b>'.$order['comment'].'</td>
		    </tr>
		    <tr><td><b>Airway Bill Number:</b><br><div style="vertical-align: middle; text-align: center; font-size: 1.5em; width: 100%;">'.$order['awb'].'</div></td>
	    </tr>';
		$data .= '</table>';
		$total = $order['total'][0]['value'];
		$total = round($total, 2);
		//$qty = $order['total_pieces']; //Qty from DHL form
		$qty = 0;
		$grossweight = $order['total_gross_weight'];
		$netweight = 0;

		foreach ($order['product'] as $product) {
			$qty += $product['quantity'];
			//echo '<pre>'; var_dump($product); echo '</pre>';
			$netweight += $product['weight'];
		}
		$unitvalue = ($qty > 0) ? round($total/$qty, 2, PHP_ROUND_HALF_DOWN) : 0;
		$unitweight = ($qty > 0) ? $netweight/$qty : 0;
		$unitweight = round(floatval($unitweight), 3);
		$netweight = round(floatval($netweight), 3, PHP_ROUND_HALF_UP);

		$data .= '<table border="1" style="width: 100%; border-collapse: collapse;" cellpadding="3">
		            <thead><tr>
		                <td width="3%"></td>
		                <td width="25%">Full Description of Goods</td>
		                <td width="5%">QTY</td>
		                <td width="5%">UOM</td>
		                <td width="12%">Commodity Code</td>
		                <td width="10%">Unit Value</td>
		                <td width="10%">Subtotal Value</td>
		                <td width="10%">Unit Net Weight, lbs</td>
		                <td width="10%">Subtotal Weight, lbs</td>
		                <td width="10%">Country of Origin</td>
		            </tr></thead>
		            <tr>
		                <td width="3%">1</td>
		                <td width="25%">LED products</td>
		                <td width="5%">'.$qty.'</td>
		                <td width="5%">N/A</td>
		                <td width="12%">9405409000</td>
		                <td width="10%">'.$unitvalue.'</td>
		                <td width="10%">'.$total.'</td>
		                <td width="10%">'.$unitweight.'</td>
		                <td width="10%">'.$netweight.'</td>
		                <td width="10%">USA</td>
		            </tr>
		            <tr>
		                <td colspan="4"></td>
		                <td colspan="3"><b>Declared Value:</b>&nbsp;'.$total.' USD</td>
		                <td colspan="3"><b>Total Net Weight:</b>&nbsp;'.$netweight.' '.$product['weight_unit'].'</td>
		            </tr>
		            <tr>
		                <td colspan="4"></td>
		                <td colspan="3"><b>Total Pieces:</b>&nbsp;'.$qty.'</td>
		                <td colspan="3"><b>Total Gross Weight:</b>&nbsp;'.$grossweight.' lbs</td>
		            </tr>
		        </table>
		';


		$this->writeHTML($data, true, false, true, false, '');
	}

	function addinfo($order,$txt)
	{
		$data  = '';
		$data .= '<table style="width: 100%;">
		    <tr>
		        <td style="width: 40%;"><b>Payer of GST/VAT:</b> Receiver (No: )</td>
		        <td><b>Currency Code:</b> USD</td>
		    </tr>
		    <tr>
		        <td><b>Type of Export:</b> Permanent</td>
		        <td><b>Incoterm:</b></td>
		    </tr>
		    <tr>
		        <td><b>Terms of Payment:</b></td>
		        <td></td>
		    </tr>
		</table>';
		$this->writeHTML($data, true, false, true, false, '');
	}

	function addtext($order,$txt)
	{
		$data  = '';
		$data .= '<p>I/We hereby certify that the information of this invoice is true and correct and that the contents of this shipment are as stated above.</p>';
		$this->writeHTML($data, true, false, true, false, '');
	}

	function addsigns($order,$txt)
	{
		$data  = '';
		$data .= '<p>Signature: ____________________________________ </p>
		<p>Position in Company:</p>
		<p>Shipping Consultant: _______________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		Company Stamp:</p>';
		$this->writeHTML($data, true, false, true, false, '');
	}

	function products($order,$txt)
	{
		$data  = '';
		$data .= '<table cellpadding="3">';
		$data .= '<thead>';
		$data .= '<tr>';
		$data .= '<td bgcolor="#eeeeee" style=" padding-left:10px;" width="50%"><b>'.$txt['column_product'].'</b></td>';
		$data .= '<td bgcolor="#eeeeee" width="20%"><b>'.$txt['column_model'].'</b></td>';
		$data .= '<td align="right" bgcolor="#eeeeee" width="10%"><b>'.$txt['column_quantity'].'</b></td>';
		$data .= '<td align="right" bgcolor="#eeeeee" width="10%"><b>'.$txt['column_price'].'</b></td>';
		$data .= '<td align="right" bgcolor="#eeeeee" width="10%"><b>'.$txt['column_total'].'</b></td>';
		$data .= '</tr>';
		$data .= '</thead>';
		foreach ($order['product'] as $product)
		{
			$data .= '<tr>';
			$data .= '<td width="50%" style="border-bottom:#dddddd solid 1px">'.$product['name'];
			foreach ($product['option'] as $option)
			{
				$data .= '<br />';
				$data .= '&nbsp;<small> - '.$option['name'].' : '.$option['value'].'</small>';
			}
			$data .= '</td>';
			$data .= '<td width="20%" style="border-bottom:#dddddd solid 1px">'.$product['model'].'</td>';
			$data .= '<td width="10%" align="right" style="border-bottom:#dddddd solid 1px">'.$product['quantity'].'</td>';
			$data .= '<td width="10%" align="right" style="border-bottom:#dddddd solid 1px">'.$product['price'].'</td>';
			$data .= '<td width="10%" align="right" style="border-bottom:#dddddd solid 1px">'.$product['total'].'</td>';
			$data .= '</tr>';
		}
		foreach ($order['total'] as $total)
		{
			$data .= '<tr><td>&nbsp;</td>';
			$data .= '<td align="right" colspan="3" style="border-bottom:#dddddd solid 1px"><b>'.$total['title'].':</b></td>';
			$data .= '<td align="right" style="border-bottom:#dddddd solid 1px">'.$total['text'].'</td>';
			$data .= '</tr>';
		}
		$data .= '</table>';
		if (isset($order['comment']))
		{
			$data .= '<table class="comment">';
			$data .= '<tr class="heading">';
			$data .= '<td><b>'.$txt['column_comment'].'</b></td>';
			$data .= '</tr>';
			$data .= '<tr>';
			$data .= '<td>'.$order['comment'].'</td>';
			$data .= '</tr>';
			$data .= '</table>';
		}
		$this->writeHTML($data, true, false, true, false, '');
	}
}
?>