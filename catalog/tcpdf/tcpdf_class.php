<?php

require_once(DIR_CATALOG.'tcpdf/config/lang/eng.php');
require_once(DIR_CATALOG.'tcpdf/tcpdf.php');

class PDF extends TCPDF {

	public function Header($header_data = '')
	{
		$this->SetFont('freeserif', '', 7);
		if( STORE_LOGO && STORE_LOGO != '')
		{
			$this->Image('../image/'.STORE_LOGO, 10, 10, '', '', '', '', 'T', false, 100, '', false, false, 0, false, false, false);
		}
		$w = 100;
		$h = 100;
		$this->writeHTMLCell($w,$h,'100','10',$html = $header_data ,$border = 0,$ln = 0,$fill = false,$reseth = true,$align = '',$autopadding = true );
	}

	public function Footer($footer_data = '')
	{
		$this->SetY(-10);
		$this->SetFont('freeserif', '', 7);
		$data = '';
		$data .='<table cellpadding="3">';
		$data .= '<tr>';
		$data .= '<td width="70%" style=" border-top:#000000 solid 1px;">'.$footer_data.'</td>';
		$data .= '<td width="30%" align="right" style=" border-top:#000000 solid 1px;">Page: '.$this->getAliasNumPage().'/'.$this->getAliasNbPages().'</td>';
		$data .= '</tr>';
		$data .= '</table>';
		$this->writeHTML($data, true, false, true, false, '');
	}

	function invoiceData($order,$txt,$store_data)
	{
		$data = '';
		$data .='<table width="100%">';
		$data .= '<tr>';
		$data .= '<td width="40%">';
		$data .= $store_data;
		$data .= '</td>';
		$data .= '<td  width="60%">';
			$data .='<table  width="100%">';
			$data .= '<tr>';
			$data .= '<td>&nbsp;</td>';
			$data .= '<td align="right" valign="top">';
			$data .= '<table>';
			$data .= '<tr>';
			$data .= '<td><b>'.$txt['text_date_added'].'</b></td>';
			$data .= '<td>'.$order['date_added'].'</td>';
			$data .= '</tr>';
			if ($order['invoice_no'])
			{
				$data .= '<tr>';
				$data .= '<td><b>'.$txt['text_invoice_no'].'</b></td>';
				$data .= '<td>'.$order['invoice_no'].'</td>';
				$data .= '</tr>';
			}
			$data .= '<tr>';
			$data .= '<td><b>'.$txt['text_order_id'].'</b></td>';
			$data .= '<td>'.$order['order_id'].'</td>';
			$data .= '</tr>';
			$data .= '</table>';
			$data .= '</td>';
			$data .= '</tr>';
			$data .= '</table>';
		$data .= '</td>';
		$data .= '</tr>';
		$data .= '</table>';
		$this->writeHTML($data, true, false, true, false, '');
	}

	function address($order,$txt)
	{
		$data  = '';
		$data .= '<table cellpadding="3">';
		$data .= '<tr>';
		$data .= '<td width="40%" bgcolor="#eeeeee"><b>'.$txt['text_to'].'</b></td>';
		$data .= '<td width="20%">&nbsp;</td>';
		if($order['shipping_address'])
		{
			$data .= '<td width="40%" bgcolor="#eeeeee" align="right"><b>'.$txt['text_ship_to'].'</b></td>';
		}
		else
		{
			$data .= '<td width="40%">&nbsp;</td>';
		}
		$data .= '</tr>';
		$data .= '<tr>';
		$data .= '<td>'.$order['payment_address'].'<br/>';
		$data .= $order['email'].'<br/>';
		$data .= $order['telephone'].'</td>';
		$data .= '<td>&nbsp;</td>';
		if($order['shipping_address'])
		{
			$data .= '<td align="right">'.$order['shipping_address'].'</td>';
		}
		else
		{
			$data .= '<td>&nbsp;</td>';
		}
		$data .= '</tr>';
		$data .= '</table>';
		$this->writeHTML($data, true, false, true, false, '');
	}

	function products($order,$txt)
	{
		$data  = '';
		$data .= '<table cellpadding="3">';
		$data .= '<thead>';
		$data .= '<tr>';
		$data .= '<td bgcolor="#eeeeee" style=" padding-left:10px;" width="50%"><b>'.$txt['column_product'].'</b></td>';
		$data .= '<td bgcolor="#eeeeee" width="20%"><b>'.$txt['column_model'].'</b></td>';
		$data .= '<td align="right" bgcolor="#eeeeee" width="10%"><b>'.$txt['column_quantity'].'</b></td>';
		$data .= '<td align="right" bgcolor="#eeeeee" width="10%"><b>'.$txt['column_price'].'</b></td>';
		$data .= '<td align="right" bgcolor="#eeeeee" width="10%"><b>'.$txt['column_total'].'</b></td>';
		$data .= '</tr>';
		$data .= '</thead>';
		foreach ($order['product'] as $product)
		{
			$data .= '<tr>';
			$data .= '<td width="50%" style="border-bottom:#dddddd solid 1px">'.$product['name'];
			foreach ($product['option'] as $option)
			{
				$data .= '<br />';
				$data .= '&nbsp;<small> - '.$option['name'].' : '.$option['value'].'</small>';
			}
			$data .= '</td>';
			$data .= '<td width="20%" style="border-bottom:#dddddd solid 1px">'.$product['model'].'</td>';
			$data .= '<td width="10%" align="right" style="border-bottom:#dddddd solid 1px">'.$product['quantity'].'</td>';
			$data .= '<td width="10%" align="right" style="border-bottom:#dddddd solid 1px">'.$product['price'].'</td>';
			$data .= '<td width="10%" align="right" style="border-bottom:#dddddd solid 1px">'.$product['total'].'</td>';
			$data .= '</tr>';
		}
		foreach ($order['total'] as $total)
		{
			$data .= '<tr><td>&nbsp;</td>';
			$data .= '<td align="right" colspan="3" style="border-bottom:#dddddd solid 1px"><b>'.$total['title'].':</b></td>';
			$data .= '<td align="right" style="border-bottom:#dddddd solid 1px">'.$total['text'].'</td>';
			$data .= '</tr>';
		}
		$data .= '</table>';
		if (isset($order['comment']))
		{
			$data .= '<table class="comment">';
			$data .= '<tr class="heading">';
			$data .= '<td><b>'.$txt['column_comment'].'</b></td>';
			$data .= '</tr>';
			$data .= '<tr>';
			$data .= '<td>'.$order['comment'].'</td>';
			$data .= '</tr>';
			$data .= '</table>';
		}
		$this->writeHTML($data, true, false, true, false, '');
	}
}
?>