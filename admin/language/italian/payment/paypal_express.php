<?php

// Heading
$_['heading_title'] = 'PayPal Express Checkout';

// Text
$_['text_payment'] = 'Pagamento';
$_['text_success'] = 'Successo: Hai modificato i dettagli del modulo PayPal Express Checkout!';
$_['text_paypal_express'] = '<a href="https://www.paypal.com/uk/mrb/pal=S53S5NBZGRYF4" target="_blank"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Authorization';
$_['text_sale'] = 'Sale';
$_['text_noshipping_auto'] = 'Scelta automatica dell\'estensione (consigliato)';
$_['text_noshipping_display'] = 'PayPal mostra l\'indirizzo di spedizione sulle pagine PayPal';
$_['text_noshipping_notdisplay'] = 'PayPal non mostra nessun campo per l\'indirizzo';
$_['text_noshipping_profile'] = 'Se il cliente non passa l\'indirizzo, PayPal lo ottiene dal profilo del cliente';
$_['text_shipping_address'] = 'Indirizzo di Spedizione';
$_['text_payment_address'] = 'Indirizzo di Fatturazione';
$_['text_createaccount_customer_choose'] = 'Il cliente decide se registrare un account';

// Entry
$_['entry_username'] = 'API Username:';
$_['entry_password'] = 'API Password:';
$_['entry_signature'] = 'API Firma:';
$_['entry_username_test'] = 'API Username (Test):';
$_['entry_password_test'] = 'API Password (Test):';
$_['entry_signature_test'] = 'API Firma (Test):';
$_['entry_title'] = 'Titolo del Modulo:';
$_['entry_test'] = 'Modalit&agrave; Test:';
$_['entry_method'] = 'Metodo transizione:';
$_['entry_logo'] = 'Abilita Logo:<br /><span class="help">L\'immagine deve avere una dimensione massima di 750 pixel di larghezza e 90 pixel di altezza. Se non si specifica un\'immagine, la ragione sociale sar&egrave; visualizzata.</span>';
$_['entry_createaccount'] = 'Crea Account Cliente:<br /><span class="help">Automaticamente, dai dati PayPal, si crea un account. Il cliente sar&agrave; avvisato via e-mail, anche per la password generata automaticamente. Se l\'e-mail gi&agrave; esiste il cliente sar&agrave; loggato.</span>';
$_['entry_merchant_country'] = 'Paese Merchant:<br /><span class="help">Se non specificato, la Nazione di Default sar&agrave; presa dall\'Indirizzo del Cliente.</span>';
$_['entry_default_currency'] = 'Valuta Predefinita:<br /><span class="help">Se il cliente sceglie una valuta non supportata, sar&agrave; convertita automaticamente con questa. Valute supportate da PayPal Express: AUD, CAD, CZK, DKK, EUR, HKD, HUF, ILS, JPY, MXN, NOK, NZD, PLN, GBP, SGD, SEK, CHF, USD, TWD, THB, BRL (solo per Brasile), MYR (solo per Malesia), TRY (solo per Turchia)</span>';
$_['entry_skip_confirm'] = 'Salta Conferma Pagamento:<br /><span class="help">Se &egrave; impostato su Si, il cliente salta la pagina di conferma dopo il login su PayPal ed esso viene reindirizzato automaticamente nella pagina di successo.<br /><strong>NOTA</strong>: questa non ha effetto quando il cliente deve scegliere il metodo di spedizione e/o registrarsi e/o procedere all\'acquisto come ospite con conferma dell\'indirizzo, ecc. dopo il login su PayPal.</span>';
$_['entry_confirm_order'] = 'Conferma Ordine prima del login su PayPal:<br /><span class="help">Se &egrave; impostato su Si, l\'ordine sar&agrave; confermato prima che il cliente va su PayPal con uno stato ordine "Standard".<br /><strong>NOTA</strong>: questa non ha effetto se l\'opzione "Salta Conferma Pagamento" &egrave; impostata su "No" o se il cliente clicca sul Pulsante PayPal Express.</span>';
$_['entry_send_address'] = 'Invia l\'indirizzo di Spedizione o di Fatturazione:<br /><span class="help">Scegli se inviare l\'indirizzo di Spedizione o l\'indirizzo di Fatturazione.</span>';
$_['entry_noshipping'] = 'Invia indirizzo:<br /><span class="help">Per articoli digitali:' . $_['text_noshipping_notdisplay'] . '.</span>';
$_['entry_senditem'] = 'Mostra Sommario in PayPal:<br /><span class="help">Mostra articoli nel sito PayPal. Se esce l\'errore "10413" il modulo disattiver&agrave; automaticamente questa funzione.</span>';
$_['entry_landing'] = 'Landing Page:<br /><span class="help">Pagina PayPal da visualizzare: Login o Carta di Credito.</span>';
$_['entry_total'] = 'Totale minimo:<br /><span class="help">Il totale che deve essere raggiunto per abilitare questo metodo di pagamento.</span>';
$_['entry_total_over'] = 'Totale massimo:<br /><span class="help">Il totale che deve essere raggiunto per disabilitare questo metodo di pagamento.</span>';
$_['entry_order_status'] = 'Stato ordine:';
$_['entry_order_status_complete'] = 'Stato ordine completo:<br /><span class="help">Lo stato dell\'ordine quando il pagamento &egrave; stato completato.</span>';
$_['entry_order_status_refunded'] = 'Stato ordine rimborsato:<br /><span class="help">Lo stato dell\'ordine quanto il pagamento &egrave; stato rimborsato.</span>';
$_['entry_order_status_voided'] = 'Stato ordine annullato:<br /><span class="help">Lo stato dell\'ordine quanto il pagamento &egrave; stato annullato.</span>';
$_['entry_enableincheckout'] = 'Abilita questo modulo nel checkout:<br /><span class="help">Quando questa opzione &egrave; disattivata il modulo non si vede nella nella lista dei pagamenti ne checkout ma funziona solo con il pulsante express checkout.</span>';
$_['entry_geo_zone'] = 'Zona geografica:';
$_['entry_status'] = 'Stato:';
$_['entry_sort_order'] = 'Ordine di visualizzazione:';

// Error
$_['error_permission'] = 'Attenzione: non hai i permessi per modificare il modulo PayPal Express Checkout!';
$_['error_username'] = 'API Username Richiesto!';
$_['error_password'] = 'API Password Richiesta!';
$_['error_signature'] = 'API Firma Richiesta!';
$_['error_title'] = 'Titolo richiesto!';
$_['error_title_max'] = 'Il titolo deve essere massimo 128 caratteri! N.B.: i caratteri speciali vengono considerati decodificati (ad esempio: "&amp;" = "&amp;amp;", "&gt;" = "&amp;gt;", ecc.)';

$_['error_license'] = 'Attenzione: Il dominio non risulta avere chiave di licenza attiva per questo modulo, &egrave; possibile ottenerne una cliccando <a href="%s?languages=it" target="_blank">qui</a> oppure acquistare una licenza, se non se ne possiede una, cliccando <a href="%s" target="_blank">qui</a>.';
$_['error_opencart_version'] = 'Attenzione: Questo modulo non pu&ograve; essere usato con questa versione di OpenCart. Devi scaricare il modulo PayPal Express Checkout compatibile con OpenCart %s';

#Button
$_['button_refund'] = 'Rimborsa Transazione PayPal';
$_['button_void'] = 'Annulla Transazione PayPal';
$_['button_capture'] = 'Riscuoti Transazione PayPal';
$_['button_get_api'] = 'Ottieni le credenziali API';
$_['button_get_api_test'] = 'Ottieni le credenziali API SansBox';

#Api Reference
$_['text_transaction_id'] = 'ID Transazione';
$_['text_message_api_paypal_express'] = 'La transazione avverr&agrave; automaticamente tramite le API PayPal';

#Refund
$_['text_refund_type'] = 'Tipo di Rimborso';
$_['text_refund_amt'] = 'Importo di rimborso: l\'importo &egrave; obbligatorio se il tipo di rimborso &egrave; <em>Partial</em>';
$_['success_refunded'] = 'La Transazione &egrave; stata rimborsata!';

#Void
$_['success_voided'] = 'La Transazione &egrave; stata annullata!';

#Capture
$_['text_capture_type'] = 'Tipo';
$_['text_capture_amt'] = 'Totale da riscuotere';
$_['success_captured'] = 'La Transazione &egrave; stata riscossa!';

#Tab
$_['tab_live'] = 'Produzione';
$_['tab_test'] = 'Test';
?>