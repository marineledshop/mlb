<?php
// Heading
$_['heading_title']    = 'CSV Import / Export';

// Text
$_['text_csvmenu']     = 'CSV Import/Export';
$_['text_backup']      = 'St�hnout - Z�lohovat';
$_['text_success']     = '�sp�ch: v� CSV soubor byl �sp�n�  nahr�n!';

// Entry
$_['entry_export']     = 'Export souboru CSV:';
$_['entry_import']     = 'Import souboru CSV:';

// Error
$_['error_permission'] = 'Upozorn�n�: Nem�te opr�vn�n� m�nit soubor CSV!';
$_['error_empty']      = 'Upozorn�n�: Soubor kter� jste nahr�ly byl pr�zdn�!';
?>