<?php
// Heading
$_['heading_title']       = 'PayPal Express Checkout Modul';

// Text
$_['text_module']         = 'Module';
$_['text_success']        = 'Erfolg: Sie haben das Modul PayPal Express Checkout ge&auml;ndert!';
$_['text_content_top']    = 'Inhalt Top';
$_['text_content_bottom'] = 'Inhalt Bottom';
$_['text_column_left']    = 'Spalte links';
$_['text_column_right']   = 'Spalte rechts';

// Entry
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sortierung:';

// Error
$_['error_permission']    = 'Warnung: Sie haben keine Berechtigung, dieses Modul zu &auml;ndern!';
?>