<?php

// Heading
$_['heading_title'] = 'PayPal Express Checkout';

// Text
$_['text_payment'] = 'Zahlung';
$_['text_success'] = 'Erfolg: Haben Sie die Details der Form PayPal Express Checkout ge&auml;ndert!';
$_['text_paypal_express'] = '<a href="https://www.paypal.com/uk/mrb/pal=S53S5NBZGRYF4" target="_blank"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Authorization';
$_['text_sale'] = 'Sale';
$_['text_noshipping_auto'] = 'Automatische Wahl der Verl&auml;ngerung (empfohlen)';
$_['text_noshipping_display'] = 'PayPal zeigt die Lieferadresse auf der PayPal-Seiten';
$_['text_noshipping_notdisplay'] = 'PayPal nicht Lieferadresse Felder &uuml;berhaupt angezeigt werden';
$_['text_noshipping_profile'] = 'Wenn K&auml;ufer die Lieferadresse nicht geht, PayPal erh&auml;lt er vom Konto des K&auml;ufers Profil';
$_['text_shipping_address'] = 'Versandadresse';
$_['text_payment_address'] = 'Zahlungsadresse';
$_['text_createaccount_customer_choose'] = 'Der Kunde entscheidet, ob ein Konto registrieren';

// Entry
$_['entry_username'] = 'API Username:';
$_['entry_password'] = 'API Password:';
$_['entry_signature'] = 'API Unterschrift:';
$_['entry_username_test'] = 'API Username (Test):';
$_['entry_password_test'] = 'API Password (Test):';
$_['entry_signature_test'] = 'API Unterschrift (Test):';
$_['entry_title'] = 'Modul Titel:';
$_['entry_test'] = 'Test-Modus:';
$_['entry_method'] = 'Transition Method:';
$_['entry_logo'] = 'Logo:<br /><span class="help">Das Bild hat eine maximale Gr&ouml;ße von 750 Pixel breit mal 90 Pixel hoch. Wenn Sie nicht angeben, ein Bild, zeigt die Firma.</span>';
$_['entry_createaccount'] = 'Neues Kundenkonto anlegen:<br /><span class="help">Erstellen Sie automatisch ein Konto bei PayPal-Daten. Der Kunde wird per E-Mail benachrichtigt werden, auch f&uuml;r die automatisch generierte Passwort. Wenn die E-Mail ist bereits vorhanden wird der Kunde angemeldet sein.</span>';
$_['entry_merchant_country'] = 'H&auml;ndler Land:<br /><span class="help">Wenn nicht angegeben, wird Standard Land vom Kunden Adresse verwendet werden.</span>';
$_['entry_default_currency'] = 'Standardw&auml;hrung:<br /><span class="help">Wenn der Kunde die Wahl eines nicht unterstützten W&auml;hrung, wird diese W&auml;hrung automatisch umgewandelt werden. Unterst&uuml;tzte PayPal Express W&auml;hrungen: AUD, CAD, CZK, DKK, EUR, GBP, HUF, ILS, JPY, GBP, NOK, NZD, PLN, EUR, SGD, SEK, CHF, USD, TWD, THB, BRL (nur f&uuml;r Brasilien) MYR (nur f&uuml;r Malaysia), TRY (Nur f&uuml;r die T&uuml;rkei)</span>';
$_['entry_skip_confirm'] = 'Zur Zahlung best&auml;tigen:<br /><span class="help">Wenn es auf yes gesetzt ist, &uuml;berspringen der Kunde die Best&auml;tigung Seite nach PayPal anmelden und es wird automatisch Zur Kasse Erfolg umgeleitet.<br /><strong>Fußnote</strong>: dies nicht bewirken, wenn der Kunde die Versandart zu w&auml;hlen und/oder Account registrieren und/oder Kasse als Gast mit der Best&auml;tigung der Adressen, etc. k&ouml;nnen nach PayPal Login.</span>';
$_['entry_confirm_order'] = 'Best&auml;tigen Bestellung vor PayPal Login:<br /><span class="help">Wenn es auf yes gesetzt, wird die Bestellung best&auml;tigt werden, bevor der Kunde PayPal gehen mit dem "Standard", um den Status zu.<br /><strong>Fußnote</strong>: Dies hat keine Auswirkung, wenn die Option "Zur Zahlung best&auml;tigen" als "Nein" oder wenn der Kunde Klick auf PayPal Express-Taste eingestellt ist.</span>';
$_['entry_send_address'] = 'Senden Zahlung oder Lieferadresse:<br /><span class="help">W&auml;hlen Sie, wenn senden Sie die Zahlung Adresse oder Lieferadresse bei PayPal.</span>';
$_['entry_noshipping'] = 'Senden Adresse:<br /><span class="help">F&uuml;r digitale G&uuml;ter:' . $_['text_noshipping_notdisplay'] . '.</span>';
$_['entry_senditem'] = 'Anzeigen in PayPal:<br /><span class="help">Anzeige Artikel in PayPal-Seite. Wenn es die Fehlermeldung "10413" bekommen das Modul diese Funktion automatisch deaktiviert werden.</span>';
$_['entry_landing'] = 'Landing Page:<br /><span class="help">Art der PayPal-Seite angezeigt werden: Login-Seite oder Kreditkartenabrechnung Seite.</span>';
$_['entry_total'] = 'Minimale insgesamt:<br /><span class="help">Die Summe, die erreicht diese Zahlungsart erm&ouml;glichen werden muss.</span>';
$_['entry_total_over'] = 'Insgesamt maximum:<br /><span class="help">Die Summe, die erreicht diese Zahlungsart deaktivieren werden muss.</span>';
$_['entry_order_status'] = 'Status der Bestellung:';
$_['entry_order_status_complete'] = 'Status der Bestellung F&uuml;llen Sie:<br /><span class="help">Der Status der Bestellung, wenn die Zahlung abgeschlossen ist.</span>';
$_['entry_order_status_refunded'] = 'Bestell-Status Zur&uuml;ckgezahlte:<br /><span class="help">Der Auftragsstatus, wenn die Zahlung zur&uuml;ckerstattet wird.</span>';
$_['entry_order_status_voided'] = 'Bestell-Status Voided:<br /><span class="help">Der Auftragsstatus, wenn die Zahlung storniert wird.</span>';
$_['entry_enableincheckout'] = 'Dieses Modul aktivieren, in Kasse:<br /><span class="help">Wenn diese Option deaktiviert wird dieses Modul nicht in Zahlungsverzug Kasse Liste zu sehen, aber es funktioniert nur mit der Schaltfl&auml;che Express-Checkout.</span>';
$_['entry_geo_zone'] = 'Geografisches Gebiet:';
$_['entry_status'] = 'Status:';
$_['entry_sort_order'] = 'Art:';

// Error
$_['error_permission'] = 'Achtung: haben keine Berechtigung, um das Formular zu bearbeiten PayPal Express Checkout!';
$_['error_username'] = 'API Username erforderlich!';
$_['error_password'] = 'API Password erforderlich!';
$_['error_signature'] = 'API Unterschrift erforderlich!';
$_['error_title'] = 'Titel erforderlich!';
$_['error_title_max'] = 'Der Titel muss maximal 128 Zeichen lang sein! Note: Sonderzeichen werden als decodiert werden (zB: "&amp;" = "&amp;amp;", "&gt;" = "&amp;gt;", etc.)';

$_['error_license'] = 'Warnung: Die Domain scheint keinen Lizenzschl&uuml;ssel haben, um dieses Modul zu aktivieren, k&ouml;nnen Sie ein, indem Sie <a href="%s?languages=de" target="_blank">hier</a> erhalten oder eine Lizenz erwerben, wenn Sie noch kein Konto haben, klicken Sie <a href="%s" target="_blank">hier</a>.';
$_['error_opencart_version'] = 'Achtung: Dieses Modul kann nicht mit diesem OpenCart Version verwendet werden. Sie m&uuml;ssen die PayPal Express Checkout-Modul kompatibel mit OpenCart %s herunterladen';

#Button
$_['button_refund'] = 'R&uuml;ckerstattung PayPal Transaction';
$_['button_void'] = 'Rechtswidrig PayPal Transaction';
$_['button_capture'] = 'Nehmen Sie PayPal Transaction';
$_['button_get_api'] = 'Holen API Credentials';
$_['button_get_api_test'] = 'Holen SandBox API Credentials';

#Api Reference
$_['text_transaction_id'] = 'Transaction ID';
$_['text_message_api_paypal_express'] = 'Die Transaktion wird automatisch durch den PayPal-API vorgenommen werden';

#Refund
$_['text_refund_type'] = 'Refund Typ';
$_['text_refund_amt'] = 'Erstattungsbetrag: Der Betrag ist erforderlich, wenn Refund Typ ist <em>Partial</em>';
$_['success_refunded'] = 'Die Transaktion wurde erstattet!';

#Void
$_['success_voided'] = 'Die Transaktion wurde storniert!';

#Capture
$_['text_capture_type'] = 'Aufnahmeart';
$_['text_capture_amt'] = 'Erfassen Menge';
$_['success_captured'] = 'Die Transaktion wurde gefangen genommen!';

#Tab
$_['tab_live'] = 'Produktion';
$_['tab_test'] = 'Test';
?>