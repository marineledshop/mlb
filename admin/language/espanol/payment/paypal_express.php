<?php

// Heading
$_['heading_title'] = 'PayPal Express Checkout';

// Text
$_['text_payment'] = 'Pago';
$_['text_success'] = '&Eacute;: Ha cambiado los detalles de la forma PayPal Express Checkout!';
$_['text_paypal_express'] = '<a href="https://www.paypal.com/uk/mrb/pal=S53S5NBZGRYF4" target="_blank"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Authorization';
$_['text_sale'] = 'Sale';
$_['text_noshipping_auto'] = 'Selecci&oacute;n autom&aacute;tica de la extensi&oacute;n (recomendado)';
$_['text_noshipping_display'] = 'PayPal muestra la direcci&oacute;n de env&iacute;o en las p&aacute;ginas de PayPal';
$_['text_noshipping_notdisplay'] = 'PayPal no muestra los campos de direcci&oacute;n de env&iacute;o en absoluto';
$_['text_noshipping_profile'] = 'Si el comprador no pasa la direcci&oacute;n del env&iacute;o, PayPal lo consigue desde el perfil de la cuenta del comprador';
$_['text_shipping_address'] = 'Direcci&oacute;n de env&iacute;o';
$_['text_payment_address'] = 'Direcci&oacute;n de Pago';
$_['text_createaccount_customer_choose'] = 'El cliente decide si desea registrar una cuenta';

// Entry
$_['entry_username'] = 'API Username:';
$_['entry_password'] = 'API Password:';
$_['entry_signature'] = 'API Firma:';
$_['entry_username_test'] = 'API Username (Test):';
$_['entry_password_test'] = 'API Password (Test):';
$_['entry_signature_test'] = 'API Firma (Test):';
$_['entry_title'] = 'T&iacute;tulo del m&oacute;dulo:';
$_['entry_test'] = 'Modo Test:';
$_['entry_method'] = 'M&eacute;todo de transici&oacute;n:';
$_['entry_logo'] = 'logotipo:<br /><span class="help">La imagen tiene un tama&ntilde;o m&aacute;ximo de 750 p&iacute;xeles de ancho por 90 p&iacute;xeles de alto. Si no se especifica una imagen, el nombre de la empresa muestra.</span>';
$_['entry_createaccount'] = 'Crear cuenta de cliente:<br /><span class="help">Creaci&oacute;n autom&aacute;tica de una cuenta a partir de datos de PayPal. El cliente ser&aacute; notificado por correo electr&oacute;nico, tambi&eacute;n para la contrase&ntilde;a generada autom&aacute;ticamente. Si el e-mail ya existe el cliente se registra.</span>';
$_['entry_merchant_country'] = 'Merchant Pa&iacute;s:<br /><span class="help">Si no se especifica, Pa&iacute;s defecto de Direcci&oacute;n del cliente se utilizar&aacute;.</span>';
$_['entry_default_currency'] = 'Moneda Predeterminada:<br /><span class="help">Si el cliente elige una moneda no es compatible, esta moneda se convertir&aacute; automáticamente. Compatibles PayPal Express de Monedas: AUD, CAD, CZK, DKK, EUR, GBP, HUF, ILS, JPY, MXN, NOK, NZD, PLN, GBP, EUR, SEK, CHF, USD, TWD, THB, BRL (s&oacute;lo para Brasil ), EUR (s&oacute;lo para M&eacute;xico), TRY (s&oacute;lo para Turqu&iacute;a)</span>';
$_['entry_skip_confirm'] = 'Saltar Confirmar Pago:<br /><span class="help">If it set to yes, the customer skip the confirmation page after PayPal login and it is redirected automatically to checkout success page.<br /><strong>NOTA</strong>: esto no tiene efecto cuando el cliente debe elegir el m&eacute;todo de env&iacute;o y/o registro de cuenta y/o pago y env&eacute;o como invitado con la confirmaci&oacute;n de las direcciones, etc despu&eacute;s de anunciarse PayPal.</span>';
$_['entry_confirm_order'] = 'Confirmar pedido antes de inicio de sesi&oacute;n PayPal:<br /><span class="help">Si se establece en yes, el pedido ser&aacute; confirmado antes de que el cliente vaya a PayPal con el estado "Standard" orden.<br /><strong>NOTA</strong>: esto no tiene efecto si la opci&oacute;n "Saltar Confirmar Pago" est&aacute; configurado como "No" o si el cliente haga clic en el bot&oacute;n de PayPal expreso.</span>';
$_['entry_send_address'] = 'Enviar la direcci&oacute;n de pago o env&iacute;o:<br /><span class="help">Seleccione si enviar la direcci&oacute;n de pago o direcci&oacute;n de env&iacute;o en PayPal.</span>';
$_['entry_noshipping'] = 'Enviar direcci&oacute;n:<br /><span class="help">Para los productos digitales:' . $_['text_noshipping_notdisplay'] . '.</span>';
$_['entry_senditem'] = 'Mostrar vista previa en PayPal:<br /><span class="help">Mostrar elementos en la p&aacute;gina de PayPal. Si sale el error "10413", el m&oacute;dulo se desactivar&aacute; esta funci&oacute;n autom&aacute;ticamente.</span>';
$_['entry_landing'] = 'La p&aacute;gina de destino:<br /><span class="help">Tipo de p&aacute;gina de PayPal para mostrar: P&aacute;gina de Inicio de sesi&oacute;n o en la p&aacute;gina de facturaci&oacute;n de tarjetas de cr&eacute;dito.</span>';
$_['entry_total'] = 'Totale m&iacute;nimo:<br /><span class="help">El total que se debe alcanzar para que este m&eacute;todo de pago.</span>';
$_['entry_total_over'] = 'Totale m&aacute;ximo:<br /><span class="help">El total que se debe alcanzar para desactivar este m&eacute;todo de pago.</span>';
$_['entry_order_status'] = 'Estado del pedido:';
$_['entry_order_status_complete'] = 'Estado del pedido completa:<br /><span class="help">El estado de los pedidos cuando el pago se ha realizado.</span>';
$_['entry_order_status_refunded'] = 'Order Status Reembolsado:<br /><span class="help">El estado del pedido cuando el pago se reembolsar&aacute;.</span>';
$_['entry_order_status_voided'] = 'Estado del pedido anulado:<br /><span class="help">El estado del pedido cuando el pago se anula.</span>';
$_['entry_enableincheckout'] = 'Activar este m&oacute;dulo en la caja:<br /><span class="help">Cuando esta opci&oacute;n est&aacute; desactivada, este m&oacute;dulo no aparece en la lista de pago checkout pero s&oacute;lo funcionan con el bot&oacute;n check-out expr&eacute;s.</span>';
$_['entry_geo_zone'] = 'Zona geogr&agrave;fica:';
$_['entry_status'] = 'Estado:';
$_['entry_sort_order'] = 'Tipo:';

// Error
$_['error_permission'] = 'Atenci&oacute;n: no tienen permiso para editar el formulario de PayPal Express Checkout!';
$_['error_username'] = 'API Username obligatorio!';
$_['error_password'] = 'API Password obligatorio!';
$_['error_signature'] = 'API Firma obligatorio!';
$_['error_title'] = 'T&iacute;tulo Requerido!';
$_['error_title_max'] = 'El t&iacute;tulo debe ser como m&aacute;ximo de 128 caracteres! N.B.: caracteres especiales se consideran ser decodificada (por ejemplo: "&amp;" = "&amp;amp;", "&gt;" = "&amp;gt;", etc.)';

$_['error_license'] = 'Advertencia: El dominio no parece tener la clave de licencia para activar este m&oacute;dulo, usted puede obtener una haciendo clic <a href="%s?languages=es" target="_blank">aqu&iacute;</a> o comprar una licencia, si no lo tiene, haga clic <a href="%s" target="_blank">aqu&iacute;</a>.';
$_['error_opencart_version'] = 'Advertencia: Este m&oacute;dulo no se pueden utilizar con esta versi&oacute;n OpenCart. Debe descargar el Pago expr&eacute;s de PayPal m&oacute;dulo compatible con OpenCart %s';

#Button
$_['button_refund'] = 'Reembolso transacci&oacute;n de PayPal';
$_['button_void'] = 'Nulo transacci&oacute;n de PayPal';
$_['button_capture'] = 'Captura de transacci&oacute;n de PayPal';
$_['button_get_api'] = 'Obtener Credenciales API';
$_['button_get_api_test'] = 'Obtener Credenciales API SandBox';

#Api Reference
$_['text_transaction_id'] = 'ID de transacci&oacute;n';
$_['text_message_api_paypal_express'] = 'La transacci&oacute;n se realiza de forma autom&aacute;tica a trav&eacute;s de la API de PayPal';

#Refund
$_['text_refund_type'] = 'Tipo de Reembolso';
$_['text_refund_amt'] = 'La cantidad del reembolso: El importe se requiere si el tipo de reembolso es <em>parcial</em>';
$_['success_refunded'] = 'La transacci&oacute;n ha sido reembolsado!';

#Void
$_['success_voided'] = 'La transacci&oacute;n se ha anulado!';

#Capture
$_['text_capture_type'] = 'Tipo de captura';
$_['text_capture_amt'] = 'Captura cantidad';
$_['success_captured'] = 'La transacci&oacute;n ha sido capturado!';

#Tab
$_['tab_live'] = 'Producci&oacute;n';
$_['tab_test'] = 'Test';
?>