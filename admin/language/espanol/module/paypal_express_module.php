<?php
// Heading
$_['heading_title']       = 'M&oacute;dulo PayPal Express Checkout';

// Text
$_['text_module']         = 'M&oacute;dulos';
$_['text_success']        = '&Eacute;xito: Se ha modificado el m&oacute;dulo de PayPal Express Checkout!';
$_['text_content_top']    = 'Contenido principal';
$_['text_content_bottom'] = 'Contenido de fondo';
$_['text_column_left']    = 'Columna izquierda';
$_['text_column_right']   = 'Columna de la derecha';

// Entry
$_['entry_layout']        = 'Disposici&oacute;n:';
$_['entry_position']      = 'Posici&oacute;n:';
$_['entry_status']        = 'Estado:';
$_['entry_sort_order']    = 'Orden de Clasificaci&oacute;n:';

// Error
$_['error_permission']    = 'Advertencia: Usted no tiene permiso para modificar este m&oacute;dulo!';
?>