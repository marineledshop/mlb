<?php

// Heading
$_['heading_title'] = 'PayPal Express Checkout';

// Text
$_['text_payment'] = 'Оплата';
$_['text_success'] = 'Вы внесли изменеия в модуль PayPal Express Checkout!';
$_['text_paypal_express'] = '<a href="https://www.paypal.com/uk/mrb/pal=S53S5NBZGRYF4" target="_blank"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Авторизация';
$_['text_sale'] = 'Продажа';
$_['text_shipping_address'] = 'Адрес доставки';
$_['text_noshipping_auto'] = 'Автоматический выбор расширением (рекомендуется)';
$_['text_noshipping_display'] = 'PayPal отображает адрес доставки на страницах PayPal';
$_['text_noshipping_notdisplay'] = 'PayPal не отображает поля адреса доставки бы то ни было';
$_['text_noshipping_profile'] = 'Если покупатель не передает адрес доставки, PayPal получает его из профиля учетной записи покупателя';
$_['text_payment_address'] = 'Адрес плательщика';
$_['text_createaccount_customer_choose'] = 'Клиент сам принимает решение о регистрации';

// Entry
$_['entry_username'] = 'API Username:';
$_['entry_password'] = 'API Password:';
$_['entry_signature'] = 'API Signature:';
$_['entry_username_test'] = 'API Username (тест):';
$_['entry_password_test'] = 'API Password (тест):';
$_['entry_signature_test'] = 'API Signature (тест):';
$_['entry_title'] = 'Название модуля:';
$_['entry_test'] = 'Режим тестирования:';
$_['entry_method'] = 'Метод транзакции:';
$_['entry_logo'] = 'Использовать Логотип:<br /><span class="help">Максимально допустимый размер изображения 750 пикселей в ширину и 90 пикселей в высоту. Если вы не укажете изображение, то будет отобража только название организации.</span>';
$_['entry_createaccount'] = 'Создать аккаунт покупателя:<br /><span class="help">Автоматическое создание аккаунта для покупателя, используя данные от PayPal. Покупателю автоматически будет выслано письмо на e-mail с автоматически сгенерированным паролем. Если e-mail уже существует в базе, то покупатель будет автоматически залогининым.</span>';
$_['entry_merchant_country'] = 'Страна торговли:<br /><span class="help">Если не указано, то по умолчанию будет использована страна адреса клиента.</span>';
$_['entry_skip_confirm'] = 'Перейти Подтверждение оплаты:<br /><span class="help">Если установлено значение Да, клиент пропустить страницу подтверждения после того, как PayPal логин и оно автоматически перенаправлены на страницу успех выезд.<br /><strong>Примечание</strong>: это не эффект, когда клиент должен быть выбор способа доставки и/или зарегистрировать аккаунт и/или проверки, как гость с подтверждением адреса и т.д. после входа в PayPal.</span>';
$_['entry_confirm_order'] = 'Подтвердить заказ до входа в PayPal:<br /><span class="help">Если установлено значение Да, заказ будет подтвержден перед клиентом пойти в PayPal с "Стандарт" статус заказа.<br /><strong>Примечание</strong>: это не имеет эффекта, если опция "Пропустить Подтвердить платеж" установлен как "Нет" или если клиент нажмите на кнопку PayPal Express.</span>';
$_['entry_default_currency'] = 'Основная валюта:<br /><span class="help">Если клиент выбрал не поддерживаемую валюту, то эта валюта будет автоматически конвертироваться. Поддерживаемые PayPal Express валюты:
<a href="http://www.xe.com/currency/aud-australian-dollar" target="_blank" alt="Australian Dollar" title="Australian Dollar">AUD</a>,
<a href="http://www.xe.com/currency/cad-canadian-dollar" target="_blank" alt="Canadian Dollar" title="Canadian Dollar">CAD</a>,
<a href="http://www.xe.com/currency/czk-czech-koruna" target="_blank" alt="Czech Koruna" title="Czech Koruna">CZK</a>,
<a href="http://www.xe.com/currency/dkk-danish-krone" target="_blank" alt="Danish Krone" title="Danish Krone">DKK</a>,
<a href="http://www.xe.com/currency/eur-euro" target="_blank" alt="Euro" title="Euro">EUR</a>,
<a href="http://www.xe.com/currency/hkd-hong-kong-dollar" target="_blank" alt="Hong Kong Dollar" title="Hong Kong Dollar">HKD</a>,
<a href="http://www.xe.com/currency/huf-hungarian-forint" target="_blank" alt="Hungarian Forint" title="Hungarian Forint">HUF</a>,
<a href="http://www.xe.com/currency/ils-israeli-shekel" target="_blank" alt="Israeli Shekel" title="Israeli Shekel">ILS</a>,
<a href="http://www.xe.com/currency/jpy-japanese-yen" target="_blank" alt="Japanese Yen" title="Japanese Yen">JPY</a>,
<a href="http://www.xe.com/currency/mxn-mexican-peso" target="_blank" alt="Mexican Peso" title="Mexican Peso">MXN</a>,
<a href="http://www.xe.com/currency/nok-norwegian-krone" target="_blank" alt="Norwegian Krone" title="Norwegian Krone">NOK</a>,
<a href="http://www.xe.com/currency/nzd-new-zealand-dollar" target="_blank" alt="New Zealand Dollar" title="New Zealand Dollar">NZD</a>,
<a href="http://www.xe.com/currency/pln-polish-zloty" target="_blank" alt="Polish Zloty" title="Polish Zloty">PLN</a>,
<a href="http://www.xe.com/currency/gbp-british-pound" target="_blank" alt="British Pound" title="British Pound">GBP</a>,
<a href="http://www.xe.com/currency/sgd-singapore-dollar" target="_blank" alt="Singapore Dollar" title="Singapore Dollar">SGD</a>,
<a href="http://www.xe.com/currency/sek-swedish-krona" target="_blank" alt="Swedish Krona" title="Swedish Krona">SEK</a>,
<a href="http://www.xe.com/currency/chf-swiss-franc" target="_blank" alt="Swiss Franc" title="Swiss Franc">CHF</a>,
<a href="http://www.xe.com/currency/usd-us-dollar" target="_blank" alt="US Dollar" title="US Dollar">USD</a>,
<a href="http://www.xe.com/currency/twd-taiwan-new-dollar" target="_blank" alt="Taiwan New Dollar" title="Taiwan New Dollar">TWD</a>,
<a href="http://www.xe.com/currency/thb-thai-baht" target="_blank" alt="Thai Baht" title="Thai Baht">THB</a>,
<a href="http://www.xe.com/currency/brl-brazilian-real" target="_blank" alt="Brazilian Real" title="Brazilian Real">BRL</a> (только для Бразилии),
<a href="http://www.xe.com/currency/myr-malaysian-ringgit" target="_blank" alt="Malaysian Ringgit" title="Malaysian Ringgit">MYR</a> (только для Малайзии),
<a href="http://www.xe.com/currency/try-turkish-lira" target="_blank" alt="Turkish Lira" title="Turkish Lira">TRY</a> (только для Турции).
</span>';
$_['entry_send_address'] = 'Адрес плательщика или адрес доставки:<br /><span class="help">Выберите адрес, который будет указан в PayPal.</span>';
$_['entry_noshipping'] = 'Отправить адрес:<br /><span class="help">Для цифровых товаров:' . $_['text_noshipping_notdisplay'] . '.</span>';
$_['entry_senditem'] = 'Отображать отчеты в PayPal:<br /><span class="help">Отображение товара на странице PayPal. Если это вызывает ошибку "10413", то эта функция будет отключина автоматически.</span>';
$_['entry_landing'] = 'Назначение Страницы:<br /><span class="help">Тип отображаемой PayPal страницы: Login page или Credit Card Billing page.</span>';
$_['entry_total'] = 'Нижняя граница:<br /><span class="help">Минимальная сумма заказа. Ниже этой суммы метод будет недоступен.</span>';
$_['entry_total_over'] = 'Верхняя граница:<br /><span class="help">Максимальная сумма заказа. Выше этой суммы метод будет недоступен..</span>';
$_['entry_order_status'] = 'Статус Заказа:';
$_['entry_order_status_complete'] = 'Статус заказа Завершить:<br /><span class="help">Статус заказа, когда он оплачен.</span>';
$_['entry_order_status_refunded'] = 'Статус заказа возвращается:<br /><span class="help">Статус заказа, когда оплата возвращается.</span>';
$_['entry_order_status_voided'] = 'Статус заказа aннулированные:<br /><span class="help">Статус заказа, при оплате недействительными.</span>';
$_['entry_enableincheckout'] = 'Включить этот модуль проверки:<br /><span class="help">Когда эта опция отключена, этот модуль не вижу в список платежей Checkout, но это работает только кнопка Express Checkout.</span>';
$_['entry_geo_zone'] = 'ГЕО Зоны:';
$_['entry_status'] = 'Статус:';
$_['entry_sort_order'] = 'Порядок Сортировки:';

// Error
$_['error_permission'] = 'У вас нет прав на изменение модуля!';
$_['error_username'] = 'API Username - обязательно!';
$_['error_password'] = 'API Password - обязательно!';
$_['error_signature'] = 'API Signature - обязательно!';
$_['error_title'] = 'Название Обязательно!';
$_['error_title_max'] = 'Название должно быть до 128 символов! примечание: специальные символы, как полагают, подлежащего декодированию (например: "&amp;" = "&amp;amp;", "&gt;" = "&amp;gt;", и т.д.)';

$_['error_license'] = 'Предупреждение: похоже у вас нет лицензионного ключа для активации этого модуля, вы можете попробовать восстановить его нажав <a href="%s" target="_blank">сюда</a>, для приобретения лицензию нажмите <a href="%s" target="_blank">здесь</a>.';
$_['error_opencart_version'] = 'Этот модуль не совместим с данной версией OpenCart. Вы должны загрузить модуль PayPal Express Checkout совместимый с OpenCart %s';

#Button
$_['button_refund'] = 'Возврат PayPal транзакции';
$_['button_void'] = 'Отмена PayPal транзакции';
$_['button_capture'] = 'Принятие PayPal транзакции';
$_['button_get_api'] = 'Получить API полномочия';
$_['button_get_api_test'] = 'Получить SandBox API Полномочия';

#Api Reference
$_['text_transaction_id'] = 'Transaction ID';
$_['text_message_api_paypal_express'] = 'Сделка будет проверяться автоматически через PayPal API';

#Refund
$_['text_refund_type'] = 'Тип возврата';
$_['text_refund_amt'] = 'Возврат суммы: сумма требуется, если тип возврата <em>Частичный</em>';
$_['success_refunded'] = 'Транзакция была возвращена!';

#Void
$_['success_voided'] = 'Транзакция была аннулирована!';

#Capture
$_['text_capture_type'] = 'Типа принятия';
$_['text_capture_amt'] = 'Принятие суммы';
$_['success_captured'] = 'Транзакция была принята!';

#Tab
$_['tab_live'] = 'производство';
$_['tab_test'] = 'тест';
?>