<?php
// Heading
$_['heading_title']    = 'USPS Priority Mail';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified USPS Priority Mail shipping!';

// Entry
$_['entry_rate']       = 'Rates:<br /><span class="help">Example: 5:10.00,7:12.00 Weight:Cost,Weight:Cost, etc..</span>';
$_['entry_tax_class']  = 'Tax Class:';
$_['entry_geo_zone']   = 'Geo Zone:';
$_['entry_status']     = 'Status:';
$_['entry_delivery']   = 'Estimated Delivery:';
$_['entry_days']       = 'business days';
$_['entry_sort_order'] = 'Sort Order:';
$_['entry_us_only']     = 'US Only:';
$_['entry_min_limit']   = 'Minimum total to show:';
$_['entry_free_limit']  = 'Total for free:';
$_['entry_free_limit_us']  = 'Total for free (US):';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify weight based shipping!';
?>