<?php
// Heading
$_['heading_title']    = 'Spend X for Free Shipping';

// Text 
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified Spend X for Free Shipping!';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Spend X for Free Shipping!';
?>