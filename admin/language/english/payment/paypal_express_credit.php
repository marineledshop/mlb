<?php

// Heading
$_['heading_title'] = 'PayPal Express Checkout (Credit Card)';

// Text 
$_['text_payment'] = 'Payment';
$_['text_success'] = 'Success: You have modified PayPal Express Checkout account details!';
$_['text_paypal_express'] = '<a href="https://www.paypal.com/uk/mrb/pal=S53S5NBZGRYF4" target="_blank"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Authorization';
$_['text_sale'] = 'Sale';
$_['text_shipping_address'] = 'Shipping Address';
$_['text_payment_address'] = 'Payment Address';
$_['text_createaccount_customer_choose'] = 'The customer decides whether to register an account';

// Entry
$_['entry_username'] = 'API Username:';
$_['entry_password'] = 'API Password:';
$_['entry_signature'] = 'API Signature:';
$_['entry_username_test'] = 'API Username (Test):';
$_['entry_password_test'] = 'API Password (Test):';
$_['entry_signature_test'] = 'API Signature (Test):';
$_['entry_title'] = 'Module\'s Title:';
$_['entry_test'] = 'Test Mode:';
$_['entry_method'] = 'Transaction Method:';
$_['entry_logo'] = 'Enable Logo:<br /><span class="help">The image has a maximum size of 750 pixels wide by 90 pixels high. If you do not specify an image, the business name displays.</span>';
$_['entry_createaccount'] = 'Create Customer Account:<br /><span class="help">Automatically create an account from PayPal data. The customer will be notified by e-mail, also for the automatically generated password. If the e-mail already exists the customer will be logged.</span>';
$_['entry_merchant_country'] = 'Merchant Country:<br /><span class="help">If not specified, Default Country from Customer Address will be used.</span>';
$_['entry_default_currency'] = 'Default Currency:<br /><span class="help">If the customer choose a not supported currency, this currency will be converted automatically. Supported PayPal Express Currencies: AUD, CAD, CZK, DKK, EUR, HKD, HUF, ILS, JPY, MXN, NOK, NZD, PLN, GBP, SGD, SEK, CHF, USD, TWD, THB, BRL (only for Brazil), MYR (only for Malaysia), TRY (Only for Turkey)</span>';
$_['entry_skip_confirm'] = 'Skip Confirm Payment:<br /><span class="help">If it set to yes, the customer skip the confirmation page after PayPal login and it is redirected automatically to checkout success page.<br /><strong>NOTE</strong>: this has not effect when the customer must be choose the shipping method and/or register account and/or checkout as guest with confirmation of the addresses, etc. after PayPal login.</span>';
$_['entry_confirm_order'] = 'Confirm Order before PayPal login:<br /><span class="help">If it set to yes, the order will be confirmed before the customer go to PayPal with the "Standard" order status.<br /><strong>NOTE</strong>: this has not effect if the option "Skip Confirm Payment" is set as "No" or if the customer click on PayPal Express Button.</span>';
$_['entry_send_address'] = 'Send Payment or Shipping address:<br /><span class="help">Choose if send the payment address or shipping address at PayPal.</span>';
$_['entry_senditem'] = 'Display Summary in PayPal:<br /><span class="help">Display Item in PayPal Page. If it get the error "10413" the module will be disabled this feature automatically.</span>';
$_['entry_landing'] = 'Landing Page:<br /><span class="help">Type of PayPal page to display: Login page or Credit Card Billing page.</span>';
$_['entry_total'] = 'Total min:<br /><span class="help">The checkout total the order must reach before this payment method becomes active.</span>';
$_['entry_total_over'] = 'Total max:<br /><span class="help">The checkout total the order must reach before this payment method becomes deactivated.</span>';
$_['entry_order_status'] = 'Order Status:';
$_['entry_order_status_complete'] = 'Order Status Complete:<br /><span class="help">The order status when payment is complete.</span>';
$_['entry_order_status_refunded'] = 'Order Status Refunded:<br /><span class="help">The order status when payment is refunded.</span>';
$_['entry_order_status_voided'] = 'Order Status Voided:<br /><span class="help">The order status when payment is voided.</span>';
$_['entry_enableincheckout'] = 'Enable this module in checkout:<br /><span class="help">When this option is disabled this module don\'t see in payment checkout list but it work only by button express checkout.</span>';
$_['entry_geo_zone'] = 'Geo Zone:';
$_['entry_status'] = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify payment PayPal Express Checkout!';
$_['error_username'] = 'API Username Required!';
$_['error_password'] = 'API Password Required!';
$_['error_signature'] = 'API Signature Required!';
$_['error_title'] = 'Title Required!';

$_['error_license'] = 'Warning: The domain appears to have no license key to activate this module, you can obtain one by clicking <a href="%s?languages=en" target="_blank">here</a> or purchase a license, if you do not have one, click <a href="%s" target="_blank">here</a>.';
$_['error_opencart_version'] = 'Warning: This module cannot be used with this OpenCart version. You must download the PayPal Express Checkout module compatible with OpenCart %s';

#Button
$_['button_refund'] = 'Refund PayPal Transaction';
$_['button_void'] = 'Void PayPal Transaction';
$_['button_capture'] = 'Capture PayPal Transaction';

#Api Reference
$_['text_transaction_id'] = 'Transaction ID';
$_['text_message_api_paypal_express'] = 'The Transaction will be made automatically through the PayPal API';

#Refund
$_['text_refund_type'] = 'Refund Type';
$_['text_refund_amt'] = 'Refund amount: The amount is required if Refund Type is <em>Partial</em>';
$_['success_refunded'] = 'The Transaction has been refunded!';

#Void
$_['success_voided'] = 'The Transaction has been voided!';

#Capture
$_['text_capture_type'] = 'Capture Type';
$_['text_capture_amt'] = 'Capture amount';
$_['success_captured'] = 'The Transaction has been captured!';
?>