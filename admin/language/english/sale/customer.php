<?php
// Heading
$_['heading_title']         = 'Customer';

// Text
$_['text_success']          = 'Success: You have modified customers!';
$_['text_default']          = 'Default';
$_['text_approved']         = 'You have approved %s accounts!';
$_['text_wait']             = 'Please Wait!';
$_['text_balance']          = 'Balance:';

// Column
$_['column_name']           = 'Customer Name';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Customer Group';
$_['column_status']         = 'Status'; 
$_['column_login']          = 'Login into Store';
$_['column_approved']       = 'Approved';
$_['column_date_added']     = 'Date Added';
$_['column_description']    = 'Description';
$_['column_amount']         = 'Amount';
$_['column_points']         = 'Points';
$_['column_ip']             = 'IP';
$_['column_total']          = 'Total Accounts';
$_['column_action']         = 'Action';

// Entry
$_['entry_firstname']       = 'First Name:';
$_['entry_lastname']        = 'Last Name:';
$_['entry_email']           = 'E-Mail:';
$_['entry_telephone']       = 'Telephone:';
$_['entry_fax']             = 'Fax:';
$_['entry_newsletter']      = 'Newsletter:';
$_['entry_customer_group']  = 'Customer Group:';
$_['entry_status']          = 'Status:';
$_['entry_password']        = 'Password:';
$_['entry_confirm']         = 'Confirm:';
$_['entry_company']         = 'Company:';
$_['entry_address_1']       = 'Address 1:';
$_['entry_address_2']       = 'Address 2:';
$_['entry_city']            = 'City:';
$_['entry_postcode']        = 'Postcode:';
$_['entry_country']         = 'Country:';
$_['entry_zone']            = 'Region / State:';
$_['entry_default']         = 'Default Address:';
$_['entry_amount']          = 'Amount:';
$_['entry_points']          = 'Points:';
$_['entry_description']     = 'Description:';

// Error
$_['error_warning']         = 'Warning: Please check the form carefully for errors!';
$_['error_permission']      = 'Warning: You do not have permission to modify customers!';
$_['error_exists']          = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']       = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']        = 'Last Name must be between 1 and 32 characters!';
$_['error_email']           = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']       = 'Telephone must be between 3 and 32 characters!';
$_['error_password']        = 'Password must be between 4 and 20 characters!';
$_['error_confirm']         = 'Password and password confirmation do not match!';
$_['error_address_1']       = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']            = 'City must be between 2 and 128 characters!';
$_['error_postcode']        = 'Postcode must be between 2 and 10 characters for this country!';
$_['error_country']         = 'Please select a country!';
$_['error_zone']            = 'Please select a region / state!';
// Enhanced Customer Information - START
$_['text_confirm_reset']	  = 'Are you sure to reset the log?';
$_['text_clear_cart']		  = 'Clear Cart';
$_['text_clear_wishlist']	  = 'Clear Wishlist';

$_['column_order_id']         = 'Order ID';
$_['column_inv_id']       	  = 'Invoice No.';
$_['column_order_products']   = 'No. Products';
$_['column_order_status']     = 'Order Status';
$_['column_store']      	  = 'Store Name';
$_['column_order_total']      = 'Order Total';
$_['column_total_value']      = 'Total Value';
$_['column_total_orders']     = 'No. Orders';
$_['column_total_accounts']   = 'Total Accounts';
$_['column_image'] 			  = 'Image';
$_['column_sku']              = 'SKU';
$_['column_model']            = 'Model';
$_['column_product']          = 'Product';
$_['column_product_options']  = 'Product Options';
$_['column_product_quantity'] = 'Quantity';
$_['column_quantity']         = 'Quantity';
$_['column_prod_total']       = 'Total';
$_['column_category'] 		  = 'Category';
$_['column_manufacturer'] 	  = 'Manufacturer';
$_['column_route']    		  = 'Page';
$_['column_ip_address']    	  = 'IP Address<br><span style="font-size:10px;">(Browser Used)</span>';
$_['column_current_url'] 	  = 'Page URL<br><span style="color:#999;">Referrer Site</span>';
$_['column_access_date']      = 'Visit Date';
$_['column_access_time']      = 'Click Time';

$_['tab_customer_history']    = 'Order History';
$_['tab_customer_products']   = 'Purchased Products';
$_['tab_cart']  		  	  = 'Shopping Cart';
$_['tab_wishlist']  		  = 'Wishlist';
$_['tab_customer_track']  	  = 'Customer Track';

$_['entry_date_start']		  = 'Date Start:';
$_['entry_date_end']		  = 'Date End:';

$_['error_cart_product']	  = 'Warning: There is a product in your customer\'s cart that is no longer in your product list!';
$_['error_wishlist_product']  = 'Warning: There is a product in your customer\'s wishlist that is no longer in your product list!';
// Enhanced Customer Information - END
?>