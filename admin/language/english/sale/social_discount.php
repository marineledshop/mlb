<?php
// Heading  
$_['heading_title']       = 'Social Discount';

// Text
$_['text_success']        = 'Success: You have modified Social Discount!';
$_['text_default']        = 'Default';
$_['text_percent']        = 'Percentage';
$_['text_amount']         = 'Fixed Amount';
$_['text_proportional']   = 'Proportional';
$_['text_fixed']          = 'Fixed';  

// Column
$_['column_name']         = 'Social Discount Name';
$_['column_social_action'] = 'Social Action';
$_['column_discount']     = 'Discount';
$_['column_date_start']   = 'Date Start';
$_['column_date_end']     = 'Date End';
$_['column_status']       = 'Status';
$_['column_order_id']     = 'Order ID';
$_['column_customer']     = 'Customer';
$_['column_amount']       = 'Amount';
$_['column_date_added']   = 'Date Added';
$_['column_action']       = 'Action';

// Entry
$_['entry_name']          = 'Social Discount Name:';
$_['entry_social_action']          = 'Social Action:<br /><span class="help">Select Social Action For Discounts</span>';
$_['entry_type']          = 'Type:<br /><span class="help">Percentage or Fixed Amount</span>';
$_['entry_discount']      = 'Discount:';
$_['entry_logged']        = 'Customer Login:<br /><span class="help">Customer must be logged in to use this social discount.</span>';
$_['entry_shipping']      = 'Free Shipping:';
$_['entry_quantity_total'] = 'Total Quantity:<br /><span class="help">The total quantity that must reached before sale is valid. <br/>Also define range 2-8.</span>';
$_['entry_total']         = 'Total Amount:<br /><span class="help">The total amount that must reached before sale is valid. <br/>Also define range 100-500.</span>';
$_['entry_date_start']    = 'Date Start:';
$_['entry_date_end']      = 'Date End:';
$_['entry_uses_total']    = 'Uses Per Sales:<br /><span class="help">The maximum number of times the sale can be used by any customer. Leave blank for unlimited</span>';
$_['entry_uses_customer'] = 'Uses Per Customer:<br /><span class="help">The maximum number of times the sale can be used by a single customer. Leave blank for unlimited</span>';
$_['entry_status']        = 'Status:';
$_['days'] 				  = 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday';

$_['social_discount_actions'] 	= 'Facebook Like,Twitter Tweet,Twitter Follow';
$_['entry_social_network_page'] = 'Social Network Page:<br /><span class="help">Only Specify if its different than defined in <br/> Extensions->Order Totals->Social Discount</span>';

$_['entry_tweet_text'] = 'Twitter Tweet Text:<br /><span class="help">Only Specify if using <br/> Twitter Tweet Discount. <br/>Limit 140 Characters.</span>';

$_['default_tweet_text'] = 'Welcome to http://opcrat.com Opcrat!';


// Error
$_['error_warning']       = 'Warning: Please check the form carefully for errors!';
$_['error_permission']    = 'Warning: You do not have permission to modify Social Discount!';
$_['error_name']          = 'Social Discount Name must be between 3 and 128 characters!';
$_['error_tweet_text'] = 'Twitter Tweet text limit 140 characters!';
$_['error_name_common']   = 'Social Discount Name must be unique. Name you have entered is already Used!';
$_['error_discount']      = 'Discount Value Must Be Numeric!';
$_['error_quantity_total']      = 'Total Quantity Must Be Numeric!';
$_['error_total']         = 'Total Value Must Be Numeric!';
$_['error_uses_customer'] = 'User Per Customer Must Be Numeric!';
$_['error_uses_total']    = 'Total Uses Must Be Numeric!';
?>
