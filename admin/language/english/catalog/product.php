<?php
// Heading
$_['heading_title']          = 'Products'; 

// Text  
$_['text_success']           = 'Success: You have modified products!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Default';
$_['text_image_manager']     = 'Image Manager';
$_['text_browse']            = 'Browse Files';
$_['text_clear']             = 'Clear Image';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option Value';
$_['text_percent']           = 'Percentage';
$_['text_amount']            = 'Fixed Amount';

// Column
$_['column_name']            = 'Product Name';
$_['column_model']           = 'Model';
$_['column_image']           = 'Image';
$_['column_price']           = 'Price';
$_['column_quantity']        = 'Quantity';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Product Name:';
// Add
$_['column_category']        = 'Category';
// End add
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords:';
$_['entry_shipping_description'] = 'Shipping Description:';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_header']           = 'Header Text:';
$_['entry_description']      = 'Description:';
$_['entry_store']            = 'Stores:';
$_['entry_keyword']          = 'SEO Keyword:';
$_['entry_model']            = 'Model:';
$_['entry_sku']              = 'SKU:';
$_['entry_upc']              = 'UPC:';
$_['entry_location']         = 'Location:';
$_['entry_manufacturer']     = 'Manufacturer:';
$_['entry_shipping']         = 'Requires Shipping:';
$_['entry_hs_code']          = 'HS Code:';
$_['entry_brochure']         = 'Brochure:';
$_['entry_date_available']   = 'Date Available:';
$_['entry_quantity']         = 'Quantity:';
$_['entry_minimum']          = 'Minimum Quantity:<br/><span class="help">Force a minimum ordered amount</span>';
$_['entry_stock_status']     = 'Out Of Stock Status:<br/><span class="help">Status shown when a product is out of stock</span>';
$_['entry_price']            = 'Price:';
$_['entry_tax_class']        = 'Tax Class:';
$_['entry_points']           = 'Points:<br/><span class="help">Number of points needed to buy this item. If you don\'t want this product to be purchased with points leave as 0.</span>';
$_['entry_option_points']    = 'Points:';
$_['entry_subtract']         = 'Subtract Stock:';
$_['entry_weight_class']     = 'Weight Class:';
$_['entry_weight']           = 'Weight:';

//$_['entry_box']           = 'Shipped in Box:<br/><span class="help">+150gr per order if more than x items</span>';
$_['entry_box']           = 'Shipped in Box:<br/><span class="help">+150gr per order if more than 2</span>';

$_['entry_length']           = 'Length Class:';
$_['entry_dimension']        = 'Dimensions (D x L x W x H):';
$_['entry_image']            = 'Image:';
$_['entry_customer_group']   = 'Customer Group:';
$_['entry_date_start']       = 'Date Start:';
$_['entry_date_end']         = 'Date End:';
$_['entry_priority']         = 'Priority:';
$_['entry_attribute']        = 'Attribute:';
$_['entry_attribute_group']  = 'Attribute Group:';
$_['entry_text']             = 'Text:';
$_['entry_option']           = 'Option:';
$_['entry_option_value']     = 'Option Value:';
$_['entry_required']         = 'Required:';
$_['entry_status']           = 'Status:';
$_['entry_sort_order']       = 'Sort Order:';
$_['entry_category']         = 'Categories:';
$_['entry_download']         = 'Downloads:';
$_['entry_related']          = 'Related Products:<br /><span class="help">(Autocomplete)</span>';
$_['entry_tag']          	 = 'Product Tags:<br /><span class="help">comma separated</span>';
$_['entry_reward']           = 'Reward Points:';
$_['entry_layout']           = 'Layout Override:';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_name']             = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Product Model must be greater than 3 and less than 64 characters!';

// Detailed Product Sales Report in Product Form - START
$_['tab_report']             	= 'Product Sales Report';

$_['column_qty_sold']       	= 'Sold';
$_['column_date_start'] 		= 'Date Start';
$_['column_date_end']   		= 'Date End';
$_['column_prod_name']     		= 'Product Name';
$_['column_total']    			= 'Total';
$_['column_date_added']     	= 'Date Added';
$_['column_order_id']       	= 'Order ID';
$_['column_inv_date']			= 'Invoice Date';
$_['column_inv_id']       	 	= 'Invoice No.';
$_['column_cust_name']      	= 'Customer Name';
$_['column_email']          	= 'Customer E-Mail';
$_['column_customer_group'] 	= 'Customer Group';
$_['column_shipping_method']	= 'Shipping Method';
$_['column_payment_method'] 	= 'Payment Method';
$_['column_order_status']   	= 'Order Status';
$_['column_store']     			= 'Store';
$_['column_prod_currency']  	= 'Currency';
$_['column_prod_price']   		= 'Price';
$_['column_prod_quantity']  	= 'Quantity';
$_['column_prod_total']     	= 'Total';
$_['column_prod_tax']      		= 'Tax';

$_['entry_date_start']  		= 'Date Start:';
$_['entry_date_end']    		= 'Date End:';
$_['entry_range']       		= 'Statistics Range:';

$_['stat_custom']       		= 'Custom (use date range)';
$_['stat_week']         		= 'Week (today -7 days)';
$_['stat_month']        		= 'Month (today -30 days)';
$_['stat_quarter']      		= 'Quarter (today -91 days)';
$_['stat_year']         		= 'Year (today -365 days)';
$_['stat_current_week']     	= 'Current Week (monday to today)';
$_['stat_current_month']    	= 'Current Month (from 1st to today)';
$_['stat_current_quarter']  	= 'Current Quarter (current term)';
$_['stat_current_year']     	= 'Current Year (1st Jan to today)';
$_['stat_last_week']        	= 'Last Week (monday to sunday)';
$_['stat_last_month']       	= 'Last Month (from 1st to 31st)';
$_['stat_last_quarter']     	= 'Last Quarter (last term)';
$_['stat_last_year']        	= 'Last Year (1st Jan to 31st Dec)';
$_['stat_all_time']         	= 'All Time';
// Detailed Product Sales Report in Product Form - END


?>