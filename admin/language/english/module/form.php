<?php
//==============================================================================
// Flexible Form v154.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
//==============================================================================

$version = 'v154.1';

// Heading
$_['heading_title']				= 'Flexible Form';

// Buttons
$_['button_save_exit']			= 'Save & Exit';
$_['button_save_keep_editing']	= 'Save & Keep Editing';
$_['button_add_field']			= 'Add Field';

// Entry
$_['entry_form_link']			= 'Form Link:<br /><span class="help">Paste the HTML code wherever you would like the form link to appear.</span>';
$_['entry_title']				= 'Title:';
$_['entry_email_results_to']	= 'E-mail Results To:<br /><span class="help">Enter the e-mail addresses where the form results are sent. Separate multiple addresses by , (commas).</span>';
$_['entry_email_subject']		= 'E-mail Subject:<br /><span class="help">Enter the subject for the form results e-mail.</span>';
$_['entry_file_size_limit']		= 'File Size Limit:<br /><span class="help">Enter the maximum file size allowed in KB for file upload fields.<br />1 MB = 1024 KB</span>';
$_['entry_allowed_extensions']	= 'Allowed Extensions:<br /><span class="help">Enter the allowed file extensions for file upload fields, separated by , (commas).</span>';
$_['entry_html_content_before']	= 'HTML Content Before Fields:';
$_['entry_include_captcha']		= 'Include Captcha:<br /><span class="help">Including captcha validation will help stop spam.</span>';
$_['entry_html_content_after']	= 'HTML Content After Fields:';
$_['entry_success_message']		= 'Success Message:';

// Field Types
$_['field_selection_based']		= 'Selection-Based';
$_['field_checkbox']			= 'Checkbox';
$_['field_radio']				= 'Radio';
$_['field_select']				= 'Select';
$_['field_text_based']			= 'Text-Based';
$_['field_email']				= 'E-mail';
$_['field_email_confirm']		= 'E-mail + Confirmation';
$_['field_password']			= 'Password';
$_['field_text']				= 'Text';
$_['field_textarea']			= 'Textarea';
$_['field_time_based']			= 'Time-Based';
$_['field_date']				= 'Date';
$_['field_time']				= 'Time';
$_['field_date_and_time']		= 'Date &amp; Time';
$_['field_upload_based']		= 'Upload-Based';
$_['field_file']				= 'File';

// Text
$_['text_add_new_form']			= 'To add a new form:<ol><li>Enter a URL keyword. The keyword must be unique and only include letters, numbers, and hyphens.</li><br /><li>Click the green add button.</li></ol><em>Note: Once the keyword is set, it cannot be changed.</em>';
$_['text_url']					= 'URL:';
$_['text_html_code']			= 'HTML Code:';
$_['text_kb']					= 'KB';
$_['text_required']				= 'Required';
$_['text_name']					= 'Name';
$_['text_type']					= 'Type';
$_['text_value']				= 'Value(s)';
$_['text_value_help']			= 'For checkbox/radio/select fields, separate values by ; (semi-colons).<br />For password fields, enter the password required to submit the form.<br />For all other fields, optionally enter the default value.';
$_['text_column']				= 'Column';
$_['text_sort_order']			= 'Sort Order';

// Copyright
$_['copyright']					= '<div style="text-align: center" class="help">' . $_['heading_title'] . ' ' . $version . ' &copy; <a target="_blank" href="http://www.getclearthinking.com">Clear Thinking, LLC</a></div>';

// Standard Text
$_['text_module']				= 'Modules';
$_['text_success']				= 'Success: You have modified ' . $_['heading_title'] . '!';

// Error
$_['error_permission']			= 'Warning: You do not have permission to modify ' . $_['heading_title'] . '!';
?>