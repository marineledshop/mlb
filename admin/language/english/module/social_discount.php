<?php
// Heading
$_['heading_title']       = 'Social Discount';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module social discount!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_product']   = 'Products';
$_['text_product_buy']   = 'Buy Products';
$_['text_product_both']   = 'Both of Above';
$_['text_social_discount_note'] = '<br/><br/>Note: If you have not selected any product in particular social discount than by default it will display products from your catalog!';



// Entry
$_['entry_product']         = 'Products:<br /><span class="help">(Autocomplete)</span>';
$_['entry_social_discount'] = 'Social Discount:';
$_['entry_product']         = 'Product:';
$_['entry_limit']           = 'Limit:';
$_['entry_image']           = 'Image (W x H):';
$_['entry_layout']          = 'Layout:';
$_['entry_position']        = 'Position:';
$_['entry_status']          = 'Status:';
$_['entry_sort_order']      = 'Sort Order:';

// Error 
$_['error_permission']    = 'Warning: You do not have permission to modify module social discount!';
$_['error_image']         = 'Image width &amp; height dimensions required!';
?>