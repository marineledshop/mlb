<?php
// -------------------------------------
// Product Tabs or Modules for OpenCart
// By Best-Byte
// www.best-byte.com
// -------------------------------------

// Heading
$_['heading_title']       = 'Product Tabs or Modules';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Product Tabs or Modules!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';

// Entry
$_['entry_image']         = 'Image (W x H):';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';
$_['entry_limit']         = 'Limit:';
$_['entry_tab']        	  = 'Use Tabs:';
$_['entry_moduleinfo']    = '<span class="help">Product Tabs or Modules by <a href="http://www.best-byte.com" target="_blank">Best-Byte</a></br /><br />For more OpenCart Extensions please visit our web site <a href="http://www.best-byte.com" target="_blank">www.best-byte.com</a></span>';

// Tabs
$_['tab_number']		  	  = 'Tab/Module Number';
$_['number']		  	      = '#';
$_['add_tab']		  	      = 'Add Tab';
$_['tab_title']		  	    = 'Tab or Heading Title';
$_['choose_products']	    = 'Choose Products to Display';
$_['special_products']	  = 'Special Products';
$_['latest_products']	    = 'Latest Products';
$_['popular_products']	  = 'Most Viewed Products';
$_['bestseller_products']	= 'Bestseller Products';
$_['choose_category']	    = 'Choose a Category';

// Error 
$_['error_permission']    = 'Warning: You do not have permission to modify module Product Tabs or Modules!';
$_['error_image']         = 'Image width and height dimensions required!';
$_['error_category']      = 'Category required!';
$_['error_numproduct']    = 'Number of products required!';
?>