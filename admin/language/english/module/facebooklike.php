<?php
/**
 * Facebook OpenCart Module. Creates a Facebook like box.
 *
 * @author 		www.opencartstore.com
 * @support		www.opencartstore.com/support/
 * @version		1.5.1.*
 */

// Heading
$_['heading_title']    			= 'Facebook Like';

// Text
$_['text_module']    			= 'Module';
$_['text_settings']				= 'Settings';
$_['text_success']     			= 'Success: You have modified Facebook Like settings!';
$_['text_content_top']    		= 'Content Top';
$_['text_content_bottom']		= 'Content Bottom';
$_['text_column_left']    		= 'Column Left';
$_['text_column_right']   		= 'Column Right';
$_['text_version_status']  		= 'Version Status';
$_['text_version_number']  		= '1.5.1.*';
$_['text_author']	     		= 'Author Details';
$_['text_facebooklike_support'] = 'Facebook Like Support Site';

// Entry
$_['entry_profile_id']			= 'Facebook Profile Id:';
$_['entry_height']				= 'Facebook Like Box Height:';
$_['entry_width']				= 'Facebook Like Box Width:';
$_['entry_stream']				= 'Display Stream:';
$_['entry_connections']			= 'No. Connections:';
$_['entry_header']				= 'Display Header:';
$_['entry_status']     			= 'Status:';
$_['entry_layout']        		= 'Layout:';
$_['entry_position']     		= 'Position:';
$_['entry_sort_order']     		= 'Sort Order:';

// Error
$_['error_permission'] 			= 'Warning: You do not have permission to modify Facebook Like module!';
?>
