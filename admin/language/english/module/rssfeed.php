<?php
// Heading
$_['heading_title']		= 'RSSFeedModule - Google Base';

// Text
$_['text_module']		= 'Modules';
$_['text_success']		= 'Success: You have modified module RSSFeedModule!';

// Entry
$_['entry_code']		= 'Code RSSFeed :<br /><span class="help"></span>';
$_['entry_status']		= 'Sort Order :';

// Error
$_['error_code']		= '* Code Required *** For Statut Disabled ( if empty textarea ) : Enter any symbol into Code RSSFeed textarea ***';
$_['error_permission']	= 'Warning: You do not have permission to modify module RSSFeedModule <b>RSSFeedModule</b> !!';
?>