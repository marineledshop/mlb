<?php
//==============================================================================
// Item-Based Fee/Discount v155.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
//==============================================================================

$name = 'Item';
$type = 'Fee/Discount';
$version = 'v155.1';

// Heading
$_['heading_title']					= $name . '-Based ' . $type;

// Buttons
$_['button_save_exit']				= 'Save & Exit';
$_['button_save_keep_editing']		= 'Save & Keep Editing';
$_['button_show_examples']			= 'Show Examples';
$_['button_add_row']				= 'Add Rate';

// Extension Settings
$_['entry_status']					= 'Status:';
$_['help_status']					= 'The status for the extension as a whole.';
$_['entry_sort_order']				= 'Sort Order:';
$_['help_sort_order']				= 'The sort order for the extension, relative to other ' . strtolower($type) . ' extensions.';
$_['entry_heading']					= 'Heading:';
$_['help_heading']					= 'The heading under which these shipping options will appear. HTML is supported.';
$_['entry_round_final_costs']		= 'Round Final Costs to the Nearest:';
$_['help_round_final_costs']		= 'If you want to round the final calculated charges, enter the rounding value to use. For example, to round to the nearest quarter of a dollar, enter 0.25';

// General Settings
$_['entry_general_settings']		= 'General Settings';
$_['help_general_settings']			= '
	<em>Title</em><br />
	Set the rate title for each language. HTML is supported.<br /><br />
	<em>Tax Class</em><br />
	Optionally select a tax class for the rate. If applying a tax class, be sure to set the Sort Order for the extension to something lower than the "Taxes" Order Total.<br /><br />
	' . ($type == 'Shipping' ? '' : '
	<em>Geo Zone Comparison</em><br />
	Select whether to compare geo zones against the customer\'s shipping address or payment address. Until the customer begins the checkout process or uses the shipping estimator, the customer\'s default address will be used if logged in, and the store\'s default address will be used if not logged in.<br /><br />
	') . '
	<em>Value for Total</em><br />
	Select the value used for ' . ($name == 'Total' ? 'Total-Based comparisons and ' : '') . 'percentage charges: the cart\'s Pre-Discounted Sub-Total (using all products\' original prices instead of Special or Discount prices), Sub-Total, Taxed Sub-Total, or Total (at the relative Sort Order of ' . ($type == 'Shipping' ? 'the "Shipping" Order Total). Products that do not require shipping are NOT included in the sub-total or taxed sub-total.' : 'this extension).');
$_['text_title']					= 'Title';
$_['text_tax_class']				= 'Tax Class';
$_['text_geo_zone_comparison']		= 'Geo Zone Comparison';
$_['text_shipping_address']			= 'Shipping Address';
$_['text_payment_address']			= 'Payment Address';
$_['text_value_for_total']			= 'Value for Total';
$_['text_prediscounted_subtotal']	= 'Pre-Discounted Sub-Total';
$_['text_subtotal']					= 'Sub-Total';
$_['text_taxed_subtotal']			= 'Taxed Sub-Total';
$_['text_total']					= 'Total';

// Order Criteria
$_['entry_order_criteria']			= 'Order Criteria';
$_['help_order_criteria']			= '
	<em>Stores</em><br />
	Select the stores for which the rate is available.<br /><br />
	<em>Currencies</em><br />
	Select the currencies for which the rate is available. "Convert Unselected" will automatically convert the cart total for unselected currencies, based on your currency rates.<br /><br />
	<em>Customer Groups</em><br />
	Select the customer groups for which the rate is available. "Not Logged In" will make the rate available to any customer not currently logged in.<br /><br />
	<em>Geo Zones</em><br />
	Select the geo zones for which the rate is available. "All Other Zones" will make the rate available to all locations not within a geo zone.';
$_['text_stores']					= 'Stores';
$_['text_currencys']				= 'Currencies';
$_['text_convert_unselected']		= '<em>Convert Unselected</em>';
$_['text_customer_groups']			= 'Customer Groups';
$_['text_not_logged_in']			= '<em>Not Logged In</em>';
$_['text_geo_zones']				= 'Geo Zones';
$_['text_all_other_zones']			= '<em>All Other Zones</em>';

// Cost Brackets
$_['entry_cost_brackets']			= 'Cost Brackets';
$_['help_cost_brackets']			= '
	<em>' . ($name == 'Postcode' ? 'Postcode Format' : $name . ' Adjustment') . '</em><br />
	' . ($name == 'Postcode' ? 'Select whether to read the postcode entered by the customer in a particular format.' : 'Optionally enter an additional factor (positive or negative, decimal or percentage) for adjusting the ' . ($name == 'Item' ? 'number of items' : strtolower($name)) . ' before calculations occur. Leave the field blank to have no adjustment.') . '<br /><br />
	<em>Notepad Icon</em><br />
	Click this to paste or enter tab-delimited data from a spreadsheet. Note that the new data will replace any current cost brackets.<br /><br />
	<em>From / To</em><br />
	' . ($name == 'Postcode' ? 'The beginning and ending postcodes (inclusive). If left blank, these will match all postcodes.' : 'The min and max bracket values (inclusive). If left blank, these will default to 0 and 999999, respectively.') . '<br /><br />
	<em>Charge</em><br />
	The ' . strtolower($type) . ' charge, as a flat rate or percentage of the total.' . ($type == 'Shipping' ? '' : ' Use positive values for fees and negative values for discounts.') . '<br /><br />
	<em>Per (unit)</em><br />
	An optional per-' . ($name == 'Postcode' ? 'item' : strtolower($name)) . '-unit multiplier.<br /><br />
	<em>Final Cost</em><br />
	' . ($name == 'Postcode' ? '' : 'Select "Single" to charge only the highest bracket reached, and "Cumulative" to charge the cumulative total of all brackets reached. ') . 'If needed, fill in an additional (Add) flat rate or percentage to add or subtract to the final cost, and an overall minimum (Min) and maximum (Max) final cost.';
$_['text_adjustment']				= ($name == 'Postcode' ? 'Postcode Format' : $name . ' Adjustment');
$_['text_uk_format']				= 'UK Format';
$_['text_from']						= 'From';
$_['text_to']						= 'To';
$_['text_charge']					= 'Charge';
$_['text_per']						= 'Per';
$_['text_final_cost']				= 'Final Cost';
$_['text_single']					= 'Single';
$_['text_cumulative']				= 'Cumulative';
$_['text_add']						= 'Add';
$_['text_min']						= 'Min';
$_['text_max']						= 'Max';

// Actions and Examples
$_['help_actions']					= '
	<em>Remove</em><br />
	Click the red minus button to remove the rate. Note that if it is the last rate remaining, it will instead be cleared of all values.<br /><br />
	<em>Copy</em><br />
	Click the gray and blue button to copy the rate to the end of the list.';
$_['help_cost_brackets_dialog']		= 'Paste or enter the From, To, Charge, and Per (unit) values in a tab-separated format. Any column can be left blank to not set a value for it. For example, to charge $5.00 for the first 3 items, and $1.25 for each item after that, select "Cumulative" and enter:<br /><pre style="margin-top: 5px">0	3	5.00<br />3	99999	1.25	1</pre>';
$_['help_examples']					= '
	<h2 class="selected">Example 1</h2><h2>Example 2</h2><h2>Example 3</h2>
	<div id="example1">
		Suppose for your Default customer group and customers that do not have an account, you want to charge a fee of $3.00 for orders containing 1-5 items, $5.00 for orders containing 6-10 items, and $10.00 for everything above that.<br /><br />
		For your Wholesale customer group, you want to charge a fee of 10% of the taxed sub-total, with a minimum of $10.00. Then you would enter:<br /><br />
		<strong>RATE #1</strong>
		<ul>
			<li><strong>Customer Groups:</strong> Not Logged In, Default</li>
			<li><strong>Cost Brackets:</strong><br />
			- From: 0, To: 5, Charge: 3.00<br />
			- From: 5, To: 10, Charge: 5.00<br />
			- From: 10, To: 99999, Charge: 10.00</li>
			<li><strong>Final Cost:</strong> Single</li>
		</ul>
		<strong>RATE #2</strong>
		<ul>
			<li><strong>Value for Total:</strong> Taxed Sub-Total</li>
			<li><strong>Customer Groups:</strong> Wholesale</li>
			<li><strong>Cost Brackets:</strong><br />
			- From: 0, To: 99999, Charge: 10%</li>
			<li><strong>Final Cost Min:</strong> 10.00<br />
		</ul>
		<em>Note: If using the "Single" Final Cost type, if you want you can set your brackets so the "To" value for one bracket and the "From" value for the following bracket don\'t match. For example:<br /><br />- From: 1, To: 5, Charge: 3.00<br />- From: 6, To: 10, Charge: 5.00<br />- From: 11, To: (blank), Charge: 10.00<br /><br />"Cumulative" Final Costs need to have the brackets match, for calculation reasons</em>
	</div>
	<div id="example2" style="display: none">
		Suppose within the U.S. you want to give a discount of $2.00 per item, with a maximum of $30.00. You want foreign currencies to be automatically calculated using your currency rates. For international locations, you want to give a discount of 5.00 for every 2 items, with a maximum of 50.00, in whatever currency the customer has selected. Then you would enter:<br /><br />
		<strong>RATE #1</strong>
		<ul>
			<li><strong>Currencies:</strong> Convert Unselected and US Dollar</li>
			<li><strong>Geo Zones:</strong> United States</li>
			<li><strong>Cost Brackets:</strong><br />
			- From: 0, To: 99999, Charge: -2.00, Per (#): 1</li>
			<li><strong>Final Cost Min:</strong> -30.00</li>
		</ul>
		<strong>RATE #2</strong>
		<ul>
			<li><strong>Currencies:</strong> (all except Convert Unselected)</li>
			<li><strong>Geo Zones:</strong> All Other Zones (or your international geo zones)</li>
			<li><strong>Cost Brackets:</strong><br />
			- From: 0, To: 99999, Charge: -5.00, Per (#): 2</li>
			</li>
			<li><strong>Final Cost Min:</strong> -50.00</li>
		</ul>
	</div>
	<div id="example3" style="display: none">
		Suppose you want to charge two different item-based fees to your customers: the first is charged cumulatively at the rate of $4.00 for the first item, $2.00 for items 2 through 5, and $1.00 for each item after that. You also want the number of items adjusted by an additional 25% for this rate.<br /><br />
		The second fee charged at 10% of the sub-total plus $6.00 for every 2 items. For this fee, you want to charge a minimum of $7.50 and maximum of $30.00. Then you would enter:<br /><br />
		<strong>RATE #1</strong>
		<ul>
			<li><strong># of Items Adjustment:</strong> 25%</li>
			<li><strong>Cost Brackets:</strong><br />
			- From: 0, To: 1, Charge: 4.00<br />
			- From: 1, To: 5, Charge: 2.00, Per (#): 1<br />
			- From: 5, To: 99999, Charge: 1.00, Per (#): 1</li>
			<li><strong>Final Cost:</strong> Cumulative</li>
		</ul>
		<strong>RATE #2</strong>
		<ul>
			<li><strong>Value for Total:</strong> Sub-Total</li>
			<li><strong>Cost Brackets:</strong><br />
			- From: 0, To: 99999, Charge: 6.00, Per (#): 2</li>
			<li><strong>Final Cost Add:</strong> 10%</li>
			<li><strong>Final Cost Min:</strong> 7.50</li>
			<li><strong>Final Cost Max:</strong> 30.00</li>
		</ul>
	</div>';

// Copyright
$_['copyright']						= '<div style="text-align: center" class="help">' . $_['heading_title'] . ' ' . $version . ' &copy; <a target="_blank" href="http://www.getclearthinking.com">Clear Thinking, LLC</a></div>';

// Standard Text
$_['standard_module']				= 'Modules';
$_['standard_shipping']				= 'Shipping';
$_['standard_payment']				= 'Payments';
$_['standard_total']				= 'Order Totals';
$_['standard_feed']					= 'Product Feeds';
$_['standard_success']				= 'Success: You have modified ' . $_['heading_title'] . '!';
$_['standard_error']				= 'Warning: You do not have permission to modify ' . $_['heading_title'] . '!';
?>