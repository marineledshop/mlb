<?php
// Heading
$_['heading_title']    = 'Social Discount';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Success: You have modified Social Discount!';

// Entry
$_['entry_status']     = 'Status:';
$_['entry_multiple']   = 'Multiple Discounts:<br /><span class="help">Enable/Disbale Multiple Discounts.</span>';
$_['entry_facebook']   = '<table><tr><td><img src="view/image/social_icon/facebook.png" /></td><td>Facebook Page</td></tr></table><span class="help">Example - http://www.facebook.com/pages/Opcrat/336082263113004</span>';
$_['entry_twitter']   = '<table><tr><td><img src="view/image/social_icon/twitter.png" /></td><td>Twitter Username</td></tr></table><span class="help">Example - opcrat</span>';

$_['entry_sort_order'] = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Social Discount!';
?>