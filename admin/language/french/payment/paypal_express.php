<?php

// Heading
$_['heading_title'] = 'PayPal Express Checkout';

// Text
$_['text_payment'] = 'Paiement';
$_['text_success'] = 'Succ&egrave;s: Avez-vous chang&eacute; les d&eacute;tails de la forme Checkout!';
$_['text_paypal_express'] = '<a href="https://www.paypal.com/uk/mrb/pal=S53S5NBZGRYF4" target="_blank"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Authorization';
$_['text_sale'] = 'Sale';
$_['text_noshipping_auto'] = 'Choix automatique de la rallonge (recommand&eacute;)';
$_['text_noshipping_display'] = 'PayPal affiche l\'adresse de livraison sur les pages PayPal';
$_['text_noshipping_notdisplay'] = 'PayPal ne pas afficher les champs d\'adresse d\'exp&eacute;dition que ce soit';
$_['text_noshipping_profile'] = 'Si l\'acheteur ne passe pas l\'adresse de livraison, PayPal l\'obtient du profil du compte de l\'acheteur';
$_['text_shipping_address'] = 'Adresse de livraison';
$_['text_payment_address'] = 'Adresse de paiement';
$_['text_createaccount_customer_choose'] = 'Le client d&eacute;cide s\'il ya lieu de cr&eacute;er un compte';

// Entry
$_['entry_username'] = 'API Username:';
$_['entry_password'] = 'API Password:';
$_['entry_signature'] = 'API Signature:';
$_['entry_username_test'] = 'API Username (Test):';
$_['entry_password_test'] = 'API Password (Test):';
$_['entry_signature_test'] = 'API Signature (Test):';
$_['entry_title'] = 'Titre du module:';
$_['entry_test'] = 'Mode Test:';
$_['entry_method'] = 'M&eacute;thode de transition:';
$_['entry_logo'] = 'Logo:<br /><span class="help">L\'image a une taille maximale de 750 pixels de largeur par 90 pixels. Si vous ne sp&eacute;cifiez pas une image, le nom d\'entreprise affiche.</span>';
$_['entry_createaccount'] = 'Cr&eacute;er un compte client:<br /><span class="help">Automatiquement cr&eacute;er un compte PayPal &agrave; partir de donn&eacute;es. Le client sera averti par e-mail, aussi pour le mot de passe g&eacute;n&eacute;r&eacute; automatiquement. Si l\'e-mail existe d&eacute;j&agrave;, le client sera connect&eacute;.</span>';
$_['entry_merchant_country'] = 'Pays du marchand:<br /><span class="help">S\'il n\'est pas sp&eacute;cifi&eacute;, pays par d&eacute;faut de l\'adresse &agrave; la client&egrave;le sera utilis&eacute;.</span>';
$_['entry_skip_confirm'] = 'Passer confirmer le paiement:<br /><span class="help">Si se establece en yes, el cliente pase la p&aacute;gina de confirmaci&oacute;n despu&eacute;s de anunciarse PayPal y es redirigido automáticamente a la p&aacute;gina de &eacute;xito caja.<br /><strong>NOTE</strong>: cela n\'a pas d\'effet quand le client doit &ecirc;tre de choisir le mode de livraison et/ou de vous inscrire en compte et/ou achat en tant qu\'invit&eacute; avec confirmation des adresses, etc apr&egrave;s connexion de PayPal.</span>';
$_['entry_confirm_order'] = 'Confirmer la commande avant de connexion de PayPal:<br /><span class="help">Si elle a pour valeur yes, la commande ne sera valid&eacute;e avant que le client va &agrave; PayPal avec le statut de la commande "Standard".<br /><strong>NOTE</strong>: cela n\'a pas d\'effet si l\'option "Passer confirmer le paiement" est r&eacute;gl&eacute; sur "Non" ou si le client le clic bouton PayPal Express.</span>';
$_['entry_default_currency'] = 'Devise par d&eacute;faut:<br /><span class="help">Si le client choisit une monnaie non pris en charge, cette monnaie sera converti automatiquement. Devises accept&eacute;es par PayPal Express: AUD, CAD, CZK, DKK, EUR, HKD, HUF, ILS, JPY, MXN, NOK, NZD, PLN, GBP, SGD, SEK, CHF, USD, TWD, THB, BRL (seulement pour le Br&eacute;sil ), FJM (seulement pour la Malaisie), TRY (seulement pour la Turquie)</span>';
$_['entry_send_address'] = 'Envoyer l\'adresse de paiement ou exp&eacute;dition:<br /><span class="help">Choisissez si envoyer l\'adresse de paiement ou de livraison &agrave; PayPal.</span>';
$_['entry_noshipping'] = 'Envoyer &agrave; l\'adresse:<br /><span class="help">Pour les produits num&eacute;riques:' . $_['text_noshipping_notdisplay'] . '.</span>';
$_['entry_senditem'] = 'Afficher le r&eacute;sum&eacute; de PayPal:<br /><span class="help">Afficher l\'article en page PayPal. S\'il obtenez l\'erreur "10413", le module sera d&eacute;sactiv&eacute; automatiquement cette fonctionnalit&eacute;.</span>';
$_['entry_landing'] = 'Landing Page:<br /><span class="help">Type de page PayPal pour afficher: page de connexion ou d\'une page de facturation par carte de cr&eacute;dit.</span>';
$_['entry_total'] = 'Total minimum:<br /><span class="help">Le total qui doit &ecirc;tre atteint pour permettre ce mode de paiement.</span>';
$_['entry_total_over'] = 'Total maximum:<br /><span class="help">Le total qui doit &ecirc;tre atteint pour d&eacute;sactiver ce mode de paiement.</span>';
$_['entry_order_status'] = 'Statut commande:';
$_['entry_order_status_complete'] = 'Statut commande compl&eacute;ter:<br /><span class="help">Le statut de la commande lorsque le paiement est termin&eacute;.</span>';
$_['entry_order_status_refunded'] = 'Statut de commande rembours&eacute;:<br /><span class="help">Le statut de la commande lorsque le paiement est rembours&eacute;.</span>';
$_['entry_order_status_voided'] = 'Suivi de commande annul&eacute;:<br /><span class="help">Le statut de la commande lorsque le paiement est annul&eacute;.</span>';
$_['entry_enableincheckout'] = 'Activer ce module dans la caisse:<br /><span class="help">Lorsque cette option est d&eacute;sactiv&eacute;e ce module ne vois pas dans la liste de caisse de paiement, mais il fonctionne uniquement par le bouton check-out express.</span>';
$_['entry_geo_zone'] = 'Zone g&eacute;ographique:';
$_['entry_status'] = 'Statut:';
$_['entry_sort_order'] = 'Sorte:';

// Error
$_['error_permission'] = 'Attention: n\'avez pas la permission de modifier le formulaire PayPal Express Checkout!';
$_['error_username'] = 'API Username Obligatoire!';
$_['error_password'] = 'API Password Obligatoire!';
$_['error_signature'] = 'API Signature Obligatoire!';
$_['error_title'] = 'Titre requis!';
$_['error_title_max'] = 'Le titre doit &ecirc;tre au maximum de 128 caract&egrave;res! Note: caract&egrave;res sp&eacute;ciaux sont consid&eacute;r&eacute;s &ecirc;tre d&eacute;cod&eacute; (par exemple: "&amp;" = "&amp;amp;", "&gt;" = "&amp;gt;", etc.)';

$_['error_license'] = 'Attention: Le nom de domaine ne semble pas avoir la cl&eacute; de licence pour activer ce module, vous pouvez en obtenir un en cliquant <a href="%s?languages=fr" target="_blank">ici</a> ou acheter une licence, si vous n\'en avez pas, cliquez <a href="%s" target="_blank">ici</a>.';
$_['error_opencart_version'] = 'Avertissement: Ce module ne peut pas &ecirc;tre utilis&eacute; avec cette version OpenCart. Vous devez t&eacute;l&eacute;charger le module de PayPal Express Checkout compatible avec OpenCart %s';

#Button
$_['button_refund'] = 'Remboursement de transaction de PayPal';
$_['button_void'] = 'Annuler de transactions de PayPal';
$_['button_capture'] = 'Capturez des transactions PayPal';
$_['button_get_api'] = 'Obtenez informations d\'authentification API';
$_['button_get_api_test'] = 'Obtenez SandBox pouvoirs API';

#Api Reference
$_['text_transaction_id'] = 'ID de transaction';
$_['text_message_api_paypal_express'] = 'L\'op&eacute;ration se fera automatiquement via l\'API PayPal';

#Refund
$_['text_refund_type'] = 'Type de remboursement';
$_['text_refund_amt'] = 'Montant du remboursement: Le montant est n&eacute;cessaire si le type de remboursement est <em>partiel</em>';
$_['success_refunded'] = 'L\'op&eacute;ration a &eacute;t&eacute; rembours&eacute;!';

#Void
$_['success_voided'] = 'L\'op&eacute;ration a &eacute;t&eacute; annul&eacute;e!';

#Capture
$_['text_capture_type'] = 'Type de capture';
$_['text_capture_amt'] = 'Capturez montant';
$_['success_captured'] = 'L\'op&eacute;ration a &eacute;t&eacute; captur&eacute;!';

#Tab
$_['tab_live'] = 'Production';
$_['tab_test'] = 'Test';
?>