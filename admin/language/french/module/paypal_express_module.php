<?php
// Heading
$_['heading_title']       = 'PayPal Express Checkout Module';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Succ&egrave;s: Vous avez modifi&acute; le module PayPal Express Checkout!';
$_['text_content_top']    = 'Contenu Top';
$_['text_content_bottom'] = 'Bas de contenu';
$_['text_column_left']    = 'Colonne gauche';
$_['text_column_right']   = 'La colonne de droite';

// Entry
$_['entry_layout']        = 'Disposition:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Statut:';
$_['entry_sort_order']    = 'Ordre de tri:';

// Error
$_['error_permission']    = 'Attention: Vous n\'avez pas la permission de modifier ce module!';
?>