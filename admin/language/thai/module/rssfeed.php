<?php
// Heading
$_['heading_title']		= 'RSSFeedModule - Google Base';

// Text
$_['text_module']      = 'โมดูล';
$_['text_success']     = 'สำเร็จ: แก้ไขโมดูล ยินดีต้อนรับ เรียบร้อยแล้ว!';

// Entry
$_['entry_code']		= 'Code RSSFeed :<br /><span class="help"></span>';
$_['entry_status']     = 'สถานะ:';


// Error
$_['error_code']		= '* Code Required *** For Statut Disabled ( if empty textarea ) : Enter any symbol into Code RSSFeed textarea ***';
$_['error_permission'] = 'คำเตือน: คุณไม่ได้รับอนุญาติเข้ามาในส่วนนี้ กับโมดูล RSSFeedModule!';

?>