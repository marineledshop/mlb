<?php

// Heading
$_['heading_title'] = 'PayPal快速结帐';

// Text
$_['text_payment'] = '付款';
$_['text_success'] = '成功: 你改变PayPal快速结帐形式的细节！';
$_['text_paypal_express'] = '<a href="https://www.paypal.com/uk/mrb/pal=S53S5NBZGRYF4" target="_blank"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Authorization';
$_['text_sale'] = 'Sale';
$_['text_noshipping_auto'] = '选择自动延长（推荐）';
$_['text_noshipping_display'] = '贝宝将显示在贝宝页面上的送货地址';
$_['text_noshipping_notdisplay'] = '贝宝并没有任何显示的送货地址字段';
$_['text_noshipping_profile'] = '如果买方不通过的收货地址，从贝宝买家的帐户配置文件获得它';
$_['text_shipping_address'] = '送货地址';
$_['text_payment_address'] = '付款地址';
$_['text_createaccount_customer_choose'] = '客户决定是否要注册一个帐户';

// Entry
$_['entry_username'] = 'API的用户名:';
$_['entry_password'] = 'API密码:';
$_['entry_signature'] = 'API签名:';
$_['entry_username_test'] = 'API的用户名 (测试):';
$_['entry_password_test'] = 'API密码 (测试):';
$_['entry_signature_test'] = 'API签名 (测试):';
$_['entry_title'] = '模块的标题:';
$_['entry_test'] = '测试模式:';
$_['entry_method'] = '过渡方法:';
$_['entry_logo'] = '启用标志:<br /><span class="help">图像的最大尺寸为宽750像素，高90像素。如果你不指定一个图像，显示企业名称.</span>';
$_['entry_createaccount'] = '创建客户帐户:<br /><span class="help">来自PayPal的数据自动创建一个帐户。将通过电子邮件通知客户，也为自动生成的密码。如果电子邮件已经存在的客户将被记录。</span>';
$_['entry_merchant_country'] = '商户国家:<br /><span class="help">如果没有指定，默认国家从客户地址将被使用。</span>';
$_['entry_default_currency'] = '默认货币:<br /><span class="help">如果客户选择不支持的货币，这种货币将被自动转换。支持PayPal快速货币：澳元，加元，捷克克朗，丹麦克朗，欧元，港币，匈牙利福林，ILS，日元，MXN，NOK，纽元，PLN，英镑，新加坡元，瑞典克朗，瑞士法郎，美元，台币，泰铢，BRL（仅适用于巴西），马来西亚（马来西亚），（仅适用于土耳其）</span>';
$_['entry_skip_confirm'] = '跳过确认付款:<br /><span class="help">如果将它设置为“是”，客户可跳过确认PayPal登录页面后，它会自动被重定向到结账成功页面.<br /><strong>注</strong>: 这并没有影响时，客户必须选择送货方式和/或注册帐户和/或结帐客人提供的地址，等确认后，PayPal登录.</span>';
$_['entry_confirm_order'] = '确认订单前PayPal登录:<br /><span class="help">如果将它设置为“是”，该订单将被确认之前，客户到贝宝“标准”的订单状态.<br /><strong>注</strong>: 这并没有影响，如果选择了“跳过确认付款”设置为“否”，或者如果客户点击PayPal快速按钮.</span>';
$_['entry_send_address'] = '发送付款或送货地址:<br /><span class="help">选择是否发送在PayPal的付款地址或送货地址.</span>';
$_['entry_noshipping'] = '发送地址:<br /><span class="help">对于数字商品：' . $_['text_noshipping_notdisplay'] . '.</span>';
$_['entry_senditem'] = '显示在PayPal综述:<br /><span class="help">在PayPal页面中显示的项目。如果得到错误"10413"模块将自动被禁用此功能。</span>';
$_['entry_landing'] = '登陆页:<br /><span class="help">PayPal页面类型显示：登录页面或信用卡帐单页.</span>';
$_['entry_total'] = '最小总:<br /><span class="help">必须达到使这种付款方式的总.</span>';
$_['entry_total_over'] = '最大总:<br /><span class="help">必须达到禁用此付款方式的总.</span>';
$_['entry_order_status'] = '订单状态:';
$_['entry_order_status_complete'] = '订单状态完成:<br /><span class="help">在付款的时候是完整的订单状态。</span>';
$_['entry_order_status_refunded'] = '我的订单状态已退:<br /><span class="help">订单状态时付款退还.</span>';
$_['entry_order_status_voided'] = '我的订单状态作废:<br /><span class="help">付款时是无效的订单状态.</span>';
$_['entry_enableincheckout'] = '启用这个模块，在结帐<br /><span class="help">当这个选项被禁用这个模块没有看到在付款结帐清单，但它只能通过按钮快速结账。</span>';
$_['entry_geo_zone'] = '地理区域:';
$_['entry_status'] = '州:';
$_['entry_sort_order'] = '分类:';

// Error
$_['error_permission'] = '注意: 没有权限编辑形式PayPal快速结帐！';
$_['error_username'] = 'API的用户名！';
$_['error_password'] = 'API密码！';
$_['error_signature'] = 'API签名！';
$_['error_title'] = '标题为必填项！';
$_['error_title_max'] = '本题必须是最多128个字符！ 诺塔好处: 特殊字符都被认为是要被解码（例如: "&amp;" = "&amp;amp;", "&gt;" = "&amp;gt;", 等）';

$_['error_license'] = '警告：您正在访问的域名有没有许可证密钥来激活这个模块，你可以得到一个请点击<a href="%s" target="_blank">这里</a>或购买许可证，如果你没有的话，请点击<a href="%s" target="_blank">这里</a>。';
$_['error_opencart_version'] = '警告：此模块不能使用这个Opencart最初版本。您必须下载PayPal快速结帐模块兼容OpenCart最初 %s';

#Button
$_['button_refund'] = '退款贝宝交易';
$_['button_void'] = '贝宝交易无效';
$_['button_capture'] = '捕捉贝宝交易';
$_['button_get_api'] = '获取API凭证';
$_['button_get_api_test'] = '沙盘API凭证';

#Api Reference
$_['text_transaction_id'] = '交易ID';
$_['text_message_api_paypal_express'] = '本次交易将通过贝宝API自动进行';

#Refund
$_['text_refund_type'] = '退款类型';
$_['text_refund_amt'] = '退款金额：金额是必需的，如果是部分退款类型';
$_['success_refunded'] = '该交易已退还！';

#Void
$_['success_voided'] = '本次交易已作废！';

#Capture
$_['text_capture_type'] = '拍摄类型';
$_['text_capture_amt'] = '捕获量';
$_['success_captured'] = '本次交易已经被抓住了！';

#Tab
$_['tab_live'] = '产生式';
$_['tab_test'] = '测试';
?>