<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<table class="list">
  <thead>
    <tr>
      <td class="left"><b><?php echo $column_date_added; ?></b></td>
      <td class="left"><b><?php echo $column_comment; ?></b></td>
      <td class="left"><b><?php echo $column_status; ?></b></td>
      <td class="left"><b><?php echo $column_notify; ?></b></td>
    </tr>
  </thead>
  <tbody>
    <?php if ($histories) { ?>
    <?php foreach ($histories as $history) { ?>
    <tr>
      <td class="left"><?php echo $history['date_added']; ?></td>
      <td class="left"><?php echo $history['comment']; ?></td>
      <td class="left"><?php echo $history['status']; ?></td>
      <td class="left"><?php echo $history['notify']; ?></td>
    </tr>
    <?php } ?>
    <?php } else { ?>
    <tr>
      <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<div class="pagination"><?php echo $pagination; ?></div>
<?php if ($ga_reverse_transaction) { ?>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', '<?php echo $orderDetails['ua_tracking']; ?>']);
    _gaq.push(['_trackPageview']);
    
    _gaq.push(['_addTrans',
                '<?php echo $orderDetails['order_id']; ?>',
                '<?php echo $orderDetails['store_name']; ?>',
                '<?php echo -($orderDetails['total'] - $orderDetails['shipping_total']); ?>',
                '<?php echo -$orderDetails['order_tax']; ?>',
                '<?php echo -$orderDetails['shipping_total']; ?>',
                '<?php echo $orderDetails['shipping_city']; ?>',
                '<?php echo $orderDetails['shipping_zone']; ?>',
                '<?php echo $orderDetails['shipping_country']; ?>'
    ]);

    <?php if(isset($orderProduct)) { ?>
    <?php foreach($orderProduct as $product) { ?>
    _gaq.push(['_addItem',
        "<?php echo $product['order_id']; ?>",
        <?php if(isset($product['sku'])) { ?><?php echo json_encode(html_entity_decode($product['sku'],ENT_QUOTES, 'UTF-8')); ?><?php } else { ?><?php echo json_encode(html_entity_decode($product['model'],ENT_QUOTES, 'UTF-8')); ?><?php } ?>,
        <?php echo json_encode(html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8')); ?>,
        <?php echo json_encode(html_entity_decode($product['category_name'], ENT_QUOTES, 'UTF-8')); ?>,
        "<?php echo $product['price']; ?>",
        "<?php echo -$product['quantity']; ?>"
    ]);
    <?php } ?>
    <?php } ?>

    <?php if(isset($orderProductOptions)) { ?>
    <?php foreach($orderProductOptions as $product) { ?>
    _gaq.push(['_addItem',
        "<?php echo $product['order_id']; ?>",
        "<?php if(isset($product['sku'])) { ?><?php echo addslashes($product['sku']); ?><?php } else { ?><?php echo addslashes($product['model']); ?><?php } ?> - <?php echo html_entity_decode(addslashes($product['options_data']),ENT_QUOTES, 'UTF-8'); ?>",
        <?php echo json_encode(html_entity_decode($product['name'], ENT_QUOTES, 'UTF-8')); ?>,
        <?php echo json_encode(html_entity_decode($product['category_name'], ENT_QUOTES, 'UTF-8')); ?>,
        "<?php echo $product['price']; ?>",
        "<?php echo -$product['quantity']; ?>"
    ]);
    <?php } ?>
    <?php } ?>

    _gaq.push(['_trackTrans']);

    (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
    </script>
<?php } ?>
