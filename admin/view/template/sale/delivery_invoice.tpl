<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xml:lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Orders</title>
<base href=".">
<link rel="stylesheet" type="text/css" href="view/stylesheet/invoice.css" />
</head>
<body>
<div style="page-break-after: always;">
  <table width="100%">
  <tbody><tr>
  <td width="25%" align="left"><img src="../image/data/logos/new-mls-logo-big3.png" id="logo"></td>
  <td align="right"><h1>delivery order</h1></td>
  </tr>
  </tbody></table>
  <table class="store">
    <tbody><tr>
      <td>Marine LED Shop<br>
        8110 1st Floor Chogm Villa<br>
07000 Kuah, Langkawi<br>
Kedah / Malaysia<br>
                deniz@repville.com</td>
      <td align="right" valign="top"><table>
          <tbody><tr>
            <td><b>Date:</b></td>
            <td><?php echo date('m/d/Y') ?></td>
          </tr>
        </tbody></table></td>
    </tr>
  </tbody></table>
  <table class="address">
    <tbody><tr class="heading">
      <td width="50%"><b>To</b></td>
    </tr>
    <tr>
      <td>Quantium Solutions International<br />
        No 6, Jalan U1/14, Section U1<br />
        Hicom Glenmarie Industrial Park,<br />
        Shah Alam, Selangor D.E 40150<br />
        Malaysia        <br /></td>
    </tr>
  </tbody></table>
  <table class="product">
    <tbody>
      <tr class="heading">
          <td><b>Product</b></td>
          <td><b>Model</b></td>
          <td align="right"><b>Quantity</b></td>
      </tr>
      <?php foreach($products as $product) { ?>
        <tr>
          <td><?php echo htmlentities($product['name']) ?></td>
          <td><?php echo $product['model'] ?></td>
          <td align="right"><?php echo $product['quantity'] ?></td>
        </tr>
      <?php } ?>
      </tbody></table>
  </div>

</body></html>