<Request
><ShipmentForm
><Shipper
><AccountNumber xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>550330725</AccountNumber
><Cash xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><Contact xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Tolga Okvuran</Contact
><Phone xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>012-5916091</Phone
><Reference xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Order ID <?php print $order['order_id']; ?></Reference
><Company xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Marine LED Shop</Company
><Address1 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Chogm Villa 8110 1st Floor</Address1
><Address2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><Address3 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><City xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Langkawi</City
><Country xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Malaysia</Country
><CountryCode xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>MY</CountryCode
><PostCode xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>07000</PostCode
></Shipper
><Receiver
><Contact xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['shipping_firstname']; ?> <?php print $order['shipping_lastname']; ?></Contact
><Company xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print ($order['shipping_company'] ? $order['shipping_company'] : $order['shipping_firstname'] . ' ' . $order['shipping_lastname']); ?></Company
><Phone xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['telephone']; ?></Phone
><Address1 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['shipping_address_1']; ?></Address1
><?php if ($order['shipping_address_2']) { ?><Address2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['shipping_address_2']; ?></Address2
><?php } else { ?><Address2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><?php } ?><Address3 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><City xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['shipping_city']; ?></City
><Country xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['shipping_country']; ?></Country
><CountryCode xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['shipping_iso_code_2']; ?></CountryCode
><PostCode xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['shipping_postcode']; ?></PostCode
></Receiver
><Shipment
><Products
><ProdID
>INDEWW</ProdID
><ProdName xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Express WorldWide</ProdName
><ProdType
>IND</ProdType
><ProdOthers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><ProdOthersID
>EWW</ProdOthersID
></Products
><Details
><NumOfPackages
><?php print $order['number_of_packages']; ?></NumOfPackages
><TotalWeight
><?php print $order['total_weight']; ?></TotalWeight
><DescOfContents xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Led Products</DescOfContents
><DimUnit
>C</DimUnit
><WghtUnit
>K</WghtUnit
><GlobCurrency
>Local Currency</GlobCurrency
><DoorTo
>DD</DoorTo
><ShipDate
><?php print date('Y-m-d'); ?></ShipDate
></Details
><Pieces
><PieceNumber1 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_pieces1']; ?></PieceNumber1
><Piece1Wght
><?php print $order['dhl_piece_weight1']; ?></Piece1Wght
><Piece1Length xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_length1']; ?></Piece1Length
><Piece1Width xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_width1']; ?></Piece1Width
><Piece1Height xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_height1']; ?></Piece1Height
><?php if ($order['dhl_pieces2']) { ?><PieceNumber2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_pieces2']; ?></PieceNumber2
><Piece2Wght
><?php print $order['dhl_piece_weight2']; ?></Piece2Wght
><Piece2Length xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_length2']; ?></Piece2Length
><Piece2Width xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_width2']; ?></Piece2Width
><Piece2Height xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_height2']; ?></Piece2Height
><?php } else { ?><PieceNumber2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xsi:nil="true"
/><Piece2Wght xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece2Length
/><Piece2Width
/><Piece2Height
/><?php } 
if ($order['dhl_pieces3']) {
?><PieceNumber3 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_pieces3']; ?></PieceNumber3
><Piece3Wght
><?php print $order['dhl_piece_weight3']; ?></Piece3Wght
><Piece3Length xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_length3']; ?></Piece3Length
><Piece3Width xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_width3']; ?></Piece3Width
><Piece3Height xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_height3']; ?></Piece3Height
><?php } else { ?><PieceNumber3 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece3Wght xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece3Length
/><Piece3Width
/><Piece3Height
/><?php }
if ($order['dhl_pieces4']) {
?><PieceNumber4 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_pieces4']; ?></PieceNumber4
><Piece4Wght
><?php print $order['dhl_piece_weight4']; ?></Piece4Wght
><Piece4Length xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_length4']; ?></Piece4Length
><Piece4Width xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_width4']; ?></Piece4Width
><Piece4Height xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_height4']; ?></Piece4Height
><?php } else { ?><PieceNumber4 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece4Wght xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece4Length
/><Piece4Width
/><Piece4Height
/><?php } 
if ($order['dhl_pieces5']) {
?><PieceNumber5 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_pieces5']; ?></PieceNumber5
><Piece5Wght
><?php print $order['dhl_piece_weight5']; ?></Piece5Wght
><Piece5Length xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_length5']; ?></Piece5Length
><Piece5Width xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_width5']; ?></Piece5Width
><Piece5Height xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['dhl_height5']; ?></Piece5Height
><?php } else { ?><PieceNumber5 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece5Wght xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece5Length
/><Piece5Width
/><Piece5Height
/><?php } ?><PieceNumber6 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece6Wght xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece6Length
/><Piece6Width
/><Piece6Height
/><PieceNumber7 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece7Wght xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece7Length
/><Piece7Width
/><Piece7Height
/><PieceNumber8 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece8Wght xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece8Length
/><Piece8Width
/><Piece8Height
/><PieceNumber9 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece9Wght xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece9Length
/><Piece9Width
/><Piece9Height
/><PieceNumber10 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece10Wght xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Piece10Length
/><Piece10Width
/><Piece10Height
/></Pieces
><Services
><InsuranceCode xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><InsuranceValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><InsCurrCode
>Local Currency</InsCurrCode
><DNFlag xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Y</DNFlag
><DNID
>uma@marineledshop.com; singpost@marineledshop.com</DNID
></Services
><Billing
><BillPayType
>S</BillPayType
><BillShipperAccountNumber xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>550330725</BillShipperAccountNumber
><BillToAccountNumber xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>550330725</BillToAccountNumber
></Billing
><Dutiable
><ShipperVAT xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><ReceiverVAT xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><DeclaredValue xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print number_format($order['total'], 2); ?></DeclaredValue
><DeclCurrCode
>USD</DeclCurrCode
><HarmCommCode xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>9405409000</HarmCommCode
><ExportType
>P</ExportType
><DestDutyPaid
>R</DestDutyPaid
><DestDutyPaidAccNo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/></Dutiable
></Shipment
></ShipmentForm
><CustomsForm
><BillToThirdParty xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><InvoiceDetails
><InvoiceType
>Commercial Invoice</InvoiceType
><InvoiceNumber xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['invoice_prefix'] . $order['invoice_no']; ?></InvoiceNumber
><ShipmentRef xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['order_id']; ?></ShipmentRef
><TotDeclValue
><?php print number_format($order['total'], 2); ?></TotDeclValue
><TotNetWght
><?php print $order['net_weight']; ?></TotNetWght
></InvoiceDetails
><ShippingTerms
><PayerOfVAT
/><PaymtTerms xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><IncoTerms xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/><Comments xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/></ShippingTerms
><ItemDetails
><Item
><No
>1</No
><DescOfGoods1 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>LED Products</DescOfGoods1
><DescOfGoods2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><Quantity xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['total_qty']; ?></Quantity
><UOM
>N/A</UOM
><CommCode xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>9405409000</CommCode
><UnitVal xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['unit_value']; ?></UnitVal
><SubTotVal xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print number_format($order['total'], 2); ?></SubTotVal
><UnitNetWght xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['unit_net_weight']; ?></UnitNetWght
><SubTotWght xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['net_weight']; ?></SubTotWght
><CtryOfOrigin xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Malaysia</CtryOfOrigin
><StateOfOrigin xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
/></Item
></ItemDetails
></CustomsForm
><PackagingList
><PckgNumber xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['invoice_prefix'] . $order['invoice_no']; ?></PckgNumber
><Remarks xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><TotPckg
>CTN(S)</TotPckg
><TotNetWt
><?php print $order['unit_net_weight']; ?></TotNetWt
><TotGrWt
/><TotMsrmt xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><PckgDetails
><Package
><No
>1</No
><PckgNo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><DescOfGoods1
>LED Products</DescOfGoods1
><DescOfGoods2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><Qty xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['total_qty']; ?></Qty
><UOM
>N/A</UOM
><NetWgt xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['unit_net_weight']; ?></NetWgt
><GrossWt xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><Measurement xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/></Package
></PckgDetails
></PackagingList
><PickupForm
><Shipper
><AccountNumber xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>550330725</AccountNumber
><ContactName xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Tolga Okvuran</ContactName
><CompanyName xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Marine LED Shop</CompanyName
><Phone xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>012-5916091</Phone
><PhoneExt xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><Address1 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Chogm Villa 8110 1st Floor</Address1
><Address2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><Address3 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><City xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Langkawi</City
><PostCode xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>07000</PostCode
><State xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Kedah</State
><Country xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Malaysia</Country
><CountryCode xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>MY</CountryCode
></Shipper
><PickupDetails
><Date
><?php print date('Y-m-d'); ?></Date
><Weight xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['total_weight']; ?></Weight
><WeightUnit
>K</WeightUnit
><TotalPieces xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['total_pieces']; ?></TotalPieces
><ReadyBy
>15:00</ReadyBy
><ClosingTime
>18:00</ClosingTime
><LocationType
>R</LocationType
><PckgLocation
>Front Desk</PckgLocation
><SpclInstr xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/></PickupDetails
></PickupForm
><AddressBook
><addrContact xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><addrCompany xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><addrPhone xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><addrAddress1 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><addrAddress2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><addrAddress3 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><addrCity xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><addrCountry xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><addrPostCode xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><addrIndex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/></AddressBook
><General
><Time
><?php print date('d-m-Y H:i'); ?></Time
><ApplName
>DHL EmailShip</ApplName
><ApplVersion
>02.02.01</ApplVersion
><Langauage
>EN</Langauage
><AU_CI_Flag
>N</AU_CI_Flag
><Other_CI_Flag
>Y</Other_CI_Flag
><TW_PL_Flag
>N</TW_PL_Flag
></General
><Misc
><ChargeTo
>S</ChargeTo
><ShipperCountry xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>MY-Malaysia</ShipperCountry
><ReceiverCountry xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
><?php print $order['shipping_iso_code_2'] . '-' . $order['shipping_country']; ?></ReceiverCountry
><OtherProduct xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><DN xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Y</DN
><DeclCurrency xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>USD</DeclCurrency
><ExportType
>P</ExportType
><Duty
>R</Duty
><ShpAgr xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>Y</ShpAgr
><ShipDate xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><PickupCountry xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>MY-Malaysia</PickupCountry
><PickDate xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><ReadyTimeHr xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>15</ReadyTimeHr
><ReadyTimeMin xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><CloseTimeHr xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>18</CloseTimeHr
><CloseTimeMin xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/><LocationType
>R</LocationType
><Product xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"
/></Misc
></Request
>