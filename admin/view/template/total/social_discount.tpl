<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/total.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="social_discount_status">
                <?php if ($social_discount_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
              <td><?php echo $entry_multiple; ?></td>
              <td><?php if ($social_discount_multiple) { ?>
                <input type="radio" name="social_discount_multiple" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="social_discount_multiple" value="0" />
                <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="social_discount_multiple" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="social_discount_multiple" value="0" checked="checked" />
                <?php echo $text_no; ?>
                <?php } ?></td>
          </tr>
          <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td><input type="text" name="social_discount_sort_order" value="<?php echo $social_discount_sort_order; ?>" size="1" /></td>
          </tr>
          <tr>
              <td><?php echo $entry_facebook; ?></td>
              <td><input type="text" name="facebook_page" value="<?php echo $facebook_page; ?>" size="60" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_twitter; ?></td>
              <td><input type="text" name="twitter_page" value="<?php echo $twitter_page; ?>" size="20" /></td>
            </tr>
         </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>