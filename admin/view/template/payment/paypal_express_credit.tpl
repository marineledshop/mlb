<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="form">
                    <tr>
                        <td><span class="required">*</span> <?php echo $entry_username; ?></td>
                        <td><input size="70" type="text" name="paypal_express_credit_username" value="<?php echo $paypal_express_credit_username; ?>" />
                            <?php if ($error_username) { ?>
                                <span class="error"><?php echo $error_username; ?></span>
                            <?php } ?></td>
                    </tr>
                    <tr>
                        <td><span class="required">*</span> <?php echo $entry_password; ?></td>
                        <td><input size="70" type="text" name="paypal_express_credit_password" value="<?php echo $paypal_express_credit_password; ?>" />
                            <?php if ($error_password) { ?>
                                <span class="error"><?php echo $error_password; ?></span>
                            <?php } ?></td>
                    </tr>
                    <tr>
                        <td><span class="required">*</span> <?php echo $entry_signature; ?></td>
                        <td><input size="70" type="text" name="paypal_express_credit_signature" value="<?php echo $paypal_express_credit_signature; ?>" />
                            <?php if ($error_signature) { ?>
                                <span class="error"><?php echo $error_signature; ?></span>
                            <?php } ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_username_test; ?></td>
                        <td><input size="70" type="text" name="paypal_express_credit_username_test" value="<?php echo $paypal_express_credit_username_test; ?>" /></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_password_test; ?></td>
                        <td><input size="70" type="text" name="paypal_express_credit_password_test" value="<?php echo $paypal_express_credit_password_test; ?>" /></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_signature_test; ?></td>
                        <td><input size="70" type="text" name="paypal_express_credit_signature_test" value="<?php echo $paypal_express_credit_signature_test; ?>" /></td>
                    </tr>
                    <tr>
                        <td><span class="required">*</span> <?php echo $entry_title; ?></td>
                        <td>
                            <?php foreach ($languages as $language) { ?>
                                <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" style="vertical-align: top;" /><textarea name="paypal_express_credit_title_<?php echo $language['language_id']; ?>"><?php echo ${'paypal_express_credit_title_' . $language['language_id']}; ?></textarea><br />
                                <?php if (isset(${'error_title_' . $language['language_id']})) { ?>
                                    <span class="error"><?php echo ${'error_title_' . $language['language_id']}; ?></span>
                                <?php } ?>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_test; ?></td>
                        <td><?php if ($paypal_express_credit_test) { ?>
                                <input type="radio" name="paypal_express_credit_test" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="paypal_express_credit_test" value="0" />
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="paypal_express_credit_test" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="paypal_express_credit_test" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_method; ?></td>
                        <td><select name="paypal_express_credit_method">
                                <?php if (!$paypal_express_credit_method) { ?>
                                    <option value="0" selected="selected"><?php echo $text_authorization; ?></option>
                                <?php } else { ?>
                                    <option value="0"><?php echo $text_authorization; ?></option>
                                <?php } ?>
                                <?php if ($paypal_express_credit_method) { ?>
                                    <option value="1" selected="selected"><?php echo $text_sale; ?></option>
                                <?php } else { ?>
                                    <option value="1"><?php echo $text_sale; ?></option>
                                <?php } ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_logo; ?></td>
                        <td>
                            <div class="image">
                                <img src="<?php echo $logo; ?>" alt="" id="thumb-logo" />
                                <input type="hidden" name="paypal_express_credit_logo" value="<?php echo $paypal_express_credit_logo; ?>" id="logo" />
                                <br />
                                <a onclick="image_upload('logo', 'thumb-logo');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <a onclick="$('#thumb-logo').attr('src', '<?php echo $no_image; ?>'); $('#logo').attr('value', '');"><?php echo $text_clear; ?></a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_createaccount; ?></td>
                        <td><select name="paypal_express_credit_createaccount">
                                <option value="customer"<?php echo ($paypal_express_credit_createaccount == 'customer' ? ' selected="selected"' : ''); ?>><?php echo $text_createaccount_customer_choose; ?></option>
                                <option value="create"<?php echo ($paypal_express_credit_createaccount == 'create' ? ' selected="selected"' : ''); ?>><?php echo $text_yes; ?></option>
                                <option value="no"<?php echo ($paypal_express_credit_createaccount == 'no' ? ' selected="selected"' : ''); ?>><?php echo $text_no; ?></option>
                            </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_send_address; ?></td>
                        <td><select name="paypal_express_credit_send_address">
                                <?php if ($paypal_express_credit_send_address == 'shipping') { ?>
                                    <option value="shipping" selected="selected"><?php echo $text_shipping_address; ?></option>
                                <?php } else { ?>
                                    <option value="shipping"><?php echo $text_shipping_address; ?></option>
                                <?php } ?>
                                <?php if ($paypal_express_credit_send_address == 'payment') { ?>
                                    <option value="payment" selected="selected"><?php echo $text_payment_address; ?></option>
                                <?php } else { ?>
                                    <option value="payment"><?php echo $text_payment_address; ?></option>
                                <?php } ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_merchant_country; ?></td>
                        <td>
                            <select name="paypal_express_credit_merchant_country">
                                <option value=""><?php echo $text_none; ?></option>
                                <?php
                                foreach ($paypal_express_credit_merchant_countries as $code => $country) {
                                    if (strlen($code) == '8') {
                                        $code = substr($code, 0, 5);
                                    }
                                    ?>
                                    <option value="<?php echo $code; ?>"<?php echo ($code == $paypal_express_credit_merchant_country ? ' selected="selected"' : ''); ?>><?php echo $country; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_default_currency; ?></td>
                        <td>
                            <select name="paypal_express_credit_default_currency">
                                <option value=""><?php echo $text_none; ?></option>
                                <?php
                                foreach ($paypal_express_credit_default_currencies as $code => $currency) {
                                    ?>
                                    <option value="<?php echo $code; ?>"<?php echo ($code == $paypal_express_credit_default_currency ? ' selected="selected"' : ''); ?>><?php echo $currency; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_skip_confirm; ?></td>
                        <td><?php if ($paypal_express_credit_skip_confirm) { ?>
                                <input type="radio" name="paypal_express_credit_skip_confirm" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="paypal_express_credit_skip_confirm" value="0" />
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="paypal_express_credit_skip_confirm" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="paypal_express_credit_skip_confirm" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_confirm_order; ?></td>
                        <td><?php if ($paypal_express_credit_confirm_order) { ?>
                                <input type="radio" name="paypal_express_credit_confirm_order" value="1" checked="checked" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="paypal_express_credit_confirm_order" value="0" />
                                <?php echo $text_no; ?>
                            <?php } else { ?>
                                <input type="radio" name="paypal_express_credit_confirm_order" value="1" />
                                <?php echo $text_yes; ?>
                                <input type="radio" name="paypal_express_credit_confirm_order" value="0" checked="checked" />
                                <?php echo $text_no; ?>
                            <?php } ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_senditem; ?></td>
                        <td><select name="paypal_express_credit_senditem">
                                <?php if (!$paypal_express_credit_senditem) { ?>
                                    <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                <?php } else { ?>
                                    <option value="0"><?php echo $text_no; ?></option>
                                <?php } ?>
                                <?php if ($paypal_express_credit_senditem) { ?>
                                    <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                <?php } else { ?>
                                    <option value="1"><?php echo $text_yes; ?></option>
                                <?php } ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_landing; ?></td>
                        <td><select name="paypal_express_credit_landing">
                                <?php if ($paypal_express_credit_landing == 'Billing') { ?>
                                    <option value="Billing" selected="selected">Billing</option>
                                <?php } else { ?>
                                    <option value="Billing">Billing</option>
                                <?php } ?>
                                <?php if ($paypal_express_credit_landing == 'Login' || $paypal_express_credit_landing != 'Billing') { ?>
                                    <option value="Login" selected="selected">Login</option>
                                <?php } else { ?>
                                    <option value="Login">Login</option>
                                <?php } ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_total; ?></td>
                        <td><input type="text" name="paypal_express_credit_total" value="<?php echo $paypal_express_credit_total; ?>" /></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_total_over; ?></td>
                        <td><input type="text" name="paypal_express_credit_total_over" value="<?php echo $paypal_express_credit_total_over; ?>" /></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_order_status; ?></td>
                        <td><select name="paypal_express_credit_order_status_id">
                                <?php foreach ($order_statuses as $order_status) { ?>
                                    <?php if ($order_status['order_status_id'] == $paypal_express_credit_order_status_id) { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_order_status_complete; ?></td>
                        <td><select name="paypal_express_credit_order_status_id_complete">
                                <?php foreach ($order_statuses as $order_status) { ?>
                                    <?php if ($order_status['order_status_id'] == $paypal_express_credit_order_status_id_complete) { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_order_status_refunded; ?></td>
                        <td><select name="paypal_express_credit_order_status_id_refunded">
                                <?php foreach ($order_statuses as $order_status) { ?>
                                    <?php if ($order_status['order_status_id'] == $paypal_express_credit_order_status_id_refunded) { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_order_status_voided; ?></td>
                        <td><select name="paypal_express_credit_order_status_id_voided">
                                <?php foreach ($order_statuses as $order_status) { ?>
                                    <?php if ($order_status['order_status_id'] == $paypal_express_credit_order_status_id_voided) { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_enableincheckout; ?></td>
                        <td><select name="paypal_express_credit_enableincheckout">
                                <?php if ($paypal_express_credit_enableincheckout) { ?>
                                    <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                    <option value="0"><?php echo $text_no; ?></option>
                                <?php } else { ?>
                                    <option value="1"><?php echo $text_yes; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                <?php } ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_geo_zone; ?></td>
                        <td><select name="paypal_express_credit_geo_zone_id">
                                <option value="0"><?php echo $text_all_zones; ?></option>
                                <?php foreach ($geo_zones as $geo_zone) { ?>
                                    <?php if ($geo_zone['geo_zone_id'] == $paypal_express_credit_geo_zone_id) { ?>
                                        <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_status; ?></td>
                        <td><select name="paypal_express_credit_status">
                                <?php if ($paypal_express_credit_status) { ?>
                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                    <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                    <option value="1"><?php echo $text_enabled; ?></option>
                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select></td>
                    </tr>
                    <tr>
                        <td><?php echo $entry_sort_order; ?></td>
                        <td><input type="text" name="paypal_express_credit_sort_order" value="<?php echo $paypal_express_credit_sort_order; ?>" size="1" /></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
    function image_upload(field, thumb) {
        $('#dialog').remove();

        $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

        $('#dialog').dialog({
            title: '<?php echo $text_image_manager; ?>',
            close: function (event, ui) {
                if ($('#' + field).attr('value')) {
                    $.ajax({
                        url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).val()),
                        dataType: 'text',
                        success: function(data) {
                            $('#' + thumb).replaceWith('<img src="' + data + '" alt="" id="' + thumb + '" />');
                        }
                    });
                }
            },
            bgiframe: false,
            width: 800,
            height: 400,
            resizable: false,
            modal: false
        });
    };
    //--></script>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script>
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
        CKEDITOR.config.width = '90%';
        CKEDITOR.config.height = '100';
        CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
        CKEDITOR.replace('paypal_express_credit_title_<?php echo $language['language_id']; ?>', {
            filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
            filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
            filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
            filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
            filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
            filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
        });
<?php } ?>
    //--></script>
<script type="text/javascript"><!--
    $(document).ready(function($) {
        $.ajax({
            url: 'index.php?route=payment/paypal_express/checkVersion&token=<?php echo $token; ?>',
            dataType: 'html',
            success: function(html) {
                if (html) {
                    $('.breadcrumb').after('<div class="attention" id="checkVersion" style="display:none"></div>');
                    $('#checkVersion').html(html);
                    $('#checkVersion').fadeIn('slow');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    });
    //--></script>
<?php echo $footer; ?>