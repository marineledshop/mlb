<?php echo $header; ?>
<link rel="stylesheet" type="text/css" href="../admin/view/stylesheet/ianalytics.css" />
<div id="content" class="iModuleContent">
  <!-- START BREADCRUMB -->
  <?php require_once('ianalytics/breadcrumb.php'); ?>
  <!-- END BREADCRUMB -->
  <!-- START FLASHMESSAGE -->
  <?php require_once('ianalytics/flashmessage.php'); ?>
  <!-- END FLASHMESSAGE -->
  <div class="box">
    <div class="heading">
    <h1><img src="view/image/imodules.png" style="margin-top: -3px;" alt="" /> <span class="iModulesTitle"><?php echo $heading_title; ?></span>
    <ul class="iModuleAdminSuperMenu">
    	<li class="selected needsRefresh">Dashboard</li>
    	<li id="iAnalyticsSearchSelected">Searches</li>
    	<li id="iAnalyticsProductSelected">Products</li>
    	<li>Control Panel</li>
        <li>Support</li>
    </ul>
    </h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button submitButton"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <!-- START NOT ACTIVATED CHECK -->
    <?php require_once('ianalytics/notactivated.php'); ?>
    <!-- END NOT ACTIVATED CHECK -->
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <ul class="iModuleAdminSuperWrappers">
      	<li>
        	<!-- START TAB DASHBOARD -->
        	<?php require_once('ianalytics/tab_dashboard.php'); ?>
        	<!-- END TAB DASHBOARD -->
        </li>
      	<li>
        	<!-- START TAB SEARCHES -->
        	<?php require_once('ianalytics/tab_searches.php'); ?>
        	<!-- END TAB SEARCHES -->
      	</li>
        <li>
        	<!-- START TAB PRODUCTS -->
        	<?php require_once('ianalytics/tab_products.php'); ?>
        	<!-- END TAB PRODUCTS -->
        </li>
      	<li>
        	<!-- START TAB CONTROLPANEL -->
        	<?php require_once('ianalytics/tab_controlpanel.php'); ?>
        	<!-- END TAB CONTROLPANEL -->
      	</li>
        <li>
        	<!-- START TAB SUPPORT -->
        	<?php require_once('ianalytics/tab_support.php'); ?>
        	<!-- END TAB SUPPORT -->
        </li>
      </ul>
	  <input type="hidden" class="selectedTab" name="selectedTab" value="<?=(empty($_GET['tab'])) ? 0 : $_GET['tab'] ?>" />
      <input type="hidden" class="selectedSearchTab" name="selectedSearchTab" value="<?=(empty($_GET['searchTab'])) ? 0 : $_GET['searchTab'] ?>" />
      <input type="hidden" class="selectedProductTab" name="selectedProductTab" value="<?=(empty($_GET['productTab'])) ? 0 : $_GET['productTab'] ?>" />
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
	var iAnalyticsMinDate = '<?=$this->data['iAnalyticsMinDate'];?>';
</script>

<script>
	var selectedTab = $('.selectedTab').val();
	$('.iModuleAdminSuperMenu li').removeClass('selected').eq(selectedTab).addClass('selected');
	$('.iModuleAdminSuperWrappers > li').hide().eq(selectedTab).show();
	
	$('.iModuleAdminMenu li', $('#iAnalyticsSearchWrapper')).removeClass('selected').eq($('.selectedSearchTab').val()).addClass('selected');
	$('.iModuleAdminWrappers > li', $('#iAnalyticsSearchWrapper')).hide().eq($('.selectedSearchTab').val()).show();
	
	$('.iModuleAdminMenu li', $('#iAnalyticsProductWrapper')).removeClass('selected').eq($('.selectedProductTab').val()).addClass('selected');
	$('.iModuleAdminWrappers > li', $('#iAnalyticsProductWrapper')).hide().eq($('.selectedProductTab').val()).show();
	
	$('.iModuleAdminMenu li').click(function() {
		$('.iModuleAdminMenu li',$(this).parents('li')).removeClass('selected');
		$(this).addClass('selected');
		$($('.iModuleAdminWrappers li',$(this).parents('li')).hide().get($(this).index())).fadeIn(200);
		if ($('#iAnalyticsSearchSelected').hasClass('selected')) $('.selectedSearchTab').val($(this).index());
		else if ($('#iAnalyticsProductSelected').hasClass('selected')) $('.selectedProductTab').val($(this).index()); 
		
		
	});
	
	$('.iModuleAdminSuperMenu li').click(function() {
		$('.iModuleAdminSuperMenu > li',$(this).parents('h1')).removeClass('selected');
		$(this).addClass('selected');
		$($('.iModuleAdminSuperWrappers > li',$(this).parents('#content')).hide().get($(this).index())).fadeIn(200);
		$('.selectedTab').val($(this).index());
		
		
	});
</script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="../admin/view/javascript/ianalytics.js"></script>

<?php echo $footer; ?>