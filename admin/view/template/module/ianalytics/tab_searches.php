<ul class="iModuleUL" id="iAnalyticsSearchWrapper">

    <li>
        <ul class="iModuleAdminMenu">
            <li class="selected">Search Queries</li>
            <li>Search Keywords</li>
            <li>Most Searched Products</li>
        </ul>
        <div class="content">
            <ul class="iModuleAdminWrappers">
                <li>
                	<div class="iAnalyticsSearchDateFilter">
                        <h1>Search Queries Graph</h1>
                        <?php require('element_filter.php'); ?>
                    </div>
                    <div class="help">This graph depicts what part of your users' search queries has derived results</div>
                    <div class="iModuleFields">
                    <script type="text/javascript">
                    	var monthlySearchesGraph = $.parseJSON('<?=json_encode($iAnalyticsMonthlySearchesGraph)?>'); 
                    </script>
                    	<div id="searchedFound"></div>
						<br /><br /><br />
                        <h1>Search Queries in Numbers</h1>
                        <table class="form">
                        <?php foreach($iAnalyticsMonthlySearchesTable as $day): ?>
                        	<tr><td><?=$day[0]?></td><td><?=$day[1]?></td><td><?=$day[2]?></td><td><?=$day[3]?></td></tr>
                        <?php endforeach; ?>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </li>
                <li>
                    <div class="iModuleFields">
                        
                        <div class="iAnalyticsSearchDateFilter">
                        	<h1>Keywords in Search - History</h1>
							<?php require('element_filter.php'); ?>
                        </div>
                        <div class="help">This table shows the search queries of your website users starting from the most recent.</div><br />
<br />
                        <table class="form">
                        <?php foreach($iAnalyticsKeywordSearchHistory as $index => $k): ?>
                        	<tr><td><?=$k[0]?></td><td><?=$k[1]?></td><td><?=$k[2]?></td><td><?=$k[3]?></td><td><?=$k[4]?></td><td><?=$k[5]?></td>
                            <td><?php if ($index > 0) : ?><a class="button" onclick="return confirm('Are you sure you wish to delete the record?');" href="index.php?route=module/ianalytics/deletesearchkeyword&token=<?php echo $this->session->data['token']; ?>&searchValue=<?php echo $k[6]; ?>">Delete record</a>&nbsp;&nbsp;<a  class="button" onclick="return confirm('Are you sure you wish to delete all of the searches of this keyword?');" href="index.php?route=module/ianalytics/deleteallsearchkeyword&token=<?php echo $this->session->data['token']; ?>&searchValue=<?php echo $k[0]; ?>">Delete keyword</a><?php endif; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </li>
                <li>
                	
                    <div class="iAnalyticsSearchDateFilter">
                    	<h1>Most Searched Keywords</h1>
						<?php require('element_filter.php'); ?>
                    </div>
                    <span class="help">This indicates what your visitors search the most on your site using the OpenCart search engine</span>
                    <br />
					<br />
                        <table class="form">
                        <?php foreach($iAnalyticsMostSearchedKeywords as $j => $k): ?>
                        	<tr><td><?=$k[0]?></td><td><?=$k[1]?></td><td><?php if ($j > 0) {  ?><div class="buttons"><a href="../index.php?route=product/search&filter_name=<?=$k[0]?>" target="_blank">Preview</a></div><?php } ?></td></tr>
                        <?php endforeach; ?>
                        </table>
                    <div class="iModuleFields">
                        <div class="clearfix"></div>
                    </div>
                </li>
            </ul>
        </div>
    </li>
</ul>