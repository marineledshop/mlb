<ul class="iModuleUL" id="iAnalyticsProductWrapper">

    <li>
        <ul class="iModuleAdminMenu">
            <li class="selected">Opened Products</li>
            <li>Compared Products</li>
        </ul>
        <div class="content">
            <ul class="iModuleAdminWrappers">
                <li>
                    <div class="iModuleFields">
                        <div class="iAnalyticsProductDateFilter">
                        	<h1>Opened Products</h1>
							<?php require('element_filter.php'); ?>
                        </div>
                        <div class="help">This table shows the products your visitors viewed starting from the most viewed.</div><br />
<br />
                        <table class="form">
                        <?php foreach($iAnalyticsMostOpenedProducts as $j => $k): ?>
                            <tr><td><?=$k[0]?></td><td><?=$k[1]?></td></tr>
                        <?php endforeach; ?>
                        </table>
                        <div class="clearfix"></div>
                    </div>
                </li>
                <li>
                	
                    <div class="iAnalyticsProductDateFilter">
                    	<h1>Compared Products</h1>
						<?php require('element_filter.php'); ?>
                    </div>
                    <span class="help">This table shows the products your visitors compared starting from the most compared.</span>
                    <br />
					<br />
                    <table class="form">
                    <?php foreach($iAnalyticsMostComparedProducts as $j => $k): ?>
                        <tr><td><?=$k[0]?></td><td><?=$k[1]?></td></tr>
                    <?php endforeach; ?>
                    </table>
                    <div class="iModuleFields">
                        <div class="clearfix"></div>
                    </div>
                </li>
            </ul>
        </div>
    </li>
</ul>