<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="box">
        <div class="heading">
            <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <table id="module" class="list">
                    <thead>
                        <tr>
                            <td class="left"><?php echo $entry_layout; ?></td>
                            <td class="left"><?php echo $entry_position; ?></td>
                            <td class="left"><?php echo $entry_status; ?></td>
                            <td class="right"><?php echo $entry_sort_order; ?></td>
                            <td></td>
                        </tr>
                    </thead>
                    <?php
                    if (version_compare(VERSION, '1.5.1', '<')):
                        $init = '';
                        $medium = '_';
                        $end = '';
                    else:
                        $init = 'module[';
                        $medium = '][';
                        $end = ']';
                    endif;
                    $module_row = 0;
                    foreach ($modules as $module) {
                        ?>
                        <tbody id="module-row<?php echo $module_row; ?>">
                            <tr>
                                <td class="left"><select name="paypal_express_module_<?php echo $init . $module_row . $medium; ?>layout_id<?php echo $end ?>">
                                        <?php
                                        foreach ($layouts as $layout) {
                                            if (version_compare(VERSION, '1.5.1', '<')):
                                                $layout_id = ${'paypal_express_module_' . $module . '_layout_id'};
                                            else:
                                                $layout_id = $module['layout_id'];
                                            endif;
                                            if ($layout['layout_id'] == $layout_id) {
                                                ?>
                                                <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select></td>
                                <td class="left"><select name="paypal_express_module_<?php echo $init . $module_row . $medium; ?>position<?php echo $end ?>">
                                        <?php
                                        if (version_compare(VERSION, '1.5.1', '<')):
                                            $position = ${'paypal_express_module_' . $module . '_position'};
                                        else:
                                            $position = $module['position'];
                                        endif;
                                        if ($position == 'content_top') {
                                            ?>
                                            <option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
                                        <?php } else { ?>
                                            <option value="content_top"><?php echo $text_content_top; ?></option>
                                        <?php } ?>
                                        <?php if ($position == 'content_bottom') { ?>
                                            <option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
                                        <?php } else { ?>
                                            <option value="content_bottom"><?php echo $text_content_bottom; ?></option>
                                        <?php } ?>
                                        <?php if ($position == 'column_left') { ?>
                                            <option value="column_left" selected="selected"><?php echo $text_column_left; ?></option>
                                        <?php } else { ?>
                                            <option value="column_left"><?php echo $text_column_left; ?></option>
                                        <?php } ?>
                                        <?php if ($position == 'column_right') { ?>
                                            <option value="column_right" selected="selected"><?php echo $text_column_right; ?></option>
                                        <?php } else { ?>
                                            <option value="column_right"><?php echo $text_column_right; ?></option>
    <?php } ?>
                                    </select></td>
                                <td class="left"><select name="paypal_express_module_<?php echo $init . $module_row . $medium; ?>status<?php echo $end ?>">
                                        <?php
                                        if (version_compare(VERSION, '1.5.1', '<')):
                                            $status = ${'paypal_express_module_' . $module . '_status'};
                                        else:
                                            $status = $module['status'];
                                        endif;
                                        if ($status) {
                                            ?>
                                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                            <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo $text_enabled; ?></option>
                                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                    <?php } ?>
                                    </select></td>
                                <td class="right"><input type="text" name="paypal_express_module_<?php echo $init . $module_row . $medium; ?>sort_order<?php echo $end ?>" value="<?php
                                                         if (version_compare(VERSION, '1.5.1', '<')):
                                                             echo ${'paypal_express_module_' . $module . '_sort_order'};
                                                         else:
                                                             echo $module['sort_order'];
                                                         endif
                                                         ?>" size="3" /></td>
                                <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>
                            </tr>
                        </tbody>
    <?php $module_row++; ?>
<?php } ?>
                    <tfoot>
                        <tr>
                            <td colspan="4"></td>
                            <td class="left"><a onclick="addModule();" class="button"><span><?php echo $button_add_module; ?></span></a></td>
                        </tr>
                    </tfoot>
                </table>
<?php if (version_compare(VERSION, '1.5.1', '<')):
    ?><input type="hidden" name="paypal_express_module_module" value="<?php echo $paypal_express_module_module; ?>" /><?php endif
?>
            </form>
        </div>
    </div>
    <script type="text/javascript"><!--
        var module_row = <?php echo $module_row; ?>;

        function addModule() {
            html = '<tbody id="module-row' + module_row + '">';
            html += '  <tr>';
            html += '    <td class="left"><select name="paypal_express_module_<?php echo $init ?>' + module_row + '<?php echo $medium ?>layout_id<?php echo $end ?>">';
<?php foreach ($layouts as $layout) { ?>
                html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>';
<?php } ?>
            html += '    </select></td>';
            html += '    <td class="left"><select name="paypal_express_module_<?php echo $init ?>' + module_row + '<?php echo $medium ?>position<?php echo $end ?>">';
            html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
            html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
            html += '      <option value="column_left"><?php echo $text_column_left; ?></option>';
            html += '      <option value="column_right"><?php echo $text_column_right; ?></option>';
            html += '    </select></td>';
            html += '    <td class="left"><select name="paypal_express_module_<?php echo $init ?>' + module_row + '<?php echo $medium ?>status<?php echo $end ?>">';
            html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
            html += '      <option value="0"><?php echo $text_disabled; ?></option>';
            html += '    </select></td>';
            html += '    <td class="right"><input type="text" name="paypal_express_module_<?php echo $init ?>' + module_row + '<?php echo $medium ?>sort_order<?php echo $end ?>" value="" size="3" /></td>';
            html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><span><?php echo $button_remove; ?></span></a></td>';
            html += '  </tr>';
            html += '</tbody>';

            $('#module tfoot').before(html);

            module_row++;
        }
<?php if (version_compare(VERSION, '1.5.1', '<')):
    ?>
            $('#form').bind('submit', function() {
                var module = new Array();

                $('#module tbody').each(function(index, element) {
                    module[index] = $(element).attr('id').substr(10);
                });

                $('input[name=\'paypal_express_module_module\']').attr('value', module.join(','));
            });
<?php endif ?>
        //--></script>
<?php echo $footer; ?>