<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
	  <div class="heading">
		  <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
		  <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a>
			  <a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a>
		  </div>
	  </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table>
	        <tr>
		        <td><?php echo $show_notice; ?></td>
		        <td><select name="shipping_notice_module[show]">
				        <?php if ($modules['show']) { ?>
				        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
				        <option value="0"><?php echo $text_disabled; ?></option>
				        <?php } else { ?>
				        <option value="1"><?php echo $text_enabled; ?></option>
				        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				        <?php } ?>
			        </select></td>
	        </tr>
	        <tr>
		        <td><?php echo $date_start; ?></td>
		        <td><input  readonly id="date1" type="text" name="shipping_notice_module[date_start]" value="<?php echo $modules['date_start']; ?>"/>&nbsp;&nbsp;<?php echo $date_format.' 00:00:01'; ?></td>
	        </tr>
	        <tr>
		        <td><?php echo $date_end; ?></td>
		        <td><input readonly id="date2" type="text" name="shipping_notice_module[date_end]" value="<?php echo $modules['date_end']; ?>"/>&nbsp;&nbsp;<?php echo $date_format.' 23:59:59'; ?></td>
	        </tr>
	        <tr>
		        <td><?php echo $message_text; ?></td>
		        <td><textarea style="width:350px; height:80px;" name="shipping_notice_module[message_text]"><?php echo $modules['message_text']; ?></textarea></td>
	        </tr>
	    </table>
      </form>
    </div>
  </div>
</div>
<script>
$(function() {
$( "#date1" ).datepicker();
$( "#date2" ).datepicker();
$( "#date1" ).datepicker( "option", "dateFormat", "mm/dd/yy");
$( "#date2" ).datepicker( "option", "dateFormat", "mm/dd/yy");
});
</script>
<?php echo $footer; ?>