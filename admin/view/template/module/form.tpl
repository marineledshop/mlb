<?php
//==============================================================================
// Flexible Form v154.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
//==============================================================================
?>

<?php echo $header; ?>
<style type="text/css">
	.list td {
		padding: 7px !important;
	}
	.list table td:first-child {
		width: 20px;
	}
	.list table {
		width: 95%;
	}
	.list table input {
		width: 100%;
	}
	.list table td {
		padding: 0 !important;
		border: none;
	}
	a[tab] img {
		vertical-align: text-top;
	}
	.tooltip-mark {
		background: #FF8;
		border: 1px solid #888;
		border-radius: 10px;
		color: #000;
		font-size: 10px;
		padding: 1px 4px;
	}
	.tooltip {
		background: #FFC;
		border: 1px solid #CCC;
		color: #000;
		display: none;
		font-size: 11px;
		font-weight: normal;
		line-height: 1.3;
		text-align: left;
		padding: 10px;
		position: absolute;
		max-width: 300px;
		z-index: 100;
		box-shadow: 0 6px 9px #AAA;
		-moz-box-shadow: 0 6px 9px #AAA;
		-webkit-box-shadow: 0 6px 9px #AAA;
	}
	.tooltip-mark:hover, .tooltip-mark:hover + .tooltip, .tooltip:hover {
		display: inline;
		cursor: help;
	}
</style>
<?php if (!$v14x) { ?>
<div id="content">
	<div class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
		<?php } ?>
	</div>
<?php } ?>
<?php if ($error_warning) { ?><div class="warning"><?php echo $error_warning; ?></div><?php } ?>
<?php if ($success) { ?><div class="success"><?php echo $success; ?></div><?php } ?>
<div class="box">
	<?php if ($v14x) { ?>
		<div class="left"></div>
		<div class="right"></div>
	<?php } ?>
	<div class="heading">
		<h1 style="padding: 10px 2px 0"><img src="view/image/<?php echo $type; ?>.png" alt="" style="vertical-align: middle" /> <?php echo $heading_title; ?></h1>
		<div class="buttons">
			<a id="save-and-exit" onclick="$('#form').attr('action', '<?php echo $save_exit; ?>'); $('#form').submit();" class="button"><span><?php echo $button_save_exit; ?></span></a>
			<a id="save-and-keep-editing" onclick="$('#form').attr('action', '<?php echo $save_keep_editing; ?>'); $('#form').submit();" class="button"><span><?php echo $button_save_keep_editing; ?></span></a>
			<a id="cancel" onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a>
		</div>
	</div>
	<div class="content">
		<form action="" method="post" enctype="multipart/form-data" id="form">
			<input type="hidden" name="<?php echo $name; ?>_status" value="1" />
			<div id="tab-container" style="display: inline-block; width: 100%;">
				<div id="tabs" class="vtabs">
					<a <?php echo $v14x ? 'tab' : 'href'; ?>="#tab-add-form" id="sidetab-add-form" onclick="event.preventDefault()">
						<input type="text" size="17" id="new-form-keyword" onkeyup="$(this).val($(this).val().replace(/[^\w-]/g, ''));" />
						<img src="view/image/add_form.png" alt="add_form" onclick="nfk = $.trim($('#new-form-keyword').val()); if (nfk) addForm(nfk)" />
					</a>
					<?php foreach (${$name.'_data'} as $id => $data) { ?>
						<a <?php echo $v14x ? 'tab' : 'href'; ?>="#tab-form-<?php echo $id; ?>" id="sidetab-form-<?php echo $id; ?>" onclick="event.preventDefault()">
							<?php echo $id; ?>
							<img src="view/image/delete_form.png" alt="delete_form" onclick="$('#tab-form-<?php echo $id; ?>').remove(); $(this).parent().remove(); event.preventDefault(); $('#save-keep-editing').click();" />
						</a>
					<?php } ?>
				</div>
				<div id="tab-add-form" class="vtabs<?php echo $v14x ? '_page' : '-content'; ?>"><?php echo $text_add_new_form; ?></div>
				<?php $field_count = 0; ?>
				<?php foreach (${$name.'_data'} as $id => $data) { ?>
					<div id="tab-form-<?php echo $id; ?>" class="vtabs<?php echo $v14x ? '_page' : '-content'; ?>">
						<table class="form">
						<tr>
							<td><?php echo $entry_form_link; ?></td>
							<td><?php echo $text_url; ?> <input id="form-<?php echo $id; ?>-url" type="text" style="font-size: 11px; width: 70%; background: #F8F8F8" readonly="readonly" onclick="this.select()" value="<?php echo HTTP_CATALOG . 'index.php?route=information/form&form=' . $id; ?>" />
								<br />
								<?php echo $text_html_code; ?> <input id="form-<?php echo $id; ?>-htmlcode" type="text" style="font-size: 11px; width: 70%; background: #F8F8F8" readonly="readonly" onclick="this.select()" value='<a href="<?php echo HTTP_CATALOG . 'index.php?route=information/form&form=' . $id; ?>"><?php echo $id; ?></a>' />
							</td>
						</tr>
						<tr>
							<td><?php echo $entry_title; ?></td>
							<td><table>
									<?php foreach ($languages as $language) { ?>
										<?php $lcode = $language['code']; ?>
										<tr>
											<td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></td>
											<td><input type="text" size="50" name="<?php echo $name; ?>_data[<?php echo $id; ?>][title][<?php echo $lcode; ?>]" value="<?php echo (empty($data['title'][$lcode])) ? '' : $data['title'][$lcode]; ?>" /></td>
										</tr>
									<?php } ?>
								</table>
							</td>
						</tr>
						<tr>
							<td><?php echo $entry_email_results_to; ?></td>
							<td><input type="text" size="50" name="<?php echo $name; ?>_data[<?php echo $id; ?>][email]" value="<?php echo $data['email']; ?>" /></td>
						</tr>
						<tr>
							<td><?php echo $entry_email_subject; ?></td>
							<td><table>
									<?php foreach ($languages as $language) { ?>
										<?php $lcode = $language['code']; ?>
										<tr>
											<td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></td>
											<td><input type="text" size="50" name="<?php echo $name; ?>_data[<?php echo $id; ?>][subject][<?php echo $lcode; ?>]" value="<?php echo (empty($data['subject'][$lcode])) ? '' : $data['subject'][$lcode]; ?>" /></td>
										</tr>
									<?php } ?>
								</table>
							</td>
						</tr>
						<tr>
							<td><?php echo $entry_file_size_limit; ?></td>
							<td><input type="text" size="5" name="<?php echo $name; ?>_data[<?php echo $id; ?>][filesize_limit]" value="<?php echo $data['filesize_limit']; ?>" /> <?php echo $text_kb; ?></td>
						</tr>
						<tr>
							<td><?php echo $entry_allowed_extensions; ?></td>
							<td><input type="text" size="50" name="<?php echo $name; ?>_data[<?php echo $id; ?>][allowed_ext]" value="<?php echo $data['allowed_ext']; ?>" /></td>
						</tr>
						<tr>
							<td><?php echo $entry_html_content_before; ?></td>
							<td><table>
									<?php foreach ($languages as $language) { ?>
										<?php $lcode = $language['code']; ?>
										<tr>
											<td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></td>
											<td><textarea name="<?php echo $name; ?>_data[<?php echo $id; ?>][html_before][<?php echo $lcode; ?>]"><?php echo (empty($data['html_before'][$lcode])) ? '' : $data['html_before'][$lcode]; ?></textarea></td>
										</tr>
									<?php } ?>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<table id="form-<?php echo $id; ?>-fields" class="list" style="margin-bottom: 0" cellpadding="7">
								<thead>
									<tr>
										<td><?php echo $text_required; ?></td>
										<td><?php echo $text_name; ?></td>
										<td><?php echo $text_type; ?></td>
										<td><?php echo $text_value; ?> <span class="tooltip-mark">?</span> <span class="tooltip"><?php echo $text_value_help; ?></span></td>
										<td><?php echo $text_column; ?></td>
										<td style="white-space: nowrap"><?php echo $text_sort_order; ?></td>
										<td>&nbsp;</td>
									</tr>
								</thead>
								<?php if (isset($data['fields'])) { ?>
									<?php
									$column = array();
									$sort_order = array();
									foreach ($data['fields'] as $key => $value) {
										$column[$key] = $value['column'];
										$sort_order[$key] = $value['sort_order'];
									}
									array_multisort($column, SORT_ASC, $sort_order, SORT_ASC, $data['fields']);
									?>
									<?php foreach ($data['fields'] as $field) { ?>
										<tr>
											<td><select name="<?php echo $name; ?>_data[<?php echo $id; ?>][fields][<?php echo $field_count; ?>][required]">
													<option value="1" <?php if ($field['required'] == 1) echo 'selected="selected"'; ?>><?php echo $text_yes; ?></option>
													<option value="0" <?php if ($field['required'] == 0) echo 'selected="selected"'; ?>><?php echo $text_no; ?></option>
												</select>
											</td>
											<td><table>
													<?php foreach ($languages as $language) { ?>
														<?php $lcode = $language['code']; ?>
														<tr>
															<td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></td>
															<td><input type="text" name="<?php echo $name; ?>_data[<?php echo $id; ?>][fields][<?php echo $field_count; ?>][name][<?php echo $lcode; ?>]" value="<?php echo (empty($field['name'][$lcode])) ? '' : $field['name'][$lcode]; ?>" /></td>
														</tr>
													<?php } ?>
												</table>
											</td>
											<td><select name="<?php echo $name; ?>_data[<?php echo $id; ?>][fields][<?php echo $field_count; ?>][type]" onchange="$(this).parent().next().find('input').datepicker('destroy').timepicker('destroy').datetimepicker('destroy').attr('class', $(this).val()); attachDatePickers()">
													<optgroup label="<?php echo $field_selection_based; ?>">
														<option value="checkbox" <?php if ($field['type'] == 'checkbox') echo 'selected="selected"'; ?>><?php echo $field_checkbox; ?></option>
														<option value="radio" <?php if ($field['type'] == 'radio') echo 'selected="selected"'; ?>><?php echo $field_radio; ?></option>
														<option value="select" <?php if ($field['type'] == 'select') echo 'selected="selected"'; ?>><?php echo $field_select; ?></option>
													</optgroup>
													<optgroup label="<?php echo $field_text_based; ?>">
														<option value="email" <?php if ($field['type'] == 'email') echo 'selected="selected"'; ?>><?php echo $field_email; ?></option>
														<option value="email_confirm" <?php if ($field['type'] == 'email_confirm') echo 'selected="selected"'; ?>><?php echo $field_email_confirm; ?></option>
														<option value="password" <?php if ($field['type'] == 'password') echo 'selected="selected"'; ?>><?php echo $field_password; ?></option>
														<option value="text" <?php if ($field['type'] == 'text') echo 'selected="selected"'; ?>><?php echo $field_text; ?></option>
														<option value="textarea" <?php if ($field['type'] == 'textarea') echo 'selected="selected"'; ?>><?php echo $field_textarea; ?></option>
													</optgroup>
													<optgroup label="<?php echo $field_time_based; ?>">
														<option value="date" <?php if ($field['type'] == 'date') echo 'selected="selected"'; ?>><?php echo $field_date; ?></option>
														<option value="time" <?php if ($field['type'] == 'time') echo 'selected="selected"'; ?>><?php echo $field_time; ?></option>
														<option value="datetime" <?php if ($field['type'] == 'datetime') echo 'selected="selected"'; ?>><?php echo $field_date_and_time; ?></option>
													</optgroup>
													<optgroup label="<?php echo $field_upload_based; ?>">
														<option value="file" <?php if ($field['type'] == 'file') echo 'selected="selected"'; ?>><?php echo $field_file; ?></option>
													</optgroup>
												</select>
											</td>
											<td><table>
													<?php foreach ($languages as $language) { ?>
														<?php $lcode = $language['code']; ?>
														<tr>
															<td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></td>
															<td><input type="text" name="<?php echo $name; ?>_data[<?php echo $id; ?>][fields][<?php echo $field_count; ?>][value][<?php echo $lcode; ?>]" value="<?php echo (empty($field['value'][$lcode])) ? '' : $field['value'][$lcode]; ?>" <?php if ($field['type'] == 'date' || $field['type'] == 'time' || $field['type'] == 'datetime') echo 'class="' . $field['type'] . '"'; ?> /></td>
														</tr>
													<?php } ?>
												</table>
											</td>
											<td><select name="<?php echo $name; ?>_data[<?php echo $id; ?>][fields][<?php echo $field_count; ?>][column]">
													<option value="1" <?php if ($field['column'] == 1) echo 'selected="selected"'; ?>>1</option>
													<option value="2" <?php if ($field['column'] == 2) echo 'selected="selected"'; ?>>2</option>
												</select>
											</td>
											<td><input type="text" size="1" name="<?php echo $name; ?>_data[<?php echo $id; ?>][fields][<?php echo $field_count; ?>][sort_order]" value="<?php echo $field['sort_order']; ?>" /></td>
											<td style="width: 1%"><a onclick="$(this).parent().parent().remove()"><img src="view/image/error.png" title="Remove" /></a></td>
										</tr>
										<?php $field_count++; ?>
									<?php } ?>
								<?php } ?>
								</table>
								<p><a onclick="addField('<?php echo $id; ?>');" class="button"><span><?php echo $button_add_field; ?></span></a></p>
							</td>
						</tr>
						<tr>
							<td><?php echo $entry_include_captcha; ?></td>
							<td><select name="<?php echo $name; ?>_data[<?php echo $id; ?>][captcha]">
									<option value="1" <?php if ($data['captcha']) echo 'selected="selected"'; ?>><?php echo $text_yes; ?></option>
									<option value="0" <?php if (!$data['captcha']) echo 'selected="selected"'; ?>><?php echo $text_no; ?></option>
								</select>
							</td>
						</tr>
						<tr>
							<td><?php echo $entry_html_content_after; ?></td>
							<td><table>
									<?php foreach ($languages as $language) { ?>
										<?php $lcode = $language['code']; ?>
										<tr>
											<td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></td>
											<td><textarea name="<?php echo $name; ?>_data[<?php echo $id; ?>][html_after][<?php echo $lcode; ?>]"><?php echo (empty($data['html_after'][$lcode])) ? '' : $data['html_after'][$lcode]; ?></textarea></td>
										</tr>
									<?php } ?>
								</table>
							</td>
						</tr>
						<tr>
							<td><?php echo $entry_success_message; ?></td>
							<td><table>
									<?php foreach ($languages as $language) { ?>
										<?php $lcode = $language['code']; ?>
										<tr>
											<td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></td>
											<td><textarea name="<?php echo $name; ?>_data[<?php echo $id; ?>][html_success][<?php echo $lcode; ?>]"><?php echo (empty($data['html_success'][$lcode])) ? '' : $data['html_success'][$lcode]; ?></textarea></td>
										</tr>
									<?php } ?>
								</table>
							</td>
						</tr>
						</table>
					</div>
				<?php } ?>
			</div>
		</form>
		<?php echo $copyright; ?>
	</div>
</div>
<?php if ($v14x) { ?>
	<script type="text/javascript" src="view/javascript/jquery/ui/ui.datepicker.js"></script>
	<script type="text/javascript" src="view/javascript/jquery/ui/ui.dialog.js"></script>
	<script type="text/javascript" src="view/javascript/jquery/ui/ui.draggable.js"></script>
	<script type="text/javascript" src="view/javascript/jquery/ui/ui.resizable.js"></script>
	<script type="text/javascript" src="view/javascript/jquery/ui/ui.slider.js"></script>
	<script type="text/javascript" src="view/javascript/jquery/ui/external/bgiframe/jquery.bgiframe.js"></script>
<?php } else { ?>
	</div>
<?php } ?>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
<script type="text/javascript"><!--
	$(document).ready(function(){
		attachTabs();
		attachEditors();
		attachDatePickers();	
	});
	
	function attachTabs() {
		<?php if ($v14x) { ?>
			$.tabs('#tabs a');
		<?php } else { ?>
			$('#tabs a').tabs(); 
		<?php } ?>
	}
	
	function attachEditors() {
		CKEDITOR.replaceAll(function(textarea, config) {
			if (CKEDITOR.instances[textarea.name]) CKEDITOR.instances[textarea.name].destroy();
			config.height = '100px';
			config.resize_enabled = true;
			config.filebrowserBrowseUrl = 'index.php?route=common/filemanager&token=<?php echo $token; ?>';
			config.filebrowserImageBrowseUrl = 'index.php?route=common/filemanager&token=<?php echo $token; ?>';
			config.filebrowserFlashBrowseUrl = 'index.php?route=common/filemanager&token=<?php echo $token; ?>';
			config.filebrowserUploadUrl = 'index.php?route=common/filemanager&token=<?php echo $token; ?>';
			config.filebrowserImageUploadUrl = 'index.php?route=common/filemanager&token=<?php echo $token; ?>';
			config.filebrowserFlashUploadUrl = 'index.php?route=common/filemanager&token=<?php echo $token; ?>';
		});	
	}
	
	function attachDatePickers() {
		$('.date').datepicker({dateFormat: 'yy-mm-dd'});
		$('.datetime').datetimepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: 'h:mm tt',
			separator: ' @ ',
			ampm: true
		});
		$('.time').timepicker({
			timeFormat: 'h:mm tt',
			ampm: true
		});
	}
	
	function addForm(id) {
		$('#new-form-keyword').val('');
		$('#tab-container').append('\
			<div id="tab-form-' + id + '" class="vtabs<?php echo $v14x ? '_page' : '-content'; ?>">\
				<table class="form">\
				<tr>\
					<td><?php echo $entry_form_link; ?></td>\
					<td><?php echo $text_url; ?> <input id="form-' + id + '-url" type="text" style="font-size: 11px; width: 70%; background: #F8F8F8" readonly="readonly" onclick="this.select()" value="<?php echo HTTP_CATALOG . 'index.php?route=information/form&form='; ?>' + id + '" />\
						<br />\
						<?php echo $text_html_code; ?> <input id="form-' + id + '-htmlcode" type="text" style="font-size: 11px; width: 70%; background: #F8F8F8" readonly="readonly" onclick="this.select()" value=\'<a href="<?php echo HTTP_CATALOG . 'index.php?route=information/form&form='; ?>' + id + '">' + id + '</a>\' />\
					</td>\
				</tr>\
				<tr>\
					<td><?php echo $entry_title; ?></td>\
					<td><table>\
							<?php foreach ($languages as $language) { ?>\
								<?php $lcode = $language['code']; ?>\
								<tr>\
									<td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></td>\
									<td><input type="text" size="50" name="<?php echo $name; ?>_data[' + id + '][title][<?php echo $lcode; ?>]" /></td>\
								</tr>\
							<?php } ?>\
						</table>\
					</td>\
				</tr>\
				<tr>\
					<td><?php echo $entry_email_results_to; ?></td>\
					<td><input type="text" size="50" name="<?php echo $name; ?>_data[' + id + '][email]" /></td>\
				</tr>\
				<tr>\
					<td><?php echo $entry_email_subject; ?></td>\
					<td><table>\
							<?php foreach ($languages as $language) { ?>\
								<?php $lcode = $language['code']; ?>\
								<tr>\
									<td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></td>\
									<td><input type="text" size="50" name="<?php echo $name; ?>_data[' + id + '][subject][<?php echo $lcode; ?>]" /></td>\
								</tr>\
							<?php } ?>\
						</table>\
					</td>\
				</tr>\
				<tr>\
					<td><?php echo $entry_file_size_limit; ?></td>\
					<td><input type="text" size="10" name="<?php echo $name; ?>_data[' + id + '][filesize_limit]" value="1024" /> <?php echo $text_kb; ?></td>\
				</tr>\
				<tr>\
					<td><?php echo $entry_allowed_extensions; ?></td>\
					<td><input type="text" size="50" name="<?php echo $name; ?>_data[' + id + '][allowed_ext]" value="gif, jpg, jpeg, png, pdf, txt" /></td>\
				</tr>\
				<tr>\
					<td><?php echo $entry_html_content_before; ?></td>\
					<td><table>\
							<?php foreach ($languages as $language) { ?>\
								<?php $lcode = $language['code']; ?>\
								<tr>\
									<td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></td>\
									<td><textarea name="<?php echo $name; ?>_data[' + id + '][html_before][<?php echo $lcode; ?>]"></textarea></td>\
								</tr>\
							<?php } ?>\
						</table>\
					</td>\
				</tr>\
				<tr>\
					<td colspan="2">\
						<table id="form-' + id + '-fields" class="list" style="margin-bottom: 0" cellpadding="7">\
						<thead>\
							<tr>\
								<td><?php echo $text_required; ?></td>\
								<td><?php echo $text_name; ?></td>\
								<td><?php echo $text_type; ?></td>\
								<td><?php echo $text_value; ?></td>\
								<td><?php echo $text_column; ?></td>\
								<td style="white-space: nowrap"><?php echo $text_sort_order; ?></td>\
								<td>&nbsp;</td>\
							</tr>\
						</thead>\
						</table>\
						<p><a onclick="addField(\'' + id + '\');" class="button"><span><?php echo $button_add_field; ?></span></a></p>\
					</td>\
				</tr>\
				<tr>\
					<td><?php echo $entry_include_captcha; ?></td>\
					<td><select name="<?php echo $name; ?>_data[' + id + '][captcha]">\
							<option value="1"><?php echo $text_yes; ?></option>\
							<option value="0"><?php echo $text_no; ?></option>\
						</select>\
					</td>\
				</tr>\
				<tr>\
					<td><?php echo $entry_html_content_after; ?></td>\
					<td><table>\
							<?php foreach ($languages as $language) { ?>\
								<?php $lcode = $language['code']; ?>\
								<tr>\
									<td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></td>\
									<td><textarea name="<?php echo $name; ?>_data[' + id + '][html_after][<?php echo $lcode; ?>]"></textarea></td>\
								</tr>\
							<?php } ?>\
						</table>\
					</td>\
				</tr>\
				<tr>\
					<td><?php echo $entry_success_message; ?></td>\
					<td><table>\
							<?php foreach ($languages as $language) { ?>\
								<?php $lcode = $language['code']; ?>\
								<tr>\
									<td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></td>\
									<td><textarea name="<?php echo $name; ?>_data[' + id + '][html_success][<?php echo $lcode; ?>]"></textarea></td>\
								</tr>\
							<?php } ?>\
						</table>\
					</td>\
				</tr>\
				</table>\
			</div>\
		');
		
		$('#tabs').append('\
			<a <?php echo $v14x ? 'tab' : 'href'; ?>="#tab-form-' + id + '">\
				' + id + '\
				<img src="view/image/delete_form.png" alt="delete_form" onclick="$(\'#tab-form-' + id + '\').remove(); $(this).parent().remove(); event.preventDefault(); $(\'#save-keep-editing\').click();" />\
			</a>\
		');
		
		attachTabs();
		attachEditors();
	}
	
	var fieldCount = <?php echo $field_count; ?>;
	function addField(id) {
		$('#form-' + id + '-fields').append('\
			<tr>\
				<td><select name="<?php echo $name; ?>_data[' + id + '][fields][' + fieldCount + '][required]">\
						<option value="1"><?php echo $text_yes; ?></option>\
						<option value="0" selected="selected"><?php echo $text_no; ?></option>\
					</select>\
				</td>\
				<td><table>\
						<?php foreach ($languages as $language) { ?>\
							<?php $lcode = $language['code']; ?>\
							<tr>\
								<td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></td>\
								<td><input type="text" name="<?php echo $name; ?>_data[' + id + '][fields][' + fieldCount + '][name][<?php echo $lcode; ?>]" /></td>\
							</tr>\
						<?php } ?>\
					</table>\
				</td>\
				<td><select name="<?php echo $name; ?>_data[' + id + '][fields][' + fieldCount + '][type]" onchange="$(this).parent().next().find(\'input\').datepicker(\'destroy\').timepicker(\'destroy\').datetimepicker(\'destroy\').attr(\'class\', $(this).val()); attachDatePickers()">\
						<optgroup label="<?php echo $field_selection_based; ?>">\
							<option value="checkbox"><?php echo $field_checkbox; ?></option>\
							<option value="radio"><?php echo $field_radio; ?></option>\
							<option value="select"><?php echo $field_select; ?></option>\
						</optgroup>\
						<optgroup label="<?php echo $field_text_based; ?>">\
							<option value="email"><?php echo $field_email; ?></option>\
							<option value="email_confirm"><?php echo $field_email_confirm; ?></option>\
							<option value="password"><?php echo $field_password; ?></option>\
							<option value="text"><?php echo $field_text; ?></option>\
							<option value="textarea"><?php echo $field_textarea; ?></option>\
						</optgroup>\
						<optgroup label="<?php echo $field_time_based; ?>">\
							<option value="date"><?php echo $field_date; ?></option>\
							<option value="time"><?php echo $field_time; ?></option>\
							<option value="datetime"><?php echo $field_date_and_time; ?></option>\
						</optgroup>\
						<optgroup label="<?php echo $field_upload_based; ?>">\
							<option value="file"><?php echo $field_file; ?></option>\
						</optgroup>\
					</select>\
				</td>\
				<td><table>\
						<?php foreach ($languages as $language) { ?>\
							<?php $lcode = $language['code']; ?>\
							<tr>\
								<td><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></td>\
								<td><input type="text" name="<?php echo $name; ?>_data[' + id + '][fields][' + fieldCount + '][value][<?php echo $lcode; ?>]" /></td>\
							</tr>\
						<?php } ?>\
					</table>\
				</td>\
				<td><select name="<?php echo $name; ?>_data[' + id + '][fields][' + fieldCount + '][column]">\
						<option value="1">1</option>\
						<option value="2">2</option>\
					</select>\
				</td>\
				<td><input type="text" size="1" name="<?php echo $name; ?>_data[' + id + '][fields][' + fieldCount + '][sort_order]" /></td>\
				<td style="width: 1%"><a onclick="$(this).parent().parent().remove()"><img src="view/image/error.png" title="Remove" /></a></td>\
			</tr>\
		');
		fieldCount++;
	}
//--></script>
<?php echo $footer; ?>