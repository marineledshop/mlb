<?php
/**
 * ProSlider OpenCart Module
 * Create and position a slideshow using the Nivo Slider library.
 * 
 * @author    Ian Gallacher
 * @version   1.5.1.*
 * @email     info@opencartstore.com
 * @support   www.opencartstore.com/support/
 */
 ?>

<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  
  <?php if ($error_warning) { ?>
    <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
        <a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
      </div>
    </div>

    <div class="content">
      <div id="tabs" class="htabs">
        <a href="#tab-settings">Settings</a>
        <a href="#tab-images">Images</a>
        <a href="#tab-layouts">Layouts</a>
      </div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <div id="tab-settings">
      <table class="form">  
        <tr>
          <td>
            <?php echo $entry_title_text; ?>
            <span class="help">When the slideshow is wrapped in a box this will be the title displayed in the header of the box.</span>
          </td>
          <td><input type="text" name="proslider_title_text" value="<?php echo $proslider_title_text; ?>" /></td>
        </tr>
        <tr>
          <td>
            <?php echo $entry_slide_effect; ?>
            <span class="help">This will set the slideshow transition effect.</span>
          </td>
          <td>
            <select name="proslider_slide_effect">
              <?php foreach ($available_effects as $key => $value) { ?>
          	   <option value="<?php echo $key; ?>" <?php echo ($proslider_slide_effect === $key) ? 'selected' : ''; ?>><?php echo $value; ?></option> 
              <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td>
            <?php echo $entry_display_box; ?>
            <span class="help">Whether or not the slideshow should be contained within a wrapper box.</span>
          </td>
          <td>
            <select name="proslider_display_box">
              <?php if ($proslider_display_box == 'true') { ?>
                <option value="true" selected="selected">True</option>
                <option value="false">False</option>
              <?php } else { ?>
                <option value="true">True</option>
                <option value="false" selected="selected">False</option>
              <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td>
            <?php echo $entry_show_navigation; ?>
            <span class="help">Show navigation (left/right) controls on the slideshow.</span>
          </td>
          <td>
            <select name="proslider_show_navigation">
              <?php if ($proslider_show_navigation == 'true') { ?>
                <option value="true" selected="selected">True</option>
                <option value="false">False</option>
              <?php } else { ?>
                <option value="true">True</option>
                <option value="false" selected="selected">False</option>
              <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td>
            <?php echo $entry_pause_onhover; ?>
            <span class="help">Pause the slideshow when the mouse hovers over it.</span>
          </td>
          <td>
            <select name="proslider_pause_onhover">
              <?php if ($proslider_pause_onhover == 'true') { ?>
                <option value="true" selected="selected">True</option>
                <option value="false">False</option>
              <?php } else { ?>
                <option value="true">True</option>
                <option value="false" selected="selected">False</option>
              <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td>
            <span class="required">*</span><?php echo $entry_slide_width; ?>
            <span class="help">Set the slideshow width. Recommended for default home page installations is 980.</span>
          </td>
          <td><input type="text" name="proslider_slide_width" value="<?php echo $proslider_slide_width; ?>"/></td>
        </tr>
        <tr>
          <td>
            <span class="required">*</span><?php echo $entry_slide_height; ?>
            <span class="help">Set the slideshow height. Recommended for default home page installations is 350.</span>
          </td>
          <td><input type="text" name="proslider_slide_height" value="<?php echo $proslider_slide_height; ?>"/></td>
        </tr>
        <tr>
          <td>
            <span class="required">*</span><?php echo $entry_animation_speed; ?>
            <span class="help">Set the speed at which the slideshow transitions occur. Recommended 500.</span>
          </td>
          <td><input type="text" name="proslider_animation_speed" value="<?php echo $proslider_animation_speed; ?>"/></td>
        </tr>
        <tr>
          <td>
            <span class="required">*</span><?php echo $entry_animation_pause; ?>
            <span class="help">Set the speed at which the slideshow pauses on each slide. Recommended 5000.</span>
          </td>
          <td><input type="text" name="proslider_animation_pause" value="<?php echo $proslider_animation_pause; ?>"/></td>
        </tr>
        <tr>
          <td>
            <span class="required">*</span><?php echo $entry_slices; ?>
            <span class="help">Set the number of pieces the slideshow effect will use. Recommended 20.</span>
          </td>
          <td><input type="text" name="proslider_slices" value="<?php echo $proslider_slices; ?>" size="2" /></td>
        </tr>
      </table>
      </div>

      <div id="tab-images">
      <table id="images" class="list">
        <thead>
          <tr>
            <td class="left"><?php echo $entry_title; ?></td>
            <td class="left"><?php echo $entry_link; ?></td>
            <td class="left"><?php echo $entry_image; ?></td>
            <td></td>
          </tr>
        </thead>
        <?php $image_row = 0; ?>
        <?php foreach ($proslider_images as $proslider_image) { ?>
          <tbody id="image-row<?php echo $image_row; ?>">
            <tr>
              <td class="left">
                <?php foreach ($languages as $language) { ?>
                  <input type="text" name="proslider_image[<?php echo $image_row; ?>][proslider_image_description][<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($proslider_image['proslider_image_description'][$language['language_id']]) ? $proslider_image['proslider_image_description'][$language['language_id']]['title'] : ''; ?>" />
                  <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
                <?php } ?>
              </td>
              
               <td class="left">
                <input type="text" name="proslider_image[<?php echo $image_row; ?>][link]" value="<?php echo $proslider_image['link']; ?>" />
              </td>

              <td class="left">
                <div class="image">
                  <img src="<?php echo $proslider_image['thumb']; ?>" alt="" id="thumb<?php echo $image_row; ?>" />
                  <input type="hidden" name="proslider_image[<?php echo $image_row; ?>][image]" value="<?php echo $proslider_image['image']; ?>" id="image<?php echo $image_row; ?>"  />
                  <br />
                  <a onclick="image_upload('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image<?php echo $image_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a>
                </div>
              </td>

              <td class="left">
                <a onclick="$('#image-row<?php echo $image_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a>
              </td>
            </tr>
          </tbody>
          <?php $image_row++; ?>
        <?php } ?>
        <tfoot>
            <tr>
              <td colspan="3"></td>
              <td class="left"><a onclick="addImage();" class="button"><?php echo $button_add_images; ?></a></td>
            </tr>
          </tfoot>
        </table>
        </div>

        <div id="tab-layouts">
        <table id="module" class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_layout; ?></td>
              <td class="left"><?php echo $entry_position; ?></td>
              <td class="left"><?php echo $entry_status; ?></td>
              <td class="right"><?php echo $entry_sort_order; ?></td>
              <td></td>
            </tr>
          </thead>
          <?php $module_row = 0; ?>
          <?php foreach ($modules as $module) { ?>
            <tbody id="module-row<?php echo $module_row; ?>">
              <tr>
                <td class="left">
                  <select name="proslider_module[<?php echo $module_row; ?>][layout_id]">
                    <?php foreach ($layouts as $layout) { ?>
                      <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                        <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                      <?php } else { ?>
                        <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                      <?php } ?>
                    <?php } ?>
                  </select>
                </td>
                <td class="left">
                  <select name="proslider_module[<?php echo $module_row; ?>][position]">
                    <?php if ($module['position'] == 'content_top') { ?>
                      <option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
                    <?php } else { ?>
                      <option value="content_top"><?php echo $text_content_top; ?></option>
                    <?php } ?>
                    <?php if ($module['position'] == 'content_bottom') { ?>
                      <option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
                    <?php } else { ?>
                      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>
                    <?php } ?>
                    <?php if ($module['position'] == 'column_left') { ?>
                      <option value="column_left" selected="selected"><?php echo $text_column_left; ?></option>
                    <?php } else { ?>
                      <option value="column_left"><?php echo $text_column_left; ?></option>
                    <?php } ?>
                    <?php if ($module['position'] == 'column_right') { ?>
                      <option value="column_right" selected="selected"><?php echo $text_column_right; ?></option>
                    <?php } else { ?>
                      <option value="column_right"><?php echo $text_column_right; ?></option>
                    <?php } ?>
                  </select>
                </td>
                <td class="left">
                  <select name="proslider_module[<?php echo $module_row; ?>][status]">
                    <?php if ($module['status']) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select>
                </td>
                <td class="right">
                  <input type="text" name="proslider_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" />
                </td>
                <td class="left">
                  <a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a>
                </td>
              </tr>
            </tbody>
            <?php $module_row++; ?>
            <?php } ?>
            <tfoot>
              <tr>
                <td colspan="4"></td>
                <td class="left"><a onclick="addModule();" class="button"><?php echo $button_add_module; ?></a></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript"><!--
var image_row = <?php echo $image_row; ?>;

function addImage() {
  html  = '<tbody id="image-row' + image_row + '">';
	html += '<tr>';
	html += '<td class="left">';
  <?php foreach ($languages as $language) { ?>
    html += '<input type="text" name="proslider_image[' + image_row + '][proslider_image_description][<?php echo $language['language_id']; ?>][title]" value="" /> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';
  <?php } ?>
  html += '</td>';  
  html += '<td class="left"><input type="text" name="proslider_image[' + image_row + '][link]" value="" /></td>';  
  html += '<td class="left"><div class="image"><img src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '" /><input type="hidden" name="proslider_image[' + image_row + '][image]" value="" id="image' + image_row + '" /><br /><a onclick="image_upload(\'image' + image_row + '\', \'thumb' + image_row + '\');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb' + image_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + image_row + '\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a></div></td>';
  html += '<td class="left"><a onclick="$(\'#image-row' + image_row  + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
  html += '</tr>';
  html += '</tbody>'; 
  
  $('#images tfoot').before(html);
  
  image_row++;
}
//--></script>

<script type="text/javascript"><!--
function image_upload(field, thumb) {
  $('#dialog').remove();
  
  $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
  
  $('#dialog').dialog({
    title: '<?php echo $text_image_manager; ?>',
    close: function (event, ui) {
      if ($('#' + field).attr('value')) {
        $.ajax({
          url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
          dataType: 'text',
          success: function(data) {
            $('#' + thumb).replaceWith('<img src="' + data + '" alt="" id="' + thumb + '" />');
          }
        });
      }
    },  
    bgiframe:   false,
    width:      700,
    height:     400,
    resizable:  true,
    modal:      true
  });
};
//--></script> 

<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {  
  html  = '<tbody id="module-row' + module_row + '">';
  html += '  <tr>';
  html += '    <td class="left"><select name="proslider_module[' + module_row + '][layout_id]">';
  <?php foreach ($layouts as $layout) { ?>
  html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
  <?php } ?>
  html += '    </select></td>';
  html += '    <td class="left"><select name="proslider_module[' + module_row + '][position]">';
  html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
  html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
  html += '      <option value="column_left"><?php echo $text_column_left; ?></option>';
  html += '      <option value="column_right"><?php echo $text_column_right; ?></option>';
  html += '    </select></td>';
  html += '    <td class="left"><select name="proslider_module[' + module_row + '][status]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
  html += '    <td class="right"><input type="text" name="proslider_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
  html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
  html += '  </tr>';
  html += '</tbody>';
  
  $('#module tfoot').before(html);
  
  module_row++;
}
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script> 
<?php echo $footer; ?>