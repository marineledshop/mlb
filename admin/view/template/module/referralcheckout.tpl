<?php echo $header; ?>

<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <?php foreach($module as $key => $module_layout) { ?>
          <input type="hidden" name="referralcheckout_module[<?php echo $key ?>][layout_id]" value="<?php echo $module_layout['layout_id']; ?>" />
          <input type="hidden" name="referralcheckout_module[<?php echo $key ?>][position]" value="<?php echo $module_layout['position']; ?>" />
          <input type="hidden" name="referralcheckout_module[<?php echo $key ?>][sort_order]" value="<?php echo $module_layout['sort_order']; ?>" />
          <input type="hidden" name="referralcheckout_module[<?php echo $key ?>][status]" value="1" />
      <?php }?>
        <table class="form" id="rpx_table">
          <?php /*?>
		  <tr>
            <td>*<?php echo $entry_site_id;?></td>
            <td valign="top"><input type="text" name="referralcheckout_module[0][site_id]" value="<?php echo $module[0]['site_id']; ?>" />
              </td>
          </tr>
		   <?php*/ ?>
          <tr>
            <td><?php echo $entry_status;?></td>
            <td valign="top"><select name="referralcheckout_module[0][status]">
                <?php if ($module[0]['status']) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?> 