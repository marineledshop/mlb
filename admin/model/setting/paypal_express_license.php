<?php

/*
 * Copyright (c) 2012 Web Project Solutions LLC (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 *  @author Antonello Venturino <info@webprojectsol.com>
 *  @copyright  2012 Web Project Solutions LLC
 *  @license    http://www.webprojectsol.com/license.php
 *  @url  http://www.webprojectsol.com/moduli-di-pagamento/paypal-express-checkout.html
 */

class ModelSettingPayPalExpressLicense extends Model {

    private $code = 'PAGPEXCOPC';

    public function _check() {
        return $this->check();
    }

    private function chklcs($u) {
        if (function_exists('curl_init')) {
            $c = @curl_init($u);
            @curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            return curl_exec($c);
        } elseif (@ini_get('allow_url_fopen') == '1') {
            return file_get_contents($u);
        }
    }

    private function check() {
        global $_SERVER;
        $s = strtr($_SERVER['SERVER_NAME'], array('www.' => ''));
        if (file_exists($this->dirCache() . $this->code . '_KEY.txt')) {
            if (file_get_contents($this->dirCache() . $this->code . '_KEY.txt') == sha1($s . '|' . $this->code . '|1')) {
                return true;
            } else {
                $l = $this->chklcs('http://www.webprojectsol.com/extern/CheckLicense.php?code=' . $this->code . '&domain=' . $s);
            }
        } else {
            $l = $this->chklcs('http://www.webprojectsol.com/extern/CheckLicense.php?code=' . $this->code . '&domain=' . $s);
        }
        if ($s != 'localhost' && $l != 1) {
            return false;
        }
        if ($s == 'localhost')
            $l = 1;
        $file = $this->dirCache() . $this->code . '_KEY.txt';
        $apro = fopen($file, "w");
        $c = sha1($s . '|' . $this->code . '|' . $l);
        fputs($apro, stripslashes($c));
        fclose($apro);
        return true;
    }
    
    private function dirCache() {
        $dir = DIR_CACHE;
        if (!is_dir($dir)) {
            @mkdir($dir, 0777, true);
        }
        return $dir;
    }

}