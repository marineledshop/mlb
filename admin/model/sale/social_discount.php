<?php
class ModelSaleSocialDiscount extends Model {

	public function serializeSocialDiscountOptions($data) {
		$option = array();
		$option['type'] = $data['type'];
		$option['social_action'] = $data['social_action'];
		$option['social_network_page'] = $data['social_network_page'];
		$option['tweet_text'] = $data['tweet_text'];
		$option['discount'] = $data['discount'];
		$option['quantity_total'] = $data['quantity_total'];
		$option['total'] = $data['total'];
		$option['logged'] = $data['logged'];
		$option['shipping'] = $data['shipping'];
		$option['uses_total'] = $data['uses_total'];
		$option['uses_customer'] = $data['uses_customer'];
		$options = serialize($option);
		return $options;
		}

	public function addSocialDiscount($data) {
      	
		$options =  $this->model_sale_social_discount->serializeSocialDiscountOptions($data);
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "social_discount SET name = '" . $this->db->escape($data['name']) . "', options = '" . $this->db->escape($options) . "', date_start = '" . $this->db->escape($data['date_start']) . "', date_end = '" . $this->db->escape($data['date_end']) . "', status = '" . (int)$data['status'] . "', date_added = NOW()");
		
		}

	public function editSocialDiscount($social_discount_id, $data) {
      	
		$options =  $this->model_sale_social_discount->serializeSocialDiscountOptions($data);
		
		$this->db->query("UPDATE " . DB_PREFIX . "social_discount SET name = '" . $this->db->escape($data['name']) . "', options = '" . $this->db->escape($options) . "', date_start = '" . $this->db->escape($data['date_start']) . "', date_end = '" . $this->db->escape($data['date_end']) . "', status = '" . (int)$data['status'] . "' WHERE social_discount_id = '" . (int)$social_discount_id . "'");
		
		}
	
	
	public function deleteSocialDiscount($social_discount_id) {
      	$this->db->query("DELETE FROM " . DB_PREFIX . "social_discount WHERE social_discount_id = '" . (int)$social_discount_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "social_discount_history WHERE social_discount_id = '" . (int)$social_discount_id . "'");		
		}
	
	public function getSocialDiscount($social_discount_id) {
      	$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "social_discount WHERE social_discount_id = '" . (int)$social_discount_id . "'");
		
		return $query->row;
	}
	
	public function validateSocialDiscountNameById($name,$social_discount_id) {
      	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "social_discount WHERE name = '" . $name . "' AND social_discount_id != '" . (int)$social_discount_id . "'");
		
		return $query->row;
	}
	
	public function validateSocialDiscountName($name) {
      	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "social_discount WHERE name = '" . $name . "'");
		
		return $query->row;
	}
	
	public function getSocialDiscounts($data = array()) {
		$sql = "SELECT social_discount_id, name, options, date_start, date_end, status FROM " . DB_PREFIX . "social_discount";
		
		$sort_data = array(
			'name',
			'options',
			'date_start',
			'date_end',
			'status'
		);	
			
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}		
		
		$query = $this->db->query($sql);
		
		return $query->rows;
	}
	
	public function getTotalSocialDiscount() {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "social_discount");
		
		return $query->row['total'];
	}	
	
	public function getSocialDiscountHistories($social_discount_id, $start = 0, $limit = 10) {
		$query = $this->db->query("SELECT ch.order_id, CONCAT(c.firstname, ' ', c.lastname) AS customer, ch.amount, ch.date_added FROM " . DB_PREFIX . "social_discount_history ch LEFT JOIN " . DB_PREFIX . "customer c ON (ch.customer_id = c.customer_id) WHERE ch.social_discount_id = '" . (int)$social_discount_id . "' ORDER BY ch.date_added ASC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}
	
	public function getTotalSocialDiscountHistories($social_discount_id) {
	  	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "social_discount_history WHERE social_discount_id = '" . (int)$social_discount_id . "'");

		return $query->row['total'];
	}		
	
}
	
	