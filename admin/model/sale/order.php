<?php




// Error Handler
function error_handler_for_export($errno, $errstr, $errfile, $errline) {
    global $config;
    global $log;

    switch ($errno) {
        case E_NOTICE:
        case E_USER_NOTICE:
            $errors = "Notice";
            break;
        case E_WARNING:
        case E_USER_WARNING:
            $errors = "Warning";
            break;
        case E_ERROR:
        case E_USER_ERROR:
            $errors = "Fatal Error";
            break;
        default:
            $errors = "Unknown";
            break;
    }

    if (($errors=='Warning') || ($errors=='Unknown')) {
        return true;
    }

    if ($config->get('config_error_display')) {
        //VV echo '<b>' . $errors . '</b>: ' . $errstr . ' in <b>' . $errfile . '</b> on line <b>' . $errline . '</b>';
    }

    if ($config->get('config_error_log')) {
        $log->write('PHP ' . $errors . ':  ' . $errstr . ' in ' . $errfile . ' on line ' . $errline);
    }

    return true;
}


function fatal_error_shutdown_handler_for_export()
{
    $last_error = error_get_last();
    if ($last_error['type'] === E_ERROR) {
        // fatal error
        error_handler_for_export(E_ERROR, $last_error['message'], $last_error['file'], $last_error['line']);
    }
}

class ModelSaleOrder extends Model {
    
// Enhanced Customer Information - START
	public function getCustomerOrders($customer_id){
		$sql = "SELECT o.customer_id, o.order_id, o.invoice_no, o.invoice_prefix, o.currency_code, o.currency_value, o.order_status_id, o.date_added, o.store_name, o.total, (SELECT SUM(op.quantity) FROM `" . DB_PREFIX . "order_product` op WHERE op.order_id = o.order_id GROUP BY op.order_id) AS products, (SELECT os.name FROM `" . DB_PREFIX . "order_status` os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS os_name, (SELECT SUM(o.total) FROM `" . DB_PREFIX . "order` o WHERE o.order_status_id > '0' AND o.customer_id = '" . $customer_id . "' GROUP BY o.customer_id) AS total_value FROM `" . DB_PREFIX . "order` o WHERE o.order_status_id > '0' AND o.customer_id = '" . $customer_id . "' GROUP BY o.order_id ORDER BY o.order_id DESC";
		
		$result = $this->db->query($sql);
		return $result->rows;
	}

	public function getCustomerOrdersTotal($customer_id){
		$sql = "SELECT COUNT(o.order_id) AS total_orders, SUM(o.total) AS total_value, o.currency_code, o.currency_value FROM `" . DB_PREFIX . "order` o WHERE o.order_status_id > '0' AND o.customer_id = '" . $customer_id . "'GROUP BY o.customer_id";
		
		$result = $this->db->query($sql);
		return $result->rows;
	}
	
	public function getCustomerOrderedProducts($customer_id){
		$query = 'SET SESSION group_concat_max_len=150000'; 
 
		mysql_query($query);
		
		$sql = "SELECT o.customer_id, o.order_id, o.currency_code, o.order_status_id, op.product_id, pd.name, op.model, qa.order_product_id, SUM(op.quantity) AS quantity, SUM(op.total + op.total * op.tax / 100) AS total, p.sku, p.manufacturer_id, p.image AS image, options, oovalue, ooname FROM (SELECT oo.order_product_id, GROUP_CONCAT(product_option_value_id) AS options, GROUP_CONCAT(value SEPARATOR '<br>') AS oovalue, GROUP_CONCAT(name SEPARATOR ':<br>') AS ooname FROM `" . DB_PREFIX . "order_option` oo GROUP BY order_product_id) qa, `" . DB_PREFIX . "order_product` op, `" . DB_PREFIX . "order` o, `" . DB_PREFIX . "product` p, `" . DB_PREFIX . "product_description` pd WHERE pd.product_id = op.product_id AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND o.order_status_id > '0' AND o.customer_id = '" . $customer_id . "' AND op.order_id = o.order_id AND op.order_product_id = qa.order_product_id AND op.product_id = p.product_id GROUP BY options, op.product_id";
		
		$sql .= " UNION ALL SELECT o.customer_id, o.order_id, o.currency_code, o.order_status_id, op.product_id, pd.name, op.model, op.order_product_id, SUM(op.quantity) AS quantity, SUM(op.total + op.total * op.tax / 100) AS total, p.sku, p.manufacturer_id, p.image AS image, '' AS options, '' AS oovalue, '' AS ooname FROM `" . DB_PREFIX . "order_product` op, `" . DB_PREFIX . "order` o, `" . DB_PREFIX . "product` p, `" . DB_PREFIX . "product_description` pd WHERE pd.product_id = op.product_id AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND o.order_status_id > '0' AND o.customer_id = '" . $customer_id . "' AND op.order_id = o.order_id AND op.product_id = p.product_id AND op.order_product_id NOT IN (SELECT order_product_id FROM " . DB_PREFIX . "order_option) GROUP BY op.product_id ORDER BY quantity DESC";
		
		$result = $this->db->query($sql);
		return $result->rows;
	}
	
	public function getCustomerTrack($date_options = array(), $customer_id) {
		if(empty($date_options)) { return array(); }
		$sql = "SELECT ct.route, ct.ip_address, ct.customer_id, ct.current_url, ct.referrer, ct.agent_type, ct.access_time FROM `" . DB_PREFIX . "customer_track` ct LEFT OUTER JOIN `" . DB_PREFIX . "customer` cus ON ct.customer_id = cus.customer_id WHERE ct.customer_track_id IS NOT NULL AND ct.customer_id = '" . $customer_id . "'";
		
		if(isset($date_options['start_date']) && $date_options['start_date'] != '') {
			$sql .= " AND DATE(ct.access_time) >=  '" . htmlentities($date_options['start_date']) . "'";
		}
		
		if(isset($date_options['end_date']) && $date_options['end_date'] != '') {
			$sql .= " AND DATE(ct.access_time) <= '" . htmlentities($date_options['end_date']) . "'";
		}
		
		$sql .= " ORDER BY ct.access_time DESC";
		
		$query = $this->db->query($sql);
	
		return $query->rows;
	}	

	public function deleteCustomerTrack($customer_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "customer_track` WHERE customer_id = '" . (int)$customer_id . "'");
		return true;
	}

	public function getCustomerCart($customer_id) {
		$query = $this->db->query("SELECT cart FROM " . DB_PREFIX . "customer  WHERE customer_id = '" . $customer_id . "'");
		return $query->rows;
	}

	public function clearCustomerCart($customer_id) {
		$query = $this->db->query("UPDATE " . DB_PREFIX . "customer SET cart = 'a:0:{}' WHERE customer_id = '" . (int)$customer_id . "'");
		return true;
	}

	public function getCustomerWishList($customer_id) {
		$query = $this->db->query("SELECT wishlist FROM " . DB_PREFIX . "customer  WHERE customer_id = '" . $customer_id . "'");
		return $query->rows;
	}

	public function deleteCustomerWishList($customer_id) {
		$query = $this->db->query("UPDATE " . DB_PREFIX . "customer SET wishlist = 'a:0:{}' WHERE customer_id = '" . (int)$customer_id . "'");
		return true;
	}
// Enhanced Customer Information - END
    
        private $commonOrderColumns  = array(
            'S/N' => 'auto_increment',
            'Last Name' => 'shipping_lastname',
            'Middle Name' => '',
            'First Name' => 'shipping_firstname',
            'Address 1' => 'shipping_address_1',
            'Address 2' => 'shipping_address_2',
            'City' => 'shipping_city',
            'State' => 'zone',
            'Postcode' => 'shipping_postcode',
            'Country' => 'country_name',
            'Contact No' => 'telephone',
            'Tracking Number' => 'tracking',
	    'Cust Reference No' => 'order_id',
            'Weight per Article (gm)' => 'weight',
            'Declared Currency' => 'currency_code',
            'Declared Value' => 'total',
            'Item Description' => 'shipping_description',
            'HS code' => 'hs_code',
            'Insured (Yes/No)' => 'insured',
            'Insured Currency' => '',
            'Insured value' => '',
        );

        private $dhlOrderColumns  = array(
            'Item Ref#' => 'order_id',
            'Name' => 'customer_name',
            'Address1' => 'shipping_address_1',
            'Address2' => 'shipping_address_2',
            'Address3' => '',
            'Town/City/State' => 'city_state',
            'Postal Code' => 'shipping_postcode',
            'Destination' => 'destination', 
            'Wt (g)' => 'weight',
            'Currency' => 'currency_code',
            'Origin' => 'origin', 
            'Contents' => 'contents', 
            'Content Desc 1' => 'content_desc_1', 
            'Value 1' => 'total', 
            'Content Desc 2' => '',
            'Value 2' => '',
            'Content Desc 3' => '',
            'Value 3' => '',
            'Content Desc 4' => '',
            'Value 4' => ''
        );    
    
	public function addOrder($data) {
		$invoice_no = '';
		$total = '';

		$this->load->model('setting/store');

		$store_info = $this->model_setting_store->getStore($data['store_id']);

		if ($store_info) {
			$store_name = $store_info['name'];
			$store_url = $store_info['url'];
		} else {
			$store_name = $this->config->get('config_name');
			$store_url = HTTP_CATALOG;
		}

		$this->load->model('sale/customer');

		$customer_info = $this->model_sale_customer->getCustomer($data['customer_id']);

		if ($customer_info) {
			$customer_group_id = $customer_info['customer_group_id'];
		} elseif ($store_info) {
			$customer_group_id = $store_info['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($data['shipping_country_id']);

		if ($country_info) {
			$shipping_country = $country_info['name'];
			$shipping_address_format = $country_info['address_format'];
		} else {
			$shipping_country = '';
			$shipping_address_format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
		}

		$this->load->model('localisation/zone');

		$zone_info = $this->model_localisation_zone->getZone($data['shipping_zone_id']);

		if ($zone_info) {
			$shipping_zone = $zone_info['name'];
		} else {
			$shipping_zone = '';
		}

		$country_info = $this->model_localisation_country->getCountry($data['payment_country_id']);

		if ($country_info) {
			$payment_country = $country_info['name'];
			$payment_address_format = $country_info['address_format'];
		} else {
			$payment_country = '';
			$payment_address_format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
		}

		$zone_info = $this->model_localisation_zone->getZone($data['payment_zone_id']);

		if ($zone_info) {
			$payment_zone = $zone_info['name'];
		} else {
			$payment_zone = '';
		}

		$this->load->model('localisation/currency');

		$currency_info = $this->model_localisation_currency->getCurrencyByCode($this->config->get('config_currency'));

		if ($currency_info) {
			$currency_id = $currency_info['currency_id'];
			$currency_code = $this->config->get('config_currency');
			$currency_value = $currency_info['value'];
		} else {
			$currency_id = 0;
			$currency_code = $currency_info['code'];
			$currency_value = 1.00000;
		}

      	$this->db->query("INSERT INTO " . DB_PREFIX . "order SET invoice_no = '" . (int)$invoice_no . "', invoice_prefix = '" . $this->db->escape($this->config->get('config_invoice_prefix')) . "', store_id = '" . (int)$data['store_id'] . "', store_name = '" . $this->db->escape($store_name) . "', store_url = '" . $this->db->escape($store_url) . "', customer_id = '" . (int)$data['customer_id'] . "', customer_group_id = '" . (int)$customer_group_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', shipping_firstname = '" . $this->db->escape($data['shipping_firstname']) . "', shipping_lastname = '" . $this->db->escape($data['shipping_lastname']) . "',  shipping_company = '" . $this->db->escape($data['shipping_company']) . "', shipping_address_1 = '" . $this->db->escape($data['shipping_address_1']) . "', shipping_address_2 = '" . $this->db->escape($data['shipping_address_2']) . "', shipping_city = '" . $this->db->escape($data['shipping_city']) . "', shipping_postcode = '" . $this->db->escape($data['shipping_postcode']) . "', shipping_country = '" . $this->db->escape($shipping_country) . "', shipping_country_id = '" . (int)$data['shipping_country_id'] . "', shipping_zone = '" . $this->db->escape($shipping_zone) . "', shipping_zone_id = '" . (int)$data['shipping_zone_id'] . "', shipping_address_format = '" . $this->db->escape($shipping_address_format) . "', shipping_method = '" . $this->db->escape($data['shipping_method']) . "', payment_firstname = '" . $this->db->escape($data['payment_firstname']) . "', payment_lastname = '" . $this->db->escape($data['payment_lastname']) . "', payment_company = '" . $this->db->escape($data['payment_company']) . "', payment_address_1 = '" . $this->db->escape($data['payment_address_1']) . "', payment_address_2 = '" . $this->db->escape($data['payment_address_2']) . "', payment_city = '" . $this->db->escape($data['payment_city']) . "', payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', payment_country = '" . $this->db->escape($payment_country) . "', payment_country_id = '" . (int)$data['payment_country_id'] . "', payment_zone = '" . $this->db->escape($payment_zone) . "', payment_zone_id = '" . (int)$data['payment_zone_id'] . "', payment_address_format = '" . $this->db->escape($payment_address_format) . "', payment_method = '" . $this->db->escape($data['payment_method']) . "', comment = '" . $this->db->escape($data['comment']) . "', total = '" . (float)$total . "', order_status_id = '" . (int)$data['order_status_id'] . "', affiliate_id  = '" . (int)$data['affiliate_id'] . "', language_id = '" . (int)$data['affiliate_id'] . "', currency_id = '" . $this->db->escape($this->config->get('config_currency')) . "', currency_code = '" . $this->db->escape($currency_code) . "', currency_value = '" . (float)$currency_value . "', date_added = NOW(), date_modified = NOW()");

      	$order_id = $this->db->getLastId();

      	if (isset($data['order_product'])) {
      		foreach ($data['order_product'] as $order_product) {
      			$this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$order_product['product_id'] . "', name = '" . $this->db->escape($order_product['name']) . "', model = '" . $this->db->escape($order_product['model']) . "', quantity = '" . (int)$order_product['quantity'] . "', price = '" . (float)$order_product['price'] . "', total = '" . (float)$order_product['total'] . "', tax = '" . (float)$order_product['tax'] . "'");

				$order_product_id = $this->db->getLastId();

				foreach ($order_product['order_option'] as $order_option) {
					if ($option['type'] != 'checkbox') {
						$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$option['product_option_id'] . "', product_option_value_id = '" . (int)$option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
					} else {
						foreach ($option['option_value'] as $option_value) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$option['product_option_id'] . "', product_option_value_id = '" . (int)$option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
						}
					}
				}

				foreach ($product['download'] as $download) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_download SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', name = '" . $this->db->escape($download['name']) . "', filename = '" . $this->db->escape($download['filename']) . "', mask = '" . $this->db->escape($download['mask']) . "', remaining = '" . (int)($download['remaining'] * $product['quantity']) . "'");
				}
			}
		}

      	if (isset($data['order_total'])) {
      		foreach ($data['order_total'] as $order_total) {
      			$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$return_product['product_id'] . "', name = '" . $this->db->escape($return_product['name']) . "', model = '" . $this->db->escape($return_product['model']) . "', quantity = '" . (int)$return_product['quantity'] . "', manufacturer = '" . (int)$return_product['manufacturer'] . "', return_reason_id = '" . (int)$return_product['return_reason_id'] . "', opened = '" . (int)$return_product['opened'] . "', comment = '" . $this->db->escape($return_product['comment']) . "', return_action_id = '" . (int)$return_product['return_action_id'] . "'");
			}
		}
	}

	public function editOrder($order_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "return SET order_id = '" . (int)$data['order_id'] . "', customer_id = '" . (int)$data['customer_id'] . "', invoice_no = '" . $this->db->escape($data['invoice_no']) . "', invoice_date = '" . $this->db->escape($data['invoice_date']) . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', return_status_id = '" . (int)$data['return_status_id'] . "', comment = '" . $this->db->escape($data['comment']) . "', date_modified = NOW() WHERE return_id = '" . (int)$return_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "return_product WHERE return_id = '" . (int)$return_id . "'");

		if (isset($data['return_product'])) {
      		foreach ($data['return_product'] as $return_product) {
      			$this->db->query("INSERT INTO " . DB_PREFIX . "return_product SET return_id = '" . (int)$return_id . "', product_id = '" . (int)$return_product['product_id'] . "', name = '" . $this->db->escape($return_product['name']) . "', model = '" . $this->db->escape($return_product['model']) . "', quantity = '" . (int)$return_product['quantity'] . "', return_reason_id = '" . (int)$return_product['return_reason_id'] . "', opened = '" . (int)$return_product['opened'] . "', comment = '" . $this->db->escape($return_product['comment']) . "', return_action_id = '" . (int)$return_product['return_action_id'] . "'");
			}
		}
	}

	public function deleteOrder($order_id) {
		if ($this->config->get('config_stock_subtract')) {
			$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` WHERE order_status_id > '0' AND order_id = '" . (int)$order_id . "'");

			if ($order_query->num_rows) {
				$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach($product_query->rows as $product) {
					$this->db->query("UPDATE `" . DB_PREFIX . "product` SET quantity = (quantity + " . (int)$product['quantity'] . ") WHERE product_id = '" . (int)$product['product_id'] . "'");

					$option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

					foreach ($option_query->rows as $option) {
						$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity + " . (int)$product['quantity'] . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
					}
				}
			}
		}

		$this->db->query("DELETE FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "'");
      	$this->db->query("DELETE FROM " . DB_PREFIX . "order_history WHERE order_id = '" . (int)$order_id . "'");
      	$this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
      	$this->db->query("DELETE FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "'");
	  	$this->db->query("DELETE FROM " . DB_PREFIX . "order_download WHERE order_id = '" . (int)$order_id . "'");
      	$this->db->query("DELETE FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_reward WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "affiliate_transaction WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "coupon_history WHERE order_id = '" . (int)$order_id . "'");
	}

	public function getOrder($order_id) {
		$order_query = $this->db->query("SELECT *, (SELECT CONCAT(c.firstname, ' ', c.lastname) FROM " . DB_PREFIX . "customer c WHERE c.customer_id = o.customer_id) AS customer FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");

		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");
			$d_keys = array();
			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];

				$zone_info = $this->db->query("SELECT geo_zone_id FROM `" . DB_PREFIX . "zone_to_geo_zone` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

				foreach($zone_info->rows as $zn_id)
				{
					if($this->config->get($order_query->row['shipping_provider'].'_' . $zn_id['geo_zone_id'] . '_status'))
					{
						$d_keys[] = $this->config->get($order_query->row['shipping_provider'].'_' . $zn_id['geo_zone_id'] . '_delivery');
					}
				}
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			if ($order_query->row['affiliate_id']) {
				$affiliate_id = $order_query->row['affiliate_id'];
			} else {
				$affiliate_id = 0;
			}

			$this->load->model('sale/affiliate');

			$affiliate_info = $this->model_sale_affiliate->getAffiliate($affiliate_id);

			if ($affiliate_info) {
				$affiliate_firstname = $affiliate_info['firstname'];
				$affiliate_lastname = $affiliate_info['lastname'];
			} else {
				$affiliate_firstname = '';
				$affiliate_lastname = '';
			}

			$this->load->model('localisation/language');

			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

			if ($language_info) {
				$language_code = $language_info['code'];
				$language_filename = $language_info['filename'];
				$language_directory = $language_info['directory'];
			} else {
				$language_code = '';
				$language_filename = '';
				$language_directory = '';
			}

			return array(
				'order_id'                => $order_query->row['order_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],
				'customer_id'             => $order_query->row['customer_id'],
				'customer'                => $order_query->row['customer'],
				'customer_group_id'       => $order_query->row['customer_group_id'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'email'                   => $order_query->row['email'],
				'shipping_firstname'      => $order_query->row['shipping_firstname'],
				'shipping_lastname'       => $order_query->row['shipping_lastname'],
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address_1'      => $order_query->row['shipping_address_1'],
				'shipping_address_2'      => $order_query->row['shipping_address_2'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],
				'shipping_iso_code_2'     => $shipping_iso_code_2,
				'shipping_iso_code_3'     => $shipping_iso_code_3,
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_method'         => $order_query->row['shipping_method'],
                'shipping_provider'       => $order_query->row['shipping_provider'],
                'shipping_cost'           => $order_query->row['shipping_cost'],
                'shipping_option'         => $order_query->row['shipping_option'],
				'payment_firstname'       => $order_query->row['payment_firstname'],
				'payment_lastname'        => $order_query->row['payment_lastname'],
				'payment_company'         => $order_query->row['payment_company'],
				'payment_address_1'       => $order_query->row['payment_address_1'],
				'payment_address_2'       => $order_query->row['payment_address_2'],
				'payment_postcode'        => $order_query->row['payment_postcode'],
				'payment_city'            => $order_query->row['payment_city'],
				'payment_zone_id'         => $order_query->row['payment_zone_id'],
				'payment_zone'            => $order_query->row['payment_zone'],
				'payment_zone_code'       => $payment_zone_code,
				'payment_country_id'      => $order_query->row['payment_country_id'],
				'payment_country'         => $order_query->row['payment_country'],
				'payment_iso_code_2'      => $payment_iso_code_2,
				'payment_iso_code_3'      => $payment_iso_code_3,
				'payment_address_format'  => $order_query->row['payment_address_format'],
				'payment_method'          => $order_query->row['payment_method'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'reward'                  => $order_query->row['reward'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'affiliate_id'            => $order_query->row['affiliate_id'],
				'affiliate_firstname'     => $affiliate_firstname,
				'affiliate_lastname'      => $affiliate_lastname,
				'commission'              => $order_query->row['commission'],
				'language_id'             => $order_query->row['language_id'],
				'language_code'           => $language_code,
				'language_filename'       => $language_filename,
				'language_directory'      => $language_directory,
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'date_added'              => $order_query->row['date_added'],
				'date_modified'           => $order_query->row['date_modified'],
				'ip'                      => $order_query->row['ip'],
				'track'                   => $order_query->row['track'],
				'd_keys'                  => $d_keys
			);
		} else {
			return false;
		}
	}

	public function getOrders($data = array()) {
		$sql = "SELECT o.order_id, CONCAT(o.firstname, ' ', o.lastname) AS customer, (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS status, o.total, o.currency_code, o.currency_value, o.date_added, o.date_modified, o.shipping_provider, o.shipping_cost, o.shipping_country FROM `" . DB_PREFIX . "order` o";

		if (isset($data['filter_order_status_id']) && !is_null($data['filter_order_status_id'])) {
			$sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_order_id'])) {
			$sql .= " AND o.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$sql .= " AND LCASE(CONCAT(o.firstname, ' ', o.lastname)) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_customer'])) . "%'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(o.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

        if (!empty($data['filter_shipping_provider'])) {
            $sql .= " AND o.shipping_provider='" .$this->db->escape($data['filter_shipping_provider']). "'";
        }

        if (!empty($data['filter_shipping_country_id'])) {
            $sql .= " AND o.shipping_country_id='" .$this->db->escape($data['filter_shipping_country_id']). "'";
        }

		if (!empty($data['filter_total'])) {
			$sql .= " AND o.total = '" . (float)$data['filter_total'] . "'";
		}

		$sort_data = array(
			'o.order_id',
			'customer',
			'status',
			'o.date_added',
			'o.date_modified',
			'o.total',
            'o.shipping_cost',
            'o.shipping_country_id',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY o.order_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getOrderProducts($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

		return $query->rows;
	}

	public function getOrderOptions($order_id, $order_product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product_id . "'");

		return $query->rows;
	}

	public function getOrderTotals($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order");

		return $query->rows;
	}

	public function getOrderDownloads($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_download WHERE order_id = '" . (int)$order_id . "' ORDER BY name");

		return $query->rows;
	}

    public function getOrderProvidersCount() {
        $query = $this->db->query("SELECT shipping_provider, COUNT(*) count FROM `" . DB_PREFIX . "order` GROUP BY shipping_provider");
        $res = array();
        foreach($query->rows as $row) {
            $res[$row['shipping_provider']] = $row['count'];
        }

        return$res;
    }

	public function getTotalOrders($data = array()) {
      	$sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order`";

		if (isset($data['filter_order_status_id']) && !is_null($data['filter_order_status_id'])) {
			$sql .= " WHERE order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
		} else {
			$sql .= " WHERE order_status_id > '0'";
		}

		if (!empty($data['filter_order_id'])) {
			$sql .= " AND order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$sql .= " AND CONCAT(firstname, ' ', lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if (!empty($data['filter_total'])) {
			$sql .= " AND total = '" . (float)$data['filter_total'] . "'";
		}

        if (!empty($data['filter_shipping_provider'])) {
            $sql .= " AND shipping_provider = '". $this->db->escape($data['filter_shipping_provider']) ."'";
        }

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalOrdersByStoreId($store_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE store_id = '" . (int)$store_id . "'");

		return $query->row['total'];
	}

	public function getTotalOrdersByOrderStatusId($order_status_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE order_status_id = '" . (int)$order_status_id . "' AND order_status_id > '0'");

		return $query->row['total'];
	}

	public function getTotalOrdersByLanguageId($language_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE language_id = '" . (int)$language_id . "' AND order_status_id > '0'");

		return $query->row['total'];
	}

	public function getTotalOrdersByCurrencyId($currency_id) {
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE currency_id = '" . (int)$currency_id . "' AND order_status_id > '0'");

		return $query->row['total'];
	}

	public function getTotalSales() {
      	$query = $this->db->query("SELECT SUM(total) AS total FROM `" . DB_PREFIX . "order` WHERE order_status_id > '0'");

		return $query->row['total'];
	}

	public function getTotalSalesByYear($year) {
      	$query = $this->db->query("SELECT SUM(total) AS total FROM `" . DB_PREFIX . "order` WHERE order_status_id > '0' AND YEAR(date_added) = '" . (int)$year . "'");

		return $query->row['total'];
	}

	public function createInvoiceNo($order_id, $track='') {
		$order_info = $this->getOrder($order_id);

		if ($order_info) {
                        if ($order_info['invoice_no']) {
                            return $order_info['invoice_no'];
                        }
                    
			$invoice_no = $order_id;
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET invoice_no = '" . (int)$invoice_no . "', invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "', track = '" . $this->db->escape(strip_tags($track)) . "' WHERE order_id = '" . (int)$order_id . "'");

			return $order_info['invoice_prefix'] . $invoice_no;
		}
	}

	public function addOrderHistory($order_id, $data) {
		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$data['order_status_id'] . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");

		$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$data['order_status_id'] . "', notify = '" . (isset($data['notify']) ? (int)$data['notify'] : 0) . "', comment = '" . $this->db->escape(strip_tags($data['comment'])) . "', date_added = NOW()");

		$order_info = $this->getOrder($order_id);

		// Send out any gift voucher mails
		if ($this->config->get('config_complete_status_id') == $data['order_status_id']) {
			$this->load->model('sale/voucher');

			$results = $this->model_sale_voucher->getVouchersByOrderId($order_id);

			foreach ($results as $result) {
				$this->model_sale_voucher->sendVoucher($result['voucher_id']);
			}
		}

      	if ($data['notify']) {
			$language = new Language($order_info['language_directory']);
			$language->load($order_info['language_filename']);
			$language->load('mail/order');

			$subject = sprintf($language->get('text_subject'), $order_info['store_name'], $order_id);

			$message  = $language->get('text_order') . ' ' . $order_id . "\n";
			$message .= $language->get('text_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n\n";

			$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$data['order_status_id'] . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

			if ($order_status_query->num_rows) {
				$message .= $language->get('text_order_status') . "\n";
				$message .= $order_status_query->row['name'] . "\n\n";
			}

			if ($order_info['customer_id']) {
				$message .= $language->get('text_link') . "\n";
				$message .= html_entity_decode($order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id, ENT_QUOTES, 'UTF-8') . "\n\n";
			}

			if ($data['tracking']) {
				$message .= $language->get('text_tracking') . "\n\n";
				$message .= strip_tags(html_entity_decode($data['tracking'], ENT_QUOTES, 'UTF-8')) . "\n\n";
			}

			if ($data['comment']) {
				$message .= $language->get('text_comment') . "\n\n";
				$message .= strip_tags(html_entity_decode($data['comment'], ENT_QUOTES, 'UTF-8')) . "\n\n";
			}

			$message .= $language->get('text_footer');

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');
			$mail->setTo($order_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($order_info['store_name']);
			$mail->setSubject($subject);
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();
		}
	}

	public function getOrderHistories($order_id, $start = 0, $limit = 10) {
		$query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added ASC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getTotalOrderHistories($order_id) {
	  	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_id = '" . (int)$order_id . "'");

		return $query->row['total'];
	}

	public function getTotalOrderHistoriesByOrderStatusId($order_status_id) {
	  	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_status_id = '" . (int)$order_status_id . "'");

		return $query->row['total'];
	}

    public function export(array $data, array $order_ids = array(), $factor = 1, $base_file_name = 'singpost_orders') {
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $filename = $base_file_name .'-' .date('Hi-d-m-y'). '.xls';
        if (!isset($data['order_status_id'])) {
            $data['order_status_id >'] = 0;
        }

        list($languageId, $workbook, $boxFormat, $textFormat, $columnHeaderFormat) = $this->initExport($filename);

        // Creating the categories worksheet
        $worksheet =& $workbook->addWorksheet('Sheet1');
        $worksheet->setInputEncoding ( 'UTF-8' );

        $sql = 'SELECT `order`.*, UPPER(country.name) country_name, "No" as insured, 0 as insured_value, ROUND(`order`.total * '.(float) $factor.', 2) as total, IFNULL(shipping_option, "Gift") as standards_opt, "USD  United States Dollar" as currency_code, ROUND(SUM(product.weight * order_product.quantity)) weight, `order`.shipping_zone as zone FROM `order` INNER JOIN country ON country.country_id = `order`.shipping_country_id INNER JOIN order_product USING (order_id) INNER JOIN product USING (product_id) WHERE 1';
        if (!empty($order_ids)) {
            $ids = '';
            foreach($order_ids as $order_id) {
                $ids .= (int) $order_id . ',';
            }
            $sql .= ' AND order_id IN (' .trim($ids, ','). ')';
        }
        foreach($data as $field => $value) {
            if (!in_array(substr($field, -1), array('<', '>', '='))) {
                $field = "$field =";
            }
            $sql .= sprintf(' AND %s "%s"', $field, $this->db->escape($value));
        }
        $sql .= ' GROUP BY order_id';
        $columns = array_merge($this->commonOrderColumns, array(
            'To be selected for Registered and Standards (CN22)' => 'standards_opt',
        ));
        $this->populateOrdersWorksheet( $sql, $columns, $worksheet, $languageId, $boxFormat, $textFormat, $columnHeaderFormat );

        // Creating the products worksheet
        $worksheet =& $workbook->addWorksheet('Country reference');
        $worksheet->setInputEncoding ( 'UTF-8' );
        $this->populateCountriesWorksheet( $worksheet, $boxFormat, $textFormat );

        // Let's send the file
        $workbook->close();

        // Clear the spreadsheet caches
        $this->clearSpreadsheetCache();
        exit;
    }

    public function exportPriority(array $data, array $order_ids = array(), $factor = 1, $base_file_name = 'singpost_priority_orders') {
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $filename = $base_file_name . '-'.date('Hi-d-m-y').'.xls';
        if (!isset($data['order_status_id'])) {
            $data['order_status_id >'] = 0;
        }

        list($languageId, $workbook, $boxFormat, $textFormat, $columnHeaderFormat) = $this->initExport($filename);

        // Creating the categories worksheet
        $worksheet =& $workbook->addWorksheet('Sheet1');
        $worksheet->setInputEncoding ( 'UTF-8' );
        $sql = 'SELECT `order`.*, UPPER(country.name) country_name, "No" as insured, 0 as insured_value, ROUND(`order`.total * '.(float) $factor.', 2) as total, IFNULL(shipping_option, "Sample") as priority_opt, "Return to Sender" as undelivery_opt, "USD  United States Dollar" as currency_code, ROUND(SUM(product.weight * order_product.quantity)) weight, `order`.shipping_zone zone FROM `order` INNER JOIN country ON country.country_id = `order`.shipping_country_id INNER JOIN order_product USING (order_id) INNER JOIN product USING (product_id) WHERE 1';
        if (!empty($order_ids)) {
            $ids = '';
            foreach($order_ids as $order_id) {
                $ids .= (int) $order_id . ',';
            }
            $sql .= ' AND order_id IN (' .trim($ids, ','). ')';
        }
        foreach($data as $field => $value) {
            if (!in_array(substr($field, -1), array('<', '>', '='))) {
                $field = "$field =";
            }
            $sql .= sprintf(' AND %s "%s"', $field, $this->db->escape($value));
        }
        $sql .= ' GROUP BY order_id';
        $columns = array_merge($this->commonOrderColumns, array(
            'To be selected for Priority and Registered Plus (Undelivery)' => 'undelivery_opt',
            'To be selected for Priority and Registered Plus (Custom)' => 'priority_opt',
        ));
        $this->populateOrdersWorksheet( $sql, $columns, $worksheet, $languageId, $boxFormat, $textFormat, $columnHeaderFormat );

        // Creating the products worksheet
        $worksheet =& $workbook->addWorksheet('Country reference');
        $worksheet->setInputEncoding ( 'UTF-8' );
        $this->populateCountriesWorksheet( $worksheet, $boxFormat, $textFormat );

        // Let's send the file
        $workbook->close();

        // Clear the spreadsheet caches
        $this->clearSpreadsheetCache();
        exit;
    }
    
    
    public function exportDHL(array $data, array $order_ids = array(), $factor = 1, $base_file_name = 'dhl_priority_orders') {
        date_default_timezone_set('Asia/Kuala_Lumpur');
        $filename = $base_file_name . '-'.date('Hi-d-m-y').'.xls';
        if (!isset($data['order_status_id'])) {
            $data['order_status_id >'] = 0;
        }

        list($languageId, $workbook, $boxFormat, $textFormat, $columnHeaderFormat) = $this->initExport($filename);

        // Creating the orders worksheet
        $worksheet =& $workbook->addWorksheet('Sheet1');
        $worksheet->setInputEncoding ( 'UTF-8' );
        $sql = 'SELECT `order`.*, UPPER(country.name) country_name, ROUND(`order`.total * '.(float) $factor.', 2) as total, "USD" as currency_code, ROUND(SUM(product.weight * order_product.quantity)) weight, `order`.shipping_zone zone, 
            CONCAT(shipping_firstname, " ", shipping_lastname) AS customer_name, 
            CONCAT(shipping_city, ", ", shipping_zone) AS city_state, 
            country.iso_code_2 AS destination,
            "MY" AS origin, 
            IFNULL(shipping_option, "Gift") AS contents, 
            "LED Lights" as content_desc_1
            FROM `order` INNER JOIN country ON country.country_id = `order`.shipping_country_id INNER JOIN order_product USING (order_id) INNER JOIN product USING (product_id) WHERE 1';
        if (!empty($order_ids)) {
            $ids = '';
            foreach($order_ids as $order_id) {
                $ids .= (int) $order_id . ',';
            }
            $sql .= ' AND order_id IN (' . trim($ids, ',') . ')';
        }
        foreach($data as $field => $value) {
            if (!in_array(substr($field, -1), array('<', '>', '='))) {
                $field = "$field =";
            }
            $sql .= sprintf(' AND %s "%s"', $field, $this->db->escape($value));
        }
        $sql .= ' GROUP BY order_id';
        //print $sql;
        $columns = $this->dhlOrderColumns;
        
        $this->populateOrdersWorksheet( $sql, $columns, $worksheet, $languageId, $boxFormat, $textFormat, $columnHeaderFormat );

        // Let's send the file
        $workbook->close();

        // Clear the spreadsheet caches
        $this->clearSpreadsheetCache();
        exit;
    }
    

    public function initExport($filename)
    {
        global $config;
        global $log;
        $config = $this->config;
        $log = $this->log;
        set_error_handler('error_handler_for_export', E_ALL ^ E_NOTICE);
        register_shutdown_function('fatal_error_shutdown_handler_for_export');
        $languageId = $this->getDefaultLanguageId();

        // We use the package from http://pear.php.net/package/Spreadsheet_Excel_Writer/
        chdir('../system/pear');
        require_once "Spreadsheet/Excel/Writer.php";
        chdir('../../admin');

        // Creating a workbook
        $workbook = new Spreadsheet_Excel_Writer();
        $workbook->setTempDir(DIR_CACHE);
        $workbook->setVersion(8); // Use Excel97/2000 BIFF8 Format
        $columnHeaderFormat = & $workbook->addFormat(array('Size' => 10, 'vAlign' => 'vequal_space'));
        $columnHeaderFormat->setBold();
        $boxFormat =& $workbook->addFormat(array('Size' => 10, 'vAlign' => 'vequal_space'));
        $textFormat =& $workbook->addFormat(array('Size' => 10, 'NumFormat' => "@"));

        // sending HTTP headers
        $workbook->send($filename);
        return array($languageId, $workbook, $boxFormat, $textFormat, $columnHeaderFormat);
    }

    private function populateOrdersWorksheet($sql, array $columns, & $worksheet, &$languageId, &$boxFormat, &$textFormat, & $columnHeaderFormat) {
        $res = $this->db->query($sql);
        $i = 0;
        $j = 0;
        foreach($columns as $column_name => $value) {
            $worksheet->setColumn($i, $j, strlen($column_name) + 1);
            $worksheet->writeString($i, $j, $column_name, $columnHeaderFormat);
            $j++;
        }
        foreach ($res->rows as $row) {
            $i++;
            $j = 0;
            $row['hs_code'] = $this->getMostHSCode($row['order_id']);
            $row['auto_increment'] = $i;
            $row['shipping_description'] = $this->getMostItemDescription($row['order_id'], $languageId);
            foreach($columns as $row_key) {
                if (!empty($row_key) && isset($row[$row_key])) {
                    $worksheet->writeString($i, $j++, $row[$row_key]);
                } else {
                    $j++;
                }
            }
        }
    }

    private function populateCountriesWorksheet(& $worksheet, &$boxFormat, &$textFormat) {
        $this->load->model('localisation/country');
        $countries = $this->model_localisation_country->getCountries();
        $j = 0;
        $worksheet->setColumn($j, $j++, max('Code', 5) + 1);
        $worksheet->setColumn($j, $j++, max('Description', 14) + 1);
        $i = 0;
        $j = 0;
        $worksheet->writeString($i, $j++, 'Code', $boxFormat);
        $worksheet->writeString($i, $j++, 'Description', $boxFormat);
        foreach ($countries as $country) {
            $i++;
            $j = 0;
            $worksheet->writeString($i, $j++, $country['iso_code_2'], $textFormat);
            $worksheet->writeString($i, $j++, strtoupper($country['name']), $textFormat);
        }
    }

    private  function clearSpreadsheetCache() {
        $files = glob(DIR_CACHE . 'Spreadsheet_Excel_Writer' . '*');

        if ($files) {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    @unlink($file);
                    clearstatcache();
                }
            }
        }
    }

    private function getDefaultLanguageId() {
        $code = $this->config->get('config_language');
        $sql = "SELECT language_id FROM `".DB_PREFIX."language` WHERE code = '$code'";
        $result = $this->db->query( $sql );
        $languageId = 1;
        if ($result->rows) {
            foreach ($result->rows as $row) {
                $languageId = $row['language_id'];
                break;
            }
        }
        return $languageId;
    }

    private function getMostHSCode($order_id) {
        $sql = "SELECT hs_code, SUM(weight) weight
        FROM  (
          SELECT hs_code, COUNT(*) * order_product.quantity weight
          FROM product
          INNER JOIN order_product USING (product_id)
          WHERE order_id = ".(int)$order_id."
          GROUP BY product_id
        ) der
        GROUP BY hs_code
        ORDER BY weight DESC
        LIMIT 1";
        $res = $this->db->query($sql)->row;
        return $res['hs_code'];
    }

    private function getMostItemDescription($order_id, $language_id) {
        $sql = "SELECT shipping_description, SUM(weight) weight
        FROM  (
          SELECT shipping_description, COUNT(*) * quantity weight
          FROM product_description pd
          INNER JOIN order_product USING (product_id)
          WHERE
            order_id = ".(int)$order_id."
            AND pd.language_id = ".(int)$language_id."
          GROUP BY product_id
        ) der
        GROUP BY shipping_description
        ORDER BY weight DESC
        LIMIT 1";
        $res = $this->db->query($sql)->row;
        return $res['shipping_description'];
    }

    public function updateParcel($order_id, $parcel_type) {
        return $this->db->query("UPDATE `" .DB_PREFIX. "order` SET shipping_option = '" . $this->db->escape($parcel_type) . "' WHERE order_id = " . (int) $order_id);
    }

    public function getOrderProductsGrouped(array $order_ids = array(), $language_id = null) {
        if ($language_id == null) {
            $language_id = $this->getDefaultLanguageId();
        }
        $sql = "SELECT product_id, product_description.name, product.model, SUM(order_product.quantity) quantity FROM `order_product`
        INNER JOIN product USING (product_id)
        INNER JOIN product_description USING (product_id)
        WHERE product_description.language_id = " .(int) $language_id;

        if (!empty($order_ids)) {
            $sql .= ' AND order_id IN (';
            foreach ($order_ids as $order_id) {
                $sql .= (int) $order_id . ',';
            }
            $sql = rtrim($sql, ',') . ')';
        }

        $sql .= " GROUP BY product_id";

        return $this->db->query($sql)->rows;
    }
}
?>
