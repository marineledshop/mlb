<?php
class ModelReportMtaAffiliateTT extends Model {
	public function getCommissionTT($data = array()) { 
		$sql_e = "SELECT at.affiliate_id, SUM(at.amount) AS total_earnings FROM " . DB_PREFIX . "affiliate_transaction at";
		$sql = "SELECT at.affiliate_id, CONCAT(a.firstname, ' ', a.lastname) AS affiliate, a.email, a.status, SUM(at.amount) AS commission, COUNT(o.order_id) AS orders, SUM(o.total) AS total FROM " . DB_PREFIX . "affiliate_transaction at LEFT JOIN `" . DB_PREFIX . "affiliate` a ON (at.affiliate_id = a.affiliate_id) LEFT JOIN `" . DB_PREFIX . "order` o ON (at.order_id = o.order_id)";
		
		$implode = array();
		
		if (preg_match("/^\d{4}\-\d{2}\-\d{2}$/", $data['filter_date_start'])) {
			$implode[] = "DATE(at.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (preg_match("/^\d{4}\-\d{2}\-\d{2}$/", $data['filter_date_end'])) {
			$implode[] = "DATE(at.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}	
		
		if ($implode) {
			$_imp = " WHERE " . implode(" AND ", $implode);
			$sql .= $_imp;
			$sql_e .= $_imp . " AND at.amount > 0";
		} else {
			$sql_e .= " WHERE at.amount > 0";
		}
				
		$sql .= " GROUP BY at.affiliate_id ORDER BY commission DESC";
		$sql_e .= " AND at.affiliate_id in ([[AFFILIATES]]) GROUP BY at.affiliate_id";
				
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql_e .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
			
		$query = $this->db->query($sql);
		$rows = $query->rows;
		$_aff_ids = array();		
		foreach($rows as $_i => $_r) {
			if($data['filter_balance'] > 0 && $_r['commission'] < $data['filter_balance']) {
				unset($rows[$_i]);
				continue;
			}
			$_aff_ids[] = $_r['affiliate_id'];			
		}
		if(sizeof($_aff_ids) < 1) return 0;
		$rows = array_values($rows);
		$sql_e = str_replace('[[AFFILIATES]]', implode(',', $_aff_ids), $sql_e);
		
		$query_e = $this->db->query($sql_e);
		$ret = 0;
		foreach($query_e->rows as $_r) {
			$ret += floatval($_r['total_earnings']);
		}
		return $ret;
	}

	
	public function getProductsTT($data = array()) { 
		$sql = "SELECT SUM(total) as tt FROM (SELECT SUM(o.total) AS total FROM " . DB_PREFIX . "affiliate_transaction at LEFT JOIN `" . DB_PREFIX . "affiliate` a ON (at.affiliate_id = a.affiliate_id) LEFT JOIN `" . DB_PREFIX . "order` o ON (at.order_id = o.order_id) LEFT JOIN " . DB_PREFIX . "product";
		
		$implode = array();
		
		if (!empty($data['filter_date_start'])) {
			$implode[] = "DATE(at.date_added) >= '" . $this->db->escape($data['filter_date_start']) . "'";
		}

		if (!empty($data['filter_date_end'])) {
			$implode[] = "DATE(at.date_added) <= '" . $this->db->escape($data['filter_date_end']) . "'";
		}

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}
				
		$sql .= " GROUP BY at.affiliate_id";
				
		$sql .= ") as _tmp";
		
		$query = $this->db->query($sql);
		
		return $query->row['tt'];
	}
}

