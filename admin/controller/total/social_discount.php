<?php
class ControllerTotalSocialDiscount extends Controller {
	private $error = array(); 
	 
	public function index() { 
		$this->load->language('total/social_discount');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->model_setting_setting->editSetting('social_discount', $this->request->post);
		
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->redirect($this->url->link('extension/total', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		
		$this->data['text_yes'] = $this->language->get('text_yes');
    	$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');
		
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_multiple'] = $this->language->get('entry_multiple');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$this->data['entry_google_plus'] = $this->language->get('entry_google_plus');
		$this->data['entry_facebook'] = $this->language->get('entry_facebook');
		$this->data['entry_twitter'] = $this->language->get('entry_twitter');
					
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

   		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_total'),
			'href'      => $this->url->link('extension/total', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('total/social_discount', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('total/social_discount', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/total', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['social_discount_status'])) {
			$this->data['social_discount_status'] = $this->request->post['social_discount_status'];
		} else {
			$this->data['social_discount_status'] = $this->config->get('social_discount_status');
		}
		
		if (isset($this->request->post['social_discount_multiple'])) {
			$this->data['social_discount_multiple'] = $this->request->post['social_discount_multiple'];
		} else {
			$this->data['social_discount_multiple'] = $this->config->get('social_discount_multiple');
		}
		
		if (isset($this->request->post['social_discount_sort_order'])) {
			$this->data['social_discount_sort_order'] = $this->request->post['social_discount_sort_order'];
		} else {
			$this->data['social_discount_sort_order'] = $this->config->get('social_discount_sort_order');
		} 
		
		if (isset($this->request->post['google_plus_page'])) {
			$this->data['google_plus_page'] = $this->request->post['google_plus_page'];
		} else {
			$this->data['google_plus_page'] = $this->config->get('google_plus_page');;
		}	
		
		if (isset($this->request->post['facebook_page'])) {
			$this->data['facebook_page'] = $this->request->post['facebook_page'];
		} else {
			$this->data['facebook_page'] = $this->config->get('facebook_page');;
		}	
		
		if (isset($this->request->post['twitter_page'])) {
			$this->data['twitter_page'] = $this->request->post['twitter_page'];
		} else {
			$this->data['twitter_page'] = $this->config->get('twitter_page');;
		}			

		$this->template = 'total/social_discount.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'total/social_discount')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>