<?php

/*
 * Copyright (c) 2012 Web Project Solutions LLC (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 *  @author Antonello Venturino <info@webprojectsol.com>
 *  @copyright  2012 Web Project Solutions LLC
 *  @license    http://www.webprojectsol.com/license.php
 *  @url  http://www.webprojectsol.com/moduli-di-pagamento/paypal-express-checkout.html
 */

class ControllerModulePaypalExpressModule extends Controller {

    private $error = array();

    public function index() {
        if (VERSION == '1.5.5') {
            $this->data = array_merge($this->data, $this->language->load('module/paypal_express_module'));
        } else {
            $this->data = array_merge($this->data, $this->load->language('module/paypal_express_module'));
        }

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('paypal_express_module', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_home'),
          'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
          'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_module'),
          'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
          'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
          'text' => $this->language->get('heading_title'),
          'href' => $this->url->link('module/paypal_express_module', 'token=' . $this->session->data['token'], 'SSL'),
          'separator' => ' :: '
        );

        $this->data['action'] = $this->url->link('module/paypal_express_module', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['modules'] = array();

        if (version_compare(VERSION, '1.5.1', '>=')) {
            if (isset($this->request->post['paypal_express_module_module'])) {
                $this->data['modules'] = $this->request->post['paypal_express_module_module'];
            } elseif ($this->config->get('paypal_express_module_module')) {
                $this->data['modules'] = $this->config->get('paypal_express_module_module');
            }
        } else {
            if (isset($this->request->post['paypal_express_module_module'])) {
                $this->data['modules'] = explode(',', $this->request->post['paypal_express_module_module']);
            } elseif ($this->config->get('paypal_express_module_module') != '') {
                $this->data['modules'] = explode(',', $this->config->get('paypal_express_module_module'));
            }
        }

        $this->load->model('design/layout');

        $this->data['layouts'] = $this->model_design_layout->getLayouts();

        if (version_compare(VERSION, '1.5.1', '<')) {
            foreach ($this->data['modules'] as $module) {
                if (isset($this->request->post['paypal_express_module_' . $module . '_layout_id'])) {
                    $this->data['paypal_express_module_' . $module . '_layout_id'] = $this->request->post['paypal_express_module_' . $module . '_layout_id'];
                } else {
                    $this->data['paypal_express_module_' . $module . '_layout_id'] = $this->config->get('paypal_express_module_' . $module . '_layout_id');
                }

                if (isset($this->request->post['paypal_express_module_' . $module . '_position'])) {
                    $this->data['paypal_express_module_' . $module . '_position'] = $this->request->post['paypal_express_module_' . $module . '_position'];
                } else {
                    $this->data['paypal_express_module_' . $module . '_position'] = $this->config->get('paypal_express_module_' . $module . '_position');
                }

                if (isset($this->request->post['paypal_express_module_' . $module . '_status'])) {
                    $this->data['paypal_express_module_' . $module . '_status'] = $this->request->post['paypal_express_module_' . $module . '_status'];
                } else {
                    $this->data['paypal_express_module_' . $module . '_status'] = $this->config->get('paypal_express_module_' . $module . '_status');
                }

                if (isset($this->request->post['paypal_express_module_' . $module . '_sort_order'])) {
                    $this->data['paypal_express_module_' . $module . '_sort_order'] = $this->request->post['paypal_express_module_' . $module . '_sort_order'];
                } else {
                    $this->data['paypal_express_module_' . $module . '_sort_order'] = $this->config->get('paypal_express_module_' . $module . '_sort_order');
                }
            }

            if (isset($this->request->post['paypal_express_module_module'])) {
                $this->data['paypal_express_module_module'] = $this->request->post['paypal_express_module_module'];
            } else {
                $this->data['paypal_express_module_module'] = $this->config->get('paypal_express_module_module');
            }
        }
        $this->template = 'module/paypal_express_module.tpl';
        $this->children = array(
          'common/header',
          'common/footer',
        );

        $this->response->setOutput($this->render());
    }

    private function validate() {
        if (!$this->user->hasPermission('modify', 'module/paypal_express_module')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}

?>