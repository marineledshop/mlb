<?php
// -------------------------------------
// Product Tabs or Modules for OpenCart
// By Best-Byte
// www.best-byte.com
// -------------------------------------

class ControllerModuleProductTabs extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('module/product_tabs');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		
			$this->model_setting_setting->editSetting('product_tabs', $this->request->post);		
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_limit'] = $this->language->get('entry_limit');
		$this->data['entry_tab'] = $this->language->get('entry_tab');
		$this->data['entry_moduleinfo'] = $this->language->get('entry_moduleinfo');		

    $this->data['tab_number'] = $this->language->get('tab_number');	
    $this->data['number'] = $this->language->get('number');		    	
		$this->data['add_tab'] = $this->language->get('add_tab');
		$this->data['tab_title'] = $this->language->get('tab_title');
		$this->data['choose_products'] = $this->language->get('choose_products');
		$this->data['special_products'] = $this->language->get('special_products');
		$this->data['latest_products'] = $this->language->get('latest_products');
		$this->data['popular_products'] = $this->language->get('popular_products');    		
		$this->data['bestseller_products'] = $this->language->get('bestseller_products');
		$this->data['choose_category'] = $this->language->get('choose_category');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
		$this->data['button_add_image'] = $this->language->get('button_add_image');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['category'])) {
			$this->data['error_category'] = $this->error['category'];
		} else {
			$this->data['error_category'] = array();
		}
		if (isset($this->error['numproduct'])) {
			$this->data['error_numproduct'] = $this->error['numproduct'];
		} else {
			$this->data['error_numproduct'] = array();
		}
		if (isset($this->error['image'])) {
			$this->data['error_image'] = $this->error['image'];
		} else {
			$this->data['error_image'] = array();
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/product_tabs', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/product_tabs', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];
		
		$this->data['tabs'] = array();
		
		if (isset($this->request->post['product_tabs_tab'])) {
			$this->data['tabs'] = $this->request->post['product_tabs_tab'];
		} elseif ($this->config->get('product_tabs_tab')) { 
			$this->data['tabs'] = $this->config->get('product_tabs_tab');
		}	
	
		$this->load->model('catalog/category');
		$this->data['categories'] = $this->model_catalog_category->getCategories(0);
				
		$this->data['modules'] = array();
		
		if (isset($this->request->post['product_tabs_module'])) {
			$this->data['modules'] = $this->request->post['product_tabs_module'];
		} elseif ($this->config->get('product_tabs_module')) { 
			$this->data['modules'] = $this->config->get('product_tabs_module');
		}					
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		
		$this->template = 'module/product_tabs.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);		
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/product_tabs')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (isset($this->request->post['product_tabs_tab'])) {
			foreach ($this->request->post['product_tabs_tab'] as $key => $value) {
				if ($value['filter_type'] == 'category' && !isset($value['filter_type_category'])) {
					$this->error['category'][$key] = $this->language->get('error_category');
				}
			}
		}
		if (isset($this->request->post['product_tabs_module'])) {
			foreach ($this->request->post['product_tabs_module'] as $key => $value) {
				if (!$value['limit']) {
					$this->error['numproduct'][$key] = $this->language->get('error_numproduct');
				}
				if (!$value['image_width'] || !$value['image_height']) {
					$this->error['image'][$key] = $this->language->get('error_image');
				}
			}
		}
				
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
 }
?>