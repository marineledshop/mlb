<?php
class ControllerModuleRSSFeed extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('module/rssfeed');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->model_setting_setting->editSetting('rssfeed', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect(HTTPS_SERVER . 'index.php?route=extension/module&token=' . $this->session->data['token']);
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_left'] = $this->language->get('text_left');
		$this->data['text_right'] = $this->language->get('text_right');
		
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
 		if (isset($this->error['code'])) {
			$this->data['error_code'] = $this->error['code'];
		} else {
			$this->data['error_code'] = '';
		}
		
  		$this->document->breadcrumbs = array();

   		$this->document->breadcrumbs[] = array(
       		'href'      => HTTPS_SERVER . 'index.php?route=common/home&token=' . $this->session->data['token'],
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

   		$this->document->breadcrumbs[] = array(
       		'href'      => HTTPS_SERVER . 'index.php?route=extension/module&token=' . $this->session->data['token'],
       		'text'      => $this->language->get('text_module'),
      		'separator' => ' :: '
   		);
		
   		$this->document->breadcrumbs[] = array(
       		'href'      => HTTPS_SERVER . 'index.php?route=module/rssfeed&token=' . $this->session->data['token'],
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = HTTPS_SERVER . 'index.php?route=module/rssfeed&token=' . $this->session->data['token'];
		
		$this->data['cancel'] = HTTPS_SERVER . 'index.php?route=extension/module&token=' . $this->session->data['token'];

		if (isset($this->request->post['rssfeed_code'])) {
			$this->data['rssfeed_code'] = $this->request->post['rssfeed_code'];
		} else {
			$this->data['rssfeed_code'] = $this->config->get('rssfeed_code');
		}	
		
		if (isset($this->request->post['rssfeed_position'])) {
			$this->data['rssfeed_position'] = $this->request->post['rssfeed_position'];
		} else {
			$this->data['rssfeed_position'] = $this->config->get('rssfeed_position');
		}
		
		if (isset($this->request->post['rssfeed_status'])) {
			$this->data['rssfeed_status'] = $this->request->post['rssfeed_status'];
		} else {
			$this->data['rssfeed_status'] = $this->config->get('rssfeed_status');
		}
		
		if (isset($this->request->post['rssfeed_sort_order'])) {
			$this->data['rssfeed_sort_order'] = $this->request->post['rssfeed_sort_order'];
		} else {
			$this->data['rssfeed_sort_order'] = $this->config->get('rssfeed_sort_order');
		}

		$this->data['data_feed'] = HTTP_CATALOG . 'index.php?route=feed/google_base';				
		
		$this->template = 'module/rssfeed.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/rssfeed')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->request->post['rssfeed_code']) {
			$this->error['code'] = $this->language->get('error_code');
		}
		
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}	
	}
}
?>