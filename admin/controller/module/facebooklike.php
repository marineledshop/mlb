<?php
/**
 * Facebook OpenCart Module. Creates a Facebook like box.
 *
 * @author 		www.opencartstore.com
 * @support		www.opencartstore.com/support/
 * @version		1.5.1.*
 */

class ControllerModuleFacebooklike extends Controller {
	private $error = array(); 
	
	/**
	 * Set some default values for the module.
	 *
	 * @access public
	 * @return void
	 */
	public function install () {
		$this->load->model('setting/setting');
		
		$defaults = array(
			'facebooklike_connections' 	=> '9'
		);
		
		$this->model_setting_setting->editSetting('facebooklike', $defaults);
	}

	/**
	 * Called when the module is uninstalled.
	 *
	 * @access public
	 * @return void
	 */
	public function uninstall () {
		
	}

	public function index() {   
		$this->load->language('module/facebooklike');
		$this->load->model('setting/setting');
		
		$this->document->setTitle($this->language->get('heading_title'));
						
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->_validate())) {
			$config = array();

			$config['facebooklike_profile_id'] 		= $this->request->post['facebooklike_profile_id'];
			$config['facebooklike_height'] 			= $this->request->post['facebooklike_height'];
			$config['facebooklike_width'] 			= $this->request->post['facebooklike_width'];
			$config['facebooklike_stream'] 			= $this->request->post['facebooklike_stream'];
			$config['facebooklike_connections'] 	= $this->request->post['facebooklike_connections'];
			$config['facebooklike_header'] 			= $this->request->post['facebooklike_header'];

			if (isset($this->request->post['facebooklike_module'])) {
				$config['facebooklike_module'] = $this->request->post['facebooklike_module'];
			}

			$this->model_setting_setting->editSetting('facebooklike', $config);
				
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_settings'] 				= $this->language->get('text_settings');
		$this->data['text_enabled'] 				= $this->language->get('text_enabled');
		$this->data['text_disabled'] 				= $this->language->get('text_disabled');
		$this->data['text_content_top'] 			= $this->language->get('text_content_top');
		$this->data['text_content_bottom'] 			= $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] 			= $this->language->get('text_column_left');
		$this->data['text_column_right'] 			= $this->language->get('text_column_right');
		$this->data['text_version_status'] 			= $this->language->get('text_version_status');
		$this->data['text_version_number'] 			= $this->language->get('text_version_number');
		$this->data['text_author'] 					= $this->language->get('text_author');
		$this->data['text_facebooklike_support']	= $this->language->get('text_facebooklike_support');
		
		$this->data['entry_profile_id'] 			= $this->language->get('entry_profile_id');
		$this->data['entry_height'] 				= $this->language->get('entry_height');
		$this->data['entry_width'] 					= $this->language->get('entry_width');
		$this->data['entry_stream'] 				= $this->language->get('entry_stream');
		$this->data['entry_connections'] 			= $this->language->get('entry_connections');
		$this->data['entry_header'] 				= $this->language->get('entry_header');
		$this->data['entry_status'] 				= $this->language->get('entry_status');
		$this->data['entry_layout'] 				= $this->language->get('entry_layout');
		$this->data['entry_position'] 				= $this->language->get('entry_position');
		$this->data['entry_sort_order'] 			= $this->language->get('entry_sort_order');
		
		$this->data['button_save'] 					= $this->language->get('button_save');
		$this->data['button_cancel'] 				= $this->language->get('button_cancel');
		$this->data['button_add_module'] 			= $this->language->get('button_add_module');
		$this->data['button_remove'] 				= $this->language->get('button_remove');
		
        if (isset($this->error['warning'])) {
        	$this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/facebooklike', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/facebooklike', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];
			
		if (isset($this->request->post['facebooklike_profile_id'])) {
			$this->data['facebooklike_profile_id'] = $this->request->post['facebooklike_profile_id'];
		} else {
			$this->data['facebooklike_profile_id'] = $this->config->get('facebooklike_profile_id');
		}
		
		if (isset($this->request->post['facebooklike_height'])) {
			$this->data['facebooklike_height'] = $this->request->post['facebooklike_height'];
		} else {
			$this->data['facebooklike_height'] = $this->config->get('facebooklike_height');
		}

		if (isset($this->request->post['facebooklike_width'])) {
			$this->data['facebooklike_width'] = $this->request->post['facebooklike_width'];
		} else {
			$this->data['facebooklike_width'] = $this->config->get('facebooklike_width');
		}

		if (isset($this->request->post['facebooklike_stream'])) {
			$this->data['facebooklike_stream'] = $this->request->post['facebooklike_stream'];
		} else {
			$this->data['facebooklike_stream'] = $this->config->get('facebooklike_stream');
		}
		
		if (isset($this->request->post['facebooklike_header'])) {
			$this->data['facebooklike_header'] = $this->request->post['facebooklike_header'];
		} else {
			$this->data['facebooklike_header'] = $this->config->get('facebooklike_header');
		}

		if (isset($this->request->post['facebooklike_connections'])) {
			$this->data['facebooklike_connections'] = $this->request->post['facebooklike_connections'];
		} else {
			$this->data['facebooklike_connections'] = $this->config->get('facebooklike_connections');
		}
		
		$this->data['modules'] = array();
		
		if (isset($this->request->post['facebooklike_module'])) {
			$this->data['modules'] = $this->request->post['facebooklike_module'];
		} elseif ($this->config->get('facebooklike_module')) { 
			$this->data['modules'] = $this->config->get('facebooklike_module');
		}
		
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'module/facebooklike.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);
		
		$this->response->setOutput($this->render());
	}
	
	/**
	 * Check that required fields have values.
	 * 
	 * @access private
	 * @return boolean
	 */
	private function _validate () {
		if (!$this->user->hasPermission('modify', 'module/facebooklike')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ($this->request->post['facebooklike_profile_id'] == '') {
			$this->error['warning'] = "You must set the Facebook Profile Id.";
		}
		
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}	
	}
}
?>
