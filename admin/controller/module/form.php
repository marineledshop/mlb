<?php
//==============================================================================
// Flexible Form v154.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
//==============================================================================

class ControllerModuleForm extends Controller {
	private $error = array();
	private $type = 'module';
	private $name = 'form';
	private $settings = array(
		'data'
	);
	
	public function index() {
		$this->data['type'] = $this->type;
		$this->data['name'] = $this->name;
		
		$v14x = $this->data['v14x'] = (!defined('VERSION') || VERSION < 1.5);
		$v150 = $this->data['v150'] = (defined('VERSION') && strpos(VERSION, '1.5.0') === 0);
		$token = $this->data['token'] = (isset($this->session->data['token'])) ? $this->session->data['token'] : '';
		
		$this->loadLanguage();
		$this->load->model('setting/setting');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			if (isset($this->request->post[$this->name . '_data']) && strlen(serialize($this->request->post[$this->name . '_data'])) > 65535) {
				$setting_table = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "setting WHERE Field = 'value'");
				if (strtolower($setting_table->row['Type']) == 'text') {
					$this->db->query("ALTER TABLE " . DB_PREFIX . "setting MODIFY `value` MEDIUMTEXT NOT NULL");
				}
			}
			
			$postdata = $this->request->post;
			if ($v14x || $v150) {
				foreach ($postdata as $key => $value) {
					if (is_array($value)) $postdata[$key] = serialize($value);
				}
			}
			$this->model_setting_setting->editSetting($this->name, $postdata);
			
			$this->session->data['success'] = $this->data['text_success'];
			if (isset($this->request->get['exit'])) {
				$this->redirect($this->makeURL('extension/' . $this->type, 'token=' . $token, 'SSL'));
			} else {
				$this->redirect($this->makeURL($this->type . '/' . $this->name, 'token=' . $token, 'SSL'));
			}
		}
		
		$breadcrumbs = array();
		$breadcrumbs[] = array(
			'href'		=> $this->makeURL('common/home', 'token=' . $token, 'SSL'),
			'text'		=> $this->data['text_home'],
			'separator' => false
		);
		$breadcrumbs[] = array(
			'href'		=> $this->makeURL('extension/' . $this->type, 'token=' . $token, 'SSL'),
			'text'		=> $this->data['text_' . $this->type],
			'separator' => ' :: '
		);
		$breadcrumbs[] = array(
			'href'		=> $this->makeURL($this->type . '/' . $this->name, 'token=' . $token, 'SSL'),
			'text'		=> $this->data['heading_title'],
			'separator' => ' :: '
		);
		
		$this->data['error_warning'] = isset($this->error['warning']) ? $this->error['warning'] : '';
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->data['save_exit'] = $this->makeURL($this->type . '/' . $this->name, 'token=' . $token . '&exit=true', 'SSL');
		$this->data['save_keep_editing'] = $this->makeURL($this->type . '/' . $this->name, 'token=' . $token, 'SSL');
		$this->data['cancel'] = $this->makeURL('extension/' . $this->type, 'token=' . $token, 'SSL');
		
		$this->load->model('localisation/language');
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		
		foreach ($this->settings as $setting) {
			$key = $this->name . '_' . $setting;
			$this->data[$key] = isset($this->request->post[$key]) ? $this->request->post[$key] : $this->config->get($key);
			
			// non-standard
			if (!$this->data[$key]) $this->data[$key] = array();
			// end
			
			if (is_string($this->data[$key]) && strpos($this->data[$key], 'a:') === 0) {
				$this->data[$key] = unserialize($this->data[$key]);
			}
		}
		
		$this->template = $this->type . '/' . $this->name . '.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		
		if ($v14x) {
			$this->document->title = $this->data['heading_title'];
			$this->document->breadcrumbs = $breadcrumbs;
			$this->response->setOutput($this->render(true), $this->config->get('config_compression'));
		} else {
			$this->document->setTitle($this->data['heading_title']);
			$this->data['breadcrumbs'] = $breadcrumbs;
			$this->response->setOutput($this->render());
		}
	}
	
	private function loadLanguage() {
		if (!defined('VERSION') || strpos(VERSION, '1.4.7') === 0) {
			$language = $this->db->query("SELECT * FROM " . DB_PREFIX . "language WHERE language_id = '" . $this->config->get('config_language_id') . "'");
			$directory = (file_exists(DIR_LANGUAGE . $language->row['directory'] . '/' . $this->type . '/' . $this->name . '.php')) ? $language->row['directory'] : 'english';
			if (!file_exists(DIR_LANGUAGE . $directory . '/' . $this->type . '/' . $this->name . '.php')) {
				echo 'Error: Could not load language ' . $this->type . '/' . $this->name . '!';
				exit();
			}
			$_ = array();
			require(DIR_LANGUAGE . $language->row['directory'] . '/' . $language->row['filename'] . '.php');
			require(DIR_LANGUAGE . $directory . '/' . $this->type . '/' . $this->name . '.php');
			$this->data = array_merge($this->data, $_);
		} else {
			$this->data = array_merge($this->data, $this->load->language($this->type . '/' . $this->name));
		}
	}
	
	private function makeURL($route, $args = '', $connection = 'NONSSL') {
		if (!defined('VERSION') || VERSION < 1.5) {
			$url = ($connection == 'NONSSL') ? HTTP_SERVER : HTTPS_SERVER;
			$url .= 'index.php?route=' . $route;
			$url .= ($args) ? '&' . ltrim($args, '&') : ''; 
			return $url;
		} else {
			return $this->url->link($route, $args, $connection);
		}
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', $this->type . '/' . $this->name)) {
			$this->error['warning'] = $this->data['error_permission'];
		}
		return ($this->error) ? false : true;
	}
}
?>