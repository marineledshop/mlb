<?php
/**
 * ProSlider OpenCart Module
 * Create and position a slideshow using the Nivo Slider library.
 * 
 * @author 		Ian Gallacher
 * @version		1.5.1.*
 * @email		info@opencartstore.com
 * @support		www.opencartstore.com/support/
 */

class ControllerModuleProslider extends Controller {
	private $error = array(); 

	public function index() {   
		$this->load->language('module/proslider');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		$this->load->model('tool/image');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			
			if (isset($this->request->post['proslider_image'])) {
        		$proslider_images = serialize($this->request->post['proslider_image']);
        	} else {
        		$proslider_images = "";
        	}
        	
        	$effect = ($this->request->post['proslider_slide_effect'] != '') ? $this->request->post['proslider_slide_effect'] : 'random';

        	$proslider_configs = array(
        		'proslider_title_text' => $this->request->post['proslider_title_text'],
        		'proslider_slide_effect' => $effect,
        		'proslider_display_box' => $this->request->post['proslider_display_box'],
        		'proslider_show_navigation' => $this->request->post['proslider_show_navigation'],
        		'proslider_pause_onhover' => $this->request->post['proslider_pause_onhover'],
        		'proslider_slide_width' => (int)$this->request->post['proslider_slide_width'],
        		'proslider_slide_height' => (int)$this->request->post['proslider_slide_height'],
        		'proslider_animation_speed' => (int)$this->request->post['proslider_animation_speed'],
        		'proslider_animation_pause' => (int)$this->request->post['proslider_animation_pause'],
        		'proslider_slices' => (int)$this->request->post['proslider_slices'],
        		'proslider_images' => $proslider_images,
        	);

        	if (isset($this->request->post['proslider_module'])) {
        		$proslider_configs['proslider_module'] = $this->request->post['proslider_module'];
        	}
        
			$this->model_setting_setting->editSetting('proslider', $proslider_configs);		
						
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
 		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');			

		$this->data['entry_show_navigation'] = $this->language->get('entry_show_navigation');
		$this->data['entry_pause_onhover'] = $this->language->get('entry_pause_onhover');
		$this->data['entry_title_text'] = $this->language->get('entry_title_text');
		$this->data['entry_slide_effect'] = $this->language->get('entry_slide_effect');
		$this->data['entry_slide_width'] = $this->language->get('entry_slide_width');
		$this->data['entry_slide_height'] = $this->language->get('entry_slide_height');
		$this->data['entry_slices'] = $this->language->get('entry_slices');
		$this->data['entry_animation_speed'] = $this->language->get('entry_animation_speed');
		$this->data['entry_animation_pause'] = $this->language->get('entry_animation_pause');
		$this->data['entry_display_box'] = $this->language->get('entry_display_box');
		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_link'] = $this->language->get('entry_link');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_remove'] = $this->language->get('button_remove');
		$this->data['button_add_images'] = $this->language->get('button_add_images');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		
		$this->data['token'] = $this->session->data['token'];
		
		$this->data['available_effects'] = array(
			'sliceDown' 			=> 'Slice Down',
			'sliceDownLeft' 		=> 'Slice Down Left',
			'sliceUp' 				=> 'Slice Up',
			'sliceUpLeft' 			=> 'Slice Up Left',
			'sliceUpDown' 			=> 'Slice Up Down',
			'sliceUpDownLeft' 		=> 'Slice Up Down Left',
			'fold' 					=> 'Fold',
			'fade' 					=> 'Fade',
			'random' 				=> 'Random',
			'slideInRight' 			=> 'Slide In Right',
			'slideInLeft' 			=> 'Slide In Left',
			'boxRandom' 			=> 'Box Random',
			'boxRain' 				=> 'Box Rain',
			'boxRainReverse' 		=> 'Box Rain Reverse',
			'boxRainGrow' 			=> 'Box Rain Grow',
			'boxRainGrowReverse' 	=> 'Box Rain Grow Reverse'
		);

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/proslider', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('module/proslider', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['proslider_show_navigation'])) {
			$this->data['proslider_show_navigation'] = $this->request->post['proslider_show_navigation'];
		} else {
			$this->data['proslider_show_navigation'] = $this->config->get('proslider_show_navigation');
		}
		
		if (isset($this->request->post['proslider_pause_onhover'])) {
			$this->data['proslider_pause_onhover'] = $this->request->post['proslider_pause_onhover'];
		} else {
			$this->data['proslider_pause_onhover'] = $this->config->get('proslider_pause_onhover');
		}
		
		if (isset($this->request->post['proslider_title_text'])) {
			$this->data['proslider_title_text'] = $this->request->post['proslider_title_text'];
		} else {
			$this->data['proslider_title_text'] = $this->config->get('proslider_title_text');
		}
		
		if (isset($this->request->post['proslider_slide_effect'])) {
			$this->data['proslider_slide_effect'] = $this->request->post['proslider_slide_effect'];
		} else {
			$this->data['proslider_slide_effect'] = $this->config->get('proslider_slide_effect');
		}
		
		if (isset($this->request->post['proslider_slide_width'])) {
			$this->data['proslider_slide_width'] = $this->request->post['proslider_slide_width'];
		} else {
			$this->data['proslider_slide_width'] = $this->config->get('proslider_slide_width');
		}
		
		if (isset($this->request->post['proslider_slide_height'])) {
			$this->data['proslider_slide_height'] = $this->request->post['proslider_slide_height'];
		} else {
			$this->data['proslider_slide_height'] = $this->config->get('proslider_slide_height');
		}
		
		if (isset($this->request->post['proslider_slices'])) {
			$this->data['proslider_slices'] = $this->request->post['proslider_slices'];
		} else {
			$this->data['proslider_slices'] = $this->config->get('proslider_slices');
		}
		
		if (isset($this->request->post['proslider_animation_speed'])) {
			$this->data['proslider_animation_speed'] = $this->request->post['proslider_animation_speed'];
		} else {
			$this->data['proslider_animation_speed'] = $this->config->get('proslider_animation_speed');
		}
		
		if (isset($this->request->post['proslider_animation_pause'])) {
			$this->data['proslider_animation_pause'] = $this->request->post['proslider_animation_pause'];
		} else {
			$this->data['proslider_animation_pause'] = $this->config->get('proslider_animation_pause');
		}
		
		if (isset($this->request->post['proslider_display_box'])) {
			$this->data['proslider_display_box'] = $this->request->post['proslider_display_box'];
		} else {
			$this->data['proslider_display_box'] = $this->config->get('proslider_display_box');
		}
		
		$this->load->model('localisation/language');
		
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		
		$this->load->model('tool/image');
	
		if (isset($this->request->post['proslider_image'])) {
			$proslider_images = $this->request->post['proslider_image'];
		} else {
			$proslider_images = unserialize($this->config->get('proslider_images'));
		}
		
		$this->data['proslider_images'] = array();
		
		if (is_array($proslider_images)) {
			foreach ($proslider_images as $proslider_image) {
				if ($proslider_image['image'] && file_exists(DIR_IMAGE . $proslider_image['image'])) {
					$image = $proslider_image['image'];
				} else {
					$image = 'no_image.jpg';
				}			
			
				$this->data['proslider_images'][] = array(
					'proslider_image_description' 	=> $proslider_image['proslider_image_description'],
					'link'                     		=> $proslider_image['link'],
					'image'                    		=> $image,
					'thumb'                    		=> $this->model_tool_image->resize($image, 100, 100)
				);	
			} 
		}
	
		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);		
        
        $this->data['modules'] = array();
		
		if (isset($this->request->post['proslider_module'])) {
			$this->data['modules'] = $this->request->post['proslider_module'];
		} elseif ($this->config->get('proslider_module')) { 
			$this->data['modules'] = $this->config->get('proslider_module');
		}

        $this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'module/proslider.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render());
	}
	
	/*
	 * Validation function to check that required settings have been input
	 * before saving.
	 * 
	 * @access public
	 * @return boolean
	 */
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/proslider')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if ($this->request->post['proslider_slide_width'] == '' ||
			$this->request->post['proslider_slide_height'] == '' ||
			$this->request->post['proslider_animation_speed'] == '' || 
			$this->request->post['proslider_animation_pause'] == '' || 
			$this->request->post['proslider_slices'] == '') {
			$this->error['warning'] = "Please complete all required fields.";
		}
		
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}	
	}
}
?>