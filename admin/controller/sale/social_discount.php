<?php  
class ControllerSaleSocialDiscount extends Controller {
	private $error = array();
     
  	public function index() {
		$this->load->language('sale/social_discount');
    	
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('sale/social_discount');
		
		$this->getList();
  	}
  
  	public function insert() {
    	$this->load->language('sale/social_discount');

    	$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('sale/social_discount');
		
    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_sale_social_discount->addSocialDiscount($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
						
			$this->redirect($this->url->link('sale/social_discount', 'token=' . $this->session->data['token'] . $url, 'SSL'));
    	}
    
    	$this->getForm();
  	}

  	public function update() {
    	$this->load->language('sale/social_discount');

    	$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('sale/social_discount');
				
    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_sale_social_discount->editSocialDiscount($this->request->get['social_discount_id'], $this->request->post);
      		
			$this->session->data['success'] = $this->language->get('text_success');
	  
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
						
			$this->redirect($this->url->link('sale/social_discount', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
    
    	$this->getForm();
  	}

  	public function delete() {
    	$this->load->language('sale/social_discount');

    	$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('sale/social_discount');
		
    	if (isset($this->request->post['selected']) && $this->validateDelete()) { 
			foreach ($this->request->post['selected'] as $social_discount_id) {
				$this->model_sale_social_discount->deleteSocialDiscount($social_discount_id);
			}
      		
			$this->session->data['success'] = $this->language->get('text_success');
	  
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
						
			$this->redirect($this->url->link('sale/social_discount', 'token=' . $this->session->data['token'] . $url, 'SSL'));
    	}
	
    	$this->getList();
  	}

  	private function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
				
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
			
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('sale/social_discount', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
							
		$this->data['insert'] = $this->url->link('sale/social_discount/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('sale/social_discount/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$this->data['social_discounts'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$social_discount_total = $this->model_sale_social_discount->getTotalSocialDiscount();
	
		$results = $this->model_sale_social_discount->getSocialDiscounts($data);
 
    	foreach ($results as $result) {
			$action = array();
						
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('sale/social_discount/update', 'token=' . $this->session->data['token'] . '&social_discount_id=' . $result['social_discount_id'] . $url, 'SSL')
			);
			
			
			$result_options = unserialize($result['options']);
									
			$this->data['social_discounts'][] = array(
				'social_discount_id'  => $result['social_discount_id'],
				'name'       => $result['name'],
				'discount'   => $result_options['discount'],
				'date_start' => date($this->language->get('date_format_short'), strtotime($result['date_start'])),
				'date_end'   => date($this->language->get('date_format_short'), strtotime($result['date_end'])),
				'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'selected'   => isset($this->request->post['selected']) && in_array($result['social_discount_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}
									
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_discount'] = $this->language->get('column_discount');
		$this->data['column_date_start'] = $this->language->get('column_date_start');
		$this->data['column_date_end'] = $this->language->get('column_date_end');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_action'] = $this->language->get('column_action');		
		
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_name'] = HTTPS_SERVER . 'index.php?route=sale/social_discount&token=' . $this->session->data['token'] . '&sort=name' . $url;
		$this->data['sort_discount'] = HTTPS_SERVER . 'index.php?route=sale/social_discount&token=' . $this->session->data['token'] . '&sort=discount' . $url;
		$this->data['sort_date_start'] = HTTPS_SERVER . 'index.php?route=sale/social_discount&token=' . $this->session->data['token'] . '&sort=date_start' . $url;
		$this->data['sort_date_end'] = HTTPS_SERVER . 'index.php?route=sale/social_discount&token=' . $this->session->data['token'] . '&sort=date_end' . $url;
		$this->data['sort_status'] = HTTPS_SERVER . 'index.php?route=sale/social_discount&token=' . $this->session->data['token'] . '&sort=status' . $url;
				
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $social_discount_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = HTTPS_SERVER . 'index.php?route=sale/social_discount&token=' . $this->session->data['token'] . $url . '&page={page}';
			
		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->template = 'sale/social_discount_list.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render());
  	}

  	private function getForm() {
    	$this->data['heading_title'] = $this->language->get('heading_title');

    	$this->data['text_enabled'] = $this->language->get('text_enabled');
    	$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
    	$this->data['text_yes'] = $this->language->get('text_yes');
    	$this->data['text_no'] = $this->language->get('text_no');
    	$this->data['text_percent'] = $this->language->get('text_percent');
    	$this->data['text_amount'] = $this->language->get('text_amount');
		$this->data['text_fixed'] = $this->language->get('text_fixed');
								
		$this->data['entry_name'] = $this->language->get('entry_name');
    	$this->data['entry_social_action'] = $this->language->get('entry_social_action');
		$this->data['entry_social_network_page'] = $this->language->get('entry_social_network_page');
		$this->data['entry_tweet_text'] = $this->language->get('entry_tweet_text');
    	$this->data['entry_discount'] = $this->language->get('entry_discount');
		$this->data['entry_logged'] = $this->language->get('entry_logged');
		$this->data['entry_shipping'] = $this->language->get('entry_shipping');
		$this->data['entry_type'] = $this->language->get('entry_type');
		$this->data['entry_quantity_total'] = $this->language->get('entry_quantity_total');
		$this->data['entry_total'] = $this->language->get('entry_total');
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
    	$this->data['entry_date_end'] = $this->language->get('entry_date_end');
    	$this->data['entry_uses_total'] = $this->language->get('entry_uses_total');
		$this->data['entry_uses_customer'] = $this->language->get('entry_uses_customer');
		$this->data['entry_status'] = $this->language->get('entry_status');
	
    	$this->data['button_save'] = $this->language->get('button_save');
    	$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_social_discount_history'] = $this->language->get('tab_social_discount_history');

		$this->data['token'] = $this->session->data['token'];
	
		if (isset($this->request->get['social_discount_id'])) {
			$this->data['social_discount_id'] = $this->request->get['social_discount_id'];
		} else {
			$this->data['social_discount_id'] = 0;
		}
				
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
	 	
		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['tweet_text'])) {
			$this->data['error_tweet_text'] = $this->error['tweet_text'];
		} else {
			$this->data['error_tweet_text'] = '';
		}
		
		if (isset($this->error['name'])) {
			$this->data['error_name_common'] = $this->error['name'];
		} else {
			$this->data['error_name_common'] = '';
		}
		
		if (isset($this->error['discount'])) {
			$this->data['error_discount'] = $this->error['discount'];
		} else {
			$this->data['error_discount'] = '';
		}
		
		if (isset($this->error['quantity_total'])) {
			$this->data['error_quantity_total'] = $this->error['quantity_total'];
		} else {
			$this->data['error_quantity_total'] = '';
		}
			
		if (isset($this->error['total'])) {
			$this->data['error_total'] = $this->error['total'];
		} else {
			$this->data['error_total'] = '';
		}
		
		if (isset($this->error['uses_customer'])) {
			$this->data['error_uses_customer'] = $this->error['uses_customer'];
		} else {
			$this->data['error_uses_customer'] = '';
		}
		
		if (isset($this->error['uses_total'])) {
			$this->data['error_uses_total'] = $this->error['uses_total'];
		} else {
			$this->data['error_uses_total'] = '';
		}
				
		if (isset($this->error['date_start'])) {
			$this->data['error_date_start'] = $this->error['date_start'];
		} else {
			$this->data['error_date_start'] = '';
		}	
		
		if (isset($this->error['date_end'])) {
			$this->data['error_date_end'] = $this->error['date_end'];
		} else {
			$this->data['error_date_end'] = '';
		}	

		$url = '';
			
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('sale/social_discount', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      		'separator' => ' :: '
   		);
									
		if (!isset($this->request->get['social_discount_id'])) {
			$this->data['action'] = $this->url->link('sale/social_discount/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('sale/social_discount/update', 'token=' . $this->session->data['token'] . '&social_discount_id=' . $this->request->get['social_discount_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('sale/social_discount', 'token=' . $this->session->data['token'] . $url, 'SSL');
  		
		if (isset($this->request->get['social_discount_id']) && (!$this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$social_discount_info_row = $this->model_sale_social_discount->getSocialDiscount($this->request->get['social_discount_id']);
    		$social_discount_info_options = unserialize($social_discount_info_row['options']);
			unset($social_discount_info_row['options']);
			$social_discount_info = array_merge($social_discount_info_row, $social_discount_info_options);
		}
				
		
		if (isset($this->request->post['name'])) {
      		$this->data['name'] = $this->request->post['name'];
    	} elseif (!empty($social_discount_info)) {
			$this->data['name'] = $social_discount_info['name'];
		} else {
      		$this->data['name'] = '';
    	}
		
		$this->data['social_discount_actions'] = explode(',', $this->language->get('social_discount_actions'));
		
		if (isset($this->request->post['social_action'])) {
      		$this->data['social_action'] = $this->request->post['social_action'];
    	} elseif (!empty($social_discount_info)) {
			$this->data['social_action'] = $social_discount_info['social_action'];
		} else {
      		$this->data['social_action'] = '';
    	}
		
		if (isset($this->request->post['social_network_page'])) {
      		$this->data['social_network_page'] = $this->request->post['social_network_page'];
    	} elseif (!empty($social_discount_info)) {
			$this->data['social_network_page'] = $social_discount_info['social_network_page'];
		} else {
      		$this->data['social_network_page'] = '';
    	}
		
		if (isset($this->request->post['tweet_text'])) {
      		$this->data['tweet_text'] = $this->request->post['tweet_text'];
    	} elseif (!empty($social_discount_info['tweet_text'])) {
			$this->data['tweet_text'] = $social_discount_info['tweet_text'];
		} else {
      		$this->data['tweet_text'] = $this->language->get('default_tweet_text');
    	}
		
		if (isset($this->request->post['type'])) {
      		$this->data['type'] = $this->request->post['type'];
    	} elseif (!empty($social_discount_info)) {
			$this->data['type'] = $social_discount_info['type'];
		} else {
      		$this->data['type'] = '';
    	}
		
		if (isset($this->request->post['discount'])) {
      		$this->data['discount'] = $this->request->post['discount'];
    	} elseif (!empty($social_discount_info)) {
			$this->data['discount'] = $social_discount_info['discount'];
		} else {
      		$this->data['discount'] = '0';
    	}

    	if (isset($this->request->post['logged'])) {
      		$this->data['logged'] = $this->request->post['logged'];
    	} elseif (!empty($social_discount_info)) {
			$this->data['logged'] = $social_discount_info['logged'];
		} else {
      		$this->data['logged'] = '';
    	}
				
    	if (isset($this->request->post['shipping'])) {
      		$this->data['shipping'] = $this->request->post['shipping'];
    	} elseif (!empty($social_discount_info)) {
			$this->data['shipping'] = $social_discount_info['shipping'];
		} else {
      		$this->data['shipping'] = '';
    	}
			
		if (isset($this->request->post['quantity_total'])) {
      		$this->data['quantity_total'] = $this->request->post['quantity_total'];
    	} elseif (!empty($social_discount_info)) {
			$this->data['quantity_total'] = $social_discount_info['quantity_total'];
		} else {
      		$this->data['quantity_total'] = '0';
    	}
		
    	if (isset($this->request->post['total'])) {
      		$this->data['total'] = $this->request->post['total'];
    	} elseif (!empty($social_discount_info)) {
			$this->data['total'] = $social_discount_info['total'];
		} else {
      		$this->data['total'] = '0';
    	}
			
		if (isset($this->request->post['date_start'])) {
       		$this->data['date_start'] = $this->request->post['date_start'];
		} elseif (isset($social_discount_info)) {
			$this->data['date_start'] = date('Y-m-d', strtotime($social_discount_info['date_start']));
		} else {
			$this->data['date_start'] = date('Y-m-d', time());
		}

		if (isset($this->request->post['date_end'])) {
       		$this->data['date_end'] = $this->request->post['date_end'];
		} elseif (isset($social_discount_info)) {
			$this->data['date_end'] = date('Y-m-d', strtotime($social_discount_info['date_end']));
		} else {
			$this->data['date_end'] = date('Y-m-d', time());
		}

    	if (isset($this->request->post['uses_total'])) {
      		$this->data['uses_total'] = $this->request->post['uses_total'];
		} elseif (isset($social_discount_info)) {
			$this->data['uses_total'] = $social_discount_info['uses_total'];
    	} else {
      		$this->data['uses_total'] = 1;
    	}
  
    	if (isset($this->request->post['uses_customer'])) {
      		$this->data['uses_customer'] = $this->request->post['uses_customer'];
    	} elseif (isset($social_discount_info)) {
			$this->data['uses_customer'] = $social_discount_info['uses_customer'];
		} else {
      		$this->data['uses_customer'] = 1;
    	}
 
    	if (isset($this->request->post['status'])) { 
      		$this->data['status'] = $this->request->post['status'];
    	} elseif (isset($social_discount_info)) {
			$this->data['status'] = $social_discount_info['status'];
		} else {
      		$this->data['status'] = 1;
    	}
		
		$this->template = 'sale/social_discount_form.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render());		
  	}
	
  	private function validateForm() {
    	if (!$this->user->hasPermission('modify', 'sale/social_discount')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}
      	
		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 128)) {
        	$this->error['name'] = $this->language->get('error_name');
      	}
		
		if (((utf8_strlen($this->request->post['tweet_text']) >140))) {
        	$this->error['tweet_text'] = $this->language->get('error_tweet_text');
      	}
		
		$this->load->model('sale/social_discount');
		if(isset($this->request->get['social_discount_id'])) {
			$social_discount_id = $this->request->get['social_discount_id'];
			$result = $this->model_sale_social_discount->validateSocialDiscountNameById($this->request->post['name'],$social_discount_id);
		}
		else {
			$result = $this->model_sale_social_discount->validateSocialDiscountName($this->request->post['name']);
		}
		
		if ($result) {
       		 	$this->error['name'] = $this->language->get('error_name_common');
      	}
			
		if (!is_numeric($this->request->post['discount'])) {
        	$this->error['discount'] = $this->language->get('error_discount');
      	}
						
		if(isset($this->request->post['quantity_total'])) {
			if(strpos($this->request->post['quantity_total'],"-") !== false) {
				$quantity_total_range = explode('-',$this->request->post['quantity_total']);	
				if ((!is_numeric($quantity_total_range[0])) or (!is_numeric($quantity_total_range[1]))) {
					$this->error['quantity_total'] = $this->language->get('error_quantity_total');
				}
			}
			else {
				if (!is_numeric($this->request->post['quantity_total'])) {
        			$this->error['quantity_total'] = $this->language->get('error_quantity_total');
				}
			}
		}
		
		if(isset($this->request->post['total'])) {
			if(strpos($this->request->post['total'],"-") !== false) {
				$total_range = explode('-',$this->request->post['total']);	
				if ((!is_numeric($total_range[0])) or (!is_numeric($total_range[1]))) {
					$this->error['total'] = $this->language->get('error_total');
				}
			}
			else {
				if (!is_numeric($this->request->post['total'])) {
        			$this->error['total'] = $this->language->get('error_total');
				}
			}
		}
			
		
		if (!is_numeric($this->request->post['uses_total'])) {
        	$this->error['uses_total'] = $this->language->get('error_uses_total');
      	}
		
		if (!is_numeric($this->request->post['uses_customer'])) {
        	$this->error['uses_customer'] = $this->language->get('error_uses_customer');
      	}
				
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}   
		   	
    	if (!$this->error) {
      		return true;
    	} else {
      		return false;
    	}
  	}

  	private function validateDelete() {
    	if (!$this->user->hasPermission('modify', 'sale/social_discount')) {
      		$this->error['warning'] = $this->language->get('error_permission');  
    	}
	  	
		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
  	}	
	
	public function history() {
    	$this->language->load('sale/social_discount');
		
		$this->load->model('sale/social_discount');
				
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_order_id'] = $this->language->get('column_order_id');
		$this->data['column_customer'] = $this->language->get('column_customer');
		$this->data['column_amount'] = $this->language->get('column_amount');
		$this->data['column_date_added'] = $this->language->get('column_date_added');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}  
		
		$this->data['histories'] = array();
			
		$results = $this->model_sale_social_discount->getSocialDiscountHistories($this->request->get['social_discount_id'], ($page - 1) * 10, 10);
      		
		foreach ($results as $result) {
        	$this->data['histories'][] = array(
				'order_id'   => $result['order_id'],
				'customer'   => $result['customer'],
				'amount'     => $result['amount'],
        		'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
        	);
      	}			
		
		$history_total = $this->model_sale_social_discount->getTotalSocialDiscountHistories($this->request->get['social_discount_id']);
			
		$pagination = new Pagination();
		$pagination->total = $history_total;
		$pagination->page = $page;
		$pagination->limit = 10; 
		$pagination->url = $this->url->link('sale/social_discount/history', 'token=' . $this->session->data['token'] . '&social_discount_id=' . $this->request->get['social_discount_id'] . '&page={page}', 'SSL');
			
		$this->data['pagination'] = $pagination->render();
		
		$this->template = 'sale/social_discount_history.tpl';		
		
		$this->response->setOutput($this->render());
  	}		
		
}
?>