<?php

/*
 * Copyright (c) 2013 Web Project Solutions LLC (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 *  @author Antonello Venturino <info@webprojectsol.com>
 *  @copyright  2013 Web Project Solutions LLC
 *  @license    http://www.webprojectsol.com/license.php
 *  @url  http://www.webprojectsol.com/en/modules-of-payment/paypal-express-checkout.html
 */

class ControllerPaymentPayPalExpress extends Controller {

    private $error = array();
    private $version = '1.3.2';
    private $code = 'PAGPEXCOPC';
    private $_supportedCurrencyCodes = array('AUD', 'CAD', 'CZK', 'DKK', 'EUR', 'HKD', 'HUF', 'ILS', 'JPY', 'MXN', 'NOK', 'NZD', 'PLN', 'GBP', 'SGD', 'SEK', 'CHF', 'USD', 'TWD', 'THB', 'BRL', 'MYR', 'TRY');
    private $curl;
    private $p_user;
    private $p_pwd;
    private $p_signature;

    public function __construct($registry) {
        parent::__construct($registry);
        if (!$this->config->get('paypal_express_test')) {
            $this->curl = 'https://api-3t.paypal.com/nvp';
            $this->p_user = $this->config->get('paypal_express_username');
            $this->p_pwd = $this->config->get('paypal_express_password');
            $this->p_signature = $this->config->get('paypal_express_signature');
        } else {
            $this->curl = 'https://api-3t.sandbox.paypal.com/nvp';
            $this->p_user = $this->config->get('paypal_express_username_test');
            $this->p_pwd = $this->config->get('paypal_express_password_test');
            $this->p_signature = $this->config->get('paypal_express_signature_test');
        }
    }

    public function index() {
        if (VERSION == '1.5.5') {
            $this->language->load('payment/paypal_express');
        } else {
            $this->load->language('payment/paypal_express');
        }
        $this->load->model('setting/paypal_express_license');
        if (false && !$this->model_setting_paypal_express_license->_check()) {
            $this->session->data['error'] = sprintf($this->language->get('error_license'), 'http://www.webprojectsol.com/clients/', ($this->language->get('code') == 'it' ? 'http://www.webprojectsol.com/it/moduli-di-pagamento/paypal-express-checkout-italiano.html' : 'http://www.webprojectsol.com/en/modules-of-payment/paypal-express-checkout.html'));
            $this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
        }

        if (!$this->checkOpenCartVersion()) {
            $this->session->data['error'] = sprintf($this->language->get('error_opencart_version'), VERSION);
            $this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('paypal_express', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->data['heading_title'] = $this->language->get('heading_title');
        $this->data['tab_live'] = $this->language->get('tab_live');
        $this->data['tab_test'] = $this->language->get('tab_test');
        $this->data['button_get_api'] = $this->language->get('button_get_api');
        $this->data['button_get_api_test'] = $this->language->get('button_get_api_test');

        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_all_zones'] = $this->language->get('text_all_zones');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
        $this->data['text_authorization'] = $this->language->get('text_authorization');
        $this->data['text_sale'] = $this->language->get('text_sale');
        $this->data['text_noshipping_auto'] = $this->language->get('text_noshipping_auto');
        $this->data['text_noshipping_display'] = $this->language->get('text_noshipping_display');
        $this->data['text_noshipping_notdisplay'] = $this->language->get('text_noshipping_notdisplay');
        $this->data['text_noshipping_profile'] = $this->language->get('text_noshipping_profile');
        $this->data['text_shipping_address'] = $this->language->get('text_shipping_address');
        $this->data['text_payment_address'] = $this->language->get('text_payment_address');
        $this->data['text_createaccount_customer_choose'] = $this->language->get('text_createaccount_customer_choose');
        $this->data['text_none'] = $this->language->get('text_none');
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');
        $this->data['text_browse'] = $this->language->get('text_browse');
        $this->data['text_clear'] = $this->language->get('text_clear');

        $this->data['entry_username'] = $this->language->get('entry_username');
        $this->data['entry_password'] = $this->language->get('entry_password');
        $this->data['entry_signature'] = $this->language->get('entry_signature');
        $this->data['entry_username_test'] = $this->language->get('entry_username_test');
        $this->data['entry_password_test'] = $this->language->get('entry_password_test');
        $this->data['entry_signature_test'] = $this->language->get('entry_signature_test');
        $this->data['entry_title'] = $this->language->get('entry_title');
        $this->data['entry_test'] = $this->language->get('entry_test');
        $this->data['entry_language'] = $this->language->get('entry_language');
        $this->data['entry_method'] = $this->language->get('entry_method');
        $this->data['entry_logo'] = $this->language->get('entry_logo');
        $this->data['entry_createaccount'] = $this->language->get('entry_createaccount');
        $this->data['entry_send_address'] = $this->language->get('entry_send_address');
        $this->data['entry_noshipping'] = $this->language->get('entry_noshipping');
        $this->data['entry_merchant_country'] = $this->language->get('entry_merchant_country');
        $this->data['entry_default_currency'] = $this->language->get('entry_default_currency');
        $this->data['entry_skip_confirm'] = $this->language->get('entry_skip_confirm');
        $this->data['entry_confirm_order'] = $this->language->get('entry_confirm_order');
        $this->data['entry_senditem'] = $this->language->get('entry_senditem');
        $this->data['entry_landing'] = $this->language->get('entry_landing');
        $this->data['entry_total'] = $this->language->get('entry_total');
        $this->data['entry_total_over'] = $this->language->get('entry_total_over');
        $this->data['entry_order_status'] = $this->language->get('entry_order_status');
        $this->data['entry_order_status_complete'] = $this->language->get('entry_order_status_complete');
        $this->data['entry_order_status_refunded'] = $this->language->get('entry_order_status_refunded');
        $this->data['entry_order_status_voided'] = $this->language->get('entry_order_status_voided');
        $this->data['entry_enableincheckout'] = $this->language->get('entry_enableincheckout');
        $this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');

        $this->data['help_encryption'] = $this->language->get('help_encryption');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        $this->data['tab_general'] = $this->language->get('tab_general');

        if (VERSION == '1.5.5') {
            $this->language->load('setting/setting');
        } else {
            $this->load->language('setting/setting');
        }
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');
        $this->data['text_browse'] = $this->language->get('text_browse');
        $this->data['text_clear'] = $this->language->get('text_clear');

        $this->data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }
        if (isset($this->error['username'])) {
            $this->data['error_username'] = $this->error['username'];
        } else {
            $this->data['error_username'] = '';
        }
        if (isset($this->error['password'])) {
            $this->data['error_password'] = $this->error['password'];
        } else {
            $this->data['error_password'] = '';
        }
        if (isset($this->error['signature'])) {
            $this->data['error_signature'] = $this->error['signature'];
        } else {
            $this->data['error_signature'] = '';
        }
        foreach ($languages as $language) {
            if (isset($this->error['error_title_' . $language['language_id']])) {
                $this->data['error_title_' . $language['language_id']] = $this->error['error_title_' . $language['language_id']];
            } else {
                $this->data['error_title_' . $language['language_id']] = '';
            }
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_home'),
          'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
          'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
          'text' => $this->language->get('text_payment'),
          'href' => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
          'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
          'text' => $this->language->get('heading_title'),
          'href' => $this->url->link('payment/paypal_express', 'token=' . $this->session->data['token'], 'SSL'),
          'separator' => ' :: '
        );

        $this->data['action'] = $this->url->link('payment/paypal_express', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->post['paypal_express_username'])) {
            $this->data['paypal_express_username'] = $this->request->post['paypal_express_username'];
        } else {
            $this->data['paypal_express_username'] = $this->config->get('paypal_express_username');
        }

        if (isset($this->request->post['paypal_express_password'])) {
            $this->data['paypal_express_password'] = $this->request->post['paypal_express_password'];
        } else {
            $this->data['paypal_express_password'] = $this->config->get('paypal_express_password');
        }

        if (isset($this->request->post['paypal_express_signature'])) {
            $this->data['paypal_express_signature'] = $this->request->post['paypal_express_signature'];
        } else {
            $this->data['paypal_express_signature'] = $this->config->get('paypal_express_signature');
        }

        if (isset($this->request->post['paypal_express_username_test'])) {
            $this->data['paypal_express_username_test'] = $this->request->post['paypal_express_username_test'];
        } else {
            $this->data['paypal_express_username_test'] = $this->config->get('paypal_express_username_test');
        }

        if (isset($this->request->post['paypal_express_password_test'])) {
            $this->data['paypal_express_password_test'] = $this->request->post['paypal_express_password_test'];
        } else {
            $this->data['paypal_express_password_test'] = $this->config->get('paypal_express_password_test');
        }

        if (isset($this->request->post['paypal_express_signature_test'])) {
            $this->data['paypal_express_signature_test'] = $this->request->post['paypal_express_signature_test'];
        } else {
            $this->data['paypal_express_signature_test'] = $this->config->get('paypal_express_signature_test');
        }

        foreach ($languages as $language) {
            if (isset($this->request->post['paypal_express_title_' . $language['language_id']])) {
                $this->data['paypal_express_title_' . $language['language_id']] = $this->request->post['paypal_express_title_' . $language['language_id']];
            } elseif ($this->config->get('paypal_express_title_' . $language['language_id'])) {
                $this->data['paypal_express_title_' . $language['language_id']] = $this->config->get('paypal_express_title_' . $language['language_id']);
            } elseif ($this->config->get('paypal_express_title_' . $language['code'])) {
                $this->data['paypal_express_title_' . $language['language_id']] = $this->config->get('paypal_express_title_' . $language['code']);
            } else {
                $locales = explode(',', $language['locale']);
                $locale = 'en_US';
                if (isset($locales['1'])) {
                    $locale = $locales['1'];
                }
                $this->data['paypal_express_title_' . $language['language_id']] = '<img src="https://www.paypal.com/' . $locale . '/i/bnr/horizontal_solution_PP.gif" />';
            }
        }

        if (isset($this->request->post['paypal_express_test'])) {
            $this->data['paypal_express_test'] = $this->request->post['paypal_express_test'];
        } else {
            $this->data['paypal_express_test'] = $this->config->get('paypal_express_test');
        }

        if (isset($this->request->post['paypal_express_method'])) {
            $this->data['paypal_express_method'] = $this->request->post['paypal_express_method'];
        } else {
            $this->data['paypal_express_method'] = $this->config->get('paypal_express_method');
        }

        if (isset($this->request->post['paypal_express_logo'])) {
            $this->data['paypal_express_logo'] = $this->request->post['paypal_express_logo'];
        } elseif ($this->config->get('paypal_express_logo')) {
            $this->data['paypal_express_logo'] = $this->config->get('paypal_express_logo');
        } else {
            $this->data['paypal_express_logo'] = $this->config->get('config_logo');
        }

        $this->load->model('tool/image');
        $this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
        if ($this->data['paypal_express_logo'] && file_exists(DIR_IMAGE . $this->data['paypal_express_logo']) && is_file(DIR_IMAGE . $this->data['paypal_express_logo'])) {
            $this->data['logo'] = $this->model_tool_image->resize($this->data['paypal_express_logo'], 100, 100);
        } else {
            $this->data['logo'] = $this->data['no_image'];
        }

        if (isset($this->request->post['paypal_express_createaccount'])) {
            $this->data['paypal_express_createaccount'] = $this->request->post['paypal_express_createaccount'];
        } elseif ($this->config->get('paypal_express_createaccount')) {
            $this->data['paypal_express_createaccount'] = $this->config->get('paypal_express_createaccount');
        } else {
            $this->data['paypal_express_createaccount'] = 'customer';
        }

        if (isset($this->request->post['paypal_express_send_address'])) {
            $this->data['paypal_express_send_address'] = $this->request->post['paypal_express_send_address'];
        } elseif ($this->config->get('paypal_express_send_address')) {
            $this->data['paypal_express_send_address'] = $this->config->get('paypal_express_send_address');
        } else {
            $this->data['paypal_express_send_address'] = 'shipping';
        }

        if (isset($this->request->post['paypal_express_noshipping'])) {
            $this->data['paypal_express_noshipping'] = $this->request->post['paypal_express_noshipping'];
        } elseif ($this->config->get('paypal_express_noshipping') || $this->config->get('paypal_express_noshipping') === '0') {
            $this->data['paypal_express_noshipping'] = $this->config->get('paypal_express_noshipping');
        } else {
            $this->data['paypal_express_noshipping'] = '3';
        }

        if (isset($this->request->post['paypal_express_merchant_country'])) {
            $this->data['paypal_express_merchant_country'] = $this->request->post['paypal_express_merchant_country'];
        } elseif ($this->config->get('paypal_express_merchant_country')) {
            $this->data['paypal_express_merchant_country'] = $this->config->get('paypal_express_merchant_country');
        } else {
            $this->data['paypal_express_merchant_country'] = '';
        }

        $this->load->model('localisation/country');
        $countries = $this->model_localisation_country->getCountries();
        $this->data['paypal_express_merchant_countries'] = array();
        $this->data['paypal_express_merchant_countries']['he_IL'] = 'All Country (Hebrew Default Language)';
        foreach ($countries as $country) {
            $this->data['paypal_express_merchant_countries'][$country['iso_code_2']] = $country['name'];
            if ($country['iso_code_2'] == 'DK') {
                $this->data['paypal_express_merchant_countries']['da_DK'] = $country['name'] . ' (Danish Default Language)';
            } elseif ($country['iso_code_2'] == 'ID') {
                $this->data['paypal_express_merchant_countries']['id_ID'] = $country['name'] . ' (Indonesian Default Language)';
            } elseif ($country['iso_code_2'] == 'JP') {
                $this->data['paypal_express_merchant_countries']['jp_JP'] = $country['name'] . ' (Japanese Default Language)';
            } elseif ($country['iso_code_2'] == 'NO') {
                $this->data['paypal_express_merchant_countries']['no_NO'] = $country['name'] . ' (Norwegian Default Language)';
            } elseif ($country['iso_code_2'] == 'BR') {
                $this->data['paypal_express_merchant_countries']['pt_BR_' . $country['iso_code_2']] = $country['name'] . ' (Brazilian Portuguese Default Language)';
            } elseif ($country['iso_code_2'] == 'PT') {
                $this->data['paypal_express_merchant_countries']['pt_BR_' . $country['iso_code_2']] = $country['name'] . ' (Brazilian Portuguese Default Language)';
            } elseif ($country['iso_code_2'] == 'LT') {
                $this->data['paypal_express_merchant_countries']['ru_RU_' . $country['iso_code_2']] = $country['name'] . ' (Russian Default Language)';
            } elseif ($country['iso_code_2'] == 'LV') {
                $this->data['paypal_express_merchant_countries']['ru_RU_' . $country['iso_code_2']] = $country['name'] . ' (Russian Default Language)';
            } elseif ($country['iso_code_2'] == 'UA') {
                $this->data['paypal_express_merchant_countries']['ru_RU_' . $country['iso_code_2']] = $country['name'] . ' (Russian Default Language)';
            } elseif ($country['iso_code_2'] == 'SE') {
                $this->data['paypal_express_merchant_countries']['sv_SE'] = $country['name'] . ' (Swedish Default Language)';
            } elseif ($country['iso_code_2'] == 'TH') {
                $this->data['paypal_express_merchant_countries']['th_TH'] = $country['name'] . ' (Thai Default Language)';
            } elseif ($country['iso_code_2'] == 'TR') {
                $this->data['paypal_express_merchant_countries']['tr_TR'] = $country['name'] . ' (Turkish Default Language)';
            } elseif ($country['iso_code_2'] == 'CN') {
                $this->data['paypal_express_merchant_countries']['zh_CN'] = $country['name'] . ' (Simplified Chinese Default Language)';
            } elseif ($country['iso_code_2'] == 'HK') {
                $this->data['paypal_express_merchant_countries']['zh_HK'] = $country['name'] . ' (Traditional Chinese Default Language)';
            } elseif ($country['iso_code_2'] == 'TW') {
                $this->data['paypal_express_merchant_countries']['zh_TW'] = $country['name'] . ' (Traditional Chinese Default Language)';
            }
        }

        if (isset($this->request->post['paypal_express_default_currency'])) {
            $this->data['paypal_express_default_currency'] = $this->request->post['paypal_express_default_currency'];
        } elseif ($this->config->get('paypal_express_default_currency')) {
            $this->data['paypal_express_default_currency'] = $this->config->get('paypal_express_default_currency');
        } else {
            $this->data['paypal_express_default_currency'] = 'USD';
        }

        $this->load->model('localisation/currency');
        $currencies = $this->model_localisation_currency->getCurrencies();
        $this->data['paypal_express_default_currencies'] = array();
        foreach ($currencies as $currency) {
            if (in_array($currency['code'], $this->_supportedCurrencyCodes)) {
                $this->data['paypal_express_default_currencies'][$currency['code']] = $currency['title'] . ' [' . $currency['code'] . ']';
            }
        }

        if (isset($this->request->post['paypal_express_skip_confirm'])) {
            $this->data['paypal_express_skip_confirm'] = $this->request->post['paypal_express_skip_confirm'];
        } elseif ($this->config->get('paypal_express_skip_confirm')) {
            $this->data['paypal_express_skip_confirm'] = $this->config->get('paypal_express_skip_confirm');
        } else {
            $this->data['paypal_express_skip_confirm'] = '0';
        }

        if (isset($this->request->post['paypal_express_confirm_order'])) {
            $this->data['paypal_express_confirm_order'] = $this->request->post['paypal_express_confirm_order'];
        } elseif ($this->config->get('paypal_express_confirm_order')) {
            $this->data['paypal_express_confirm_order'] = $this->config->get('paypal_express_confirm_order');
        } else {
            $this->data['paypal_express_confirm_order'] = '0';
        }

        if (isset($this->request->post['paypal_express_senditem'])) {
            $this->data['paypal_express_senditem'] = $this->request->post['paypal_express_senditem'];
        } else {
            $this->data['paypal_express_senditem'] = $this->config->get('paypal_express_senditem');
        }

        if (isset($this->request->post['paypal_express_landing'])) {
            $this->data['paypal_express_landing'] = $this->request->post['paypal_express_landing'];
        } else {
            $this->data['paypal_express_landing'] = $this->config->get('paypal_express_landing');
        }

        if (isset($this->request->post['paypal_express_total'])) {
            $this->data['paypal_express_total'] = $this->request->post['paypal_express_total'];
        } else {
            $this->data['paypal_express_total'] = $this->config->get('paypal_express_total');
        }

        if (isset($this->request->post['paypal_express_total_over'])) {
            $this->data['paypal_express_total_over'] = $this->request->post['paypal_express_total_over'];
        } else {
            $this->data['paypal_express_total_over'] = $this->config->get('paypal_express_total_over');
        }

        if (isset($this->request->post['paypal_express_order_status_id'])) {
            $this->data['paypal_express_order_status_id'] = $this->request->post['paypal_express_order_status_id'];
        } elseif ($this->config->get('paypal_express_order_status_id')) {
            $this->data['paypal_express_order_status_id'] = $this->config->get('paypal_express_order_status_id');
        } else {
            $this->data['paypal_express_order_status_id'] = '1';
        }

        if (isset($this->request->post['paypal_express_order_status_id_complete'])) {
            $this->data['paypal_express_order_status_id_complete'] = $this->request->post['paypal_express_order_status_id_complete'];
        } elseif ($this->config->get('paypal_express_order_status_id_complete')) {
            $this->data['paypal_express_order_status_id_complete'] = $this->config->get('paypal_express_order_status_id_complete');
        } else {
            $this->data['paypal_express_order_status_id_complete'] = '2';
        }

        if (isset($this->request->post['paypal_express_order_status_id_refunded'])) {
            $this->data['paypal_express_order_status_id_refunded'] = $this->request->post['paypal_express_order_status_id_refunded'];
        } elseif ($this->config->get('paypal_express_order_status_id_refunded')) {
            $this->data['paypal_express_order_status_id_refunded'] = $this->config->get('paypal_express_order_status_id_refunded');
        } else {
            $this->data['paypal_express_order_status_id_refunded'] = '11';
        }

        if (isset($this->request->post['paypal_express_order_status_id_voided'])) {
            $this->data['paypal_express_order_status_id_voided'] = $this->request->post['paypal_express_order_status_id_voided'];
        } elseif ($this->config->get('paypal_express_order_status_id_voided')) {
            $this->data['paypal_express_order_status_id_voided'] = $this->config->get('paypal_express_order_status_id_voided');
        } else {
            $this->data['paypal_express_order_status_id_voided'] = '7';
        }

        $this->load->model('localisation/order_status');

        $this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        if (isset($this->request->post['paypal_express_enableincheckout'])) {
            $this->data['paypal_express_enableincheckout'] = $this->request->post['paypal_express_enableincheckout'];
        } else {
            $this->data['paypal_express_enableincheckout'] = $this->config->get('paypal_express_enableincheckout');
        }

        if (isset($this->request->post['paypal_express_geo_zone_id'])) {
            $this->data['paypal_express_geo_zone_id'] = $this->request->post['paypal_express_geo_zone_id'];
        } else {
            $this->data['paypal_express_geo_zone_id'] = $this->config->get('paypal_express_geo_zone_id');
        }

        $this->load->model('localisation/geo_zone');

        $this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

        if (isset($this->request->post['paypal_express_status'])) {
            $this->data['paypal_express_status'] = $this->request->post['paypal_express_status'];
        } else {
            $this->data['paypal_express_status'] = $this->config->get('paypal_express_status');
        }

        if (isset($this->request->post['paypal_express_sort_order'])) {
            $this->data['paypal_express_sort_order'] = $this->request->post['paypal_express_sort_order'];
        } else {
            $this->data['paypal_express_sort_order'] = $this->config->get('paypal_express_sort_order');
        }

        $this->data['languages'] = $languages;

        $this->template = 'payment/paypal_express.tpl';
        $this->children = array(
          'common/header',
          'common/footer'
        );
        $this->response->setOutput($this->render());
    }

    private function validate() {
        if (!$this->user->hasPermission('modify', 'payment/paypal_express')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->request->post['paypal_express_username']) {
            $this->error['username'] = $this->language->get('error_username');
        }

        if (!$this->request->post['paypal_express_password']) {
            $this->error['password'] = $this->language->get('error_password');
        }

        if (!$this->request->post['paypal_express_signature']) {
            $this->error['signature'] = $this->language->get('error_signature');
        }

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        foreach ($languages as $language) {
            if (!$this->request->post['paypal_express_title_' . $language['language_id']]) {
                $this->error['error_title_' . $language['language_id']] = $this->language->get('error_title');
            } elseif (strlen(htmlentities($this->request->post['paypal_express_title_' . $language['language_id']])) > 128) {
                $this->error['error_title_' . $language['language_id']] = $this->language->get('error_title_max');
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function checkVersion() {
        $url = 'http://www.webprojectsol.com/extern/CheckVersion.php?code=' . $this->code . '&version=' . $this->version;
        if (function_exists('curl_init')) {
            $c = @curl_init($url);
            @curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($c);
        } elseif (@ini_get('allow_url_fopen') == '1') {
            $result = file_get_contents($url);
        }
        if (isset($result)) {
            if (version_compare($result, $this->version, '>')) {
                echo 'We are happy to announce the availability of PayPal Express Checkout for OpenCart "' . $result . '" version for download and upgrade. Your version is "' . $this->version . '".';
            }
        }
    }

    private function checkOpenCartVersion() {
        if (version_compare(VERSION, '1.5', '>=')) {
            return true;
        }
    }

    public function RefundTransaction() {
        #Check for supported currency, otherwise convert to USD.
        $currencies = array('AUD', 'CAD', 'CZK', 'DKK', 'EUR', 'HUF', 'JPY', 'NOK', 'NZD', 'PLN', 'GBP', 'SGD', 'SEK', 'CHF', 'USD');
        if (in_array($this->request->post['currency'], $currencies)) {
            $currency = $this->request->post['currency'];
        } else {
            $currency = 'USD';
        }

        $request = 'USER=' . $this->p_user;
        $request .= '&PWD=' . $this->p_pwd;
        $request .= '&VERSION=85.0';
        $request .= '&SIGNATURE=' . $this->p_signature;
        $request .= '&METHOD=RefundTransaction';
        $request .= '&TRANSACTIONID=' . trim(urlencode($this->request->post['transaction_id']));
        $request .= '&REFUNDTYPE=' . trim(urlencode($this->request->post['refund_type']));
        $request .= '&AMT=' . trim(urlencode($this->request->post['amount']));
        $request .= '&CURRENCYCODE=' . trim(urlencode($currency));
        $request .= '&NOTE=' . trim(urlencode($this->request->post['paypal_express_note']));

        $response = $this->sendTransactionToGateway($this->curl, $request);

        $response_data = array();
        parse_str($response, $response_data);

        $json = array();
        if (($response_data['ACK'] == 'Success') || ($response_data['ACK'] == 'SuccessWithWarning')) {
            if (VERSION == '1.5.5') {
                $this->language->load('payment/paypal_express');
            } else {
                $this->load->language('payment/paypal_express');
            }
            $json['success'] = $this->language->get('success_refunded');
        } else {
            $json['error'] = '';
            for ($i = 0; $i < 10; ++$i) {
                if (isset($response_data['L_ERRORCODE' . $i])) {
                    if (isset($response_data['L_SEVERITYCODE' . $i]) && isset($response_data['L_ERRORCODE' . $i]) && isset($response_data['L_LONGMESSAGE' . $i])) {
                        $json['error'] .= $response_data['L_SEVERITYCODE' . $i] . ': ' . $response_data['L_ERRORCODE' . $i] . ' - ' . $response_data['L_LONGMESSAGE' . $i] . '<br />';
                    }
                } else {
                    $i = 10;
                }
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function DoCapture() {
        #Check for supported currency, otherwise convert to USD.
        $currencies = array('AUD', 'CAD', 'CZK', 'DKK', 'EUR', 'HUF', 'JPY', 'NOK', 'NZD', 'PLN', 'GBP', 'SGD', 'SEK', 'CHF', 'USD');
        if (in_array($this->request->post['currency'], $currencies)) {
            $currency = $this->request->post['currency'];
        } else {
            $currency = 'USD';
        }

        $request = 'USER=' . $this->p_user;
        $request .= '&PWD=' . $this->p_pwd;
        $request .= '&VERSION=85.0';
        $request .= '&SIGNATURE=' . $this->p_signature;
        $request .= '&METHOD=DoCapture';
        $request .= '&AUTHORIZATIONID=' . trim(urlencode($this->request->post['transaction_id']));
        $request .= '&COMPLETETYPE=' . trim(urlencode($this->request->post['capture_type']));
        $request .= '&AMT=' . trim(urlencode($this->request->post['amount']));
        $request .= '&CURRENCYCODE=' . trim(urlencode($currency));
        $request .= '&NOTE=' . trim(urlencode($this->request->post['paypal_express_note']));

        $response = $this->sendTransactionToGateway($this->curl, $request);

        $response_data = array();
        parse_str($response, $response_data);

        $json = array();
        if (($response_data['ACK'] == 'Success') || ($response_data['ACK'] == 'SuccessWithWarning')) {
            if (VERSION == '1.5.5') {
                $this->language->load('payment/paypal_express');
            } else {
                $this->load->language('payment/paypal_express');
            }
            $json['success'] = $this->language->get('success_captured');

            $message = '';
            if (isset($response_data['TRANSACTIONID'])) {
                $message .= 'TRANSACTIONID: ' . $response_data['TRANSACTIONID'] . "\n";
            }
            if (isset($response_data['PAYMENTSTATUS'])) {
                $message .= 'PAYPAL STATUS: ' . $response_data['PAYMENTSTATUS'] . "\n";
            }

            if ($message) {
                $json['comment'] = $message . "\n";
            }
        } else {
            $json['error'] = '';
            for ($i = 0; $i < 10; ++$i) {
                if (isset($response_data['L_ERRORCODE' . $i])) {
                    if (isset($response_data['L_SEVERITYCODE' . $i]) && isset($response_data['L_ERRORCODE' . $i]) && isset($response_data['L_LONGMESSAGE' . $i])) {
                        $json['error'] .= $response_data['L_SEVERITYCODE' . $i] . ': ' . $response_data['L_ERRORCODE' . $i] . ' - ' . $response_data['L_LONGMESSAGE' . $i] . '<br />';
                    }
                } else {
                    $i = 10;
                }
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function DoVoid() {
        $request = 'USER=' . $this->p_user;
        $request .= '&PWD=' . $this->p_pwd;
        $request .= '&VERSION=85.0';
        $request .= '&SIGNATURE=' . $this->p_signature;
        $request .= '&METHOD=DoVoid';
        $request .= '&AUTHORIZATIONID=' . trim(urlencode($this->request->post['transaction_id']));
        $request .= '&NOTE=' . trim(urlencode($this->request->post['paypal_express_note']));

        $response = $this->sendTransactionToGateway($this->curl, $request);

        $response_data = array();
        parse_str($response, $response_data);

        $json = array();
        if (($response_data['ACK'] == 'Success') || ($response_data['ACK'] == 'SuccessWithWarning')) {
            if (VERSION == '1.5.5') {
                $this->language->load('payment/paypal_express');
            } else {
                $this->load->language('payment/paypal_express');
            }
            $json['success'] = $this->language->get('success_voided');
        } else {
            $json['error'] = '';
            for ($i = 0; $i < 10; ++$i) {
                if (isset($response_data['L_ERRORCODE' . $i])) {
                    if (isset($response_data['L_SEVERITYCODE' . $i]) && isset($response_data['L_ERRORCODE' . $i]) && isset($response_data['L_LONGMESSAGE' . $i])) {
                        $json['error'] .= $response_data['L_SEVERITYCODE' . $i] . ': ' . $response_data['L_ERRORCODE' . $i] . ' - ' . $response_data['L_LONGMESSAGE' . $i] . '<br />';
                    }
                } else {
                    $i = 10;
                }
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    private function sendTransactionToGateway($url, $parameters) {
        $server = parse_url($url);

        if (!isset($server['port'])) {
            $server['port'] = ($server['scheme'] == 'https') ? 443 : 80;
        }

        if (!isset($server['path'])) {
            $server['path'] = '/';
        }

        if (function_exists('curl_init')) {
            $ch = curl_init($server['scheme'] . '://' . $server['host'] . $server['path'] . (isset($server['query']) ? '?' . $server['query'] : ''));
            curl_setopt($ch, CURLOPT_PORT, $server['port']);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);

            $result = curl_exec($ch);

            if (!isset($result) || !$result) {
                $this->log->write('PayPal Express Checkout Request failed: ' . curl_error($ch) . '(' . curl_errno($ch) . ')');
                curl_close($ch);
                return false;
            } else {
                curl_close($ch);
            }
        } else {
            $this->log->write('PayPal Express Checkout Request failed: Your server doesn\'t support the curl functions, it\'s need to work correctly');
            $result = '';
        }

        return $result;
    }

}

?>
