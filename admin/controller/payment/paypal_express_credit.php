<?php

/*
 * Copyright (c) 2012 Web Project Solutions LLC (info@webprojectsol.com)
 *
 * SOFTWARE LICENSE AGREEMENT
 *
 * This Software is not free.
 *
 * Developer hereby grants to Licensee a perpetual, non-exclusive,
 * limited license to use the Software as set forth in this Agreement.
 *
 * Licensee shall not modify, copy, duplicate, reproduce, license or sublicense the Software,
 * or transfer or convey the Software or any right in the Software to anyone else
 * without the prior written consent of Developer; provided that Licensee may
 * make one copy of the Software for backup or archival purposes.
 *
 *
 *  @author Antonello Venturino <info@webprojectsol.com>
 *  @copyright  2012 Web Project Solutions LLC
 *  @license    http://www.webprojectsol.com/license.php
 *  @url  http://www.webprojectsol.com/en/modules-of-payment/paypal-express-checkout.html
 */

class ControllerPaymentPayPalExpressCredit extends Controller {

    private $error = array();
    private $version = '1.2.9';
    private $code = 'PAGPEXCOPC';
    private $_supportedCurrencyCodes = array('AUD', 'CAD', 'CZK', 'DKK', 'EUR', 'HKD', 'HUF', 'ILS', 'JPY', 'MXN', 'NOK', 'NZD', 'PLN', 'GBP', 'SGD', 'SEK', 'CHF', 'USD', 'TWD', 'THB', 'BRL', 'MYR', 'TRY');

    public function index() {
        $this->load->language('payment/paypal_express_credit');
        $this->load->model('setting/paypal_express_license');
        if (!$this->model_setting_paypal_express_license->_check()) {
            $this->session->data['error'] = sprintf($this->language->get('error_license'), 'http://www.webprojectsol.com/clients/', ($this->language->get('code') == 'it' ? 'http://www.webprojectsol.com/it/moduli-di-pagamento/paypal-express-checkout-italiano.html' : 'http://www.webprojectsol.com/en/modules-of-payment/paypal-express-checkout.html'));
            $this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
        }

        if (!$this->checkOpenCartVersion()) {
            $this->session->data['error'] = sprintf($this->language->get('error_opencart_version'), VERSION);
            $this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('paypal_express_credit', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_all_zones'] = $this->language->get('text_all_zones');
        $this->data['text_yes'] = $this->language->get('text_yes');
        $this->data['text_no'] = $this->language->get('text_no');
        $this->data['text_authorization'] = $this->language->get('text_authorization');
        $this->data['text_sale'] = $this->language->get('text_sale');
        $this->data['text_shipping_address'] = $this->language->get('text_shipping_address');
        $this->data['text_payment_address'] = $this->language->get('text_payment_address');
        $this->data['text_createaccount_customer_choose'] = $this->language->get('text_createaccount_customer_choose');
        $this->data['text_none'] = $this->language->get('text_none');
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');
        $this->data['text_browse'] = $this->language->get('text_browse');
        $this->data['text_clear'] = $this->language->get('text_clear');

        $this->data['entry_username'] = $this->language->get('entry_username');
        $this->data['entry_password'] = $this->language->get('entry_password');
        $this->data['entry_signature'] = $this->language->get('entry_signature');
        $this->data['entry_username_test'] = $this->language->get('entry_username_test');
        $this->data['entry_password_test'] = $this->language->get('entry_password_test');
        $this->data['entry_signature_test'] = $this->language->get('entry_signature_test');
        $this->data['entry_title'] = $this->language->get('entry_title');
        $this->data['entry_test'] = $this->language->get('entry_test');
        $this->data['entry_language'] = $this->language->get('entry_language');
        $this->data['entry_method'] = $this->language->get('entry_method');
        $this->data['entry_logo'] = $this->language->get('entry_logo');
        $this->data['entry_createaccount'] = $this->language->get('entry_createaccount');
        $this->data['entry_send_address'] = $this->language->get('entry_send_address');
        $this->data['entry_merchant_country'] = $this->language->get('entry_merchant_country');
        $this->data['entry_default_currency'] = $this->language->get('entry_default_currency');
        $this->data['entry_skip_confirm'] = $this->language->get('entry_skip_confirm');
        $this->data['entry_confirm_order'] = $this->language->get('entry_confirm_order');
        $this->data['entry_senditem'] = $this->language->get('entry_senditem');
        $this->data['entry_landing'] = $this->language->get('entry_landing');
        $this->data['entry_total'] = $this->language->get('entry_total');
        $this->data['entry_total_over'] = $this->language->get('entry_total_over');
        $this->data['entry_order_status'] = $this->language->get('entry_order_status');
        $this->data['entry_order_status_complete'] = $this->language->get('entry_order_status_complete');
        $this->data['entry_order_status_refunded'] = $this->language->get('entry_order_status_refunded');
        $this->data['entry_order_status_voided'] = $this->language->get('entry_order_status_voided');
        $this->data['entry_enableincheckout'] = $this->language->get('entry_enableincheckout');
        $this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');

        $this->data['help_encryption'] = $this->language->get('help_encryption');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');

        $this->data['tab_general'] = $this->language->get('tab_general');

        $this->load->language('setting/setting');
        $this->data['text_image_manager'] = $this->language->get('text_image_manager');
        $this->data['text_browse'] = $this->language->get('text_browse');
        $this->data['text_clear'] = $this->language->get('text_clear');

        $this->data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }
        if (isset($this->error['username'])) {
            $this->data['error_username'] = $this->error['username'];
        } else {
            $this->data['error_username'] = '';
        }
        if (isset($this->error['password'])) {
            $this->data['error_password'] = $this->error['password'];
        } else {
            $this->data['error_password'] = '';
        }
        if (isset($this->error['signature'])) {
            $this->data['error_signature'] = $this->error['signature'];
        } else {
            $this->data['error_signature'] = '';
        }
        foreach ($languages as $language) {
            if (isset($this->error['error_title_' . $language['language_id']])) {
                $this->data['error_title_' . $language['language_id']] = $this->error['error_title_' . $language['language_id']];
            } else {
                $this->data['error_title_' . $language['language_id']] = '';
            }
        }

        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_payment'),
            'href' => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('payment/paypal_express_credit', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['action'] = $this->url->link('payment/paypal_express_credit', 'token=' . $this->session->data['token'], 'SSL');

        $this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->post['paypal_express_credit_username'])) {
            $this->data['paypal_express_credit_username'] = $this->request->post['paypal_express_credit_username'];
        } else {
            $this->data['paypal_express_credit_username'] = $this->config->get('paypal_express_credit_username');
        }

        if (isset($this->request->post['paypal_express_credit_password'])) {
            $this->data['paypal_express_credit_password'] = $this->request->post['paypal_express_credit_password'];
        } else {
            $this->data['paypal_express_credit_password'] = $this->config->get('paypal_express_credit_password');
        }

        if (isset($this->request->post['paypal_express_credit_signature'])) {
            $this->data['paypal_express_credit_signature'] = $this->request->post['paypal_express_credit_signature'];
        } else {
            $this->data['paypal_express_credit_signature'] = $this->config->get('paypal_express_credit_signature');
        }

        if (isset($this->request->post['paypal_express_credit_username_test'])) {
            $this->data['paypal_express_credit_username_test'] = $this->request->post['paypal_express_credit_username_test'];
        } else {
            $this->data['paypal_express_credit_username_test'] = $this->config->get('paypal_express_credit_username_test');
        }

        if (isset($this->request->post['paypal_express_credit_password_test'])) {
            $this->data['paypal_express_credit_password_test'] = $this->request->post['paypal_express_credit_password_test'];
        } else {
            $this->data['paypal_express_credit_password_test'] = $this->config->get('paypal_express_credit_password_test');
        }

        if (isset($this->request->post['paypal_express_credit_signature_test'])) {
            $this->data['paypal_express_credit_signature_test'] = $this->request->post['paypal_express_credit_signature_test'];
        } else {
            $this->data['paypal_express_credit_signature_test'] = $this->config->get('paypal_express_credit_signature_test');
        }

	foreach ($languages as $language) {
            if (isset($this->request->post['paypal_express_credit_title_' . $language['language_id']])) {
                $this->data['paypal_express_credit_title_' . $language['language_id']] = $this->request->post['paypal_express_credit_title_' . $language['language_id']];
            } elseif ($this->config->get('paypal_express_credit_title_' . $language['language_id'])) {
                $this->data['paypal_express_credit_title_' . $language['language_id']] = $this->config->get('paypal_express_credit_title_' . $language['language_id']);
            } elseif ($this->config->get('paypal_express_credit_title_' . $language['code'])) {
                $this->data['paypal_express_credit_title_' . $language['language_id']] = $this->config->get('paypal_express_credit_title_' . $language['code']);
            } else {
                $locales = explode(',', $language['locale']);
                $locale = 'en_US';
                if (isset($locales['1'])) {
                    $locale = $locales['1'];
                }
                $this->data['paypal_express_credit_title_' . $language['language_id']] = '<img src="https://www.paypal.com/' . $locale . '/i/bnr/horizontal_solution_PP.gif" alt="PayPal Express Checkout" />';
            }
        }

        if (isset($this->request->post['paypal_express_credit_test'])) {
            $this->data['paypal_express_credit_test'] = $this->request->post['paypal_express_credit_test'];
        } else {
            $this->data['paypal_express_credit_test'] = $this->config->get('paypal_express_credit_test');
        }

        if (isset($this->request->post['paypal_express_credit_method'])) {
            $this->data['paypal_express_credit_method'] = $this->request->post['paypal_express_credit_method'];
        } else {
            $this->data['paypal_express_credit_method'] = $this->config->get('paypal_express_credit_method');
        }

        if (isset($this->request->post['paypal_express_credit_logo'])) {
            $this->data['paypal_express_credit_logo'] = $this->request->post['paypal_express_credit_logo'];
		} elseif ($this->config->get('paypal_express_credit_logo')) {
            $this->data['paypal_express_credit_logo'] = $this->config->get('paypal_express_credit_logo');
        } else {
            $this->data['paypal_express_credit_logo'] = $this->config->get('config_logo');
        }

	$this->load->model('tool/image');
	$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
	if ($this->data['paypal_express_credit_logo'] && file_exists(DIR_IMAGE . $this->data['paypal_express_credit_logo']) && is_file(DIR_IMAGE . $this->data['paypal_express_credit_logo'])) {
            $this->data['logo'] = $this->model_tool_image->resize($this->data['paypal_express_credit_logo'], 100, 100);
        } else {
            $this->data['logo'] = $this->data['no_image'];
        }

        if (isset($this->request->post['paypal_express_credit_createaccount'])) {
            $this->data['paypal_express_credit_createaccount'] = $this->request->post['paypal_express_credit_createaccount'];
        } elseif ($this->config->get('paypal_express_credit_createaccount')) {
            $this->data['paypal_express_credit_createaccount'] = $this->config->get('paypal_express_credit_createaccount');
        } else {
            $this->data['paypal_express_credit_createaccount'] = 'customer';
        }

        if (isset($this->request->post['paypal_express_credit_senditem'])) {
            $this->data['paypal_express_credit_senditem'] = $this->request->post['paypal_express_credit_senditem'];
	} elseif ($this->config->get('paypal_express_credit_send_address')) {
            $this->data['paypal_express_credit_send_address'] = $this->config->get('paypal_express_credit_send_address');
        } else {
            $this->data['paypal_express_credit_send_address'] = 'shipping';
        }

        if (isset($this->request->post['paypal_express_credit_merchant_country'])) {
            $this->data['paypal_express_credit_merchant_country'] = $this->request->post['paypal_express_credit_merchant_country'];
        } elseif ($this->config->get('paypal_express_credit_merchant_country')) {
            $this->data['paypal_express_credit_merchant_country'] = $this->config->get('paypal_express_credit_merchant_country');
        } else {
            $this->data['paypal_express_credit_merchant_country'] = '';
        }

	$this->load->model('localisation/country');
        $countries = $this->model_localisation_country->getCountries();
        $this->data['paypal_express_credit_merchant_countries'] = array();
        $this->data['paypal_express_credit_merchant_countries']['he_IL'] = 'All Country (Hebrew Default Language)';
	foreach ($countries as $country) {
            $this->data['paypal_express_credit_merchant_countries'][$country['iso_code_2']] = $country['name'];
            if ($country['iso_code_2'] == 'DK') {
                $this->data['paypal_express_credit_merchant_countries']['da_DK'] = $country['name'] . ' (Danish Default Language)';
            } elseif ($country['iso_code_2'] == 'ID') {
                $this->data['paypal_express_credit_merchant_countries']['id_ID'] = $country['name'] . ' (Indonesian Default Language)';
            } elseif ($country['iso_code_2'] == 'JP') {
                $this->data['paypal_express_credit_merchant_countries']['jp_JP'] = $country['name'] . ' (Japanese Default Language)';
            } elseif ($country['iso_code_2'] == 'NO') {
                $this->data['paypal_express_credit_merchant_countries']['no_NO'] = $country['name'] . ' (Norwegian Default Language)';
            } elseif ($country['iso_code_2'] == 'BR') {
                $this->data['paypal_express_credit_merchant_countries']['pt_BR_' . $country['iso_code_2']] = $country['name'] . ' (Brazilian Portuguese Default Language)';
            } elseif ($country['iso_code_2'] == 'PT') {
                $this->data['paypal_express_credit_merchant_countries']['pt_BR_' . $country['iso_code_2']] = $country['name'] . ' (Brazilian Portuguese Default Language)';
            } elseif ($country['iso_code_2'] == 'LT') {
                $this->data['paypal_express_credit_merchant_countries']['ru_RU_' . $country['iso_code_2']] = $country['name'] . ' (Russian Default Language)';
            } elseif ($country['iso_code_2'] == 'LV') {
                $this->data['paypal_express_credit_merchant_countries']['ru_RU_' . $country['iso_code_2']] = $country['name'] . ' (Russian Default Language)';
            } elseif ($country['iso_code_2'] == 'UA') {
                $this->data['paypal_express_credit_merchant_countries']['ru_RU_' . $country['iso_code_2']] = $country['name'] . ' (Russian Default Language)';
            } elseif ($country['iso_code_2'] == 'SE') {
                $this->data['paypal_express_credit_merchant_countries']['sv_SE'] = $country['name'] . ' (Swedish Default Language)';
            } elseif ($country['iso_code_2'] == 'TH') {
                $this->data['paypal_express_credit_merchant_countries']['th_TH'] = $country['name'] . ' (Thai Default Language)';
            } elseif ($country['iso_code_2'] == 'TR') {
                $this->data['paypal_express_credit_merchant_countries']['tr_TR'] = $country['name'] . ' (Turkish Default Language)';
            } elseif ($country['iso_code_2'] == 'CN') {
                $this->data['paypal_express_credit_merchant_countries']['zh_CN'] = $country['name'] . ' (Simplified Chinese Default Language)';
            } elseif ($country['iso_code_2'] == 'HK') {
                $this->data['paypal_express_credit_merchant_countries']['zh_HK'] = $country['name'] . ' (Traditional Chinese Default Language)';
            } elseif ($country['iso_code_2'] == 'TW') {
                $this->data['paypal_express_credit_merchant_countries']['zh_TW'] = $country['name'] . ' (Traditional Chinese Default Language)';
            }
        }

	if (isset($this->request->post['paypal_express_credit_default_currency'])) {
            $this->data['paypal_express_credit_default_currency'] = $this->request->post['paypal_express_credit_default_currency'];
        } elseif ($this->config->get('paypal_express_credit_default_currency')) {
            $this->data['paypal_express_credit_default_currency'] = $this->config->get('paypal_express_credit_default_currency');
        } else {
            $this->data['paypal_express_credit_default_currency'] = 'USD';
        }

	$this->load->model('localisation/currency');
        $currencies = $this->model_localisation_currency->getCurrencies();
        $this->data['paypal_express_credit_default_currencies'] = array();
		foreach ($currencies as $currency) {
            if (in_array($currency['code'], $this->_supportedCurrencyCodes)) {
                $this->data['paypal_express_credit_default_currencies'][$currency['code']] = $currency['title'] . ' [' . $currency['code'] . ']';
            }
        }

	if (isset($this->request->post['paypal_express_credit_skip_confirm'])) {
            $this->data['paypal_express_credit_skip_confirm'] = $this->request->post['paypal_express_credit_skip_confirm'];
        } elseif ($this->config->get('paypal_express_credit_skip_confirm')) {
            $this->data['paypal_express_credit_skip_confirm'] = $this->config->get('paypal_express_credit_skip_confirm');
        } else {
            $this->data['paypal_express_credit_skip_confirm'] = '0';
        }

	if (isset($this->request->post['paypal_express_credit_confirm_order'])) {
            $this->data['paypal_express_credit_confirm_order'] = $this->request->post['paypal_express_credit_confirm_order'];
        } elseif ($this->config->get('paypal_express_credit_confirm_order')) {
            $this->data['paypal_express_credit_confirm_order'] = $this->config->get('paypal_express_credit_confirm_order');
        } else {
            $this->data['paypal_express_credit_confirm_order'] = '0';
        }

	if (isset($this->request->post['paypal_express_credit_senditem'])) {
            $this->data['paypal_express_credit_senditem'] = $this->request->post['paypal_express_credit_senditem'];
        } else {
            $this->data['paypal_express_credit_senditem'] = $this->config->get('paypal_express_credit_senditem');
        }

	if (isset($this->request->post['paypal_express_credit_landing'])) {
            $this->data['paypal_express_credit_landing'] = $this->request->post['paypal_express_credit_landing'];
        } else {
            $this->data['paypal_express_credit_landing'] = $this->config->get('paypal_express_credit_landing');
        }

        if (isset($this->request->post['paypal_express_credit_total'])) {
            $this->data['paypal_express_credit_total'] = $this->request->post['paypal_express_credit_total'];
        } else {
            $this->data['paypal_express_credit_total'] = $this->config->get('paypal_express_credit_total');
        }

        if (isset($this->request->post['paypal_express_credit_total_over'])) {
            $this->data['paypal_express_credit_total_over'] = $this->request->post['paypal_express_credit_total_over'];
        } else {
            $this->data['paypal_express_credit_total_over'] = $this->config->get('paypal_express_credit_total_over');
        }

        if (isset($this->request->post['paypal_express_credit_order_status_id'])) {
            $this->data['paypal_express_credit_order_status_id'] = $this->request->post['paypal_express_credit_order_status_id'];
        } elseif ($this->config->get('paypal_express_credit_order_status_id')) {
            $this->data['paypal_express_credit_order_status_id'] = $this->config->get('paypal_express_credit_order_status_id');
        } else {
            $this->data['paypal_express_credit_order_status_id'] = '1';
        }

        if (isset($this->request->post['paypal_express_credit_order_status_id_complete'])) {
            $this->data['paypal_express_credit_order_status_id_complete'] = $this->request->post['paypal_express_credit_order_status_id_complete'];
        } elseif ($this->config->get('paypal_express_credit_order_status_id_complete')) {
            $this->data['paypal_express_credit_order_status_id_complete'] = $this->config->get('paypal_express_credit_order_status_id_complete');
        } else {
            $this->data['paypal_express_credit_order_status_id_complete'] = '2';
        }

        if (isset($this->request->post['paypal_express_credit_order_status_id_refunded'])) {
            $this->data['paypal_express_credit_order_status_id_refunded'] = $this->request->post['paypal_express_credit_order_status_id_refunded'];
        } elseif ($this->config->get('paypal_express_credit_order_status_id_refunded')) {
            $this->data['paypal_express_credit_order_status_id_refunded'] = $this->config->get('paypal_express_credit_order_status_id_refunded');
        } else {
            $this->data['paypal_express_credit_order_status_id_refunded'] = '11';
        }

        if (isset($this->request->post['paypal_express_credit_order_status_id_voided'])) {
            $this->data['paypal_express_credit_order_status_id_voided'] = $this->request->post['paypal_express_credit_order_status_id_voided'];
        } elseif ($this->config->get('paypal_express_credit_order_status_id_voided')) {
            $this->data['paypal_express_credit_order_status_id_voided'] = $this->config->get('paypal_express_credit_order_status_id_voided');
        } else {
            $this->data['paypal_express_credit_order_status_id_voided'] = '7';
        }

        $this->load->model('localisation/order_status');

        $this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        if (isset($this->request->post['paypal_express_credit_enableincheckout'])) {
            $this->data['paypal_express_credit_enableincheckout'] = $this->request->post['paypal_express_credit_enableincheckout'];
        } else {
            $this->data['paypal_express_credit_enableincheckout'] = $this->config->get('paypal_express_credit_enableincheckout');
        }

        if (isset($this->request->post['paypal_express_credit_geo_zone_id'])) {
            $this->data['paypal_express_credit_geo_zone_id'] = $this->request->post['paypal_express_credit_geo_zone_id'];
        } else {
            $this->data['paypal_express_credit_geo_zone_id'] = $this->config->get('paypal_express_credit_geo_zone_id');
        }

        $this->load->model('localisation/geo_zone');

        $this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

        if (isset($this->request->post['paypal_express_credit_status'])) {
            $this->data['paypal_express_credit_status'] = $this->request->post['paypal_express_credit_status'];
        } else {
            $this->data['paypal_express_credit_status'] = $this->config->get('paypal_express_credit_status');
        }

        if (isset($this->request->post['paypal_express_credit_sort_order'])) {
            $this->data['paypal_express_credit_sort_order'] = $this->request->post['paypal_express_credit_sort_order'];
        } else {
            $this->data['paypal_express_credit_sort_order'] = $this->config->get('paypal_express_credit_sort_order');
        }

        $this->data['languages'] = $languages;

        $this->template = 'payment/paypal_express_credit.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );
        $this->response->setOutput($this->render());
    }

    private function validate() {
        if (!$this->user->hasPermission('modify', 'payment/paypal_express_credit')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!@$this->request->post['paypal_express_credit_username']) {
            $this->error['username'] = $this->language->get('error_username');
        }

        if (!@$this->request->post['paypal_express_credit_password']) {
            $this->error['password'] = $this->language->get('error_password');
        }

        if (!@$this->request->post['paypal_express_credit_signature']) {
            $this->error['signature'] = $this->language->get('error_signature');
        }

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        foreach ($languages as $language) {
            if (!$this->request->post['paypal_express_credit_title_' . $language['language_id']]) {
                $this->error['error_title_' . $language['language_id']] = $this->language->get('error_title');
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function checkVersion() {
        $url = 'http://www.webprojectsol.com/extern/CheckVersion.php?code=' . $this->code . '&version=' . $this->version;
        if (function_exists('curl_init')) {
            $c = @curl_init($url);
            @curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($c);
        } elseif (@ini_get('allow_url_fopen') == '1') {
            $result = file_get_contents($url);
        }
        if (isset($result)) {
            if (version_compare($result, $this->version, '>')) {
                echo 'We are happy to announce the availability of PayPal Express Checkout for OpenCart "' . $result . '" version for download and upgrade. Your version is "' . $this->version . '".';
            }
        }
    }

    private function checkOpenCartVersion() {
        if (version_compare(VERSION, '1.5.1', '>=') && version_compare(VERSION, '1.5.2', '<')) {
            return true;
        }
    }
    public function RefundTransaction() {
        if (!$this->config->get('paypal_express_credit_test')) {
            $curl = 'https://api-3t.paypal.com/nvp';
            $paypal_url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout';
            $p_user = $this->config->get('paypal_express_credit_username');
            $p_pwd = $this->config->get('paypal_express_credit_password');
            $p_signature = $this->config->get('paypal_express_credit_signature');
        } else {
            $curl = 'https://api-3t.sandbox.paypal.com/nvp';
            $paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout';
            $p_user = $this->config->get('paypal_express_credit_username_test');
            $p_pwd = $this->config->get('paypal_express_credit_password_test');
            $p_signature = $this->config->get('paypal_express_credit_signature_test');
        }

        #Check for supported currency, otherwise convert to USD.
        $currencies = array('AUD', 'CAD', 'CZK', 'DKK', 'EUR', 'HUF', 'JPY', 'NOK', 'NZD', 'PLN', 'GBP', 'SGD', 'SEK', 'CHF', 'USD');
        if (in_array($this->request->post['currency'], $currencies)) {
            $currency = $this->request->post['currency'];
        } else {
            $currency = 'USD';
        }

        $request = 'USER=' . $p_user;
        $request .= '&PWD=' . $p_pwd;
        $request .= '&VERSION=85.0';
        $request .= '&SIGNATURE=' . $p_signature;
        $request .= '&METHOD=RefundTransaction';
        $request .= '&TRANSACTIONID=' . trim(urlencode($this->request->post['transaction_id']));
        $request .= '&REFUNDTYPE=' . trim(urlencode($this->request->post['refund_type']));
        $request .= '&AMT=' . trim(urlencode($this->request->post['amount']));
        $request .= '&CURRENCYCODE=' . trim(urlencode($currency));
        $request .= '&NOTE=' . trim(urlencode($this->request->post['paypal_express_credit_note']));

        $response = $this->sendTransactionToGateway($curl, $request);

        $response_data = array();
        parse_str($response, $response_data);

        $json = array();
        if (($response_data['ACK'] == 'Success') || ($response_data['ACK'] == 'SuccessWithWarning')) {
            $this->load->language('payment/paypal_express');
            $json['success'] = $this->language->get('success_refunded');
        } else {
            $json['error'] = '';
            for ($i = 0; $i < 10; ++$i) {
                if (isset($response_data['L_ERRORCODE' . $i])) {
                    if (isset($response_data['L_SEVERITYCODE' . $i]) && isset($response_data['L_ERRORCODE' . $i]) && isset($response_data['L_LONGMESSAGE' . $i])) {
                        $json['error'] .= $response_data['L_SEVERITYCODE' . $i] . ': ' . $response_data['L_ERRORCODE' . $i] . ' - ' . $response_data['L_LONGMESSAGE' . $i] . '<br />';
                    }
                } else {
                    $i = 10;
                }
            }
        }
        $this->response->setOutput(json_encode($json));
    }
    
    public function DoCapture() {
        if (!$this->config->get('paypal_express_credit_test')) {
            $curl = 'https://api-3t.paypal.com/nvp';
            $paypal_url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout';
            $p_user = $this->config->get('paypal_express_credit_username');
            $p_pwd = $this->config->get('paypal_express_credit_password');
            $p_signature = $this->config->get('paypal_express_credit_signature');
        } else {
            $curl = 'https://api-3t.sandbox.paypal.com/nvp';
            $paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout';
            $p_user = $this->config->get('paypal_express_credit_username_test');
            $p_pwd = $this->config->get('paypal_express_credit_password_test');
            $p_signature = $this->config->get('paypal_express_credit_signature_test');
        }

        #Check for supported currency, otherwise convert to USD.
        $currencies = array('AUD', 'CAD', 'CZK', 'DKK', 'EUR', 'HUF', 'JPY', 'NOK', 'NZD', 'PLN', 'GBP', 'SGD', 'SEK', 'CHF', 'USD');
        if (in_array($this->request->post['currency'], $currencies)) {
            $currency = $this->request->post['currency'];
        } else {
            $currency = 'USD';
        }

        $request = 'USER=' . $p_user;
        $request .= '&PWD=' . $p_pwd;
        $request .= '&VERSION=85.0';
        $request .= '&SIGNATURE=' . $p_signature;
        $request .= '&METHOD=DoCapture';
        $request .= '&AUTHORIZATIONID=' . trim(urlencode($this->request->post['transaction_id']));
        $request .= '&COMPLETETYPE=' . trim(urlencode($this->request->post['capture_type']));
        $request .= '&AMT=' . trim(urlencode($this->request->post['amount']));
        $request .= '&CURRENCYCODE=' . trim(urlencode($currency));
        $request .= '&NOTE=' . trim(urlencode($this->request->post['paypal_express_credit_note']));

        $response = $this->sendTransactionToGateway($curl, $request);

        $response_data = array();
        parse_str($response, $response_data);

        $json = array();
        if (($response_data['ACK'] == 'Success') || ($response_data['ACK'] == 'SuccessWithWarning')) {
            $this->load->language('payment/paypal_express');
            $json['success'] = $this->language->get('success_captured');

            $message = '';
            if (isset($response_data['TRANSACTIONID'])) {
                $message .= 'TRANSACTIONID: ' . $response_data['TRANSACTIONID'] . "\n";
            }
            if (isset($response_data['PAYMENTSTATUS'])) {
                $message .= 'PAYPAL STATUS: ' . $response_data['PAYMENTSTATUS'] . "\n";
            }

            if ($message) {
                $json['comment'] = $message . "\n";
            }
        } else {
            $json['error'] = '';
            for ($i = 0; $i < 10; ++$i) {
                if (isset($response_data['L_ERRORCODE' . $i])) {
                    if (isset($response_data['L_SEVERITYCODE' . $i]) && isset($response_data['L_ERRORCODE' . $i]) && isset($response_data['L_LONGMESSAGE' . $i])) {
                        $json['error'] .= $response_data['L_SEVERITYCODE' . $i] . ': ' . $response_data['L_ERRORCODE' . $i] . ' - ' . $response_data['L_LONGMESSAGE' . $i] . '<br />';
                    }
                } else {
                    $i = 10;
                }
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function DoVoid() {
        if (!$this->config->get('paypal_express_credit_test')) {
            $curl = 'https://api-3t.paypal.com/nvp';
            $paypal_url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout';
            $p_user = $this->config->get('paypal_express_credit_username');
            $p_pwd = $this->config->get('paypal_express_credit_password');
            $p_signature = $this->config->get('paypal_express_credit_signature');
        } else {
            $curl = 'https://api-3t.sandbox.paypal.com/nvp';
            $paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout';
            $p_user = $this->config->get('paypal_express_credit_username_test');
            $p_pwd = $this->config->get('paypal_express_credit_password_test');
            $p_signature = $this->config->get('paypal_express_credit_signature_test');
        }

        $request = 'USER=' . $p_user;
        $request .= '&PWD=' . $p_pwd;
        $request .= '&VERSION=85.0';
        $request .= '&SIGNATURE=' . $p_signature;
        $request .= '&METHOD=DoVoid';
        $request .= '&AUTHORIZATIONID=' . trim(urlencode($this->request->post['transaction_id']));
        $request .= '&NOTE=' . trim(urlencode($this->request->post['paypal_express_credit_note']));

        $response = $this->sendTransactionToGateway($curl, $request);

        $response_data = array();
        parse_str($response, $response_data);

        $json = array();
        if (($response_data['ACK'] == 'Success') || ($response_data['ACK'] == 'SuccessWithWarning')) {
            $this->load->language('payment/paypal_express');
            $json['success'] = $this->language->get('success_voided');
        } else {
            $json['error'] = '';
            for ($i = 0; $i < 10; ++$i) {
                if (isset($response_data['L_ERRORCODE' . $i])) {
                    if (isset($response_data['L_SEVERITYCODE' . $i]) && isset($response_data['L_ERRORCODE' . $i]) && isset($response_data['L_LONGMESSAGE' . $i])) {
                        $json['error'] .= $response_data['L_SEVERITYCODE' . $i] . ': ' . $response_data['L_ERRORCODE' . $i] . ' - ' . $response_data['L_LONGMESSAGE' . $i] . '<br />';
                    }
                } else {
                    $i = 10;
                }
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    private function sendTransactionToGateway($url, $parameters) {
        $server = parse_url($url);

        if (!isset($server['port'])) {
            $server['port'] = ($server['scheme'] == 'https') ? 443 : 80;
        }

        if (!isset($server['path'])) {
            $server['path'] = '/';
        }

        if (function_exists('curl_init')) {
            $curl = curl_init($server['scheme'] . '://' . $server['host'] . $server['path'] . (isset($server['query']) ? '?' . $server['query'] : ''));
            curl_setopt($curl, CURLOPT_PORT, $server['port']);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);

            $result = curl_exec($curl);

            if (!isset($result) || !$result) {
                $this->log->write('PayPal Express Checkout Request failed: ' . curl_error($curl) . '(' . curl_errno($curl) . ')');
                curl_close($curl);
                return false;
            } else {
                curl_close($curl);
            }
        } else {
            $this->log->write('PayPal Express Checkout Request failed: Your server doesn\'t support the curl functions, it\'s need to work correctly');
            $result = '';
        }

        return $result;
    }

}

?>