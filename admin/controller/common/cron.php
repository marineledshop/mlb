<?php
require_once(DIR_CATALOG.'tcpdf/tcpdf_class.php');

class ControllerCommonCron extends Controller {

	public function index() {
		echo 'Cron job';
	}

	public function update_tracking($dateRangeType = 'Confirm') {
		$params = array(
			'BeginDate' => date('c', time() - 30*24*60*60), //30days before for test
			'EndDate' => date('c', time() + 2*24*60*60),
			'DateRangeType' => $dateRangeType,
		);
		$client = new Soap_3PL();
		$result = $client->FindOrders($params, 1000);

		if (isset($result['totalOrders']) && $result['totalOrders'] > 0) {
			$to = $result['totalOrders'];
			$this->load->model('sale/order');
            $onumbers = ''; //String with orders numbers which updated

			$result = new SimpleXMLElement($result['FindOrdersResult']);

			foreach ($result->order as $order) {

                $is_shipped = false;
                $onum = (string)$order->ReferenceNum;

                if (!is_numeric($onum)) continue; //For email orders that has no our system OrderID

				$statuses = $this->model_sale_order->getOrderHistories($onum);

                //Checking is order has Shipped status or not
                if (!empty($statuses)) {
                    foreach ($statuses as $status) {
                        if ($status['status'] == 'Shipped') $is_shipped = true;
                    }
                }

				if (!$is_shipped) {
					$res = $this->model_sale_order->createInvoiceNo($onum, (string)$order->TrackingNumber,  array('order_status_id'=>'3', 'notify'=>'1', 'comment'=>'Tracking# '.(string)$order->TrackingNumber));
					$this->sendPdfInvoice($onum, '');
                    $onumbers = $onum . ', ';
                    $res = $this->model_sale_order->addOrderHistory($onum, array('order_status_id'=>'3', 'notify'=>'0', 'comment'=>'Tracking# '.(string)$order->TrackingNumber));
				}
			}

            if (!empty($onumbers)) {
                global $log;
                $log->write('Cron successfully syncronized at ' . date('Y-i-d H:i:s') . '  Updated orders: ' . $onumbers);

                $this->check_stock();
            } else {
                echo 'Cron syncronized at ' . date('Y-i-d H:i:s') . '. Nothing was changed.';
            }
		}
		if (isset($this->session->data['token'])) {
			$this->redirect($this->url->link('sale/order', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}

	public function check_stock() {
        $echo = array();
        $client = new Soap_3PL();
		$this->load->model('catalog/product');
		$result = $client->ReportStockStatus();

        if ($result) {
			$products = $this->model_catalog_product->getProductQtyArray();
			$result = new SimpleXMLElement($result);

			foreach ($result->Q as $q) {
                $model = (string)$q->SKU;

                if (isset($q->SUMOFAVAILABLE) && !empty($q->SUMOFAVAILABLE)) {
                    $qty = (int)$q->SUMOFAVAILABLE;
                } else if (isset($q->CARTONS) && !empty($q->CARTONS)) {
                    $qty = (int)$q->CARTONS;
                } else if (isset($q->SUMOFONHAND) && !empty($q->SUMOFONHAND)) {
                    $qty = (int)$q->SUMOFONHAND;
                } else {
                    $qty = 0;
                }
				$available = (int)$q->HASAVAILABLE;

				if (array_key_exists($model, $products)) {
                    if (isset($products[$model]['pid']) && $qty != $products[$model]['qty']) {
                        $res = $this->model_catalog_product->UpdateProductQuantity($products[$model]['pid'], $qty);
                        $up = '';
                        if ($res === true) {
                            $up = '. Quantity syncronized. ID='.$products[$model]['pid'];
                        }
						$echo[] = $model . ' - Opencart qty='.$products[$model]['qty'].' not equal Warehouse qty='.$qty.$up;
					} elseif (!isset($products[$model]['pid'])) {
                        $echo[] = $model . ' - Has no quantity in Opencart or has quantity in options';
                    }
				} else {
					$echo[] = 'WARNING >>> ' . $model . ' - Not exists in Opencart';
				}
			}

            if (!empty($echo)) {
                $client->sendEmail(implode("\n<br>", $echo), 'Warehouse syncronization');
            }
		}
        return $echo;
	}

    public function update_stock() {
        $echo = $this->check_stock();
        echo 'Syncronization finished';
        echo '<br>Email about sincronization result was sent to admin';
        echo '<pre>'; var_dump($echo); echo '</pre>';
    }

	private function sendPdfInvoice($order_id='', $no_email='') {
        require_once DIR_CATALOG.'tcpdf/pdf_invoice.php';
	}

	public function test_api() {
		$client = new Soap_3PL();
		$client->sendEmail('This is test message from API !', 'TEST Cron API email');

		echo 'Test email was sent.';
	}

    public function test_pdf() {
        $this->sendPdfInvoice('3', '');
        echo 'PDF Invoice sent';
    }

    //Image synchronizer from dev5 to dev3
    public function productim() {
        exit();
        $query = $this->db->query("SELECT product_id, model FROM oc_product ORDER BY model ASC");
        $dev3 = $query->rows;

        $query = $this->db->query("SELECT product_id, image, model FROM product_dev5 ORDER BY model ASC");
        $dev5 = array();

        //Product images table
        //foreach ($query->rows as $row) {
        //    $dev5[$row['model']] = $row['product_id'];
        //}
        //
        //foreach ($dev3 as $row) {
        //    $i = 0;
        //    if ($dev5[$row['model']]) {
        //        $query = $this->db->query("SELECT * FROM product_image_dev5 WHERE product_id=" . (int)$dev5[$row['model']] . " ORDER BY sort_order ASC");
        //        $images = $query->rows;
        //        if ($images) {
        //            $this->db->query("DELETE FROM oc_product_image WHERE product_id=" . (int)$row['product_id']);
        //            foreach ($images as $image) {
        //                $i++;
        //                $sql = "INSERT INTO oc_product_image SET product_id=" . (int)$row['product_id'] . ", sort_order=" . (int)$image['sort_order'] . ", image='" . $this->db->escape($image['image']) . "'";
        //                //echo '<pre>'; var_dump($sql); echo '</pre>';
        //                $res = $this->db->query($sql);
        //                echo '<pre>'; var_dump($image); echo '</pre>';
        //                echo '-==-=--==-=-=-=-=-=-=-=-';
        //            }
        //        }
        //    }
        //    echo 'Copied ' . $i . ' images';
        //}

        //Main images
        foreach ($query->rows as $row) {
            $dev5[$row['model']] = $row['image'];
        }

        foreach ($dev3 as $row) {
            $i = 0;
            if ($dev5[$row['model']]) {
                $i++;
                $sql = "UPDATE oc_product SET image='" . $this->db->escape($dev5[$row['model']]) . "' WHERE model = '" . $row['model'] . "'";
                $res = $this->db->query($sql);
            }
        }
        echo 'Copied ' . $i . ' images';
    }

    //Image copier for video
    public function imcopier() {
        exit();
        $query = $this->db->query("SELECT p.product_id, p.model, pi.image, pi.product_image_id FROM oc_product p LEFT JOIN oc_product_image pi ON p.product_id=pi.product_id ORDER BY p.model ASC");
        $images = $query->rows;
        $i = 0;

        //if ($images) {
        //    foreach ($images as $image) {
        //        $path = DIR_IMAGE . 'forvideo2/'.$image['model'];
        //        if (!file_exists($path)) {
        //            mkdir($path, 0777);
        //        }
        //        $ext = explode('.', $image['image']);
        //        $path = $path . '/' . $image['product_image_id'] . '.' . array_pop($ext);
        //        if (file_exists($path)) unlink($path);
        //        $res = copy(DIR_IMAGE.$image['image'], $path);
        //        if (!$res) {
        //            echo 'Can\'t copy image from ' . DIR_IMAGE.$image['image'] . ' to '. $path . "<br>";
        //        } else {
        //            $i++;
        //        }
        //    }
        //}


        $query = $this->db->query("SELECT p.product_id, p.model, p.image FROM oc_product p ORDER BY p.model ASC");
        $images = $query->rows;

        if ($images) {
            foreach ($images as $image) {
                $path = DIR_IMAGE . 'forvideo2/'.$image['model'];
                if (!file_exists($path)) {
                    mkdir($path, 0777);
                }
                $ext = explode('.', $image['image']);
                $path = $path . '/main-on-site' . '.' . array_pop($ext);
                if (file_exists($path)) unlink($path);
                $res = copy(DIR_IMAGE.$image['image'], $path);
                if (!$res) {
                    echo 'Can\'t copy image from ' . DIR_IMAGE.$image['image'] . ' to '. $path . "<br>";
                } else {
                    $i++;
                }
            }
        }
        echo 'Copied ' . $i . ' images' . "<br>";
    }

    //Brochure synchronizer from dev5 to dev3
    public function syncbro() {
        exit();
        $query = $this->db->query("SELECT product_id, model FROM oc_product ORDER BY model ASC");
        $dev3 = $query->rows;

        $query = $this->db->query("SELECT product_id, brochure, model FROM product_dev5 ORDER BY model ASC");
        $dev5 = array();

        foreach ($query->rows as $row) {
            $dev5[$row['model']] = $row['brochure'];
        }

        $i = 0;
        foreach ($dev3 as $row) {
            if ($dev5[$row['model']]) {
                $i++;
                $sql = "UPDATE oc_product SET product_file='" . $this->db->escape($dev5[$row['model']]) . "' WHERE model = '" . $row['model'] . "'";
                $res = $this->db->query($sql);
            }
        }
        echo 'Copied ' . $i . ' brochures';
    }

    //Product in order sync (old with option assigning with new without but different product_id
    public function syncprod() {

        $query = $this->db->query("SELECT * FROM oc_order_product ORDER BY order_id");
        $ops = $query->rows;

        $i = 0;
        $err = 0;

        foreach ($ops as $op) {
            $query = $this->db->query('SELECT product_id FROM oc_product WHERE model = "' . $op['model'] . '"');
            $newid = $query->row;
            if ($newid) {
                $newid = $newid['product_id'];
                $i++;
            } else {
                $newid = $op['product_id'];
            }

            $sql = 'UPDATE oc_order_product SET product_id = ' . $newid . ' WHERE order_product_id = ' . $op['order_product_id'];
            $res = $this->db->query($sql);

            if (!$res) $err++;

            echo 'Product ID = '.$op['product_id'].'<br>';
            echo 'Model = '.$op['model'].'<br>';
            echo 'New Product ID = '.$newid.'<br>';
            echo 'ID = '.$op['order_product_id'].'<br>';
            echo '<br><br>';
        }

        echo 'Changed ' . $i . ' products' .'<br>';
        echo 'Errors ' . $err;
    }

    //Product MLS-MLB sync
    public function syncprodmlb() {

        $query = $this->db->query("SELECT * FROM oc_product ORDER BY model");
        $prods = $query->rows;

        $f = $nf = 0;
        $err = 0;

        foreach ($prods as $prod) {
            $query = $this->db->query('SELECT * FROM product WHERE model = "' . $prod['model'] . '"');
            $found = $query->row;

            if ($found) {
                //Product found, will edit
                $pid = $found['product_id'];

                echo '--- Found: Product ID = '.$pid.'  Model = '.$found['model'].'<br>';

                //Status change
                $sql = 'UPDATE product SET status = 1 WHERE product_id = ' . (int)$pid;
                $res = $this->db->query($sql);
                if (!$res) {
                    $err++;
                    echo '---> Error: Status not changed.  Product ID = ' . $pid . '  Model = ' . $prod['model'] . '<br>';
                }


                $sql = 'DELETE FROM product_attribute WHERE product_id = ' . (int)$pid;
                $res = $this->db->query($sql);
                if (!$res) {
                    $err++;
                    echo '---> Error: Attributes not deleted.  Product ID = ' . $pid . '  Model = ' . $prod['model'] . '<br>';
                }

                //Add new attributes
                $query = $this->db->query('SELECT * FROM oc_product_attribute WHERE product_id = ' . $prod['product_id']);
                $attrs = $query->rows;

                foreach ($attrs as $attr) {
                    $sql = 'INSERT INTO product_attribute (product_id, attribute_id, language_id, text) VALUES (' . $pid . ',' . $attr['attribute_id'] . ',1,"' . $attr['text'] . '")';
                    $res = $this->db->query($sql);
                    if (!$res) {
                        $err++;
                        echo '---> Error: New attributes not set.  Product ID = ' . $pid . '  Model = ' . $prod['model'] . '<br>';
                    }
                }

                $f++;
            } else {
                //Product not found, will add
                $newid = $prod['product_id'];
                echo '===> NOT Found: Product ID = '.$newid.'  Model = '.$prod['model'].'<br>';

                $this->db->query("INSERT INTO product SET 
                  model = '" . $this->db->escape($prod['model']) . "', 
                  sku = '" . $this->db->escape($prod['sku']) . "', 
                  upc = '" . $this->db->escape($prod['upc']) . "', 
                  location = '" . $this->db->escape($prod['location']) . "', 
                  quantity = '" . (int)$prod['quantity'] . "', 
                  minimum = '" . (int)$prod['minimum'] . "', 
                  subtract = '" . (int)$prod['subtract'] . "', 
                  stock_status_id = '" . (int)$prod['stock_status_id'] . "', 
                  date_available = '" . $this->db->escape($prod['date_available']) . "', 
                  manufacturer_id = '" . (int)$prod['manufacturer_id'] . "', 
                  shipping = '" . (int)$prod['shipping'] . "', 
                  hs_code = '".$prod['hscode']."',
                  image = '".$prod['image']."',
                  thumb = '".$prod['thumb']."',
                  price = '" . (float)$prod['price'] . "', 
                  points = '" . (int)$prod['points'] . "', 
                  box = " . (int)$prod['box'] . ",
                  weight = '" . (float)$prod['weight'] . "',
                  weight_class_id = '" . (int)$prod['weight_class_id'] . "', 
                  length = '" . (float)$prod['length'] . "', 
                  width = '" . (float)$prod['width'] . "', 
                  height = '" . (float)$prod['height'] . "', 
                  length_class_id = '" . (int)$prod['length_class_id'] . "', 
                  status = '" . (int)$prod['status'] . "', 
                  tax_class_id = '" . $this->db->escape($prod['tax_class_id']) . "', 
                  sort_order = '" . (int)$prod['sort_order'] . "', 
                  date_added = NOW(), brochure = '" . $this->db->escape($prod['product_file']) . "'");

                $product_id = $this->db->getLastId();

                //Product Store
                $this->db->query("INSERT INTO product_to_store SET product_id = '" . (int)$product_id . "', store_id = 1");

                //Add new attributes
                $query = $this->db->query('SELECT * FROM oc_product_attribute WHERE product_id = ' . $prod['product_id']);
                $attrs = $query->rows;
                foreach ($attrs as $attr) {
                    $sql = 'INSERT INTO product_attribute (product_id, attribute_id, language_id, text) VALUES (' . $product_id . ',' . $attr['attribute_id'] . ',1,"' . $attr['text'] . '")';
                    $res = $this->db->query($sql);
                }

                //Product Description
                $query = $this->db->query('SELECT * FROM oc_product_description WHERE product_id = ' . $prod['product_id']);
                $res = $query->row;
                $sql = 'INSERT INTO product_description (product_id, name, language_id, description, meta_description, meta_keyword, header_description) VALUES (' . $product_id . ',"' . $res['attribute_id'] . '",1,"' . $res['description'] . '", "' . $res['meta_description'] . '", "' . $res['meta_keyword'] . '", "' . $res['header_description'] . '")';
                $res = $this->db->query($sql);
                if (!$res) {
                    $err++;
                    echo '---> Error: New description not set.  Product ID = ' . $product_id . '  Model = ' . $prod['model'] . '<br>';
                }

                //Add new product images
                $query = $this->db->query('SELECT * FROM oc_product_image WHERE product_id = ' . $prod['product_id']);
                $imgs = $query->rows;
                foreach ($imgs as $img) {
                    $sql = 'INSERT INTO product_image (product_id, sort_order, image) VALUES (' . $product_id . ',' . $img['sort_order'] . ',"' . $img['image'] . '")';
                    $res = $this->db->query($sql);
                    if (!$res) {
                        $err++;
                        echo '---> Error: New image not set.  Product ID = ' . $product_id . '  Model = ' . $prod['model'] . '<br>';
                    }
                }

                echo '       Set Categories for: Product ID = ' . $product_id . ',  (Old id = ' . $prod['product_id'] . ')<br>';
                $nf++;
            }

            //
            //echo 'Product ID = '.$op['product_id'].'<br>';
            //echo 'Model = '.$op['model'].'<br>';
            //echo 'New Product ID = '.$newid.'<br>';
            //echo 'ID = '.$op['order_product_id'].'<br>';
            //echo '<br><br>';
        }

        echo 'Changed ' . $f . ' products' .'<br>';
        echo 'Added ' . $nf . ' products' .'<br>';
        echo 'Errors ' . $err;
    }

    //Product price MLS-MLB sync
    public function syncprice() {

        $query = $this->db->query("SELECT * FROM oc_product ORDER BY model");
        $prods = $query->rows;

        $f = $nf = 0;
        $err = 0;

        foreach ($prods as $prod) {
            $query = $this->db->query('SELECT * FROM product WHERE model = "' . $prod['model'] . '"');
            $found = $query->row;

            if ($found) {
                //Product found, will edit
                $pid = $found['product_id'];

                echo '--- Found: Product ID = '.$pid.'  Model = '.$found['model'].' Price old = ' . $found['price'] . ' - Price new = ' . $prod['price'] . '<br>';

                //Status change
                $sql = 'UPDATE product SET price = ' . $prod['price'] . ' WHERE product_id = ' . (int)$pid;
                $res = $this->db->query($sql);
                if (!$res) {
                    $err++;
                    echo '---> Error: Price not changed.  Product ID = ' . $pid . '  Model = ' . $prod['model'] . '<br>';
                }




                $f++;
            } else {
                $nf++;
                //Product not found
                $newid = $prod['product_id'];
                echo '===> NOT Found: Product ID = '.$newid.'  Model = '.$prod['model'].'<br>';

            }

            //
            //echo 'Product ID = '.$op['product_id'].'<br>';
            //echo 'Model = '.$op['model'].'<br>';
            //echo 'New Product ID = '.$newid.'<br>';
            //echo 'ID = '.$op['order_product_id'].'<br>';
            //echo '<br><br>';
        }

        echo 'Changed ' . $f . ' products' .'<br>';
        echo 'Not found ' . $nf . ' products' .'<br>';
        echo 'Errors ' . $err;
    }
}
