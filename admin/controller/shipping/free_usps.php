<?php
class ControllerShippingFreeUsps extends Controller {
	private $error = array(); 

	public function index() {   
		$this->language->load('shipping/free_usps');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('free_usps', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
		$this->data['text_none'] = $this->language->get('text_none');

		$this->data['entry_total'] = $this->language->get('entry_total');
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_delivery'] = $this->language->get('entry_delivery');
		$this->data['entry_days'] = $this->language->get('entry_days');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_us_only'] = $this->language->get('entry_us_only');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/free_usps', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->data['action'] = $this->url->link('shipping/free_usps', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['free_usps_total'])) {
			$this->data['free_usps_total'] = $this->request->post['free_usps_total'];
		} else {
			$this->data['free_usps_total'] = $this->config->get('free_usps_total');
		}

		if (isset($this->request->post['free_usps_geo_zone_id'])) {
			$this->data['free_usps_geo_zone_id'] = $this->request->post['free_usps_geo_zone_id'];
		} else {
			$this->data['free_usps_geo_zone_id'] = $this->config->get('free_usps_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$this->data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['free_usps_status'])) {
			$this->data['free_usps_status'] = $this->request->post['free_usps_status'];
		} else {
			$this->data['free_usps_status'] = $this->config->get('free_usps_status');
		}

		if (isset($this->request->post['free_usps_sort_order'])) {
			$this->data['free_usps_sort_order'] = $this->request->post['free_usps_sort_order'];
		} else {
			$this->data['free_usps_sort_order'] = $this->config->get('free_usps_sort_order');
		}

		if (isset($this->request->post['free_usps_delivery'])) {
			$this->data['free_usps_delivery'] = $this->request->post['free_usps_delivery'];
		} else {
			$this->data['free_usps_delivery'] = $this->config->get('free_usps_delivery');
		}

		if (isset($this->request->post['free_usps_us_only'])) {
			$this->data['free_usps_us_only'] = $this->request->post['free_usps_us_only'];
		} else {
			$this->data['free_usps_us_only'] = $this->config->get('free_usps_us_only');
		}

		$this->template = 'shipping/free_usps.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/free_usps')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>