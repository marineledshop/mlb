dir=`dirname $0`
mkdir $dir/system/cache/
chmod -R 777 $dir/system/cache/

mkdir $dir/system/logs/ 
chmod -R 777 $dir/system/logs/

mkdir $dir/vqmod/vqcache
chmod -R 777 $dir/vqmod/vqcache

touch $dir/vqmod/vqmod.log
chmod 777 $dir/vqmod/vqmod.log

mkdir $dir/catalog/tcpdf/cache
chmod -R 777 $dir/catalog/tcpdf/cache

mkdir $dir/image/ 
chmod -R 777 $dir/image/

mkdir $dir/image/cache 
chmod -R 777 $dir/image/cache 

mkdir $dir/image/cache/data
chmod -R 777 $dir/image/cache/data

mkdir $dir/download/
chmod -R 777 $dir/download/

mkdir $dir/pdf
chmod -R 777 $dir/pdf
