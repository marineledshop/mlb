#######################################################

 Magic Zoom�
 OpenCart module version v2.8.25 [v1.1.13:v4.0.5]
 
 www.magictoolbox.com
 support@magictoolbox.com

 Copyright 2012 Magic Toolbox

#######################################################

INSTALLATION:

NOTE: To manually install Magic Zoom please read 'readme_manual.txt'.

1. Unzip the contents of the zip file and upload them to your OpenCart directory.

2. Go to http://your.site.url/magiczoom/ in your browser and follow the instructions.

3. Open the 'Extensions' page in your OpenCart admin to activate and customize module.

