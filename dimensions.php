<?php
/**
 * User: avasilenko
 * Date: 26.03.12
 * Time: 14:51
 */
require_once(dirname(__FILE__)."/config.php");
require_once(dirname(__FILE__)."/common.php");
require_once(DIR_SYSTEM . 'startup.php');
require_once(DIR_DATABASE . 'mysql.php');
$need_configs = array(
	'config_url',
	'config_ssl',
	'config_customer_group_id',
	'config_language'
);
function dimensions($diameter, $length, $width, $height, $text_by, $dimension) {
    if ($diameter > 0) {
        $dimensions = "&oslash;{$diameter}$dimension {$text_by} ";
    } else {
        $dimensions = '';
    }
    if ($length > 0) {
        $dimensions .= "L{$length}$dimension {$text_by} ";
    }
    if ($width > 0) {
        $dimensions .= "W{$width}$dimension {$text_by} ";
    }
    if ($height > 0) {
        $dimensions .= "H{$height}$dimension {$text_by} ";
    }
    return trim($dimensions, "$text_by ");
}

// Config
$config = new Config();
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

// Settings
$query = $db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '0' OR store_id = '" . (int)$config->get('config_store_id') . "' ORDER BY store_id ASC");

foreach ($query->rows as $setting) {
    if (!$setting['serialized']) {
        $config->set($setting['key'], $setting['value']);
    } else {
        $config->set($setting['key'], unserialize($setting['value']));
    }
}

$db->query("DELETE FROM product_attribute WHERE attribute_id = 43 AND language_id = 1");
$products = $db->query("SELECT product_id, width, length, height, diameter FROM product")->rows;
$to_inch = 0.0393700787;
foreach($products as $product) {
    $product['product_id'];
    $length_us = round($product['length'] * $to_inch, 1);
    $width_us = round($product['width'] * $to_inch, 1);
    $height_us = round($product['height'] * $to_inch, 1);
    $diameter_us = round($product['diameter'] * $to_inch, 1);
    $dimension = dimensions(round($product['diameter']), round($product['length']), round($product['width']), round($product['height']), 'X', 'mm');
    $dimension .= '<br />';
    $dimension .= dimensions($diameter_us, $length_us, $width_us, $height_us, 'X', '"');
    $dimension = htmlentities($dimension);
    $db->query("INSERT INTO product_attribute (product_id, attribute_id, language_id, text) VALUES ({$product['product_id']}, 43, 1, '" .$db->escape($dimension). "')");
}
