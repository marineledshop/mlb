<?php

/**
 * Soap_3PL
 *
 * Copyright (c) 2014 by LIAL (lial@inbox.ru)
 *
 * This class using for work with Warehouse that uses 3PL API
 * In our case this is http://apsfulfillment.com/
 *
 * Documentation: https://www.rubyhas.com/wp-content/uploads/2016/04/API-Doc.pdf
 */
class Soap_3PL
{

    private $extLoginData = array(
        'ThreePLKey' => "{a5064a1f-f234-4aab-bd28-5a8d882a5ef8}",
        'FacilityID' => 1,

        // Production account
        'Login'      => 'marineledshop01',
        'Password'   => 'marineledshop01',
        'CustomerID' => 10,
    );

    private $extLoginDataTest = array(
        'ThreePLKey' => "{a5064a1f-f234-4aab-bd28-5a8d882a5ef8}",
        'FacilityID' => 1,

        // Test account
        'Login'      => "marineledshop1",
        'Password'   => "marineledshop1",
        'CustomerID' => 25,
    );

    private $loginData = array(
        'ThreePLID' => 605,
        'Login'     => "marineledshop01",
        'Password'  => "marineledshop01"
    );

    public $client = null;
    const WSDL_URL = 'https://secure-wms.com/webserviceexternal/contracts.asmx?WSDL'; //This is a url to the endpoint, so you may have to hardcode it with a full http address
    const WSDL_FILE = 'wsdl.xml';
    public $soapAction = 'http://www.JOI.com/schemas/ViaSub.WMS/';
    public $options = array();

    function __construct($source = 'url') {
        $this->options['trace'] = true;
        if ($source == 'file') {
            $source = self::WSDL_FILE;
        }
        else {
            $source = self::WSDL_URL;
        }
        $this->client = new SoapClient($source, $this->options);
    }

    function CreateOrders($ArrayOfOrder, $warnings, $customer_email = '') {
        global $log;

        $is_live = IS_LIVE;
        if (!$is_live)
            $this->extLoginData = $this->extLoginDataTest;

        try {
            $result = $this->client->CreateOrders($this->extLoginData, $ArrayOfOrder, $warnings);
            $a = $this->client->__getLastRequest();
            file_put_contents(DIR_ROOT . 'xml/' . $ArrayOfOrder['Order']['TransInfo']['ReferenceNum'] . '_WH_CreateOrder_request.xml', $a);
            $a = $this->client->__getLastResponse();
            file_put_contents(DIR_ROOT . 'xml/' . $ArrayOfOrder['Order']['TransInfo']['ReferenceNum'] . '_WH_CreateOrder_response.xml', $a);
            //echo '<pre>'; var_dump($a); echo '</pre>';
        }
        catch (Exception $e) {
            $err = '3PL (CreateOrders) SOAP Error: - ' . $e->getMessage();
            $err .= "\n\n" . 'Order #: ' . $ArrayOfOrder['Order']['TransInfo']['ReferenceNum'];
            $log->write($err);
            //print('3PL (CreateOrdersResult) SOAP Error: - ' . $e->getMessage());
            $a = $this->client->__getLastRequest();
            file_put_contents(DIR_ROOT . 'xml/' . $ArrayOfOrder['Order']['TransInfo']['ReferenceNum'] . '_WH_CreateOrder_request.xml', $a);
            $a = $this->client->__getLastResponse();
            file_put_contents(DIR_ROOT . 'xml/' . $ArrayOfOrder['Order']['TransInfo']['ReferenceNum'] . '_WH_CreateOrder_response.xml', $a);
            $this->sendEmail($err, 'RED order #' . $ArrayOfOrder['Order']['TransInfo']['ReferenceNum'] . ' - NOT send to WH');

            return false;
            exit;
        }

        return $result;
    }

    function CancelOrders($ArrayOfOrder, $warnings) {
        global $log;

        $is_live = IS_LIVE;
        if ($is_live) {
            try {
                $result = $this->client->CancelOrders($this->extLoginData, $ArrayOfOrder, $warnings);
                $a = $this->client->__getLastRequest();
                file_put_contents(DIR_ROOT . 'xml/' . $ArrayOfOrder['CancelOrder']['WarehouseTransactionID'] . '_WH_CancelOrder_request.xml', $a);
                $a = $this->client->__getLastResponse();
                file_put_contents(DIR_ROOT . 'xml/' . $ArrayOfOrder['CancelOrder']['WarehouseTransactionID'] . '_WH_CancelOrder_response.xml', $a);
            }
            catch (Exception $e) {
                $err = '3PL (CancelOrders) SOAP Error: - ' . $e->getMessage();
                $log->write($err);
                //print('3PL (CreateOrdersResult) SOAP Error: - ' . $e->getMessage());
                $a = $this->client->__getLastRequest();
                file_put_contents(DIR_ROOT . 'xml/' . $ArrayOfOrder['CancelOrder']['WarehouseTransactionID'] . '_WH_CancelOrder_request.xml', $a);
                $a = $this->client->__getLastResponse();
                file_put_contents(DIR_ROOT . 'xml/' . $ArrayOfOrder['CancelOrder']['WarehouseTransactionID'] . '_WH_CancelOrder_response.xml', $a);
                $this->sendEmail($err);

                return false;
                exit;
            }

            return $result;
        }
        else {
            return true;
        }
    }

    function FindOrders($params, $limitCount = 1000) {
        global $log;
        $is_live = IS_LIVE;
        try {
            if (is_array($params)) {
                $params['CustomerID'] = $this->extLoginData['CustomerID'];
                $params['FacilityID'] = $this->extLoginData['FacilityID'];
                $params['RetailerID'] = 0;
                $params['MarkForListID'] = 0;
                $params['OrderFtpID'] = 0;
                $params['Stage'] = 0;
                $params['BatchID'] = 0;
                $params['BeginTrans'] = 0;
                $params['EndTrans'] = 0;
                $params['AddressStatus'] = 'Any';
                $params['OverAlloc'] = 'Any';
                $params['Closed'] = 'Any';
                $params['ASNSent'] = 'Any';
                $params['RouteSent'] = 'Any';
                $params['SpExactMatch'] = 0;
                $params['PickTicketPrinted'] = 'Any';
            }
            $result = $this->client->FindOrders($this->loginData, $params, $limitCount);
            //$a = htmlentities($this->client->__getLastRequest());
            //echo 'Request = <pre>'; var_dump($a); echo '</pre>';
            //echo 'Params = ';print_r($params);
            //$a = htmlentities($this->client->__getLastResponse());
            //echo 'Response = <pre>'; var_dump($result); echo '</pre>';
            //print_r($result);

        }
        catch (Exception $e) {
            $err = '3PL (FindOrders) SOAP Error: - ' . $e->getMessage();
            $log->write($err);
            $this->sendEmail($err);

            //print('3PL (FindOrdersResult) SOAP Error: - ' . $e->getMessage());
            return false;
            exit;
        }

        return $result;
    }

    function ReportStockStatus() {
        global $log;
        try {
            $result = $this->client->ReportStockStatus($this->loginData);
        }
        catch (Exception $e) {
            $err = '3PL (ReportStockStatus) SOAP Error: - ' . $e->getMessage();
            $log->write($err);
            $this->sendEmail($err);

            return false;
        }

        return $result;
    }

    //If error will occured, so message about will send to OpenCart system email
    public function sendEmail($body = '', $subject = '') {
        global $loader, $registry;
        $loader->model('setting/setting');
        $model = $registry->get('model_setting_setting');

        $mail = new Mail();
        $mail->protocol = $model->config->get('config_mail_protocol');
        $mail->parameter = $model->config->get('config_mail_parameter');
        $mail->hostname = $model->config->get('config_smtp_host');
        $mail->username = $model->config->get('config_smtp_username');
        $mail->password = $model->config->get('config_smtp_password');
        $mail->port = $model->config->get('config_smtp_port');
        $mail->timeout = $model->config->get('config_smtp_timeout');

        if ($model->config->get('config_asana_use') == 1) {

            $asana_email = 'x+' . $model->config->get('config_asana_projectid') . '@mail.asana.com';
            $mail->setTo($asana_email);
            $mail->setFrom($model->config->get('config_asana_email'));
            $mail->setCc($model->config->get('config_asana_followers'));
            $mail->setSender($model->config->get('config_owner'));
            if (empty($subject)) {
                $mail->setSubject('Warehouse API Error');
            }
            else {
                $mail->setSubject($subject);
            }
        }
        else {
            $mail->setTo($model->config->get('config_email'));
            //$mail->setCc('');
            $mail->setFrom($model->config->get('config_email'));

            if ($model->config->get('config_email_reply')) {
                $mail->setReplyTo($model->config->get('config_email_reply'), $model->config->get('config_name'));
            }
            $mail->setSender($model->config->get('config_owner'));

            if (empty($subject)) {
                $mail->setSubject($model->config->get('config_owner') . ' - Warehouse API email');
            }
            else {
                $mail->setSubject($model->config->get('config_owner') . ' - ' . $subject);
            }
        }
        $body = 'Server: ' . $_SERVER['HTTP_HOST'] . "\n\n<br>" . $body;
        $mail->setHtml(html_entity_decode($body, ENT_QUOTES, 'UTF-8'));
        $mail->send();
    }
}
