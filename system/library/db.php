<?php
// Old version from 1.5.5.1
//final class DB {
//	private $driver;
//
//	public function __construct($driver, $hostname, $username, $password, $database) {
//		if (file_exists(DIR_DATABASE . $driver . '.php')) {
//			require_once(DIR_DATABASE . $driver . '.php');
//		} else {
//			exit('Error: Could not load database file ' . $driver . '!');
//		}
//
//		$this->driver = new $driver($hostname, $username, $password, $database);
//	}
//
//  	public function query($sql) {
//		return $this->driver->query($sql);
//  	}
//
//	public function escape($value) {
//		return $this->driver->escape($value);
//	}
//
//  	public function countAffected() {
//		return $this->driver->countAffected();
//  	}
//
//  	public function getLastId() {
//		return $this->driver->getLastId();
//  	}
//}

//Copied from 1.5.6.4
class DB {
	private $driver;

	public function __construct($driver, $hostname, $username, $password, $database) {
		$file = DIR_DATABASE . $driver . '.php';

		if (file_exists($file)) {
			require_once($file);

			$class = 'DB' . $driver;

			$this->driver = new $class($hostname, $username, $password, $database);
		} else {
			exit('Error: Could not load database driver type ' . $driver . '!');
		}
	}

	public function query($sql) {
		return $this->driver->query($sql);
	}

	public function escape($value) {
		return $this->driver->escape($value);
	}

	public function countAffected() {
		return $this->driver->countAffected();
	}

	public function getLastId() {
		return $this->driver->getLastId();
	}
}
?>